
mainpath = 'D:\fMRI_Karita\Behavior';

sList = [160 161];

modelname = '1h';
cond = 2;
param = 2;

for sub = 1:length(sList)

    regressor = [];
    blocklength = [];
    
    for run = 1:4
        
       regfile = fullfile(mainpath,['S' num2str(sList(sub))],['S' num2str(sList(sub)) '_block' num2str(run+1) '_CSUSconds_Model_Par_' modelname]);
       regdata = load(regfile);
       
       regressor = [regressor; regdata.pmod(cond).param{1,param}];
       trials = length(regdata.pmod(cond).param{1,param});
       if run > 1; blocklength = [blocklength; trials+blocklength(run-1)];
       else blocklength = [blocklength; trials];
       end
       
    end
    
    figure('Position',[10 10 600 200])
    line([0 172],[0 0],'Color',[0.6 0.6 0.6],'LineWidth',1.5) % zero line
    hold on
    for bl = 1:3
        line([blocklength(bl) blocklength(bl)],[-1 1],'Color',[0.6 0.6 0.6],'LineWidth',1.5) % block 1 end
    end
    plot(regressor,'Color','r','LineWidth',2,'MarkerSize',2,'MarkerFaceColor','k','MarkerEdgeColor','k','Marker','o')
    ylim([-1 1])
    xlim([1 length(regressor)])
    xlabel('Trial')
    set(gca,'XTick',0:20:length(regressor))
    ylabel('Regressor value')
    
end