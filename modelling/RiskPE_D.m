%% back-of-the-envelope calculations of RPE for PCF study
% Dominik R Bach 21.101.2021
% -------------------------------------------------------------------------

% coding of the two possible outcomes 
USp = 1; 
USm = 0;

% probability of US (= expected outcome) for 6 conditions
p = [0 1/3 2/3;
       1/3 2/3  1];

eo = p * USp;

% actual outcome for 6 conditions   
o = [USm, USm, USm;
          USp, USp, USp];   
     
% PEx  = (eo - o)    - (p .* (eo - USp)    + (1-p) .* (eo - USm)   );     
% RPEx = (eo - o).^2 - (p .* (eo - USp).^2 + (1-p) .* (eo - USm).^2);     

% PE  = (o - eo)    - (p .* (USp - eo)    + (1-p) .* (USm - eo)   );     

RP = [(1-p(1,:)) .* (USm - eo(1,:)).^2; p(2,:) .* (USp - eo(2,:)).^2];
RPx = [RP(1) RP(2)+RP(3) RP(4)+RP(5) RP(6)];

% RP = p .* (USp - eo).^2 + (1-p) .* (USm - eo).^2;
RPE = (o - eo).^2 - RP;

% rP = (p .* (eo - USp).^2 + (1-p) .* (eo - USm).^2);

%RPE = (eo - o).^2 - (p(4:6) .* (eo(4:6) - USp).^2 + (1-p(1:3)) .* (eo(1:3) - USm).^2);     

% RPE2 = Current risk (SD) - Predicted risk (SD)

% General risk before CS, EV = 0.5, SD = sqrt(sum((x-EV)^2)) for both
% outcomes = 0.5

% Specific risk at CS
% x = (eo-o).^2;
% CS0_risk = sqrt(sum(p(1)*x(1)));
% CS1_risk = sqrt(sum(p([2 4]).*x([2 4])));
% CS2_risk = sqrt(sum(p([3 5]).*x([3 5])));
% CS4_risk = sqrt(sum(p(6)*x(6)));