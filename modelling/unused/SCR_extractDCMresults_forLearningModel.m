%Extract SCR DCM results for learning models

% Subject list
SUBJ_LABELS = [159 161:162 164:167 169:177 179:182]; % 160 bad SCR, excluded

MAIN_PATH = fullfile(cd,'..','..');

%SCR = table();

for iSubj = 1:length(SUBJ_LABELS)
   
    SCR_DATA = fullfile(MAIN_PATH, 'Psychophysiology', 'PCF05_fmri', 'Physio', 'Data', 'SCR', 'DCM', 'Allblocks_SplitSessions', ['dcm_S' num2str(SUBJ_LABELS(iSubj)) '.mat']);
    load(SCR_DATA)
    
    % Trial labels
    for trl = 1:length(dcm.trlnames)
        if strcmp(dcm.trlnames{trl},'CS-')
            SCR(iSubj,1,trl) = 1; %#ok<*SAGROW>
        elseif strcmp(dcm.trlnames{trl},'CS(1/3)+|US+')
            SCR(iSubj,1,trl) = 2;
        elseif strcmp(dcm.trlnames{trl},'CS(1/3)+|US-')
            SCR(iSubj,1,trl) = 3;
        elseif strcmp(dcm.trlnames{trl},'CS(2/3)+|US+')
            SCR(iSubj,1,trl) = 4;
        elseif strcmp(dcm.trlnames{trl},'CS(2/3)+|US-')
            SCR(iSubj,1,trl) = 5;
        elseif strcmp(dcm.trlnames{trl},'CS(1)+|US+')
            SCR(iSubj,1,trl) = 6;
        end
    end

    SCR(iSubj,2,:) = dcm.stats(:,4); % Fixed/fixed model
    SCR(iSubj,3,:) = dcm.stats(:,1); % Fixed/flexible model
    
end

save(fullfile(MAIN_PATH,'Psychophysiology', 'PCF05_fmri', 'Models', 'SCR', 'DCM', 'LearningModels', 'SCR.mat'),'SCR');