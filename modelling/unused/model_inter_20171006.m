function [ls,ydata,xdata,Q1,Q2] = model_inter_20171006(x,cs,us,xdata,model,out_f,incl_us)

% Model interface function. Gives predictions for Reinforcement Learning,
% Bayesian and Null models. 

% Inputs:

%   x: vector of parameters of each model, the first 2 entries always
%       correspond to the mapping function (slope+intersect). Additional entries
%       in the vector, if any, will be the models' parameters.

%   cs: vector with the index of each trial: 1/2, for our two stimuli (CS+/-)
%   us: vector with the index of each trial: 1/0, for the presence of a reinforcer (US+/-)
%   xdata: vector with physiological estimates per trial, that will be modeled.
%   model: the label of the desired model.
%   out_f: mapping function, from model's estimates to physiological data
%   incl_us: will US+ trials be used in estimating model evidence? recommended: 0 (1/0)


% Outputs:

%   ls: RSS estimate
%   ydata: Model estimates
%   xdata: physiological estimates
%   Q1, Q2: Model estimates before the mapping

% First created on 02.12.2014 by Athina Tzovara, University of Zurich,
% Switzerland.
% Last updated on 03.05.2017 by Filip Melinscak.
% 20170503 - added support for missing values in physiological data

beta_prior = 1;

switch model
    case 'RW1'
        init = 0.5;
        %initialize values for CS+ and CS-:
        Q1 = init*ones(size(us));
        Q2 = init*ones(size(us));
        
        %update rule for every trial:
        for tr = 1:length(us)-1
            if cs(tr) == 1 %cs+, update cs+ representation:
                Q1(tr+1:end) = Q1(tr)+x(end)*(us(tr)-Q1(tr));
            else %update cs- representation:
                Q2(tr+1:end) = Q2(tr)+x(end)*(us(tr)-Q2(tr));
            end
        end
        
    case 'BM0'
        trials = length(cs);
        init = beta_prior;
        A = init*ones(trials+1,1);
        B = init*ones(trials+1,1);
        A1 = init*ones(trials+1,1);
        B1 = init*ones(trials+1,1);
        Q1 = (A(1)/(A(1)+B(1)))*ones(trials,1);
        Q2 = (A1(1)/(A1(1)+B1(1)))*ones(trials,1);
        
        
        for tr = 1:trials
            
            if cs(tr) == 1
                
                %model update:
                A(tr+1:end) = A(tr)+us(tr);
                B(tr+1:end) = B(tr)-us(tr)+1;
                
                %bayesian mean update:
                BM = A(tr)/(A(tr)+B(tr));
                
                Q1(tr+1:end) = BM;
                
            else
                
                A1(tr+1:end) = A1(tr)+us(tr);
                B1(tr+1:end) = B1(tr)-us(tr)+1;
                
                %bayesian mean update:
                BM = A1(tr)/(A1(tr)+B1(tr));
                
                Q2(tr+1:end) = BM;
            end
        end
        
    case 'VO0'
        trials = length(cs);
        init = beta_prior;
        A = init*ones(trials+1,1);
        B = init*ones(trials+1,1);
        A1 = init*ones(trials+1,1);
        B1 = init*ones(trials+1,1);
        Q1 = (A(1)/(A(1)+B(1)))*ones(trials,1);
        Q2 = (A1(1)/(A1(1)+B1(1)))*ones(trials,1);
        
        for tr = 1:trials
            
            
            if cs(tr) == 1
                
                %model update:
                A(tr+1:end) = A(tr)+us(tr);
                B(tr+1:end) = B(tr)-us(tr)+1;
                
                %final update:
                Q1(tr+1:end) = -log(A(tr)+B(tr));
                
            else
                
                %model update:
                A1(tr+1:end) = A1(tr)+us(tr);
                B1(tr+1:end) = B1(tr)-us(tr)+1;
                
                %final update:
                Q2(tr+1:end) = -log(A1(tr)+B1(tr));
                
            end
        end
    case 'BH1'
        trials = length(cs);
        init = beta_prior;
        A = init*ones(trials+1,1);
        B = init*ones(trials+1,1);
        A1 = init*ones(trials+1,1);
        B1 = init*ones(trials+1,1);
        Q1 = (A(1)/(A(1)+B(1)))*ones(trials,1);
        Q2 = (A1(1)/(A1(1)+B1(1)))*ones(trials,1);
        
        for tr = 1:trials
            
            if cs(tr) == 1
                
                %model update:
                A(tr+1:end) = A(tr)+us(tr);
                B(tr+1:end) = B(tr)-us(tr)+1;
                
                %bayesian mean update:
                BM = A(tr)/(A(tr)+B(tr));
                
                %"volatility":
                VO = -log(A(tr)+B(tr));
                Q1(tr+1:end) = BM+VO;
                
            else
                
                %model update:
                A1(tr+1:end) = A1(tr)+us(tr);
                B1(tr+1:end) = B1(tr)-us(tr)+1;
                
                %bayesian mean update:
                BM = A1(tr)/(A1(tr)+B1(tr));
                VO = -log(A(tr)+B(tr));
                Q2(tr+1:end) = BM+VO;
                
            end
        end
        
    case 'HM1' %hybrid model between PH and RW
        init1 = 1;
        init2 = 1;
        %initialize values for CS+ and CS-:
        Q1 = 0.5*ones(size(us));
        Q2 = 0.5*ones(size(us));
        a1 = init1*ones(size(us));
        a2 = init2*ones(size(us));
        
        %update rule for every trial:
        for i = 1:length(us)-1
            if cs(i) == 1 %cs+, update cs+ representation:
                
                a1(i+1:end) = x(end)*abs(us(i)-Q1(i))+(1-x(end))*a1(i);
                Q1(i+1:end) = Q1(i)+a1(i)*(us(i)-Q1(i));
                
            else %update cs- representation:
                a2(i+1:end) = x(end)*abs(us(i)-Q2(i))+(1-x(end))*a2(i);
                Q2(i+1:end) = Q2(i)+a2(i)*(us(i)-Q2(i));
            end
        end
        
        Q1 = a1;
        Q2 = a2;
        
    case 'HM2' %hybrid model between PH and RW
        init = 0.5;
        %initialize values for CS+ and CS-:
        Q1 = init*ones(size(us));
        Q2 = init*ones(size(us));
        a1 = 2*init*ones(size(us));
        a2 = 2*init*ones(size(us));
        
        %update rule for every trial:
        for i = 1:length(us)-1
            if cs(i) == 1 %cs+, update cs+ representation:
                
                a1(i+1:end) = x(end)*abs(us(i)-Q1(i))+(1-x(end))*a1(i);
                Q1(i+1:end) = Q1(i)+a1(i)*(us(i)-Q1(i));
                
            else %update cs- representation:
                a2(i+1:end) = x(end)*abs(us(i)-Q2(i))+(1-x(end))*a2(i);
                Q2(i+1:end) = Q2(i)+a2(i)*(us(i)-Q2(i));
            end
        end
        
        
        
    case 'NL0'
        Q1 = ones(length(cs),1);
        Q2 = zeros(length(cs),1);
        
end

switch out_f
    case 'li2' %linear function, no temporal component but with intersect
        
        Q1_mapped = x(1)*Q1+x(2);
        Q2_mapped = x(1)*Q2+x(2);
end

ydata1 = [Q1_mapped Q2_mapped];
for i = 1:size(ydata1,1)
    ydata(i) = ydata1(i,cs(i));
end

% xdata = xdata ./ nanmax(xdata); % MOD: 20170503

%compute sum of squared residuals (with or without US+ trials):
idxXdataNotnan = find(~isnan(xdata));
if ~incl_us
    
    ls = 0;
    for i = idxXdataNotnan'
        if us(i) == 0
            ls = ls + (ydata(i)-xdata(i))^2;
        end
    end
    
else
    ls = 0;
    for i = idxXdataNotnan'
        
        ls = ls + (ydata(i)-xdata(i))^2;
        
    end
    
end

