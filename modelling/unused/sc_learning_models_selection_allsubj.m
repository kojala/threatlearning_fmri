%% Model selection for all subjects, for DoxMem skin conductance data
% Author: Filip Melinscak (filip.melinscak@gmail.com)
% Date of creation: 2017/05/03

%% Imports
%addpath(genpath(fullfile('..', 'external'))); % BehMod functions for computational learning models
addpath(fullfile('D:\fMRI_Karita\scripts', 'spm12_BMS')); % Using SPM functions for Bayesian model selection
%addpath(fullfile('.', 'functions')); % Add utility functions

%% Constant parameters
% Paths
INPUT_PATH = fullfile('D:\fMRI_Karita\','SCR', 'DCM', 'LearningModels');
OUTPUT_PATH = fullfile('D:\fMRI_Karita\','SCR', 'DCM', 'LearningModels');

NORMALIZATION_TYPE = 'none'; % [none | zscore | csmdiv] type of normalization
SAVE_RESULTS = true;

%% Do the FFX and RFX model comparison
% Load the results of fitting learning models
switch NORMALIZATION_TYPE
    case 'none'
        filename = fullfile(INPUT_PATH, 'learning-results-v5');
    case 'zscore'
        filename = fullfile(INPUT_PATH, 'learning-results-v5-zscored');
    case 'csmdiv'
        filename = fullfile(INPUT_PATH, 'learning-results-v5-csmdiv');
end
allModelResults = readtable([filename '.csv']);

N_MODELS = length(unique(allModelResults.BehModel));

% Filter only rows with AIC/BIC values
allModelResultsModSel = allModelResults(ismember(allModelResults.BehModelProperty,{'AIC','BIC'}), :);

% Drop the trial number column
allModelResultsModSel.NumTrials = [];


% Reshape subject-wise AIC/BICs into a wide format
allModelResultsModSel_wide = unstack(allModelResultsModSel,...
    'BehModelPropertyVal',...
    'Subject');

% Extract the matrices with AIC/BIC results
rowIdxs = ismember(allModelResultsModSel_wide.BehModelProperty,{'AIC'});
%colIdxs = cellfun(@(m) ~isempty(m), regexp(allModelResultsModSel_wide.Properties.VariableNames, 'S[1-20]')); % Find columns with subject labels
colIdxs = [3:22];
AIC_matrix = table2array(allModelResultsModSel_wide(rowIdxs, colIdxs));

rowIdxs = ismember(allModelResultsModSel_wide.BehModelProperty,{'BIC'});
%colIdxs = cellfun(@(m) ~isempty(m), regexp(allModelResultsModSel_wide.Properties.VariableNames, 'S[1-20]')); % Find columns with subject labels
colIdxs = [3:22];
BIC_matrix = table2array(allModelResultsModSel_wide(rowIdxs, colIdxs));

% Get FFX results
sum_AIC = sum(AIC_matrix, 2);
[~,ref_model_AIC] = min(sum_AIC);
sum_AIC_ref = sum_AIC-sum_AIC(ref_model_AIC);

sum_BIC = sum(BIC_matrix, 2);
[~,ref_model_BIC] = min(sum_BIC);
sum_BIC_ref = sum_BIC-sum_BIC(ref_model_BIC);

% Get RFX results with BIC
bms = struct(); % Results of Bayesian Model Selection
[bms.alpha,bms.exp_r,bms.xp,bms.pxp_BIC,bms.bor] =...
    spm_BMS(-BIC_matrix'); % BMS expects (subjects X models) and model evidence (negative BIC)
bms.bor = repmat(bms.bor, 1, N_MODELS);

% Transpose struct fields to get them in table format
bmsFields = fieldnames(bms);
for iField = 1 : length(bmsFields)
    bms.(bmsFields{iField}) = bms.(bmsFields{iField})';
end
bmsTable = struct2table(bms);

allSubjModelSelectionResults = table();
allSubjModelSelectionResults = unique(allModelResultsModSel_wide(:,...
    {'BehModel'}), 'rows','stable');
allSubjModelSelectionResults.sum_AIC_ref = sum_AIC_ref;
allSubjModelSelectionResults.sum_BIC_ref = sum_BIC_ref;
allSubjModelSelectionResults = [allSubjModelSelectionResults, bmsTable];




%% Save the results to disk
if SAVE_RESULTS
    switch NORMALIZATION_TYPE
        case 'none'
            filename = fullfile(OUTPUT_PATH, 'learning-model-selection');
        case 'zscore'
            filename = fullfile(OUTPUT_PATH, 'learning-model-selection-zscored');
        case 'csmdiv'
            filename = fullfile(OUTPUT_PATH, 'learning-model-selection-csmdiv');
    end
    
    writetable(allSubjModelSelectionResults, [filename '.csv']);
end