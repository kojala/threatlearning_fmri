%Plot SCR DCM results

% Subject list
SUBJ_LABELS = [159 161:162 164:167 169:177 179:182]; % 160 bad SCR, excluded

MAIN_PATH = fullfile(cd,'..','..');
LEARN_PATH = fullfile(MAIN_PATH,'Psychophysiology', 'PCF05_fmri', 'Models', 'SCR', 'DCM', 'SCR', 'DCM', 'LearningModels');

MODEL_DATA = fullfile(LEARN_PATH, 'learning-results-v5.mat');
load(MODEL_DATA)

MODELNAMES = {'RW1' 'HM1' 'HM2' 'BM' 'BH1' 'VO' 'NL'};

for iModel = 1:7
    
figure;
hold on

for iSubj = 1:length(SUBJ_LABELS)

    SCR = xdatal{iModel,iSubj};
    ModelPred = ydatal{iModel,iSubj};

    subplot(ceil(sqrt(length(SUBJ_LABELS)))-1, ceil(sqrt(length(SUBJ_LABELS))), iSubj)
    
    plot(SCR,'-or','MarkerFaceColor','r','MarkerSize',1);
    hold on
    
    plot(ModelPred,'-ok','MarkerFaceColor','k','MarkerSize',1,'LineWidth',1)
    hold on
    
    title(['Subject ' num2str(SUBJ_LABELS(iSubj))])
    xlabel('Trials')
    ylabel('SCR')
    xlim([1 224])
    
end

suptitle({;'SCR DCM estimates vs. ' MODELNAMES{iModel} 'model predictions, N = 20'})

end

MODELSEL_DATA = fullfile(LEARN_PATH, 'learning-model-selection.csv');
load(MODELSEL_DATA)