% BEHAVIORAL MODEL ESTIMATES
% For parametric regressors in model-based fMRI analysis

filep = 'D:\fMRI_Karita\Behavior\';

load([filep, 'AllSubs_behvecs.mat'])

nbl = size(behvectors,2);
plot_on = 1;
nsubj = size(behvectors,1);
ncs = 8;

for ss = 1:nsubj
    
    cs = [];
    us = [];
    
    for bl = 1:nbl
        
        if bl ==6
            behvectors{ss, bl}.CS = behvectors{ss, bl}.CS+4;
        end
        us = [us; behvectors{ss, bl}.US];
        cs = [cs; behvectors{ss, bl}.CS];
    end
    
    %loop across CS:
    
    for c = 1:ncs
        
        clear BV_out BM_out BE_out KL_out BV_out SO_out seq b_post b_prio a_post a_prio pd_prio
        
        a_post = 1;
        b_post = 1;
        seq = us(cs == c);
        
        init = 0.5;
        BM_out = init*ones(length(seq),1);
        
        N = 0;
        n = 0;
        for kk = 1:length(seq)
            
            N = N+1;
            n = n+seq(kk);
            
            a_prio = a_post;
            b_prio = b_post;
            %update beta parameters:
            a_post = a_prio+seq(kk);
            b_post = b_prio-seq(kk)+1;
            
            %KL Divergence:
            KL_out(kk+1) = log(beta(a_prio, b_prio)/beta(a_post, b_post))+(a_post-a_prio)*psi(a_post)+(b_post-b_prio)*psi(b_post)+(a_prio-a_post+b_prio-b_post)*psi(a_post+b_post);
            
            %Prior entropy:
            BE_out(kk+1) = log(beta(a_prio,b_prio))-(a_prio-1)*psi(a_prio)-(b_prio-1)*psi(b_prio)+(a_prio+b_prio-2)*psi(a_prio+b_prio);
            
            pd_prio = makedist('Beta', 'a', a_prio, 'b', b_prio);
            
            %Mean of prior:
            BM_out(kk+1) = mean(pd_prio);
            
            %Surprise about outcome: 
            if seq(kk) == 1
                p_outcome = mean(pd_prio);
            else
                p_outcome = 1 - mean(pd_prio);
            end
            
            SO_out(kk+1) = log(1/p_outcome);
            
            %Volatility:
            %BV_out(kk+1) = 1-log(a_prio+b_prio);
            BV_out(kk+1) = -log(a_prio+b_prio);
            
        end
        
        if c<5
            
            Pred1{ss}.BM(c,:) = BM_out;
            Pred1{ss}.BE(c,:) = BE_out;
            Pred1{ss}.KL(c,:) = KL_out;
            Pred1{ss}.VO(c,:) = BV_out;
            Pred1{ss}.SO(c,:) = SO_out;
        else
            Pred2{ss}.BM(c-4,:) = BM_out;
            Pred2{ss}.BE(c-4,:) = BE_out;
            Pred2{ss}.KL(c-4,:) = KL_out;
            Pred2{ss}.VO(c-4,:) = BV_out;
            Pred2{ss}.SO(c-4,:) = SO_out;
        end
        
    end
    
    if plot_on
        
        model_lab = {'Prior mean'; 'Prior entropy'; 'KL divergence - model update'; 'Uncertainty / volatility'; 'Surprise'};
        
        cscol = [189,201,225
            103,169,207
            28,144,153
            1,108,89]./256; % add color
        
        ec2 = [103,169,207]/256;
        ec = [239,138,98]/256;
        fsize = 10;
        
        ff = figure;
        c_ax = [0.2 0.2 0.2];
        %set(ff,'Position', [100 100 300 200])
        
        for cc = 1:4 % add subplot
            
            subplot(3,2,1)
            %axes('linewidth', 2, 'box', 'off', 'YColor', c_ax, 'XColor',c_ax)
            hold on,
            plot(Pred1{ss}.BM(cc,:), 'Color', cscol(cc,:),'LineWidth',3)
            set(gca,'YTickLabel',{})
            title(model_lab(1))
            
            subplot(3,2,2)
            %axes('linewidth', 2, 'box', 'off', 'YColor', c_ax, 'XColor',c_ax)
            hold on,
            plot(Pred1{ss}.BE(cc,:), 'Color', cscol(cc,:),'LineWidth',3)
            set(gca,'YTickLabel',{})
            title(model_lab(2))
            
            subplot(3,2,3)
            %axes('linewidth', 2, 'box', 'off', 'YColor', c_ax, 'XColor',c_ax)
            hold on,
            plot(Pred1{ss}.KL(cc,:), 'Color', cscol(cc,:),'LineWidth',3)
            set(gca,'YTickLabel',{})
            title(model_lab(3))
            
            subplot(3,2,4)
            %axes('linewidth', 2, 'box', 'off', 'YColor', c_ax, 'XColor',c_ax)
            hold on,
            plot(Pred1{ss}.VO(cc,:), 'Color', cscol(cc,:),'LineWidth',3)
            set(gca,'YTickLabel',{})
            title(model_lab(4))
            
            subplot(3,2,5)
            %axes('linewidth', 2, 'box', 'off', 'YColor', c_ax, 'XColor',c_ax)
            hold on,
            plot(Pred1{ss}.SO(cc,:), 'Color', cscol(cc,:),'LineWidth',3)
            set(gca,'YTickLabel',{})
            title(model_lab(5))
            
            legend('CS-','CS+(1/3)','CS+(2/3)','CS+(1)')
            
        end
    end
    
end


%save([filep, 'AllSubs_behvecs_Predictions_v2.mat'], 'Pred1', 'Pred2')
