% BEHAVIORAL MODEL ESTIMATES
% For parametric regressors in model-based fMRI analysis

filep = 'D:\fMRI_Karita\Behavior\';

load([filep, 'AllSubs_behvecs.mat'])

nbl = size(behvectors,2);
plot_on = 1;
nsubj = size(behvectors,1);
ncs = 8;

for ss = 1:nsubj
    
    cs = [];
    us = [];
    
    for bl = 1:nbl
        
        if bl ==6
            behvectors{ss, bl}.CS = behvectors{ss, bl}.CS+4;
        end
        us = [us; behvectors{ss, bl}.US];
        cs = [cs; behvectors{ss, bl}.CS];
    end
    
    %loop across CS:
    
    for c = 1:ncs
        
        clear BV_out BM_out BE_out KL_out BV_out SO_out PE_out seq b_post b_prio a_post a_prio pd_prio
        seq = us(cs == c);
        n_trials = length(seq);
        a_init = 1;
        b_init = 1;
        a = nan(n_trials, 1);
        b = nan(n_trials, 1);
        a(1) = a_init;
        b(1) = b_init;
        
        % Run all updates (except the last one which does not have
        % observable consequences, i.e. CRs or URs)
        for i_trial = 1: n_trials
            a(i_trial+1) = a(i_trial) + (seq(i_trial) == 1);
            b(i_trial+1) = b(i_trial) + (seq(i_trial) == 0);
        end
        
        % Compute the model quantities
        BM_out = a./(a+b);
        SO_out = seq.*log(1./BM_out(1:end-1)) + (1-seq).* log(1./(1-BM_out(1:end-1))); % Select log(1/p_us) or log(1/(1-p_us)) depending on trial outcome
        BE_out = log(beta(a,b))-(a-1).*psi(a)-(b-1).*psi(b)+(a+b-2).*psi(a+b);
        BV_out = -log(a+b);
        
        a_post = a(2:end);
        b_post = b(2:end);
        KL_out = arrayfun(@(a,b,a_post,b_post) log(beta(a, b)/beta(a_post, b_post))+(a_post-a)*psi(a_post)+(b_post-b)*psi(b_post)+(a-a_post+b-b_post)*psi(a_post+b_post),...
            a(1:end-1), b(1:end-1), a_post, b_post);
        
        % Shorten all the arrays by removing the last value that does not
        % affect any responses
        BM_out = BM_out(1:end-1);
        BE_out = BE_out(1:end-1);
        BV_out = BV_out(1:end-1);
        
        
 
        
    end
end

%save([filep, 'AllSubs_behvecs_Predictions_v3.mat'], 'Pred1', 'Pred2')
