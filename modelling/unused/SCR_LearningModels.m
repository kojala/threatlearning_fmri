%% Fitting learning models for DoxMem skin conductance data
% Author: Filip Melinscak (filip.melinscak@gmail.com)
% Date of creation: 2017/05/03

%% Imports
%addpath(fullfile('.', 'functions')); % Add utility functions

%% Constant parameters
% Paths
MAIN_PATH = 'D:\fMRI_Karita';
BEH_DATA_PATH = fullfile(MAIN_PATH, 'Behavior');
SCR_DATA_PATH = fullfile(MAIN_PATH, 'SCR', 'DCM');

% Subject list
SUBJ_LABELS = [159 161:162 164:167 169:177 179:182];

fn = fullfile(SCR_DATA_PATH, 'LearningModels', 'SCR.mat');
indata = load(fn);

% drugfn = fullfile(DATA_PATH, 'DoxMem_drug.mat');
% druginfo = load(drugfn);

% plausibility check
% subID = druginfo.slist;
% fprintf('Subject index discrepancies: %1.0f\n', sum(subID(:)~=SUBJ_LABELS(:)));

% Subjects excluded
% SUBJ_EXCL_LABELS_DOMINIK = [1004, 1018];
% SUBJ_EXCL_LABELS_MISSINGDATA = [1045, 1081]; % Added because these subjects have NaNs in trial label data
% SUBJ_EXCL_LABELS_MISSINGPHYS = [1006]; % All phys estimates NaN in acquisition
% SUBJ_EXCL_LABELS = union(SUBJ_EXCL_LABELS_DOMINIK, SUBJ_EXCL_LABELS_MISSINGDATA);
% SUBJ_EXCL_LABELS = union(SUBJ_EXCL_LABELS, SUBJ_EXCL_LABELS_MISSINGPHYS);
% 
% SUBJ_RETAINED_IDX = find(~ismember(SUBJ_LABELS, SUBJ_EXCL_LABELS));
% drug = druginfo.drug(SUBJ_RETAINED_IDX);
% SUBJ_RETAINED_LABELS = SUBJ_LABELS(SUBJ_RETAINED_IDX);

SUBJ_RETAINED_IDX = find(SUBJ_LABELS);
SUBJ_RETAINED_LABELS = find(SUBJ_LABELS);

%sn = 1; % session 1: acquisition&maintenance, 3 - re-learning

%phase = {'AcqMaint' 'ReLearn'};

OUTPUT_PATH = fullfile(SCR_DATA_PATH, 'LearningModels');
% if ~exist(OUTPUT_PATH,'dir')
%     mkdir(OUTPUT_PATH)
% end

%SCR = indata.SCR{sn}(SUBJ_RETAINED_IDX, 1:2, :); % 1- trial labels, 2- fix/fix, 3- flex/fix model
SCR = indata.SCR(SUBJ_RETAINED_IDX, 1:3, :); % 1- trial labels, 2- fix/fix, 3- flex/fix model

% Trial labels coding
% 1 = CS-
% 2 = CS+(1/3)+US+
% 3 = CS+(1/3)US-
% 4 = CS+(2/3)+US+
% 5 = CS+(2/3)+US-
% 6 = CS+(1)+US+
CSP_LABELS = [2, 3, 4, 5, 6];
CSM_LABELS = [1];
USP_LABELS = [2, 4, 6];
USM_LABELS = [1, 3, 5];


N_SUBJ = size(SCR, 1);
N_TRIAL = size(SCR, 3);


% Model fitting variables
NORMALIZATION_TYPE = 'none'; % [none | zscore | csmdiv] type of normalization
SAVE_RESULTS = true;

INCLUDE_US = 0; %Include US+ trials when estimating models? (Recommended: 0)

% Optimization parameters
OPTPARAM = optimset('fmincon');
OPTPARAM.Algorithm = 'active-set';
OPTPARAM.Algorithm = 'interior-point';
OPTPARAM.MaxFunEvals = 1000000;
OPTPARAM.MaxIter = 1000;
OPTPARAM.Display = 'off';
OPTPARAM.LargeScale = 'on';
OPTPARAM.TolFun = 1e-30;
OPTPARAM.TolX = 1e-30;
OPTPARAM.DiffMinChange = 1e-30;
%for fmincon
A = 1;
b = 1;
Aeq = [];
beq = [];
lb = 0;
ub = 1;
nonlcon = [];

% Model space
MODEL_LABELS = ['RW1i';'HM1i';'HM2i';'BM0i';'BH1i';'VO0i';'NL0i'];
% MODEL_LABELS = ['BH1i'];
N_MODELS = size(MODEL_LABELS, 1);


% Misc variables
DEBUG = false;

%% Main processing


% Prepare table for results
allModelResults = table();


%store the model estimates after optimisation here:
xdatal = cell(N_MODELS, N_SUBJ);  % Cell array to aggregate input data (physiological responses)
ydatal = cell(N_MODELS, N_SUBJ); % Cell array to aggregate predicted responses


for iModel = 1 : N_MODELS
    modelLabel = MODEL_LABELS(iModel,1:end-1);
    
    out_f = ['l',MODEL_LABELS(iModel,end),'2']; % Type of mapping function from model parameters to responses
    
    for iSubj = 1 : N_SUBJ
        subjLabel = num2str(SUBJ_RETAINED_LABELS(iSubj));
        
        clear indata x Data Header ls_m post resp total_tr output us cs xdata ydata block fval f
        
        
        fprintf('Model %i / %i (%s); Subject %i / %i (%s)\n',...
            iModel, N_MODELS, modelLabel,...
            iSubj, N_SUBJ, subjLabel);
        
        % Extract responses for current subject
        %physResponse = squeeze(SCR(iSubj, 2, :)); % Fix-fix model
        physResponse = squeeze(SCR(iSubj, 3, :)); % Flexfix model
        trialLabels = squeeze(SCR(iSubj, 1, :));
        
        % Normalize physiological data
        switch NORMALIZATION_TYPE
            case 'zscore'
                physResponse = (physResponse - nanmean(physResponse)) ./ nanstd(physResponse);
            case 'csmdiv'
                isCsmTrial = ismember(trialLabels, CSM_LABELS);
                meanOfCsmTrials = nanmean(physResponse(isCsmTrial));
                physResponse = physResponse ./ meanOfCsmTrials;
        end
               
        
        % Format input data for the behavioral model
        xdata = physResponse;% Responses (1 per trial)
        us = ismember(trialLabels, USP_LABELS); % 1/0 for US+/-
        cs = 2 - ismember(trialLabels, CSP_LABELS); % 1/2 for CS+/-s
        total_tr = length(trialLabels);
        
        % Number of samples in the model estimation
        if INCLUDE_US
            numSamples = sum(~isnan(xdata));
        else
            numSamples = sum(us == 0 & ~isnan(xdata));
        end
        
        %set the initial parameters:
        switch out_f %mapping function:
            
            case {'li2'}
                x0(1) = 1; %slope cs+/-
                x0(2) = 0; %intersect cs+/-
                
                %range of values:
                low_lim = [-100 -100];
                upp_lim = [100 100];
                
        end
        
        %additional parameters per model, if there are any:
        
        switch modelLabel
            case 'RW1'
                x0 = [x0 1];
                low_lim = [low_lim 0];
                upp_lim = [upp_lim 1];
                
            case 'RW2'
                x0 = [x0 0.001];
                low_lim = [low_lim 0];
                upp_lim = [upp_lim 1];
                
            case 'PH1'
                
                x0 = [x0 0.5 0.5];
                low_lim = [low_lim 0 0];
                upp_lim = [upp_lim 1 1];
                
            case 'HM1'
                
                x0 = [x0 0.5];
                low_lim = [low_lim 0];
                upp_lim = [upp_lim 1];
                
            case 'HM2'
                
                x0 = [x0 0.5];
                low_lim = [low_lim 0];
                upp_lim = [upp_lim 1];
                
            case 'HM3'
                
                x0 = [x0 0.5 0.5];
                low_lim = [low_lim 0 0];
                upp_lim = [upp_lim 1 1];
                
            case 'BH2'
                x0 = [x0 1];
                low_lim = [low_lim 0];
                upp_lim = [upp_lim 1];
                
        end
        
        % Optimize model parameters (minimize RSS)
        f = @(x)model_inter_20171020(x,cs,us,xdata,modelLabel,out_f,INCLUDE_US); % Returns the RSS of the model given params and data
        [x,fval,exitflag,output] = fmincon(f,x0,[],[],Aeq,beq,[low_lim],[upp_lim],nonlcon,OPTPARAM);
        
        % Model parameters from file
%         behdata = importdata(fullfile(BEH_DATA_PATH,'AllSubs_behvecs_Predictions_v2.mat'));
%         ydata = behdata.Pred1{iSubj};
        
        % Run the model with optimized parameters
        [ls_RW,ydata,xdata,Q1,Q2] = model_inter_20171020(x,cs,us,xdata,modelLabel,out_f,INCLUDE_US);
        
        % Aggregate parameters and outputs
        xdatal{iModel, iSubj} = xdata; % Physiological responses
        ydatal{iModel, iSubj} = ydata; % Responses predicted by the model
        
        
        %exclude US+ trials before computing Explained Variance:
        %             xd = xdata(find(us == 0));
        %             yd = ydata(find(us == 0));
        %             explainedVar(iModel,iSubj) = exvar(xd,yd);
        
        % Create table with current results
        currentResults = table();
        currentResults.BehModel = {modelLabel};
        currentResults.Subject = {['S' subjLabel]};
        currentResults.MSE = ls_RW/numSamples;
        currentResults.AIC = AIC_f(ls_RW,length(x0),numSamples);
        currentResults.BIC = BIC_f(ls_RW,length(x0),numSamples);
        currentResults.NumTrials = numSamples;
        
        behParamNames = cell(1, length(x));
        for iBehParam = 1 : length(x)
            behParamNames{iBehParam} = ['Param' num2str(iBehParam)];
            currentResults.(behParamNames{iBehParam}) = x(iBehParam);
        end
        
        currentResultsLong = stack(currentResults,...
            {'MSE', 'AIC', 'BIC', behParamNames{:}},...
            'IndexVariableName', 'BehModelProperty',...
            'NewDataVariableName', 'BehModelPropertyVal');
        
%         currentResultsLong = stack(currentResults,...
%             {behParamNames{:}},...
%             'IndexVariableName', 'BehModelProperty',...
%             'NewDataVariableName', 'BehModelPropertyVal');
        
        % Aggregate results
        allModelResults = [allModelResults; currentResultsLong];
        
        
        clear x x0 ls_RW
        
        clear yd xd Data Header indata ydata xdata low_lim upp_lim Q1 Q2 cs us tr2keep
    end
end


%% Save the results to disk
if SAVE_RESULTS
    switch NORMALIZATION_TYPE
        case 'none'
            filename = fullfile(OUTPUT_PATH, 'learning-results-v5');
        case 'zscore'
            filename = fullfile(OUTPUT_PATH, 'learning-results-v5-zscored');
        case 'csmdiv'
            filename = fullfile(OUTPUT_PATH, 'learning-results-v5-csmdiv');
    end
    
    writetable(allModelResults, [filename '.csv']);
    save(filename,...
        'SCR', 'MODEL_LABELS','SUBJ_RETAINED_LABELS',...
        'xdatal', 'ydatal');
end