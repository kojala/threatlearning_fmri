%Format model beta values data matrix into a structure for later use
clear all

opts = get_options();
fpath = fullfile(opts.mainpath,'fMRIdata_biascorrected\Masks\ReslicedMasks\CurrentMasks\ModelChecks');
mbnames = {'MeanBetas_PAG_CS' 'MeanBetas_PAG_US' 'MeanBetas_BilateralAmygdala_CS' 'MeanBetas_BilateralAmygdala_US'};

for mb = 1:length(mbnames) % models
    
    file = fullfile(fpath,mbnames{mb});
    betas = load(file);
    
    mbvalues = betas.betas_allsubs;
    
    for sub = 1:size(mbvalues,1)
        
        bhvfile = load(fullfile(mainpath,'Behavior',['S' num2str(opts.subj_beh(sub))], ['Behavior_S' num2str(opts.subj_beh(sub)) '.mat']));
        
        CS = [];
        US = [];
        
        for block = 1:6
           CS = [CS; bhvfile.cond(block).CS];  %#ok<*AGROW>
           US = [US; bhvfile.cond(block).US]; 
        end
        
        CS(CS == 6) = 1; %#ok<*SAGROW>
        CS(CS == 7) = 2;
        CS(CS == 8) = 3;
        CS(CS == 5) = 4;
        
        subdata(sub).cs = CS';
        subdata(sub).us = US';
        subdata(sub).resp = mbvalues(sub,:);
        
    end
    
    save([fpath '\PCF_05_' mbnames{mb} '.mat'],'subdata')
    
end