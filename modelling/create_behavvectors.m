function create_behavvectors(opts)
%Create vectors of data info (CS type, US type) for use in calculating 
%model quantities for the normative Bayesian learning model

filepath = opts.bhvpath;
subj_beh  = opts.subj_beh;

for subInd = 1:length(subj_beh)

    behvectors_CS = [];
    behvectors_US = [];
    
    % Load behavioural file with timings
    load(fullfile(filepath,['S',num2str(subj_beh(subInd))],['Behavior_S',num2str(subj_beh(subInd))])); %#ok<LOAD>

    for block = 1:length(cond)
    
        clear conds

        behvectors_CS = [behvectors_CS; cond(block).CS]; %#ok<*AGROW>
        behvectors_US = [behvectors_US; cond(block).US];
        
    end
    
    behvectors{subInd}.CS = behvectors_CS; %#ok<*SAGROW>
    behvectors{subInd}.US = behvectors_US;
    
end
    
save(fullfile(filepath,'AllSubs_behvecs_v3.mat'),'behvectors')

end