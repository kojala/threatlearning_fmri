%% PCF (predictive coding for fear conditioning)
% fMRI experiment 1
% this script implements a discriminant delay fear conditioning task with
% - 4 simple visual CS (colored triangles)
% - pain US
% - key-press to keep attention
% - send markers to WinDaq, EyeLink, and video-signaling LED
% - initial blocks with only one CS to facilitate learning
% - CS/probability mapping balanced but no simple mappings
% based on PCF_002
% Dominik R Bach 03.02.2016
% -------------------------------------------------------------------------
% last edited 26.04.2016

% clear all
% close all
fpath = 'C:\Users\admin\Desktop\Athina\Experiment_fMRI\'
addpath([fpath, 'io']);
addpath([fpath]);
cd([fpath,'data\'])

% user input and header info
% -------------------------------------------------------------------------
overwrite=0;
while overwrite==0
    subject.number=input('Participant number: ', 's');
    block2start=input('Enter block number: ', 's');
    if isempty(str2num(subject.number))
        resfile='testoutput.mat';
        edffile='test';
    else
        resfile=['PCF_05_', num2str(subject.number),'.mat'];
        edffile=['PCF', num2str(subject.number)];
        inputfile=['PCF_05_', num2str(subject.number),'_input.mat'];
    end;
    if exist(resfile)==2
        overwrite=input('File already exists. Load parameters (y/n)? ', 's');
        if ~isempty(overwrite)
            if overwrite=='y', overwrite=1; load(inputfile); else overwrite=0; end;
        else
            overwrite=0;
        end;
    else
        overwrite=1;
    end;
end;
disp(['Result file: ', resfile]);
test_mode = 0;
if test_mode == 0
    ctrl.respkeys = [ctrl.keys.K1, ctrl.keys.K2, ctrl.keys.K3, ctrl.keys.K4];
    ctrl.confirmkey = ctrl.keys.K1;
    ctrl.expkey = ctrl.keys.Escape;
    ctrl.scannerkey = ctrl.keys.K5;
    
    ctrl.leftkey = ctrl.keys.K1;
    ctrl.rightkey = ctrl.keys.K2;
    ctrl.Okkey = ctrl.keys.K3;
    ctrl.expkey = ctrl.keys.Pad5;
else
    ctrl.respkeys = [ctrl.keys.Pad1, ctrl.keys.Pad2, ctrl.keys.Pad3, ctrl.keys.Pad4];
    ctrl.confirmkey = ctrl.keys.Pad1;
    ctrl.expkey = ctrl.keys.Escape;
    ctrl.scannerkey = ctrl.keys.Pad5;
    ctrl.leftkey = ctrl.keys.Pad1;
    ctrl.rightkey = ctrl.keys.Pad2;
    ctrl.Okkey = ctrl.keys.Pad3;
    ctrl.expkey = ctrl.keys.Pad5;

end
subject.test_mode = 'n';
if strcmp(subject.test_mode, 'y')
    test_mode = 1;
    ctrl.screen_display = 0;
    ctrl.phys.eyetracking = 0;
   % save(inputfile, 'subject', 'ctrl', 'header', 'bl','tm');
end

ctrl.screen_display = 1; 

% create sounds, create sprites, start cogent, initialise EyeLink
% -------------------------------------------------------------------------
config_display(ctrl.screen_display, ctrl.screen_resolution);
config_keyboard(500,1,'nonexclusive');

%check if we need sounds:
if ctrl.sound.startle_on
    cgsound('Open', 1, 16, ctrl.sound.bitrate);
end

start_cogent;

for kk = 1:11
    cgfreesprite(kk)
end

gsd = cggetdata('gsd');
scrWidth = gsd.ScreenWidth;
scrHeight = gsd.ScreenHeight;

%check if we need sounds:
if ctrl.sound.startle_on
    wn = randn(ctrl.sound.bitrate * 5, 1);
    wndate = date;
    save('PCF_001_WN_5_sec.mat', 'wn', 'date');
    load('PCF_001_WN_5_sec.mat');
    subject.wndate = wndate;
    wn = ctrl.sound.startleamp * wn(1:(ctrl.sound.bitrate * ctrl.sound.startleduration));
    cgsound('matrixSND', 1, wn(:)');
end

% make sprites for the four NEW CS:
for iSprite = 1:ctrl.nCS
    cgmakesprite(iSprite, ctrl.symbol.x, ctrl.symbol.y, ctrl.bckgrnd);
    cgsetsprite(iSprite);
    cgpencol(ctrl.symbol.color(ctrl.nCS+subject.mapping2(iSprite), :));
    cgpolygon([-ctrl.symbol.x/2, +ctrl.symbol.x/2, ctrl.symbol.x/2 -ctrl.symbol.x/2], [-ctrl.symbol.y/2, -ctrl.symbol.y/2, ctrl.symbol.y/2 ctrl.symbol.y/2]);
    cgsetsprite(0);
end;

% make sprites for the next four CS:
% for iSprite = 1:ctrl.nCS
%     cgmakesprite(ctrl.nCS+iSprite, ctrl.symbol.x, ctrl.symbol.y, ctrl.bckgrnd);
%     cgsetsprite(ctrl.nCS+iSprite);
%     cgpencol(ctrl.symbol.color(ctrl.nCS+subject.mapping2(iSprite), :));
%     cgpolygon([-ctrl.symbol.x/2, +ctrl.symbol.x/2, ctrl.symbol.x/2 -ctrl.symbol.x/2], [-ctrl.symbol.y/2, -ctrl.symbol.y/2, ctrl.symbol.y/2 ctrl.symbol.y/2]);
%     cgsetsprite(0);
% end;

% % make sprites for the next four CS:
% for iSprite = 1:ctrl.nCS
%     cgmakesprite(ctrl.nCS+iSprite, ctrl.symbol.x, ctrl.symbol.y, ctrl.bckgrnd);
%     cgsetsprite(ctrl.nCS+iSprite);
%     cgpencol(ctrl.symbol.color(ctrl.nCS+subject.mapping2(iSprite), :));
%     cgpolygon([-ctrl.symbol.x/2, +ctrl.symbol.x/2, 0 0], [-ctrl.symbol.y/2, -ctrl.symbol.y/2, ctrl.symbol.y/2 ctrl.symbol.y/2]);
%     cgsetsprite(0);
% end;

nCS_total = ctrl.nCS*2;

% make response mapping sprite
cgmakesprite(ctrl.RespSprite, scrWidth, scrHeight, ctrl.bckgrnd);
cgsetsprite(ctrl.RespSprite);

%xy = [300 0 -300 0; 0 300 0 -300]; %coordinates where to place the four CS
%coordinates where to place the four CS: 0 is the middle line, -xx down
%the first row corresponds to x coordinates
%the second row to y:
xy = [-350 -125 125 350; 0 0 0 0];

for iSprite = 1:ctrl.nCS
    cgdrawsprite(iSprite, xy(1, subject.keymapping2(iSprite)),  xy(2, subject.keymapping2(iSprite)));
    %cgdrawsprite(iSprite+ctrl.nCS, xy(1, subject.keymapping2(iSprite)),  xy(2, subject.keymapping2(iSprite)));
end;

%make fixation cross sprite:
cgsetsprite(ctrl.CrossSprite);
cgpencol(ctrl.fix);
cgfont('Arial', 90);
cgtext('+', 0, 0);
cgmakesprite(ctrl.CrossSprite, scrWidth, scrHeight, ctrl.bckgrnd);
cgsetsprite(0);

cgflip(0, 0, 0);

ctrl.keys = getkeymap;
%adapted for the scanner:
%We have 2x4 push buttons, the keys are : Right : blue=1, yellow=2, green=3, red=4 / Left : blue=6, yellow=7, green=8, red=9
ctrl.respkeys = [ctrl.keys.Pad1, ctrl.keys.Pad2, ctrl.keys.Pad3, ctrl.keys.Pad4];
ctrl.confirmkey = ctrl.keys.Pad1;
ctrl.expkey = ctrl.keys.Escape;
ctrl.scannerkey = ctrl.keys.Pad5;

test_mode = 0;
if test_mode == 0
    ctrl.respkeys = [ctrl.keys.K1, ctrl.keys.K2, ctrl.keys.K3, ctrl.keys.K4];
    ctrl.confirmkey = ctrl.keys.K1;
    ctrl.expkey = ctrl.keys.Escape;
    ctrl.scannerkey = ctrl.keys.K5;
    
    ctrl.leftkey = ctrl.keys.K1;
    ctrl.rightkey = ctrl.keys.K2;
    ctrl.Okkey = ctrl.keys.K3;
    ctrl.expkey = ctrl.keys.Pad5;
else
    ctrl.respkeys = [ctrl.keys.Pad1, ctrl.keys.Pad2, ctrl.keys.Pad3, ctrl.keys.Pad4];
    ctrl.confirmkey = ctrl.keys.Pad1;
    ctrl.expkey = ctrl.keys.Escape;
    ctrl.scannerkey = ctrl.keys.Pad5;
    ctrl.leftkey = ctrl.keys.Pad1;
    ctrl.rightkey = ctrl.keys.Pad2;
    ctrl.Okkey = ctrl.keys.Pad3;
    ctrl.expkey = ctrl.keys.Pad5;

end

% Parallel port
if ctrl.phys.bits > 0, config_io; outp(888, 0); end;
subject.edffile2=['PCF', num2str(subject.number), 'B'];
% eyelink
if ctrl.phys.eyetracking
    Eyelink('Initialize');
    Eyelink('OpenFile', subject.edffile2);
    Eyelink('Startrecording');
end

% main loop
% -------------------------------------------------------------------------

cgfont('Arial', 24);
cgpencol(1, 1, 1);
text2screen(ctrl.instructions{5}, 70, 28, 0);
cgflip(ctrl.bckgrnd);
waitkeydown(inf, ctrl.confirmkey);
cgsetsprite(ctrl.FinSprite);
cgdrawsprite(ctrl.RespSprite, 0, 0);
cgfont('Arial', 34);
cgtext('Appuyez sur la touche sous votre doigt index pour continuer.', 0, 200);
cgsetsprite(0);
cgdrawsprite(ctrl.FinSprite, 0, 0);
cgflip(0, 0, 0);
waitkeydown(inf, ctrl.confirmkey);
cgfont('Arial', 24);
cgpencol(1, 1, 1);
text2screen(ctrl.instructions{6}, 70, 28, 0);
cgflip(ctrl.bckgrnd);
waitkeydown(inf, ctrl.confirmkey);
subject.perform.ratio(1) = 0;
subject.perform.indx = 1;

while subject.perform.ratio(subject.perform.indx) < .80
    trls = [1:ctrl.nCS, 1:ctrl.nCS];
    trls = trls(randperm(8));
    respindx = zeros(8, 1);
    for iTrl = 1:8
        cgsetsprite(0);
        cgdrawsprite(trls(iTrl), 0, 0);
        cgflip(ctrl.bckgrnd);
        readkeys;
        waitkeydown(inf);
        [key.ID, key.time, key.n]=getkeydown;
        if ~ismember(key.ID, ctrl.respkeys)
            %cgtext('Bitte nur die Cursor (Pfeil)-Tasten dr�cken!', 0, 0);
        elseif key.ID ~= ctrl.respkeys(subject.keymapping2(trls(iTrl)))
            cgsetsprite(ctrl.FinSprite);
            cgdrawsprite(ctrl.RespSprite, 0, 0);
            cgtext('Mauvais bouton', 0, 0);
            cgsetsprite(0);
            cgdrawsprite(ctrl.FinSprite, 0, 0);
        else
            respindx(iTrl) = 1;
        end;
        cgflip(ctrl.bckgrnd);
        wait(2000);
    end;
    subject.perform.indx = subject.perform.indx + 1;
    subject.perform.ratio(subject.perform.indx) = mean(respindx);
end;


%allow subjects to read the instructions:
cgflip(0, 0, 0);
text2screen(ctrl.instructions{3}, 70, 28, 0);
cgflip(ctrl.bckgrnd);
waitkeydown(inf, ctrl.confirmkey);
cgpencol(ctrl.fix);
cgfont('Arial', 90);
cgtext('+', 0, 0);
cgflip(ctrl.bckgrnd);
wait (2000);

%Repeat for every block:
for bb = length(bl) % Total number of blocks
    clear indata condTime
    condTime = bl{1,bb}.indata;
    
    %once they are ready wait for scanner trigger:
    if ctrl.phys.scannertri
        
        % wait for scanner
        text2screen('...Attendez...', 70, 28, 0);
        cgflip(ctrl.bckgrnd);
        
        keys = getkeymap;
        [keyout, t_key, n] = waitkeyup(inf, ctrl.scannerkey);
        t0 = time;
        
        %Start the block as soon as the MR allows:
        % present fixation cross and wait till iti
        cgdrawsprite(ctrl.CrossSprite,0,0)
        cgpenwid(2)
        cgfont('Arial', 90)
        cgtext('+', 0, 0)
        cgflip(ctrl.bckgrnd);
        
        %initialize the scanner trigger variables:
        scanner.ID = [];
        scanner.time = [];
        
    end
    
    for trl = 1:size(condTime,1)% size(tm, 1)
        
        waituntil(t0+condTime(trl,5)) %first, wait for the random ISI time.
        tStart = time;
        
        cgdrawsprite(condTime(trl,2)-ctrl.nCS,0,0);%display CS
        cgflip(0,0,0);
        if ctrl.phys.eyetracking == 1
            Eyelink('Message', ['CS ', num2str(condTime(trl,2))]);
        end;
        tSS = nan;
        if condTime(trl,3) %if 1, then present US:
            waituntil(t0+condTime(trl,6));
            tSS = time;
            deliver(ctrl.shock.bits);
            fprintf('US \n');
            
        else
            %check for startle:
            if condTime(trl,4)
%                 waituntil(t0+condTime(trl,8)); %wait until the onset of startle sound
%                 cgsound('play',10);
%                 tSS = time;
%                 waituntil(t0+condTime(trl,7));
            end
        end
                
        waituntil(t0+condTime(trl,7)); %wait until stimulus offset.
        % present fixation cross and wait till iti
        cgdrawsprite(ctrl.CrossSprite,0,0)
        cgpenwid(2)
        cgpencol(ctrl.fix);
        cgfont('Arial', 90)
        cgtext('+', 0, 0)
        cgflip(ctrl.bckgrnd);
        timeoff = time;
        
        %give feedback:
        % give feedback
        clear key TR_tri
        readkeys;
        [key.ID, key.time, key.n]=getkeydown(ctrl.respkeys);
       % clearkeys
        key_all{trl} = key;
        c_k = 1;
        
        %check for scanner triggers:
        TR_tri = find(key.ID(:) == ctrl.scannerkey);
        if ~isempty(TR_tri)
            scanner.ID = [scanner.ID; key.ID(TR_tri)];
            scanner.time = [scanner.time; key.time(TR_tri)];
            
            key.ID(TR_tri) = [];
            key.time(TR_tri) = [];
            key.n = key.n-length(TR_tri);
        end
            
        if key.n==0
            key.ID=0;
            key.time=tStart;
            correct = -1;
            cgfont('Arial',  24);
            cgtext('.', 0, 0);
            cgflip(ctrl.bckgrnd);
            wait(1000 * 1);
            cgfont('Arial', 90);
            cgpencol(ctrl.fix);
            cgtext('+', 0, 0);
            cgflip(ctrl.bckgrnd);
        elseif ~ismember(key.ID(1), ctrl.respkeys)
            correct = -2;
            cgfont('Arial',  24);
            cgtext('Pas de r�ponse.', 0, 0);
            cgflip(ctrl.bckgrnd);
            wait(1000 * 1);
            cgfont('Arial', 90);
            cgpencol(ctrl.fix);
            cgtext('+', 0, 0);
            cgflip(ctrl.bckgrnd);
        elseif trl > 1
           key.correct = ctrl.respkeys(subject.keymapping2(condTime(trl, 2)-4));
            if ~any(key.ID == key.correct)
                correct = 0;
                cgsetsprite(ctrl.FinSprite);
                cgdrawsprite(ctrl.RespSprite, 0, 0);
                cgfont('Arial', 24);
                cgtext('Mauvais bouton', 0, 0);
                cgsetsprite(0);
                cgdrawsprite(ctrl.FinSprite, 0, 0);
                cgflip(ctrl.bckgrnd);
                wait(1000 * 1);
                cgfont('Arial', 90);
                cgpencol(ctrl.fix);
                cgtext('+', 0, 0);
                cgflip(ctrl.bckgrnd);
            else
                correct = 1;
              %  wait(1000 * 1);
            end;
        else
            correct = NaN;
            %wait(1000 * 1);
        end;
        
        cgdrawsprite(ctrl.CrossSprite,0,0)
        cgpenwid(2)
        cgpencol(ctrl.fix);
        cgfont('Arial', 90)
        cgtext('+', 0, 0)
        cgflip(ctrl.bckgrnd);
        
        indata(trl,7) = timeoff-t0; %end of trial
        indata(trl,1) = trl;
        indata(trl,2) = condTime(trl, 2);
        indata(trl,3) = condTime(trl,3);
        indata(trl,4) = condTime(trl,4);
        indata(trl,5) = tStart-t0; %trial onset
        indata(trl,6) = tSS-t0; %time before stimulus delivery
        indata(trl,8:12) = [key.n, key.ID(1), key.time(1) - tStart, correct, tStart]
        %save intermediate data:
        save(['PCF_05_', num2str(subject.number),'_Block', num2str(bb),'_Part2.mat'], 'subject', 'key_all','header', 'ctrl', 'indata','condTime','tm','bl', 'scanner');
        
        clearkeys
    end
    
end

% eyelink
if ctrl.phys.eyetracking == 1
    Eyelink('StopRecording');
    Eyelink('CloseFile');
end


% make sprites for the four NEW CS:
for iSprite = 1:ctrl.nCS
    cgmakesprite(iSprite, ctrl.symbol.x, ctrl.symbol.y, ctrl.bckgrnd);
    cgsetsprite(iSprite);
    cgpencol(ctrl.symbol.color(ctrl.nCS+subject.mapping2(iSprite), :));
    cgpolygon([-ctrl.symbol.x/2, +ctrl.symbol.x/2, ctrl.symbol.x/2 -ctrl.symbol.x/2], [-ctrl.symbol.y/2, -ctrl.symbol.y/2, ctrl.symbol.y/2 ctrl.symbol.y/2]);
    cgsetsprite(0);
end;


% post test
% -------------------------------------------------------------------------
% #5-7 ratesprite
cgflip(0, 0, 0);
cgmakesprite (ctrl.RespSprite, scrWidth, scrHeight, ctrl.bckgrnd);    %make black ratesprite
cgsetsprite (ctrl.RespSprite);                        %ready sprite to draw into
cgalign ('c', 'c');                     %center alignment
cgpencol (1, 1, 1);                     %black on white background
cgrect (0, 0, 500, 4);                  %draw horizontal line
cgrect (-250, 0, 4, 15);                %draw left major tick
cgrect (250, 0, 4, 15);                 %draw right major tick
for tick = 1:9
    cgrect (50* (tick - 5), 0, 2, 15);  %draw minor ticks
end;
cgfont ('Arial', 30);
cgtext ('0%', -250, -35);                %write anchors
cgtext ('100%', 250, -35);
cgfont ('Arial', 24);
cgtext ('Utilisez les taches sous vos doigt idex/majeur pour contr�ler la fl�che, et la tache sous votre doigt annulaire pour confirmer.', 0, -70);

cgmakesprite (ctrl.FinSprite, 20, 20, ctrl.bckgrnd);               %make arrowsprite
cgsetsprite (ctrl.FinSprite);
cgpencol (1, 1, 1);                     %black arrow
cgpolygon ([-10 -10 10 10], [-10 10 -10 10]);
cgtrncol(ctrl.FinSprite, 'n');
cgsetsprite (0);

cgmakesprite (7, 1000, 800, ctrl.bckgrnd);    %make black full ratesprite for later use

confirmkey =  ctrl.leftkey;
cgpencol (1, 1, 1);
cgfont('Arial', 30);

instruction = ...
    ['Par la suite, vous verrez de nouveau les differents rectangles color�s. ', ...
    'Chaque fois nous vous demandons d''indiquer quelle est la probabilit� que vous ayez re�u un choc apr�s ce rectangle. ', ...
    'Si vous n''�tes pas s�r, donnez-nous votre estimation la plus rapide et intuitive. Il n y a pas de bonnes ou mauvaises ', ...
    'r�ponses. Nous sommes int�ress�s par votre estimation personnelle.\n\n', ...
    'S''il vous pla�t, utilisez les touches sous votre index et majeur pour contr�ler la fl�che. Appuyez sur ', ...
    'la touche sous votre annulaire pour confirmer.\n\n'];

text2screen(instruction, 80, 32, 0);
cgflip (0, 0, 0);
waitkeydown (inf, ctrl.leftkey);

tm = randperm(4);

cgfont ('Arial', 36);
cgpencol (1, 1, 1);
cgflip (ctrl.bckgrnd);
cgalign ('c', 'c');

counter = 1;
clear key

for iTrl = 1:numel(tm)
    % show question
    xpos = 0;
    ypos = 0;
    cgsetsprite (0);
    cgpencol(1, 1, 1);
    cgtext ('Quelle est la probabilit� que vous obtenez', 0, 25);
    cgtext ('une stimulation avec ce rectangle?', 0, -25);
    cgflip (ctrl.bckgrnd);
    wait (1000);
    cgsetsprite (0);
    
    key.down.ID = 0;
    key.direction = 0;
    clearkeys;
    key.down.ID = 0;
    while key.down.ID ~= ctrl.Okkey
        cgsetsprite (7);                % ready whole ratingsprite to draw into
        cgdrawsprite (ctrl.RespSprite, 0, 0);         % draw ratingscale
        cgdrawsprite (ctrl.FinSprite, xpos, -12);    % draw ratingarrow
       
        cgsetsprite (0);
        cgdrawsprite (7, 0, -ypos);     % draw ratingsprite onto offscreen
        cgdrawsprite(tm(iTrl), 0, 200),
        cgflip(ctrl.bckgrnd);
        readkeys;
       
        [key.down.ID, key.down.time] = lastkeydown;
        [key.up.ID, key.up.time] = lastkeyup;
        if key.down.ID == key.up.ID     % was key pressed & released?
            if key.down.ID == ctrl.rightkey  % go one step into direction of keypress
                key.direction = 0;      % then define direction as zero
                xpos = xpos + 2;
                if xpos > 250
                    xpos = 250;
                end;
            elseif key.down.ID == ctrl.leftkey
                key.direction = 0;
                xpos = xpos - 2;
                if xpos < -250
                    xpos = -250;
                end;
            end;
        elseif key.down.ID == ctrl.rightkey  % if key is pressed and held then define direction
            key.direction = 2;
        elseif key.down.ID == ctrl.leftkey
            key.direction = -2;
        elseif (key.up.ID == ctrl.rightkey) || (key.up.ID == ctrl.leftkey) % if key is released only - then stop movement
            key.direction = 0;
        end;
        
        xpos = xpos + key.direction;        % update xpos
        if xpos > 250
            xpos = 250;
        elseif xpos < -250
            xpos = -250;
        end;
        clearkeys;
    end;
    postdata(tm(iTrl), :) = [tm(iTrl), iTrl, xpos/5 + 50];
end;

clck = clock;
header.postendtime = sprintf('%02.0f:%02.0f:%02.0f', clck(4:6));
save(subject.resfile2, 'header', 'subject', 'ctrl', 'postdata','tm','bl');

% bye-bye
% -------------------------------------------------------------------------
cgflip(0, 0, 0);
cgfont('Arial', 24);
cgtext('Merci! ', 0, 0);
cgflip(0, 0, 0);
waitkeydown(inf, ctrl.keys.Pad5);

stop_cogent;
