function [subject, ctrl, bl,tm] = PCF_randBlocks(ctrl, subject)


% (1) first 24 trials with separation of CS+
nCS = ctrl.nCS;
blocks = randperm(nCS);
for blk = 1:numel(blocks)
    foo = repmat([NaN, blk, NaN, blocks(blk), NaN], 6, 1);
    foo(:, 3) = 1:6;
    outcome = zeros(6, 1);
    outcome(1:round(6 * ctrl.shock.probs(blocks(blk)))) = 1;
    outcome = outcome(randperm(6));
    foo(:, 5) = outcome;
    tm{blk, 1} = foo;
end;

% (2) remaining trials with constraints on randomised trial sequence
doRepeat = 1;
cRepeat = 1;
nblocks = length(blocks);

ntr_perm = floor(ctrl.trials.main/nCS/4);
ttm = [];
tm{5} = [];
for bl_c = 1:nblocks
    ttm = [];
doRepeat = 1;
while doRepeat
    fprintf('Trial matrix iteration # %3.0f ... \n', cRepeat)
    cRepeat = cRepeat + 1;
    trlpool = {};
    for CS = 1:nCS %creat a trial pool for each CS including the number of US
        trlpool{CS}(:, 1) = CS * ones(ntr_perm, 1);
        trlpool{CS}(:, 2) = zeros(ntr_perm, 1);
        trlpool{CS}(1:round(ntr_perm * ctrl.shock.probs(CS)), 2) = 1;
    end;
    consec.CS = 0;
    consec.US = 0;
    same.CS = 0;
    same.US = 0;
    trl = 1;
    while trl < floor(ctrl.trials.main/nblocks)
      
        foo = randperm(nCS);
        CS = foo(1);
        foo = randperm(size(trlpool{CS}, 1));
        % still trials with this CS left?
        if isempty(foo)
            trlno = 0;
        else
            trlno = foo(1);
        end;
        if trlno > 0
            US = trlpool{CS}(trlno, 2);
            % same CS or US as on previous trial
            if trl > 1
                if CS == ttm(trl - 1, 1)
                    same.CS = 1;
                else
                    same.CS = 0;
                    consec.CS = 0;
                end;
                if same.CS > 0 && US == ttm(trl - 1, 2)
                    same.US = 1;
                else
                    same.US = 0;
                    consec.US = 0;
                end;
            end;
            if consec.US == (ctrl.maxUS - 1) && same.US && ~ismember(CS, [1, nCS])
                % 3 consecutive CS and US? check whether other US are still
                % available, reset same.CS/US, otherwise break
                if all(trlpool{CS}(:, 2) == US)
                    %trl = 200;
                    trl = floor(ctrl.trials.main/nblocks)+20;
                end;
            elseif ~(consec.CS == (ctrl.maxCS - 1) && same.CS)
                % 4 consecutive CS? If not, insert this trial into tm and delete from trlpool
                ttm(trl, :) = trlpool{CS}(trlno, :);
                trlpool{CS}(trlno, :) = [];
                trl = trl + 1;
                consec.CS = consec.CS + same.CS;
                consec.US = consec.US + same.US;
            else
                % any other CS still left in the pool, if yes reset same.CS
                % otherwise break
                for k = 1:nCS, empty(k) = isempty(trlpool{k}); end;
                if sum(empty) == 3, 
                    trl = floor(ctrl.trials.main/nblocks)+20;
                    %trl = 200; 
                end;
            end;
        end;
        trl
    end;
    if trl == floor(ctrl.trials.main/4), doRepeat = 0; end;
    
end;
tm{5} = [tm{5}; ttm];
end

blk2 = size(tm,1)+1; %keep a counter where to add the extra trials
iindx = [5:8];
% (3) last 24 trials with separation of CS+: new CS:
blocks = randperm(nCS)+nCS; %the stimulus id
for blk = 1:numel(blocks)
    foo = repmat([NaN, blk+iindx(end), NaN, blocks(blk), NaN], 6, 1);
    foo(:, 3) = 1:6;
    outcome = zeros(6, 1); %initialize US presence
    outcome(1:round(6 * ctrl.shock.probs(blocks(blk)-1))) = 1;
    outcome = outcome(randperm(6));
    foo(:, 5) = outcome; %US presence
    tm{blk2, 1} = foo;
    blk2 = blk2+1;
end;

tm{5} = [NaN(size(tm{5},1), 3), tm{5}];
% foo = repmat((iindx), ntr_perm, 1);
% tm{5}(:, 2) = foo(:);
% foo = repmat((1:ntr_perm)', 1, nCS);
% tm{5}(:, 3) = foo(:);

% (3) put it all together
tm = cell2mat(tm);
ntrials = size(tm,1);
tm(:, 1) = 1:ntrials; %add trial index

% (4) add jitter
jitter = repmat((ctrl.jitters_all), 1, ceil(ntrials/3));
jitter = jitter(randperm(numel(jitter)));
tm(:, 6) = jitter(1:size(tm, 1))*1000; %ITI in ms

ctrl.tm = tm;

%how many specify blocks do we have:

nblocks = 2 + (ctrl.trials.main/ntr_perm);
ntr1 = 24;
%specify the range of fixed sequences (first+last block):

bb = 1;
bl{bb}.range = [1:ntr1];
bl{bb}.indata = zeros(length(bl{bb}.range),7);
bl{bb}.indata(:,1) = [1:length(bl{bb}.range)];
bl{bb}.indata(:,2:3) = tm(bl{bb}.range, 4:5);
bl{bb}.indata(:,4) = zeros(1,length(bl{bb}.range));

t0 = 7000; %wait 7sec before the first CS
CS_onsets = [0; cumsum(tm(bl{bb}.range(1:end-1), 6)+ctrl.symbol.duration*1000)]' + t0;

US_onsets = zeros(size(bl{bb}.indata,1),1)*NaN;
US_onsets(find(bl{bb}.indata(:,3)==1)) = CS_onsets(find(bl{bb}.indata(:,3)==1))+ctrl.symbol.SOA*1000;

fix_onsets = [CS_onsets+ctrl.symbol.duration*1000];
bl{bb}.indata(:,5) = CS_onsets; %CS onset times
bl{bb}.indata(:,6) = US_onsets; %CS offset times
bl{bb}.indata(:,7) = fix_onsets; %US onset time
nblocks = 6;
bb = nblocks;
nbb = 4;

bl{bb}.range = [floor(ctrl.trials.tr_bl*nbb)+1+ntr1:size(tm,1)];
bl{bb}.indata = zeros(length(bl{bb}.range),7);
bl{bb}.indata(:,1) = [1:length(bl{bb}.range)];
bl{bb}.indata(:,2:3) = tm(bl{bb}.range, 4:5);
bl{bb}.indata(:,4) = zeros(1,length(bl{bb}.range));

t0 = 7000; %wait 7sec before the first CS
CS_onsets = [0; cumsum(tm(bl{bb}.range(1:end-1), 6)+ctrl.symbol.duration*1000)]' + t0;

US_onsets = zeros(size(bl{bb}.indata,1),1)*NaN;
US_onsets(find(bl{bb}.indata(:,3)==1)) = CS_onsets(find(bl{bb}.indata(:,3)==1))+ctrl.symbol.SOA*1000;

fix_onsets = [CS_onsets+ctrl.symbol.duration*1000];
bl{bb}.indata(:,5) = CS_onsets; %CS onset times
bl{bb}.indata(:,6) = US_onsets; %CS offset times
bl{bb}.indata(:,7) = fix_onsets; %US onset time

coun = bl{1}.range(end)+1;
nblocks = 6;
for bb = 2:nblocks-1
    bl{bb}.range = [coun:coun+ctrl.trials.tr_bl-1];
    bl{bb}.indata = zeros(length(bl{bb}.range),7);
    bl{bb}.indata(:,1) = [1:length(bl{bb}.range)];
    bl{bb}.indata(:,2:3) = tm(bl{bb}.range, 4:5);
    bl{bb}.indata(:,4) = zeros(1,length(bl{bb}.range));
    
    t0 = 7000; %wait 7sec before the first CS
    CS_onsets = [0; cumsum(tm(bl{bb}.range(1:end-1), 6)+ctrl.symbol.duration*1000)]' + t0;
    
    US_onsets = zeros(size(bl{bb}.indata,1),1)*NaN;
    US_onsets(find(bl{bb}.indata(:,3)==1)) = CS_onsets(find(bl{bb}.indata(:,3)==1))+ctrl.symbol.SOA*1000;
    
    fix_onsets = [CS_onsets+ctrl.symbol.duration*1000];
    bl{bb}.indata(:,5) = CS_onsets; %CS onset times
    bl{bb}.indata(:,6) = US_onsets; %CS offset times
    bl{bb}.indata(:,7) = fix_onsets; %US onset time
    
    coun = 1+ bl{bb}.range(end);
end

