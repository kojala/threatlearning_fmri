%% PCF (predictive coding for fear conditioning)
% fMRI experiment 1
% this script implements a discriminant delay fear conditioning task with
% - 4 simple visual CS (colored triangles)
% - pain US
% - key-press to keep attention
% - send markers to WinDaq, EyeLink, and video-signaling LED
% - initial blocks with only one CS to facilitate learning
% - CS/probability mapping balanced but no simple mappings
% based on PCF_002
% Dominik R Bach 03.02.2016
% -------------------------------------------------------------------------
% last edited 09.06.2016

% version 0.2
% changes from version 0.1:
% - visual instead of auditory
% - different incidental task with training at start
% - shock/colour mapping randomised instead of balanced (too many
% permutations)
% - startle trials started by experimenter, not by subject

% clear all
% close all
fpath = ''C:\Users\admin\Desktop\Athina\Experiment_fMRI\''
addpath([fpath, ''io'']);
addpath(fpath)
cd([fpath,''data\''])
% constants
% -------------------------------------------------------------------------
ctrl.sound.startle_on = 0;
ctrl.sound.startleduration = 0.04;
ctrl.sound.bitrate = 44100;
ctrl.sound.startleamp = 0.25;               % ca. 102 dBC, tested on 13.10.2015
deliv = 0;
ctrl.nCS = 4;

ctrl.symbol.x = 200;
ctrl.symbol.y = 200;
ctrl.symbol.color = [1 1 0; 1 0 1; 0 1 1; 1 1 1; 0.5*([1 1 0; 1 0 1; 0 1 1; 1 1 1])];
ctrl.symbol.duration = 6.5;
ctrl.symbol.SOA = 6;

ctrl.trials.tr_bl = 45;
ctrl.trials.main = 180;

ctrl.screen_resolution = 3; % 1= 640x480, 2= 800x600, 3 = 1024x768, 4=1152x864, 5=1280x1024, 6=1600x1200
ctrl.screen_display = 1; % window display size - 0 = window, 1 = full screen, 2= second screen

ctrl.bckgrnd = [.75 .75 .75];               % minimum lighting for video
ctrl.fix = [.6, .6, .6];
ctrl.RespSprite = 9;
ctrl.FinSprite = 10;
ctrl.CrossSprite = 11;

ctrl.phys.eyetracking = 1;
ctrl.phys.scannertri = 1;
ctrl.phys.windaq = 2;  % bits to be send to Windaq
ctrl.phys.video = 4;   % bits to be send to Video LED
ctrl.shock.bits = 1;        % bits to be used for US

ctrl.phys.bits = ctrl.phys.video + ctrl.phys.windaq;

ctrl.maxCS = 4;
ctrl.maxUS = 3;

ctrl.jitters_all = [5 6 6 7];
ctrl.shock.probs = [0 1/3 2/3 1 0 1/3 2/3 1];

% instructions in French
% -------------------------------------------------------------------------
ctrl.instructions{1} = [''Dans cette exp�rience vous verrez des triangles de couleur et obtiendrez une stimulation �lectrique. Vous remarquerez que sa fr�quece varie en fonction de la couleur. \n\n'',...
    ''Pour chaque triangle nous vous demandons d''''appuyer sur une touche, en fonction de la couleur.\n\n'', ...
    ''Avez-vous des questions? Si non, appuyez sur la touche de votre index pour que l''''attribution des couleurs commence''];

ctrl.instructions{2} = [''Vous aurez maintenant une s�quence d''''entra�nement. Appuyez sur le bouton correct pour chaque couleur. Il n''''y aura pas de chocs. \n\n'', ...
    ''Avez-vous des questions? Si non, appuyez sur la touche de votre index pour que la prochaine s�quence commence''];

ctrl.instructions{3} = [''Pour la suite, les objets sont suivis de temps en temps par des chocs �lectriques. \n\n'', ...
    ''Si possible, essayez de regarder entre les objets, sur la croix de fixation. Entre deux triangles, il y a des pauses pendant lesquels vous pouvez d�placer votre regard. \n\n'', ...
    ''Avez-vous des questions? Si non, appuyez sur la touche de votre index pour que la prochaine s�quence commence''];

ctrl.instructions{4} = [''...''];
    
ctrl.instructions{5} = [''Pour la suite, vous verrez des rectangles de nouvelles couleurs.\n\n'',...
''Pour chaque rectangle, nous vous demandons d''''appuyer sur une touche, en fonction de la couleur. \n \n'',...
''Avez-vous des questions? Si non, appuyez sur la touche de votre index pour que l''''attribution des nouvelles couleurs commence''];

ctrl.instructions{6} = [''Vous aurez maintenant une s�quence d''''entra�nement pour les nouvelles couleurs . Appuyez sur le bouton correct pour chaque couleur. Il n y aura pas de chocs. \n\n'', ...
''Avez-vous des questions? Si non, appuyez sur la touche de votre index pour que la prochaine s�quence commence''];


% user input and header info
% -------------------------------------------------------------------------
overwrite=0;
while overwrite==0
subject.number=input(''Participant number: '', ''s'');
block2start=input(''Enter block number: '', ''s'');
if isempty(str2num(subject.number))
    resfile=''testoutput.mat'';
    edffile=''test'';
else
    resfile=[''PCF_05_'', num2str(subject.number),''.mat''];
    resfile2=[''PCF_05_'', num2str(subject.number),''_Part2.mat''];
    edffile=[''PC'', num2str(subject.number)];
    inputfile=[''PCF_05_'', num2str(subject.number),''_input.mat''];
end;
if exist(resfile)==2 && str2num(block2start) == 1
    overwrite=input(''File already exists. Overwrite (y/n)? '', ''s'');
    if ~isempty(overwrite)
        if overwrite==''y'', overwrite=1; else overwrite=0; end;
    else
        overwrite=0;
    end;
else
    
    if exist(resfile)==2 && str2num(block2start) > 1
        overwrite=input([''File already exists. Load and continue from block '', block2start, '' (y/n)? ''], ''s'');
        if ~isempty(overwrite)
            if overwrite==''y'', overwrite=1; else overwrite=0; end;
        else
            overwrite=0;
        end;
        
    else
        overwrite=1;
    end
end;
end;
disp([''Result file: '', resfile]);


%check if the subject file already exists, in that case we need to keep the
%same randperms:

if exist([inputfile], ''file'')>0
    load(inputfile);
else
    
    subject.mapping = randperm(4);
    subject.mapping2 = randperm(4);
    subject.keymapping = randperm(4);
    subject.keymapping2 = randperm(4);
    
    subject.resfile   = resfile;
    subject.resfile2   = resfile2;
    subject.edffile   = edffile;
    subject.inputfile   = inputfile;
    
    header.experiment = ''PCF 0.3: fMRI'';
    header.principal  = ''Dominik Bach'';
    header.assistant  = '''';
    header.version   = ver;
    header.script = fileread([fpath, ''PCF_005.m'']);
    header.cogent    = ''Cogent2000v1.32'';
    header.date = date;
    foo = clock; % local dummy variable
    header.starttime = sprintf(''%02.0f:%02.0f:%02.0f'', foo(4:6));
    clear foo    % remove local dummy variable
    header.tmkey = {''Trl #'', ''Blk'', ''Trl/Blk'', ''CS'', ''US'', ''Jitter''};
    header.datakey = {''Trl #'', ''Blk'', ''Trl/Blk'', ''CS'', ''US'', ''Jitter'', ''Key #'', ''Key ID 1'', ''RT 1'', ''Correct''};
    subject.test_mode = input(''Test mode? (y/n): '', ''s'');
    
    subject.age       = input(''Participant age: '', ''s'');
    subject.gender    = input(''Participant gender: '', ''s'');
    subject.current   = input(''Electric stimulation current, used for experiment, in mA: '', ''s'');
        subject.current   = input(''Electric stimulation current, maximum used in calibration, in mA: '', ''s'');
        subject.SCR       = input(''SCR coupler sensitivity in mV/mcS: '', ''s'');
        subject.TR        = input(''TR: '', ''s'');
        
        ctrl.trials.tr_bl = 44;
        ctrl.trials.main = 180;
        [subject, ctrl, bl,tm] = PCF_randBlocks(ctrl, subject);
        save(inputfile, ''subject'', ''ctrl'', ''header'', ''bl'',''tm'');
    end
    
    subject.test_mode = ''n'';
    if strcmp(subject.test_mode, ''y'')
        test_mode = 1;
        ctrl.screen_display = 0;
        ctrl.phys.eyetracking = 0;
        % save(inputfile, ''subject'', ''ctrl'', ''header'', ''bl'',''tm'');
    end
    %ctrl.screen_display = 1;
    % create sounds, create sprites, start cogent, initialise EyeLink
    % -------------------------------------------------------------------------
    config_display(ctrl.screen_display, ctrl.screen_resolution);
    config_keyboard(500,1,''nonexclusive'');
    
    %check if we need sounds:
    if ctrl.sound.startle_on
        cgsound(''Open'', 1, 16, ctrl.sound.bitrate);
    end
    
    start_cogent;
    
    gsd = cggetdata(''gsd'');
    scrWidth = gsd.ScreenWidth;
    scrHeight = gsd.ScreenHeight;
    
    % scrWidth = 1024;
    % scrHeight = 768;
    
    %check if we need sounds:
    if ctrl.sound.startle_on
        wn = randn(ctrl.sound.bitrate * 5, 1);
        wndate = date;
        save(''PCF_001_WN_5_sec.mat'', ''wn'', ''date'');
        load(''PCF_001_WN_5_sec.mat'');
        subject.wndate = wndate;
        wn = ctrl.sound.startleamp * wn(1:(ctrl.sound.bitrate * ctrl.sound.startleduration));
        cgsound(''matrixSND'', 1, wn(:)'');
    end
    
    % make sprites for the four CS:
    for iSprite = 1:ctrl.nCS
        cgmakesprite(iSprite, ctrl.symbol.x, ctrl.symbol.y, ctrl.bckgrnd);
        cgsetsprite(iSprite);
        cgpencol(ctrl.symbol.color(subject.mapping(iSprite), :));
        cgpolygon([-ctrl.symbol.x/2, +ctrl.symbol.x/2, 0], [-ctrl.symbol.y/2, -ctrl.symbol.y/2, ctrl.symbol.y/2]);
        cgsetsprite(0);
    end;
    
    % make sprites for the next four CS:
    for iSprite = 1:ctrl.nCS
        cgmakesprite(ctrl.nCS+iSprite, ctrl.symbol.x, ctrl.symbol.y, ctrl.bckgrnd);
        cgsetsprite(ctrl.nCS+iSprite);
        cgpencol(ctrl.symbol.color(ctrl.nCS+subject.mapping2(iSprite), :));
        cgpolygon([-ctrl.symbol.x/2, +ctrl.symbol.x/2, 0], [-ctrl.symbol.y/2, -ctrl.symbol.y/2, ctrl.symbol.y/2]);
        cgsetsprite(0);
    end;
    
    nCS_total = ctrl.nCS*2;
    config_log(''log1.log'')
    % make response mapping sprite
    cgmakesprite(ctrl.RespSprite, scrWidth, scrHeight, ctrl.bckgrnd);
    cgsetsprite(ctrl.RespSprite);
    
    %coordinates where to place the four CS: 0 is the middle line, -xx down
    %the first row corresponds to x coordinates
    %the second row to y:
    xy = [-350 -125 125 350; 0 0 0 0];
    
    for iSprite = 1:ctrl.nCS
        cgdrawsprite(iSprite, xy(1, subject.keymapping(iSprite)),  xy(2, subject.keymapping(iSprite)));
    end;
    
    %make fixation cross sprite:
    cgsetsprite(ctrl.CrossSprite);
    cgpencol(ctrl.fix);
    cgfont(''Arial'', 90);
    cgtext(''+'', 0, 0);
    cgmakesprite(ctrl.CrossSprite, scrWidth, scrHeight, ctrl.bckgrnd);
    cgsetsprite(0);
    
    cgflip(0, 0, 0);
    
    ctrl.keys = getkeymap;
    %adapted for the scanner:
    %We have 2x4 push buttons, the keys are : Right : blue=1, yellow=2, green=3, red=4 / Left : blue=6, yellow=7, green=8, red=9
    test_mode = 0;
    if test_mode == 0
        ctrl.respkeys = [ctrl.keys.K1, ctrl.keys.K2, ctrl.keys.K3, ctrl.keys.K4];
        ctrl.confirmkey = ctrl.keys.K1;
        ctrl.expkey = ctrl.keys.Escape;
        ctrl.scannerkey = ctrl.keys.K5;
        
        ctrl.leftkey = ctrl.keys.K1;
        ctrl.rightkey = ctrl.keys.K2;
        ctrl.Okkey = ctrl.keys.K3;
        ctrl.expkey = ctrl.keys.Pad5;
        
        
    else
        ctrl.respkeys = [ctrl.keys.Pad1, ctrl.keys.Pad2, ctrl.keys.Pad3, ctrl.keys.Pad4];
        ctrl.confirmkey = ctrl.keys.Pad1;
        ctrl.expkey = ctrl.keys.Escape;
        ctrl.scannerkey = ctrl.keys.Pad5;
        ctrl.leftkey = ctrl.keys.Pad1;
        ctrl.rightkey = ctrl.keys.Pad2;
        ctrl.Okkey = ctrl.keys.Pad3;
        ctrl.expkey = ctrl.keys.Pad5;
        
    end
    
    
    % Parallel port
    if ctrl.phys.bits > 0, config_io; outp(888, 0); end;
    
    save(inputfile, ''subject'', ''ctrl'', ''header'', ''bl'',''tm'');
    %ctrl.phys.eyetracking = 1;
    % eyelink
    if ctrl.phys.eyetracking
        Eyelink(''Initialize'');
        Eyelink(''OpenFile'', subject.edffile);
        Eyelink(''Startrecording'');
    end
    
    % main loop
    % -------------------------------------------------------------------------
    if strcmp(block2start,''1'') %start the experiment from block 1, including training
        cgfont(''Arial'', 24);
        cgpencol(1, 1, 1);
        text2screen(ctrl.instructions{1}, 70, 28, 0);
        cgflip(ctrl.bckgrnd);
        waitkeydown(inf, ctrl.confirmkey);
        cgsetsprite(ctrl.FinSprite);
        cgdrawsprite(ctrl.RespSprite, 0, 0);
        cgfont(''Arial'', 34);
        cgtext(''Appuyez sur la touche sous votre doigt index pour continuer.'', 0, 200);
        cgsetsprite(0);
        cgdrawsprite(ctrl.FinSprite, 0, 0);
        cgflip(0, 0, 0);
        waitkeydown(inf, ctrl.confirmkey);
        cgfont(''Arial'', 24);
        cgpencol(1, 1, 1);
        text2screen(ctrl.instructions{2}, 70, 28, 0);
        cgflip(ctrl.bckgrnd);
        waitkeydown(inf, ctrl.confirmkey);
        subject.perform.ratio(1) = 0;
        subject.perform.indx = 1;
        
        while subject.perform.ratio(subject.perform.indx) < .80
            trls = [1:ctrl.nCS, 1:ctrl.nCS];
            trls = trls(randperm(8));
            respindx = zeros(8, 1);
            for iTrl = 1:8
                cgsetsprite(0);
                cgdrawsprite(trls(iTrl), 0, 0);
                cgflip(ctrl.bckgrnd);
                readkeys;
                waitkeydown(inf);
                [key.ID, key.time, key.n]=getkeydown;
                if ~ismember(key.ID, ctrl.respkeys)
                    %cgtext(''Bitte nur die Cursor (Pfeil)-Tasten dr�cken!'', 0, 0);
                elseif key.ID ~= ctrl.respkeys(subject.keymapping(trls(iTrl)))
                    cgsetsprite(ctrl.FinSprite);
                    cgdrawsprite(ctrl.RespSprite, 0, 0);
                    cgtext(''Mauvais bouton'', 0, 0);
                    cgsetsprite(0);
                    cgdrawsprite(ctrl.FinSprite, 0, 0);
                else
                    respindx(iTrl) = 1;
                end;
                cgflip(ctrl.bckgrnd);
                wait(2000);
            end;
            subject.perform.indx = subject.perform.indx + 1;
            subject.perform.ratio(subject.perform.indx) = mean(respindx);
        end;
    end
    
    cgfont(''Arial'', 24);
    cgpencol(1, 1, 1);
    
    %allow subjects to read the instructions:
    cgflip(0, 0, 0);
    text2screen(ctrl.instructions{3}, 70, 28, 0);
    cgflip(ctrl.bckgrnd);
    waitkeydown(inf, ctrl.confirmkey);
    cgpencol(ctrl.fix);
    cgfont(''Arial'', 90);
    cgtext(''+'', 0, 0);
    cgflip(ctrl.bckgrnd);
    wait (2000);
    
    TR = str2num(subject.TR);
    save(subject.resfile, ''header'', ''subject'', ''ctrl'',''tm'',''bl'');
    
    %Repeat for every block:
    for bb = str2num(block2start):length(bl)-1 % Total number of blocks
        
        text2screen(''...Attendez (1/2)...'', 60, 20, 0);
            cgflip(ctrl.bckgrnd);
        % keys = getkeymap;
        [keyout, t_key, n] = waitkeydown(inf, ctrl.expkey);
        
        clear indata condTime
        condTime = bl{1,bb}.indata;
        
        total_time = condTime(end,end);
        nu_TRs = total_time/TR/1000;
        
        disp([''Total time: '' num2str(total_time/1000/60), '' s, or '', num2str(nu_TRs), '' TRs. ''])
        
        %once they are ready wait for scanner trigger:
        if ctrl.phys.scannertri
            
            % wait for scanner
            text2screen(''...Attendez (2/2)...'', 60, 20, 0);
                cgflip(ctrl.bckgrnd);
            
            % keys = getkeymap;
            [keyout, t_key, n] = waitkeydown(inf, ctrl.scannerkey);
            t0 = time;
            clearkeys
            %Start the block as soon as the MR allows:
            % present fixation cross and wait till iti
            cgdrawsprite(ctrl.CrossSprite,0,0)
            cgpenwid(2)
            cgfont(''Arial'', 90)
            cgtext(''+'', 0, 0)
            cgflip(ctrl.bckgrnd);
            
            %initialize the scanner trigger variables:
            scanner.ID = [];
            scanner.time = [];
            
        end
        
        for trl = 1:size(condTime,1)% size(tm, 1)
            
            waituntil(t0+condTime(trl,5)) %first, wait for the random ISI time.
            
            
            cgdrawsprite(condTime(trl,2),0,0);%display CS
            cgflip(0,0,0);
            tStart = time;
            clearkeys
            if ctrl.phys.eyetracking == 1
                Eyelink(''Message'', [''CS '', num2str(condTime(trl,2))]);
            end;
            tSS = nan;
            
            if condTime(trl,3) %if 1, then present US:
                waituntil(t0+condTime(trl,6));
                tSS = time;
                tss = condTime(trl,6);
                deliver(ctrl.shock.bits);
                fprintf(''US \n'');
                
            end
            
            waituntil(t0+condTime(trl,7)); %wait until stimulus offset.
            % present fixation cross and wait till iti
            cgdrawsprite(ctrl.CrossSprite,0,0)
            cgpenwid(2)
            cgpencol(ctrl.fix);
            cgfont(''Arial'', 90)
            cgtext(''+'', 0, 0)
            cgflip(ctrl.bckgrnd);
            timeoff = time;
            
            %give feedback:
            % give feedback
            clear key TR_tri
            readkeys;
            [key.ID, key.time, key.n]=getkeydown(ctrl.respkeys);
            % clearkeys
            key_all{trl} = key;
            c_k = 1;
            %check for scanner triggers:
            TR_tri = find(key.ID(:) == ctrl.scannerkey);
            logkeys
            if ~isempty(TR_tri)
                scanner.ID = [scanner.ID; key.ID(TR_tri)];
                scanner.time = [scanner.time; key.time(TR_tri)];
                
                key.ID(TR_tri) = [];
                key.time(TR_tri) = [];
                key.n = key.n-length(TR_tri);
            end
            
            if key.n==0
                key.ID=0;
                key.time=tStart;
                correct = -1;
                cgfont(''Arial'',  24);
                cgtext(''mauvais boutton'', 0, 0);
                cgflip(ctrl.bckgrnd);
                wait(1000 * 1);
                cgfont(''Arial'', 90);
                cgpencol(ctrl.fix);
                cgtext(''+'', 0, 0);
                cgflip(ctrl.bckgrnd);
            elseif ~any(ismember(key.ID(:), ctrl.respkeys))
                correct = -2;
                cgfont(''Arial'',  24);
                cgtext(''Pas de r�ponse.'', 0, 0);
                cgflip(ctrl.bckgrnd);
                wait(1000 * 1);
                cgfont(''Arial'', 90);
                cgpencol(ctrl.fix);
                cgtext(''+'', 0, 0);
                cgflip(ctrl.bckgrnd);
            elseif trl >= 1
                key.correct = ctrl.respkeys(subject.keymapping(condTime(trl, 2)));
                if ~any(key.ID == key.correct)
                    correct = 0;
                    cgsetsprite(ctrl.FinSprite);
                    cgdrawsprite(ctrl.RespSprite, 0, 0);
                    cgfont(''Arial'', 24);
                    cgtext(''Mauvais bouton'', 0, 0);
                    cgsetsprite(0);
                    cgdrawsprite(ctrl.FinSprite, 0, 0);
                    cgflip(ctrl.bckgrnd);
                    wait(1000 * 1);
                    cgfont(''Arial'', 90);
                    cgpencol(ctrl.fix);
                    cgtext(''+'', 0, 0);
                    cgflip(ctrl.bckgrnd);
                else
                    correct = 1;
                    c_k = find(key.ID == key.correct);
                    if length(c_k) > 1
                        c_k = c_k(1);
                    end
                    %  wait(1000 * 1);
                end;
            else
                correct = NaN;
                %wait(1000 * 1);
            end;
            
            cgdrawsprite(ctrl.CrossSprite,0,0)
            cgpenwid(2)
            cgpencol(ctrl.fix);
            cgfont(''Arial'', 90)
            cgtext(''+'', 0, 0)
            cgflip(ctrl.bckgrnd);
            
            indata(trl,7) = timeoff-t0; %end of trial
            indata(trl,1) = trl;
            indata(trl,2) = condTime(trl, 2);
            indata(trl,3) = condTime(trl,3);
            indata(trl,4) = condTime(trl,4);
            indata(trl,5) = tStart-t0; %trial onset
            indata(trl,6) = tSS-t0; %time before stimulus delivery
            indata(trl,8:12) = [key.n, key.ID(c_k), key.time(c_k) - tStart, correct, tStart];
            %save intermediate data:
            save([''PCF_05_'', num2str(subject.number),''_Block'', num2str(bb),''.mat''], ''subject'', ''key_all'',''header'', ''ctrl'', ''indata'',''condTime'',''tm'',''bl'', ''scanner'', ''t0'');
            
            %clearkeys
        end
        
        clearkeys
        
    end
    
    % eyelink
    if ctrl.phys.eyetracking == 1
        Eyelink(''StopRecording'');
        Eyelink(''CloseFile'');
    end
    
    % post test
    % -------------------------------------------------------------------------
    % #5-7 ratesprite
    cgflip(0, 0, 0);
    cgmakesprite (ctrl.RespSprite, scrWidth, scrHeight, ctrl.bckgrnd);    %make black ratesprite
    cgsetsprite (ctrl.RespSprite);                        %ready sprite to draw into
    cgalign (''c'', ''c'');                     %center alignment
    cgpencol (1, 1, 1);                     %black on white background
    cgrect (0, 0, 500, 4);                  %draw horizontal line
    cgrect (-250, 0, 4, 15);                %draw left major tick
    cgrect (250, 0, 4, 15);                 %draw right major tick
    for tick = 1:9
        cgrect (50* (tick - 5), 0, 2, 15);  %draw minor ticks
    end;
    cgfont (''Arial'', 30);
    cgtext (''0%'', -250, -35);                %write anchors
    cgtext (''100%'', 250, -35);
    cgfont (''Arial'', 24);
    cgtext (''Utilisez les touches sous vos doigt idex/majeur pour contr�ler la fl�che, et la touche sous votre doigt annulaire pour confirmer.'', 0, -70);
    
    cgmakesprite (ctrl.FinSprite, 20, 20, ctrl.bckgrnd);               %make arrowsprite
    cgsetsprite (ctrl.FinSprite);
    cgpencol (1, 1, 1);                     %black arrow
    cgpolygon ([-10 0 10], [-10 10 -10]);
    cgtrncol(ctrl.FinSprite, ''n'');
    cgsetsprite (0);
    
    cgmakesprite (7, scrWidth, scrHeight, ctrl.bckgrnd);    %make black full ratesprite for later use
    
    
    % ctrl.leftkey = ctrl.keys.K1;
    %     ctrl.rightkey = ctrl.keys.K2;
    %     ctrl.Okkey = ctrl.keys.K3;
    %
    confirmkey = ctrl.leftkey;
    cgpencol (1, 1, 1);
    cgfont(''Arial'', 30);
    
    instruction = ...
        [''Par la suite, vous verrez de nouveau les differents triangles color�s. '', ...
        ''Chaque fois nous vous demandons d''''indiquer quelle est la probabilit� que vous ayez re�u un choc apr�s ce triangle. '', ...
        ''Si vous n''''�tes pas s�r, donnez-nous votre estimation la plus rapide et intuitive. Il n y a pas de bonnes ou mauvaises '', ...
        ''r�ponses. Nous sommes int�ress�s par votre estimation personnelle.\n\n'', ...
        ''S''''il vous pla�t, utilisez les touches sous votre index et majeur pour contr�ler la fl�che. Appuyez sur '', ...
        ''la touche sous votre annulaire pour confirmer.\n\n''];
    
    text2screen(instruction, 80, 32, 0);
    cgflip (0, 0, 0);
    waitkeydown (inf, ctrl.leftkey);
    
    tm = randperm(4);
    
    cgfont (''Arial'', 36);
    cgpencol (1, 1, 1);
    cgflip (ctrl.bckgrnd);
    cgalign (''c'', ''c'');
    
    counter = 1;
    clear key
    
    for iTrl = 1:numel(tm)
        % show question
        xpos = 0;
        ypos = 0;
        cgsetsprite (0);
        cgpencol(1, 1, 1);
        cgtext (''Quelle est la probabilit� que vous obtenez'', 0, 25);
        cgtext (''une stimulation avec ce triangle?'', 0, -25);
        cgflip (ctrl.bckgrnd);
        wait (1000);
        cgsetsprite (0);
        
        key.down.ID = 0;
        key.direction = 0;
        clearkeys;
        key.down.ID = 0;
        while key.down.ID ~= ctrl.Okkey
            cgsetsprite (7);                % ready whole ratingsprite to draw into
            
            cgdrawsprite (ctrl.RespSprite, 0, 0);         % draw ratingscale
            cgdrawsprite (ctrl.FinSprite, xpos, -12);    % draw ratingarrow
            cgsetsprite (0);
            cgdrawsprite (7, 0, -ypos);     % draw ratingsprite onto offscreen
            cgdrawsprite(tm(iTrl), 0, 200),
            cgflip(ctrl.bckgrnd);
            readkeys;
            [key.down.ID, key.down.time] = lastkeydown;
            [key.up.ID, key.up.time] = lastkeyup;
            if key.down.ID == key.up.ID     % was key pressed & released?
                if key.down.ID == ctrl.rightkey  % go one step into direction of keypress
                    key.direction = 0;      % then define direction as zero
                    xpos = xpos + 2;
                    if xpos > 250
                        xpos = 250;
                    end;
                elseif key.down.ID == ctrl.leftkey
                    key.direction = 0;
                    xpos = xpos - 2;
                    if xpos < -250
                        xpos = -250;
                    end;
                end;
            elseif key.down.ID == ctrl.rightkey  % if key is pressed and held then define direction
                key.direction = 2;
            elseif key.down.ID == ctrl.leftkey
                key.direction = -2;
            elseif (key.up.ID == ctrl.rightkey) || (key.up.ID == ctrl.leftkey) % if key is released only - then stop movement
                key.direction = 0;
            end;
            
            xpos = xpos + key.direction;        % update xpos
            if xpos > 250
                xpos = 250;
            elseif xpos < -250
                xpos = -250;
            end;
            clearkeys;
        end;
        postdata(tm(iTrl), :) = [tm(iTrl), iTrl, xpos/5 + 50];
    end;
    
    clck = clock;
    header.postendtime = sprintf(''%02.0f:%02.0f:%02.0f'', clck(4:6));
    save(subject.resfile, ''header'', ''subject'', ''ctrl'', ''postdata'',''tm'',''bl'');
    
    % bye-bye
    % -------------------------------------------------------------------------
    cgflip(0, 0, 0);
    cgfont(''Arial'', 24);
    cgtext(''Merci! '', 0, 0);
    cgflip(0, 0, 0);
    waitkeydown(inf, ctrl.keys.Pad5);
    
    stop_cogent;