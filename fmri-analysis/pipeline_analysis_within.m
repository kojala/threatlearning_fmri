function pipeline_analysis_within(version,sublist,models,phase,actions)
%%fMRI analysis pipeline

%% Set-up
addpath(fullfile(pwd,'utils'))

% Get options
opts = get_options();

% Add paths
addpath(opts.mainpath)
addpath(opts.fpath)
addpath(genpath(opts.codepath))
addpath(opts.spmpath)
addpath(fullfile(opts.mainpath, 'GitLabRepository', 'behavior'))

% Set SPM settings
spm('defaults', 'FMRI');
spm_jobman initcfg
spm_get_defaults('cmdline',opts.spm_cmdline) % No graphical elements during analysis

% Running a specific list of subjects
opts.subj = opts.subj(sublist);
opts.subj_beh = opts.subj_beh(sublist);

% Version
opts.version = version;

%% Behaviour
%-------------------------------------------------------------------------
if actions.run_createbehav %#ok<*UNRCH>
    create_behavfiles(opts) % Create behavioural files
end

if actions.run_checklearning
    check_CSUSlearning(opts) % Check conscious awareness of CS-US contingency
end

%% Psychophysiological data
%-------------------------------------------------------------------------
if actions.run_createphysioreg
    % PhysIO toolbox modelling psychophysiological noise
    % Create a nuisance regressor file for SPM fMRI analysis
    create_physioreg(opts)
end

%% fMRI
%-------------------------------------------------------------------------

%% 0. Unzipping
if actions.run_unzipnii
    % Need DICOM to NIFTI converted files for unzipping
    unzip_niftiis(opts)
end

%% 1. Preprocessing
if actions.run_fmripreproc
    
    %---------------------------------------------------
    % 1.1 Combine multiple echoes of an fMRI time series
    %---------------------------------------------------
    %combine_echoes(opts)
    
    %----------------------------------------------------------------
    % 1.2 Preprocessing including EPI bias correction and field maps
    %---------------------------------------------------------------
    preprocessing_withFieldmaps(opts)

end

%-----------------------
% 1.3 Data quality check
%-----------------------
if actions.run_checkreg
    check_registration(opts)
end

%----------------------
% 1.4 Head motion check
%----------------------
if actions.run_checkheadmotion
    check_headmotion(opts)
end

%% 2. First level analysis
if actions.run_fmri1stlvl
    
    %-------------------------------------------------
    % 2.1 1st level model specification and estimation
    %-------------------------------------------------
    
    if actions.run_findshortscans
        find_tooshortscans(opts)
    end
        
    if actions.run_preparebehav && any(ismember(opts.allreg_models,models))
        % Create multiple conditions files for parametric models
        prepare_behav(opts,sublist,models)
    elseif actions.run_preparebehav && any(ismember(opts.singlereg_models,models))
        prepare_behav_Model2_allregressors_separatemodels(opts,sublist,models)
    end
    
    for p = phase
        
        if actions.run_1stlvlmodel
            for m = models
                if m == 1 % Axiomatic categorical model
                    firstlevel_CSUSconditions_axiomatic(opts,m,p)
                else % Parametric models
                    firstlevel_CSUSconditions_parmod(opts,m,p)
                end
            end
        end
        
        %------------------------
        % 2.2 1st level contrasts
        %------------------------
        if actions.run_1stlvlcontrasts
            for m = models
                firstlevel_contrasts(opts,m,p)
            end
        end
        
    end

end

if actions.run_copycontrasts
    
    %-------------------------------------------------
    % 2.3 Copy contrasts and masks to 2nd level folder
    %-------------------------------------------------
    
    for m = models
        for p = phase
            copy_contrasts(opts,m,p)
        end
    end

end

%% 3. Second level analysis
if actions.run_fmri2ndlvl
    
    %-------------------------------------------------
    % 3.1 2nd level model specification and estimation
    %-------------------------------------------------
    for m = models
        
        for p = phase
            
            %------------------------------------------
            % Create explicit mask from subjects' masks
            %------------------------------------------
            
%             if actions.run_createexplmask
%                 create_explicitmask(fullfile(seclvlpath,'data','masks'),seclvlpath,subj) % This script is not correct!
%             end
%             
            %--------------------
            % Run 2nd level tests
            %--------------------
            secondlevel_tests(opts,m,p,actions.printresults)

        end
        
    end

end

end