function prepare_behav_Model2_allregressors_separatemodels(opts,sublist,models)

% File with model predictions for Model 2
load(fullfile(opts.mainpath,'Behavior','AllSubs_behvecs_Predictions_v3.mat'));
% Sampling rate
SR = 1000;

% File with information about scans that are too short to include the
% last US of each run
shortscansfile = fullfile(opts.fpath,['tooshortscans_USresp' num2str(opts.USresptime) 'sec.mat']);
load(shortscansfile)

% Number of blocks
no_runs = 6; % All blocks
runs = 1:no_runs;

% First trial of the block
ntrials = [6 11 11 11 11 6];
trial1 = [1 1+6 1+6+11 1+6+(2*11) 1+6+(3*11) 1];

for subInd = 1:length(opts.subj_beh)
    
    behavDir = fullfile(opts.mainpath,'Behavior',['S' num2str(opts.subj_beh(subInd))]);
    
    for iSess = runs
        behavFile = sprintf('Behavior_S%d.mat',opts.subj_beh(subInd));
        behavFile = fullfile(behavDir,behavFile);
        behavData = load(behavFile);
        
        % Determine block, general CS indices for all runs
        tim = behavData.tim;
        conddata = behavData.cond;
        CS = conddata(iSess).CS;
        if iSess < 6 % Acquisition 1 or Maintenance
            modelpred = Pred1; % Model prediction structure
            CS1 = CS==1;
            CS2 = CS==2;
            CS3 = CS==3;
            CS4 = CS==4;
        elseif iSess == 6 % Acquisition 2
            modelpred = Pred2; % Model prediction structure
            CS1 = CS==6; % CS code
            CS2 = CS==7;
            CS3 = CS==8;
            CS4 = CS==5;
        end
        
        CS_timings = tim(iSess).CS_on/SR;
        
        % US
        if tooshortscans(sublist(subInd),iSess+1) > 0 % If there is a mark for the scan of this session to be too short to include the last US
            US = conddata(iSess).US(1:end-1); % Then exclude the last US
            USremoved = true;
        else
            US = conddata(iSess).US;
            USremoved = false;
        end
        
        USleng = length(US);
        US_timings = tim(iSess).US_on(1:USleng)/SR;
        % Not including the last US of each session because scanning was
        % interrupted too early in some cases and there was not enough time
        % left for a US BOLD response to develop
                
        names        = {'CS','US'};
        onsets       = {CS_timings,US_timings};
        durations    = {zeros(length(onsets{1}),1), zeros(length(onsets{2}),1)};
        orth         = {1,1};
        tmod         = {0,0};
        
        % Trials for this session
        sesstrials = trial1(iSess):(trial1(iSess)+ntrials(iSess)-1);
        
        %% Model 2 individual regressors
        
        clear regressors exptShockProb priorEntrShockProb KLdivPrevTrial ...
            surprUSPrevTrial KLdivCurrTrial surprUSCurrTrial volatility
        
        % Initialize to Nan
        exptShockProb       = nan(1,length(CS1));
        volatility          = nan(1,length(CS1));
        priorEntrShockProb  = nan(1,length(CS1));
        surprUSPrevTrial    = nan(1,length(CS1));
        KLdivPrevTrial      = nan(1,length(CS1));
        KLdivCurrTrial      = nan(1,length(CS1));
        surprUSCurrTrial    = nan(1,length(CS1));
        
        % Expected shock probability
        exptShockProb(CS1) = modelpred{1,sublist(subInd)}.BM(1,sesstrials);
        exptShockProb(CS2) = modelpred{1,sublist(subInd)}.BM(2,sesstrials);
        exptShockProb(CS3) = modelpred{1,sublist(subInd)}.BM(3,sesstrials);
        exptShockProb(CS4) = modelpred{1,sublist(subInd)}.BM(4,sesstrials);
        
        % Volatility
        volatility(CS1) = modelpred{1,sublist(subInd)}.VO(1,sesstrials);
        volatility(CS2) = modelpred{1,sublist(subInd)}.VO(2,sesstrials);
        volatility(CS3) = modelpred{1,sublist(subInd)}.VO(3,sesstrials);
        volatility(CS4) = modelpred{1,sublist(subInd)}.VO(4,sesstrials);
        
        % Prior entropy of shock probability
        priorEntrShockProb(CS1) = modelpred{1,sublist(subInd)}.BE(1,sesstrials);
        priorEntrShockProb(CS2) = modelpred{1,sublist(subInd)}.BE(2,sesstrials);
        priorEntrShockProb(CS3) = modelpred{1,sublist(subInd)}.BE(3,sesstrials);
        priorEntrShockProb(CS4) = modelpred{1,sublist(subInd)}.BE(4,sesstrials);
        
        % Find indices for each CS type
        CS1_ind = find(CS1);
        CS2_ind = find(CS2);
        CS3_ind = find(CS3);
        CS4_ind = find(CS4);
        
        % KL divergence between prior and posterior from previous trial
        KLdivPrevTrial(CS1_ind(2:end)) = modelpred{1,sublist(subInd)}.KL(1,sesstrials(2:end)-1); % CS modulation possible from 2nd trial
        KLdivPrevTrial(CS2_ind(2:end)) = modelpred{1,sublist(subInd)}.KL(2,sesstrials(2:end)-1); % CS modulated by the value from the previous trial
        KLdivPrevTrial(CS3_ind(2:end)) = modelpred{1,sublist(subInd)}.KL(3,sesstrials(2:end)-1); % Not possible to modulate with value from current trial
        KLdivPrevTrial(CS4_ind(2:end)) = modelpred{1,sublist(subInd)}.KL(4,sesstrials(2:end)-1); % because outcome not observed yet at CS time
        KLdivPrevTrial(isnan(KLdivPrevTrial)) = nanmean(KLdivPrevTrial); % First CS cannot be modulated by KL div based on non-existent outcome
        % Set to mean in order to have no influence on the estimation
        % Other option: remove this CS entirely
        
        % Surprise about US from previous trial
        surprUSPrevTrial(CS1_ind(2:end)) = modelpred{1,sublist(subInd)}.SO(1,sesstrials(2:end)-1);
        surprUSPrevTrial(CS2_ind(2:end)) = modelpred{1,sublist(subInd)}.SO(2,sesstrials(2:end)-1);
        surprUSPrevTrial(CS3_ind(2:end)) = modelpred{1,sublist(subInd)}.SO(3,sesstrials(2:end)-1);
        surprUSPrevTrial(CS4_ind(2:end)) = modelpred{1,sublist(subInd)}.SO(4,sesstrials(2:end)-1);
        surprUSPrevTrial(isnan(surprUSPrevTrial)) = nanmean(KLdivPrevTrial); % First CS cannot be modulated by surprise based on non-existent outcome
        % Set to mean in order to have no influence on the estimation
        
        % KL divergence between prior and posterior from current trial
        KLdivCurrTrial(CS1) = modelpred{1,sublist(subInd)}.KL(1,sesstrials);
        KLdivCurrTrial(CS2) = modelpred{1,sublist(subInd)}.KL(2,sesstrials);
        KLdivCurrTrial(CS3) = modelpred{1,sublist(subInd)}.KL(3,sesstrials);
        KLdivCurrTrial(CS4) = modelpred{1,sublist(subInd)}.KL(4,sesstrials);
        if USremoved; KLdivCurrTrial(end) = []; end% In case last US of each session excluded
        
        % Surprise about US from current trial
        surprUSCurrTrial(CS1) = modelpred{1,sublist(subInd)}.SO(1,sesstrials);
        surprUSCurrTrial(CS2) = modelpred{1,sublist(subInd)}.SO(2,sesstrials);
        surprUSCurrTrial(CS3) = modelpred{1,sublist(subInd)}.SO(3,sesstrials);
        surprUSCurrTrial(CS4) = modelpred{1,sublist(subInd)}.SO(4,sesstrials);
        if USremoved; surprUSCurrTrial(end) = []; end % In case last US of each session excluded
                
        regressornames = {'Expected p(shock)','Volatility','Prior entropy p(shock)',...
            'KL div prior-posterior previous trial','Surprise on US previous trial'...
            'US type','KL div prior-posterior current trial','Surprise on US current trial'...
            'Expected p(shock)'};
        
        regressors_CS = [exptShockProb' volatility' priorEntrShockProb' KLdivPrevTrial' surprUSPrevTrial'];
        regressors_US = [US KLdivCurrTrial' surprUSCurrTrial'];
            
        CSreg = 0;
        USreg = 0;
        
        for model = models
            
            modelno = model-(opts.no_models-length(regressornames));
                
            clear pmod regressors
            if modelno < 6 || modelno > 8; cond = 1; regressors = regressors_CS; CSreg = CSreg+1; regno = CSreg;
            else cond = 2; regressors = regressors_US; USreg = USreg+1; regno = USreg; end
            pmod(cond).name  = regressornames(modelno);
            pmod(cond).param = {regressors(:,regno)};
            
            if model == 19
                pmod(cond).poly  = {2};
                outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_2_reg2_poly2.mat',opts.subj_beh(subInd),iSess);
            else
                pmod(cond).poly  = {1};
                outputFile = sprintf(['S%d_block%d_CSUSconds_Model_Par_2_reg' num2str(modelno) '.mat'],opts.subj_beh(subInd),iSess);
            end

            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','tmod','pmod','orth');
            
        end
        
    end
end

end