%function timecourses_extractbetas

%% Settings
opts = get_options();
model = 2; % Model 1a
phase = 2; % Maintenance
roiname = 'PE_cluster1_p001cluscorr';
regname = 'parmod_US_PE';
%regressor = 5; % Order number of the regressor of interest. True p PE
model_inp = get_modelconfig(opts,model);

%% Beta extraction for selected ROI at 2nd level
mripath = fullfile(opts.fpath, '2ndlevel', ['Version_' opts.version], ['NoiseCorr_' opts.noisecorr_name], model_inp.modelname, opts.phasenames{phase});
regpath = fullfile(mripath,['1sttest_' regname]);
roifile = fullfile(regpath,[roiname '.nii']);

%betafile = spm_select('FPList',regpath,'^beta.*nii');

% ROI coordinates
Y = spm_read_vols(spm_vol(roifile),1); % Y is 4D matrix of image data
indx = find(Y>0);
[x,y,z] = ind2sub(size(Y),indx);
XYZ = [x y z]';
%myCluster = XYZ(:, find(maskvalues>0.5)); Define threshold

% Get cluster of interest from spm graphics window (for a loaded contrast
% xSPM)
%[xyz,vx_i]=spm_XYZreg('NearestXYZ', spm_XYZreg('GetCoords',hReg),xSPM.XYZmm);
% or alternatively set pre-specified location of voxel
%xyz = [66 -27 24]; % coordinates in Talairach space
%vx_i = spm_XYZreg('FindXYZ', xyz, xSPM.XYZmm);
%XYZ = xSPM.XYZ(:, vx_i); in SPM space
% peak voxel
%myCluster = XYZ;
% Extract beta for each voxel from the ROI for this condition
%betas = spm_get_data(betafile,XYZ);

SPMfile = fullfile(regpath,'SPM.mat');
load(SPMfile)

% Get time courses
y = spm_get_data(SPM.xY.VY, XYZ); 

y = SPM.xX.W*y; % Prewhitening (sphericity correction)
y = spm_filter(SPM.xX.K, y); % High-pass filtering
%y = spm_detrend(y, 1); % Linear detrending

beta_timecourse = y;

%% Save data
%save(fullfile(opts.roimaskpath,'Timecourses','BetaTimecourse_Model1a_PE_frontal.mat'),'beta_timecourse');

figure
plot(mean(beta_timecourse,2),'-o','LineWidth',1.5,'MarkerFaceColor','b','MarkerEdgeColor','b')
title({'BOLD TIMECOURSE' 'Model 1a: Prediction error - Maintenance'})
xlim([0 22])
set(gca,'XTick',1:21)
set(gca,'XTickLabel',{round((1:21)*3.2,1)});
ylabel('Beta value')
xlabel('Seconds')

%end