function copy_contrasts(opts,model,phase)

model_inp = get_modelconfig(opts,model);

% Define the directory structure
pathBase        = opts.fpath;
dirSubjects     = opts.subj;
dirAnalysis     = fullfile(pathBase, '2ndlevel', ['Version_' opts.version], ['NoiseCorr_' opts.noisecorr_name], model_inp.modelname, opts.phasenames{phase}, opts.confolder);

% Change to root directory
cd(pathBase)

for subInd = 1:length(dirSubjects)
    pathSubject     = fullfile(pathBase, dirSubjects{subInd}, '1stlevel', ['Version_' opts.version], ['NoiseCorr_' opts.noisecorr_name], model_inp.modelname, ['First_Level_' opts.phasenames{phase}]);
    pathAnalysis    = fullfile(dirAnalysis, 'data');

    %% Copy contrasts
    if opts.copy_contrasts
        
        outdir = fullfile(pathAnalysis,'contrasts');
        if ~exist(outdir,'dir'); mkdir(outdir); end
        
        consToCopy = opts.contrasts{model};
        
        for cons = consToCopy
            if cons < 10
                conname = ['con_000' num2str(cons) '.nii'];
            else
                conname = ['con_00' num2str(cons) '.nii'];
            end
            sourceFile = spm_select('FPList', pathSubject, conname);
            [~,name,ext] = fileparts(conname);
            destFileName = sprintf('%s_%s%s', name, ['S' int2str(subInd)], ext);
            destFile = fullfile(pathAnalysis, 'contrasts', destFileName);
            copyfile(sourceFile,destFile);
        end
        
    end
    
    %% Copy masks
    if opts.copy_masks
        
        outdir = fullfile(pathAnalysis,'masks');
        if ~exist(outdir,'dir'); mkdir(outdir); end
        
        masksToCopy = {'mask.nii'};
        
        for masks = 1:length(masksToCopy)
            sourceFile = spm_select('FPList', pathSubject, ['^' masksToCopy{masks}]);
            [~,name,ext] = fileparts(masksToCopy{masks});
            destFileName = sprintf('%s_%s%s', name, ['S' int2str(subInd)], ext);
            destFile = fullfile(pathAnalysis, 'masks', destFileName);
            copyfile(sourceFile,destFile);
        end
        
    end
    
    %% Copy betas
    if opts.copy_betas
        
        outdir = fullfile(pathAnalysis,'betas');
        if ~exist(outdir,'dir'); mkdir(outdir); end
        
        betasToCopy = opts.betas{phase}.modelbetas{model};
        
        for betas = betasToCopy
        
            if betas < 10
                betaname = ['beta_000' num2str(betas) '.nii'];
            else
                betaname = ['beta_00' num2str(betas) '.nii'];
            end
            
            sourceFile = spm_select('FPList', pathSubject, betaname);
            [~,name,ext] = fileparts(betaname);
            destFileName = sprintf('%s_%s%s', name, ['S' int2str(subInd)], ext);
            destFile = fullfile(pathAnalysis, 'betas', destFileName);
            copyfile(sourceFile,destFile);
            
        end
        
    end
    
    %% Copy spmTs
    if opts.copy_spmTs
        
        outdir = fullfile(pathAnalysis,'spmTs');
        if ~exist(outdir,'dir'); mkdir(outdir); end
        
        spmTsToCopy = opts.contrasts{model};
        
        for spmT = spmTsToCopy
        
            if spmT < 10
                spmTname = ['spmT_000' num2str(spmT) '.nii'];
            else
                spmTname = ['spmT_00' num2str(spmT) '.nii'];
            end
            
            sourceFile = spm_select('FPList', pathSubject, spmTname);
            [~,name,ext] = fileparts(spmTname);
            destFileName = sprintf('%s_%s%s', name, ['S' int2str(subInd)], ext);
            destFile = fullfile(pathAnalysis, 'spmTs', destFileName);
            copyfile(sourceFile,destFile);
            
        end
        
    end
    
end

end