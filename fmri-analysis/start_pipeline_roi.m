%% fMRI ROI analysis pipeline wrapper script
clear all
addpath(genpath(cd))
spmpath = 'C:\Data\Toolboxes\spm12';
addpath(spmpath)

opts = get_options();

%% Version
version = opts.version; % which version of the analysis to use

%% Check get_options() for further ROI analysis options
% Folders, ROI names, extra plotting options

%% Subjects to run
startsubj = 1;
endsubj   = 21;
sublist = startsubj:endsubj;
%sublist = [1:7 11:14 17 19]; % subjects with monotonic CS-US ratings only

%% Models to run
% 1 = Axiomatic model
% 2 = Model 1a: Simple prediction error model
% 3 = Model 1b: Prediction error model with expected p(shock) from the Bayesian model
% 4 = Model 1c: Prediction error model with subjective rated p(shock)
% 5 = Model 1d: Prediction error model with weighted PE from Bayesian model
% 6 = Model 1e: Prediction error and time interaction model
% 7 = Model 1f: Simple positive prediction error model
% 8 = Model 1g: Simple negative prediction error model
% 9 = Model 1h: Simple unsigned prediction error model
models = 1;

%% Phases to run
phase = 2;
% 1 = Acquisition
% 2 = Maintenance
% 3 = Re-learning / Acquisition 2
% 4 = All phases together
% 5 = Both acquisition phases
% 6 = All phases together, scaled according to run length

%% ROIs to run
% ROI masks created manually in SPM or external masks used (see article for
% descriptions and references)
roitype = 3;
% 1 = Anatomical
% 2 = Anatomical frontal Brodmann Areas
% 3 = Functional (based on clusters)
% 4 = Voxels fulfilling axioms
% 5 = All anatomical ROIs, combined hemispheres

if roitype == 5, roilist = 1:17; % all combined anatomical ROIs
elseif roitype == 3; roilist = 1:7;%roilist = [2:3 14:16 19:20]; % functional PE: only full PE, negative PE and unsigned PE clusters
end
% roilist = [1:3 14:16 19:23]; % functional PE ROIs: Bayes PE, full PE x 2, neg PE x 3, unsigned PE x 2, weighted PE x 3
% roilist = [1:4 9:12 15 16:17 21 22:25]; % left + right: ACC, amygdala, anterior insula, dorsal striatum, posterior insula, thalamus, ventral striatum, 1 side: PAG, substantia nigra/VTA
% roilist = 1:25; % all anatomical ROIs
% roilist = 1:23; % all functional ROIs

%% Time window
timewindow = 16; % extracted time window in seconds

%% Contrasts to plot
con2plot = 2; % check get_modelconfig() function for contrast information

%% ROI timecourses to plot
roiset = 2; % set of ROIs to plot
% 1 = Functional ROIs: US type
% 2 = Functional ROIs: PE
% 3 = Anatomical ROIs
cond2plot = 2; % which conditions to plot
% 2 = US+/US- only (averaged over 3 conditions for each)
% 4 = Fully prediced US+/US- vs. partially predicted US+/US- (4 conditions, partially predicted averaged over 2 conditions each)
% 6 (or anything else) = All 6 conditions

%% Which parts of the pipeline to run
actions.run_voxelsaxioms                    = false;    % Find voxels fulfilling the axioms
actions.run_extractbetas                    = false;    % Extract betas from each ROI for each subject (axiomatic model)
actions.run_extractbetas_param              = false;    % Extract ROI betas for parametric models (i.e. Bayesian model)
actions.run_withinsuberror                  = false;    % Calculate within-subject error bars for ROIs
actions.run_betaPE_linreg                   = false;    % Calculate linear regression for conditionwise betas and PEs
actions.run_ttestaxiomatic                  = false;    % Axiomatic t-tests for ROIs
actions.run_ttestaxiomatic_correctp         = false;    % Calculate and save corrected p-values (Holm-Bonferroni method)
actions.run_plotroi                         = false;    % Plot ROI betas with error bars (parts of Figure 3, full Figure 5)
actions.run_longformat_funcroi              = false;    % Put betas into long format for R for functional cluster ROIs
actions.run_betaPE_longformat               = false;    % Put betas and all PEs into long format for R for ROIs
actions.run_betaPE_plotroi_BayesFactor      = false;     % Plot Bayes Factor model comparison results for each ROI (Figure 4)
actions.run_roitables                       = false;    % Create tables of ROI data
actions.run_extract_timecourses             = false;    % Extract BOLD timecourses for each ROI, over each time point
actions.run_extract_timecourses_pertrial    = false;    % Extract BOLD timecourses for each ROI, for each trial
actions.run_plot_timecourses                = true;    % Plot BOLD timecourses for each ROI, for each condition

% Before extracting beta values from ROIs, need to do the following manual steps:
% 1) Save the thresholded functional clusters of interest through SPM in the ROI mask folder
% 2) Explore the voxels fulfilling axioms beta maps and define spheres around the peak activities of interest and save these as ROI masks

%% Run
if actions.run_voxelsaxioms
    imcalc_voxels_fulfillaxioms(version,sublist,phase);
end

if actions.run_extractbetas
    extract_ROIbetas_persub(version,sublist,models,phase,roitype);
end

if actions.run_extractbetas_param
    extract_ROIbetas_persub_param(version,sublist,models,phase,roitype);
end

if actions.run_withinsuberror
    calculate_withinsuberrors(version,roitype);
end

if actions.run_betaPE_linreg
    calculate_betaLinreg(version,roitype,roilist)
end

if actions.run_ttestaxiomatic
    ttest_posthocaxiomatic(version,roitype,roilist)
end

if actions.run_ttestaxiomatic_correctp
    axiomtests_correctedp_HB(version,roitype,roilist)
end

if actions.run_plotroi
    plot_roidata(version,roitype,roilist)
end

if actions.run_longformat_funcroi
    meanbetas_longformat_func_simple(version,roitype,roilist)
end

if actions.run_betaPE_longformat
   meanbetas_longformat_PE(version,roitype,roilist) 
end

if actions.run_betaPE_plotroi_BayesFactor
    plot_roidata_BayesFactor(version,roitype)
end

if actions.run_roitables
    create_roitables(version,roitype,roilist)
end

if actions.run_extract_timecourses
    extract_timecourse(sublist,roitype)
end

if actions.run_extract_timecourses_pertrial
    extract_timecourses_pertrial(roitype,roilist,timewindow)
end

if actions.run_plot_timecourses
    plot_timecourses(roiset,phase,timewindow,cond2plot)
end

clear all