function calculate_betaPEcorrelation(version,rois,roilist)

opts = get_options();

roipath = opts.roipath;
meanbetapath = fullfile(roipath,['Version_' num2str(version)],'MeanBetas');

% ROI beta value files
ROIfiles = cellstr(ls(fullfile(meanbetapath,'MeanBetas*allconds.mat')));

% Prediction error values
outcome = [0 1 0 1 0 1];
expectation = [0 1/3 1/3 2/3 2/3 1];
fullPE = outcome-expectation;
fullPE = repmat(fullPE',[1 21]); % repeat for each subject

posPE = nan(1,6);
posPE(outcome==1) = 1-expectation(outcome==1);
%posPE(outcome==0) = nanmean(posPE); 
posPE = repmat(posPE',[1 21]); % repeat for each subject

negPE = nan(1,6);
negPE(outcome==0) = 0-expectation(outcome==0);
%negPE(outcome==1) = nanmean(negPE); 
negPE = repmat(negPE',[1 21]);

for roitype = rois
    
    roiname = opts.roinames{roitype};
    roi_ind = strfind(ROIfiles, ['_' roiname '_']);
    roi_ind2 = not(cellfun('isempty', roi_ind));
    
    mbfile = load(fullfile(meanbetapath,ROIfiles{roi_ind2})); % Mean betas
    
    for roi = roilist
        meanbetas = mbfile.meanbetas(:,:,roilist); % [sub,cond,roi]
   
        meanbetas_oversubs = squeeze(meanbetas(:,:,roi))';
        
        betas_reg = meanbetas_oversubs(:);
        
        lm_fullPE = fitlm(fullPE(:),betas_reg);
        lm_posPE = fitlm(posPE(:),betas_reg);
        lm_negPE = fitlm(negPE(:),betas_reg);
        Rsq(roi,1) = lm_fullPE.Rsquared.Ordinary;
        Rsq(roi,2) = lm_posPE.Rsquared.Ordinary;
        Rsq(roi,3) = lm_negPE.Rsquared.Ordinary;
        
        pvalue(roi,1) = lm_fullPE.Coefficients{2,4};
        pvalue(roi,2) = lm_posPE.Coefficients{2,4};
        pvalue(roi,3) = lm_negPE.Coefficients{2,4};
        
        roinames(roi,:) = mbfile.roinames(roi,3:end);
%         for sub = 1:size(meanbetas_oversubs,1)
%             fullPEcorr(sub) = corr(meanbetas_oversubs(:,sub),fullPE(:,sub));
%             posPEcorr(sub) = corr(meanbetas_oversubs(:,sub),posPE(:,sub),'rows','complete');
%             negPEcorr(sub) = corr(meanbetas_oversubs(:,sub),negPE(:,sub),'rows','complete');
%         end
%         
%         PE_corr(roi,1) = mean(fullPEcorr);
%         PE_corr(roi,2) = mean(posPEcorr);
%         PE_corr(roi,3) = mean(negPEcorr);
    end
    
    save(fullfile(meanbetapath,['MeanBetas_PEcorr_' opts.roinames{roitype} '.mat']),'Rsq','pvalue','roinames')
     
end

end