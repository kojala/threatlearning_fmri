function meanbetas_longformat_func_simple(version,rois,roilist)
%Save mean beta values extracted from functional ROIs in long format
%suitable for R, separately for each ROI, including columns Subject,
%Condition, US, Betas

opts = get_options();

roipath = opts.roipath;
meanbetapath = fullfile(roipath,['Version_' num2str(version)],'MeanBetas');

% ROI beta value files
ROIfiles = cellstr(ls(fullfile(meanbetapath,'MeanBetas*allconds.mat')));

for roitype = rois % Loop over ROI types
    
    roiname = opts.roinames{roitype}; % ROI type name
    roi_ind = strfind(ROIfiles, ['_' roiname '_']);
    roi_ind2 = not(cellfun('isempty', roi_ind)); %#ok<STRCL1>
    
    mbfile = load(fullfile(meanbetapath,ROIfiles{roi_ind2})); % Mean betas
    
    for roi = 1:length(roilist) % Loop over ROIs (clusters) within ROI type
        
        clear data
        
        meanbetas = mbfile.meanbetas(:,:,roilist); % Matrix format [subject,condition,ROI]
        meanbetas_oversubs = squeeze(meanbetas(:,:,roi)); % Betas for this ROI
        
        subno = size(meanbetas_oversubs,1); % Number of subjects
        condno = size(meanbetas_oversubs,2); % Number of conditions
        data.betas = meanbetas_oversubs(:); % Save betas in a data structure
        subject = repmat(1:subno,[1 condno])'; % Repeat subject numbers for long format
        data.subject = subject;
        % Ordering according to expectation levels 33%, 66% and 100% independently for each US
        % 1 = CS(33%)US+ and CS(66%)US-
        % 2 = CS(66%)US+ and CS(33%)US-
        % 3 = CS(100%)US+ and CS(0%)US-
        condition = [ones(subno,1)*3,ones(subno,1)*1,ones(subno,1)*2,ones(subno,1)*2,ones(subno,1)*1,ones(subno,1)*3]; % Repeat condition numbers for long format
        data.condition = condition(:); % Save in data structure as one array
        US = [zeros(subno,1), ones(subno,1) zeros(subno,1) ones(subno,1) zeros(subno,1) ones(subno,1)]; % US type (US+ = 1, US- = 0)
        data.US = US(:);
        
        save(fullfile(meanbetapath,['MeanBetas_LongFormat_unsigPE' num2str(roi) '.mat']),'data');
        csvwrite(fullfile(meanbetapath,['MeanBetas_LongFormat_unsigPE' num2str(roi) '.csv']),data);
        % csvwrite does not work in MATLAB2016a (only later versions)
        
    end

     
end

end