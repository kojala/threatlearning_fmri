%Make sure that ROI mask only includes brain
mainpath = fullfile(cd,'..','..','..');
fpath = fullfile(mainpath,'fMRI_biascorrected');
roipath = fullfile(fpath,'Masks','ReslicedMasks','CurrentMasks','Anatomical','FrontalBA');
roiList = spm_select('FPList',roipath);
roiNames = spm_select('List',roipath);

for r = 1:size(roiList,1)
    clear matlabbatch
    
    matlabbatch{1}.spm.util.imcalc.input = {
        [fpath, '\2ndlevel\NoiseCorr_RETROICOR\Model_Cat_Axiomatic\Maint\conjunction_axioms12_6cons\mask.nii,1']
        [roiList(r,:) ',1']
        };
    matlabbatch{1}.spm.util.imcalc.output = fullfile(roipath,[roiNames(r,1:end-4) '_brainmasked.nii']);
    matlabbatch{1}.spm.util.imcalc.outdir = {roipath};
    matlabbatch{1}.spm.util.imcalc.expression = 'i2.*i1';
    spm_jobman('run',matlabbatch)
end