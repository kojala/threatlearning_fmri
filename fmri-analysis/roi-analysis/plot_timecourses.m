function plot_timecourses(roisets,phases,timewindow,cond2plot)
%Plots ROI time courses for a specified experimental phase, time window and
%contrast

colors = [179, 179, 255; ... % CS(0)US-
    179, 0, 0; ... % CS(1/3)US+
    0, 0, 255; ... % CS(1/3)US-
    255, 0, 0; ... % CS(2/3)US+
    0, 0, 153; ... % CS(2/3)US-
    255, 179, 179]./255; % CS(1)US+

opts = get_options();
datapath = fullfile(opts.roipath,'RawROItimecourses','TrialData'); % Retrieve trial-wise time course data
searchterm = ['timecourses_trialdata.*timewindow' num2str(timewindow) 's.mat'];
roinames = spm_select('List',fullfile(opts.roipath,'RawROItimecourses','TrialData'),searchterm);
roilist = {NaN; [1:5 23:24]; 6:22}; % List of ROIs
% NaN - US type timecourses
% [1:5 23:24] - PE timecourses
% 6:22 - Anatomical timecourses

roiset_names = {'UStype' 'PE' 'Anatomical'};

for roiset = roisets % Loop over sets of ROIs
    
    %rois = 1:size(roinames,1);
    rois = roilist{roiset};
    
    for phase = phases % Loop over experimental phases

        if roiset == 2
            roiset_titles = {'Full PE cluster 1' 'Full PE cluster 2' ...
                'Negative PE cluster 1' 'Negative PE cluster 2' 'Negative PE cluster 3' ...
                'PE difference cluster 1' 'PE difference cluster 2'};
            rois_to_plot = [1:2 6:7 3:5];
            roi_pos = [1 2 4 5 7:9];
            label_roi = 5;
            axcolumns = 3; % Columns for ROI plot
            figwidth = 16; % 1 pdf column = 8.5 cm, 1.5 pdf column = 11.6, 2 pdf columns = 17.6
            figheight = 14;
            inmarg = 0.09;
            leftmarg = 0.16;
            rightmarg = 0.04;
            upmarg = 0.05;
            downmarg = 0.08;
        elseif roiset == 3
            roiset_titles = opts.roilist_comb;
            label_roi = 17;
            axcolumns = 4;
            rois_to_plot = [2:9 1 11 12 14 13 17 10 15 16];
            roi_pos = 1:numel(rois_to_plot);
            figwidth = 17.6; % 1 pdf column = 8.5 cm, 1.5 pdf column = 11.6, 2 pdf columns = 17.6
            figheight = 21;
            inmarg = 0.05;
            leftmarg = 0.14;
            rightmarg = 0.02;
            upmarg = 0.03;
            downmarg = 0.08;
        end
        
%         roinames = strtrim(roinames(rois,:));
        roi_no = length(rois);

        % Create figure
        roifig = figure('DefaultAxesFontSize', 12, 'DefaultAxesFontName', 'Arial', 'DefaultTextFontName', 'Arial');
        set(roifig, 'Units', 'Centimeters',...
            'PaperUnits', 'Centimeters', ...
            'PaperPositionMode','auto')
        set(roifig,'Position', [0, 0, figwidth, figheight])
        
        axrows = ceil(roi_no/axcolumns); % Number of rows
        pos = axpos(axrows, axcolumns, leftmarg, rightmarg, upmarg, downmarg, inmarg); % Axes positions
        
        roi_ind = 1; % Counter for ROI index
        
        for roi = rois_to_plot % Loop over ROIs
            
            clear fndata roidata
            roifn = fullfile(datapath,roinames(rois(roi),:)); % ROI file name
            fndata = load(roifn); % Load file
            roidata = fndata.roidata_out; % ROI data
            
            % Split into conditions per run per subject
            for sub = 1:size(roidata.CS,2) % Loop over subjects
                
                for run = 1:size(roidata.CS(sub).subject,2) % Loop over runs
                    
                    condtype = roidata.trial(sub).subject(run).type; % Condition
                    timecourse = roidata.trial(sub).subject(run).timecourse; % Timecourse data
                    timecourse_mean = nanmean(timecourse(:)); % Take average of all conditions as a baseline
                    
                    for cond = unique(condtype)' % Average the percent BOLD signal change over all trials within a condition
                        timecourse_cond = timecourse(condtype==cond,:); % Find the current condition data
                        %timecourse_mean = nanmean(timecourse_cond(:)); % Take average of the condition as a baseline
                        %timecourse_bl = timecourse_cond-timecourse_mean; % Substract that baseline from each trial's timecourse
                        %timecourse_psc = (timecourse_cond./timecourse_mean)*100-100; % Calculate signal change relative to condition baseline
                        timecourse_all(sub,run,cond,:) = nanmean(timecourse_cond); %#ok<AGROW> % Average the signal change over all trials within a condition
                    end
                    
                end
                
            end
            
            % Mean over all runs for baseline, within each subject
            %     newdim = size(timecourse_cond);
            %     timecourse_mean = reshape(timecourse_cond,[21,newdim(2)*newdim(3)*newdim(4)]);
            %     timecourse_mean = nanmean(timecourse_mean,2);
            %     %timecourse_scaled = timecourse_cond-timecourse_mean;
            %
            %     for sub = 1:21
            %         timecourse_psc(sub,:,:,:) = (timecourse_cond(sub,:,:,:)./timecourse_mean(sub)*100)-100; % percent signal change
            %     end
            
            % Mean over the whole experiment and over all subjects as baseline
            %     newdim = size(timecourse_cond);
            %     timecourse_mean = reshape(timecourse_cond,[1,newdim(1)*newdim(2)*newdim(3)*newdim(4)]);
            %     timecourse_mean = nanmean(timecourse_mean);
            %     % Calculate percent signal change
            %     timecourse_psc = (timecourse_cond./timecourse_mean)*100-100;
            
            % Average conditions over subjects & split runs into phases
            % Average over maintenance runs 2-5
            timecourse_acq = squeeze(nanmean(timecourse_all(:,1,:,:)));
            timecourse_maint_temp = nanmean(timecourse_all(:,2:5,:,:));
            timecourse_maint = squeeze(nanmean(timecourse_maint_temp,2));
            timecourse_acq2 = squeeze(nanmean(timecourse_all(:,6,:,:)));
            
            timecourse_acq_mean = mean(timecourse_acq(:));
            timecourse_maint_mean = mean(timecourse_maint(:));
            timecourse_acq2_mean = mean(timecourse_acq2(:));
            
            % relative to overall mean
            for cond = 1:size(timecourse_acq,1)
                timecourse_acq_bl(cond,:) = timecourse_acq(cond,:);%-timecourse_acq_mean; %#ok<AGROW>
                timecourse_maint_bl(cond,:) = timecourse_maint(cond,:);%-timecourse_maint_mean; %#ok<AGROW>
                timecourse_acq2_bl(cond,:) = timecourse_acq2(cond,:);%-timecourse_acq2_mean; %#ok<AGROW>
            end
            
            % set all lines to start from zero
            for cond = 1:size(timecourse_acq,1)
                timecourse_acq_zero(cond,:) = timecourse_acq_bl(cond,:)-timecourse_acq_bl(cond,1); %#ok<AGROW>
                timecourse_maint_zero(cond,:) = timecourse_maint_bl(cond,:)-timecourse_maint_bl(cond,1); %#ok<AGROW>
                timecourse_acq2_zero(cond,:) = timecourse_acq2_bl(cond,:)-timecourse_acq2_bl(cond,1); %#ok<AGROW>
            end
            
            phasedata = {timecourse_acq_zero timecourse_maint_zero timecourse_acq2_zero}; % Separate data for the phases
            
            %% Plot
            
            phasenames = {'Acquisition' 'Maintenance' 'Acquisition 2'};
            %ax = subplot(ceil(roi_no/2),ceil(roi_no/4),roi);
            
            % Define title for the figure: ROI name
            roititle = roiset_titles{roi};
%             roititle = strtrim(roinames(roi,:));
%             roititle = roititle(22:end-18);
%             roititle = strrep(roititle,'_',' ');
%             roititle = roititle(1:end-12);
            
            % Set axes for the ROI subplot
            ax = axes('Position', pos(roi_pos(roi_ind),:));
            set(gca,'LineWidth',1)
            
            % Draw timecourses for each phase and condition
            clear plotdata
            plotdata = phasedata{phase}';
            if cond2plot == 2
                clear plotdata_US
                plotdata_US(:,1) = mean(plotdata(:,[1 3 5]),2);
                plotdata_US(:,2) = mean(plotdata(:,[2 4 6]),2);
                colors = [0, 90, 181; 220, 50, 32]./255;
                for pl = 1:2
                    plot(plotdata_US(:,pl),'LineWidth',2,'Color',colors(pl,:))
                    hold on
                end
            elseif cond2plot == 4
                clear plotdata_USpUSm
                plotdata_USpUSm(:,1) = plotdata(:,1);
                plotdata_USpUSm(:,2) = mean(plotdata(:,[3 5]),2);
                plotdata_USpUSm(:,3) = mean(plotdata(:,[2 4]),2);
                plotdata_USpUSm(:,4) = plotdata(:,6);
                colors = colors([1 3 4 6],:);
                for pl = 1:size(plotdata_USpUSm,2)
                    plot(plotdata_USpUSm(:,pl),'LineWidth',2,'Color',colors(pl,:))
                    hold on
                end
            else
                for cond = 1:size(plotdata,2)
                    plot(plotdata(:,cond),'LineWidth',2,'Color',colors(cond,:))
                    hold on
                end
            end
            hold on
            
            % Set title
            title(roititle,'FontSize',12,'FontWeight','normal')
            
            % Set x-axis labels and ticks
            set(gca,'XTick',0:20:timewindow*10)
            set(gca,'XTickLabel',{'0','2','4','6','8','10','12','14','16'}) % x-axis labels as seconds
            
            if roi_ind == label_roi
                 % x-axis ticks for the time window
                xlabel('Time (s)','FontSize',10)
            else
                %set(gca,'XTick',[]);
            end
            
            xlim([0 timewindow*10]) % x-limits from 0 to end of the time window
            
            % Set y-axis labels and ticks
            %y_max = round(max(plotdata(:))+max(plotdata(:))*0.3,2); % y-axis limits depending on the % signal change amplitude
            %y_min = round(min(plotdata(:))+min(plotdata(:))*0.3,2);
            %if y_max > abs(y_min); y_min = -y_max; else; y_max = abs(y_min); end
            if roiset == 2
                if cond2plot == 2
                    y_min = -3;
                    y_max = 4;
                    set(gca,'YTick',y_min+1:2:y_max)
                elseif cond2plot == 4
                    y_min = -0.09;
                    y_max = 0.12;
                    set(gca,'YTick',y_min+0.01:0.05:y_max)
                else
                    y_min = -3;
                    y_max = 4;
                    set(gca,'YTick',y_min+1:2:y_max)
                    %y_min = -0.13;
                    %y_max = 0.15;
                    %set(gca,'YTick',y_min+0.03:0.05:y_max)
                end
                %set(gca,'YTickLabel',{'-0.05','0','0.05','0.1'}) % y-axis labels
            else
                y_min = -3;
                y_max = 4;
                set(gca,'YTick',y_min+1:2:y_max)
            end
            
            if roi_ind == label_roi
                ylabel({'BOLD signal change'; 'relative to baseline'},'FontSize',10)
            else
                %set(gca,'YTick',[]);
            end
               
            ylim([y_min y_max])
            line([60 60],[y_min y_max],'Color',[0.5 0.5 0.5],'LineStyle','--','LineWidth',1.5) % Red line at US onset (6 seconds post-CS onset)
            
            %% get hrf
            RT = 0.1;%3.2; % TR
            onset = 0; 
            us_onset = 6;
            p = [6 16 1 1 6 onset 32]; % defaults
            T = 20; % microtime resolution
            [hrf,p] = spm_hrf(RT,p,T);
            resolution = 10;
%             x = 0:numel(hrf);
            %xq = 1:(1/10):numel(hrf);
%             hrf2 = interp1(hrf,xq,'spline');
            endpoint = timewindow-us_onset;
            hrf2 = [zeros(1,us_onset*resolution) hrf(1:(endpoint*resolution),:)'];
            hrf_scaled = rescale(hrf2,0,y_max-1);
            plot(hrf_scaled,'Color','k','LineStyle',':','LineWidth',1)
            
            roi_ind = roi_ind + 1;

        end
        
%         suptitle(phasenames{phase}) % Suptitle for each experimental phase
        % subplot(3,4,12)
        % plot(0,0,  0,0,  0,0,  0,0)
        % axis off
        if cond2plot == 2 % If plotting averaged US+ and US- conditions
            legtext = {'US-' 'US+'};
        elseif cond2plot == 4
            legtext = {'Fully predicted US-' 'Unpredictable US- (33/66%)' 'Unpredictable US- (33/66%)' 'Fully predicted US+'};
        else % If plotting all conditions
            legtext = {'CS(0%)US-' 'CS(33%)US+' 'CS(33%)US-' 'CS(66%)US+' 'CS(66%)US-' 'CS(100%)US+'};
        end
        %columnlegend(2,legtext,'location','northwest')
        legend(legtext,'Location','best')
        
        % Save the plot
        plotpath = fullfile(datapath,'Plots');
        if ~exist(plotpath,'dir'); mkdir(plotpath); end
        
        % Set paper size and save figure (Matlab figure, png, svg formats)
        PAPER = get(roifig,'Position');
        set(roifig,'PaperSize',[PAPER(3), PAPER(4)]);

        if cond2plot == 2 % If plotting averaged US+ and US- conditions
            savefig(fullfile(plotpath,['timecourse_plot_psc_' roiset_names{roiset} '_' phasenames{phase} '_timewindow' num2str(timewindow) 's_USpUSm']))
            saveas(roifig,fullfile(plotpath,['timecourse_plot_psc_' roiset_names{roiset} '_' phasenames{phase} '_timewindow' num2str(timewindow) 's_USpUSm']),'png')
        elseif cond2plot == 4 % If plotting partial conditions
            savefig(fullfile(plotpath,['timecourse_plot_psc_' roiset_names{roiset} '_' phasenames{phase} '_timewindow' num2str(timewindow) 's_no-PE-vs-PE']))
            saveas(roifig,fullfile(plotpath,['timecourse_plot_psc_' roiset_names{roiset} '_' phasenames{phase} '_timewindow' num2str(timewindow) 's_USpUSm']),'png')
        else % If plotting all conditions
            savefig(fullfile(plotpath,['timecourse_plot_psc_' roiset_names{roiset} '_' phasenames{phase} '_timewindow' num2str(timewindow) 's_all-PE-conditions']))
            saveas(roifig,fullfile(plotpath,['timecourse_plot_psc_' roiset_names{roiset} '_' phasenames{phase} '_timewindow' num2str(timewindow) 's_all-PE-conditions']),'png')
        end
        
    end
    
end

end