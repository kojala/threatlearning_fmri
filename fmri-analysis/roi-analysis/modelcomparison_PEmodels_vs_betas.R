#Bayes Factor model comparison approximation for linear regression models on prediction errors explaining BOLD signals in regions-of-interest
rm(list=ls())

install.packages("bayestestR")
library(dplyr)
workdir <- "C:/Data/PCF_fMRI/"
setwd(workdir)

roitype <- 2 # 1 = anatomical, 2 = functional

if (roitype == 1) {
  data <- read.csv(paste(workdir,"fMRIdata_biascorrected/ROIanalysis/Version_11062018/MeanBetas/MeanBetas_LongFormat_allPE_anatROIs.csv",sep = ""))
  roinames <- c("ACC", "BA 8", "BA 9", "BA 10", "BA 11", "BA 44", "BA 45", "BA 46", "BA 47", "PAG", "Amygdala", "Anterior insula", 
                "Dorsal striatum", "Posterior insula", "SN/VTA", "Thalamus", "Ventral striatum")
  no_rois <- 17
  roilist <- c(1:no_rois)
  filename_output <- paste(workdir,"fMRIdata_biascorrected/ROIanalysis/Version_11062018/MeanBetas/MeanBetas_LongFormat_allPE_anatROIs_BF_results.mat",sep = "")
  filename_output2 <- paste(workdir,"fMRIdata_biascorrected/ROIanalysis/Version_11062018/MeanBetas/MeanBetas_LongFormat_allPE_anatROIs_BF_results_bestmodel.csv",sep = "")
  filename_output3 <- paste(workdir,"fMRIdata_biascorrected/ROIanalysis/Version_11062018/MeanBetas/MeanBetas_LongFormat_allPE_anatROIs_BF_results_all.csv",sep = "")
} else {
  roilist <- c(2:8) # full PE, negative PE and unsigned PE clusters
  data <- read.csv(paste(workdir,"fMRIdata_biascorrected/ROIanalysis/Version_11062018/MeanBetas/MeanBetas_LongFormat_allPE_funcROIs.csv",sep = ""))
  data <- dplyr::filter(data, is.element(data$ROI,roilist)) # get only data for the particular ROIs
  roinames <- c("Full PE cluster 1", "Full PE cluster 2", "Negative PE cluster 1", "Negative PE cluster 2", "Negative PE cluster 3", "Unsigned PE cluster 1", "Unsigned PE cluster 2")
  no_rois <- 7
  filename_output <- paste(workdir,"fMRIdata_biascorrected/ROIanalysis/Version_11062018/MeanBetas/MeanBetas_LongFormat_allPE_funcROIs_BF_results.mat",sep = "")
  filename_output2 <- paste(workdir,"fMRIdata_biascorrected/ROIanalysis/Version_11062018/MeanBetas/MeanBetas_LongFormat_allPE_funcROIs_BF_results_bestmodel.csv",sep = "")
  filename_output3 <- paste(workdir,"fMRIdata_biascorrected/ROIanalysis/Version_11062018/MeanBetas/MeanBetas_LongFormat_allPE_funcROIs_BF_results_all.csv",sep = "")
}
  
data$ROI <- factor(data$ROI)
data$Condition <- factor(data$Condition)
data$Subject <- factor(data$Subject)

no_models <- 6+1
modelcomp_best <- array(NA,dim=c(no_rois,4))
modelcomp_all <- array(NA,dim=c(no_rois*no_models,3))

roirows <- 1:no_models

for (roi in 1:no_rois){

  data_roi <- dplyr::filter(data, ROI == roilist[roi]) # get only data for this ROI
  
  tryCatch({
    model_null <- nlme::lme(Beta ~ 1, random = ~ 1|Subject, data = data_roi, method="ML")
    model_outcome <- nlme::lme(Beta ~ Outcome, random = ~ 1|Subject, data = data_roi, method="ML")
    model_fullPE_sym <- nlme::lme(Beta ~ FullPE, random = ~ 1|Subject, data = data_roi, method="ML") # Full signed PE model with 1 intercept
    model_fullPE <- nlme::lme(Beta ~ PosPE + NegPE, random = ~ 1|Subject, data = data_roi, method="ML") # Full signed PE model with separate intercepts and slopes for pos & neg PE
    model_posPE <- nlme::lme(Beta ~ PosPE, random = ~ 1|Subject, data = data_roi, method="ML") # Positive PE model (US+ only)
    model_negPE <- nlme::lme(Beta ~ NegPE, random = ~ 1|Subject, data = data_roi, method="ML") # Negative PE model (US- only)
    model_absPE <- nlme::lme(Beta ~ AbsPE, random = ~ 1|Subject, data = data_roi, method="ML") # Absolute/unsigned PE model
  }, warning=function(w){cat("WARNING :",conditionMessage(w), "\n", "ROI: ", roi, "\n")})
  
  # Compare all other models to intercept model
  BF <- bayestestR::bayesfactor_models(model_outcome, model_fullPE_sym, model_fullPE, model_posPE, model_negPE, model_absPE, denominator = model_null)
  
  highest_BF <- max(BF$BF[1:no_models])
  best_model_ind <- which(BF$BF == highest_BF)
  
  # Save only best model results relative to the intercept model
  modelcomp_best[roi,1] <- roinames[roi]
  modelcomp_best[roi,2] <- BF$Model[BF$BF == highest_BF]
  modelcomp_best[roi,3] <- best_model_ind
  modelcomp_best[roi,4] <- highest_BF
  
  # Update model comparison to be relative to the best model
  #BF2 <- update(BF, reference = best_model_ind)
  
  # Save all results
  modelcomp_all[roirows,1] <- roinames[roi] #rep(c(roi), times = no_models)
  modelcomp_all[roirows,2] <- BF$Model[1:no_models]
  modelcomp_all[roirows,3] <- BF$BF[1:no_models]
  
  roirows <- roirows+no_models # progress row index for the next ROI

}

# Make into a data frame and rename columns
modelcomp_best <- data.frame(modelcomp_best)
modelcomp_best <- modelcomp_best %>% 
  rename(
    ROI = X1,
    BestModel = X2,
    BestModelNo = X3,
    ModelBF = X4
  )

modelcomp_all <- data.frame(modelcomp_all)
modelcomp_all <- modelcomp_all %>% 
  rename(
    ROI = X1,
    ModelName = X2,
    ModelBF = X3
  )

# Save results in MATLAB format
R.matlab::writeMat(filename_output, 
                   modelcomp_best = modelcomp_best, modelcomp_all = modelcomp_all)
# Save results in csv format
write.csv(modelcomp_best,filename_output2, row.names = FALSE)
write.csv(modelcomp_all,filename_output3, row.names = FALSE)



#model_fullPE <- lm(Beta ~ FullPE, data = data_roi) # Full signed PE model
#model_fullPE_rint <- lm(Beta ~ FullPE + (1|Outcome), data = data_roi) # Full signed PE model with random intercept for either outcome (US+ vs. US-)
#model_fullPE_rslope <- lm(Beta ~ FullPE + (FullPE|Outcome), data = data_roi) # Full signed PE model with independent random intercepts and random slopes for either outcome (US+ vs. US-)
#model_fullPE_rint <- nlme::lme(Beta ~ FullPE, random = ~ 1|factor(Outcome), data = data_roi, method="ML", control=(msMaxIter=100)) # Full signed PE model with random intercept for either outcome (US+ vs. US-)
#model_fullPE_rslope <- nlme::lme(Beta ~ FullPE, random = ~ FullPE-1|factor(Outcome), data = data_roi, method="ML", control=(msMaxIter=100)) # Full signed PE model with random intercept and slope for either outcome (US+ vs. US-)
#  model_fullPE_rint <- lme(Beta ~ FullPE, random = ~ 1|Outcome, data = data_roi, method="ML", control=(msMaxIter=5000)) # Full signed PE model with random intercept for either outcome (US+ vs. US-)
#tryCatch({
#  model_fullPE_rslope <- lme(Beta ~ FullPE, random = ~ FullPE|Outcome, data = data_roi, method="ML", control=(msMaxIter=5000)) # Full signed PE model with random intercept and slope for either outcome (US+ vs. US-)
#}, error=function(e){cat("ERROR :",conditionMessage(e), "\n", "ROI: ", roi, "\n")})
# model_fullPE <- lme4::lmer(Beta ~ FullPE + (1-FullPE|Outcome), data = data_roi)

#model_fullPE <- nlme::lme(Beta ~ FullPE, random = list(Outcome = pdDiag(~ FullPE)), data = data_roi, method="ML", control=(msMaxIter=1000)) # Full signed PE model with random intercept and slope for either outcome (US+ vs. US-)