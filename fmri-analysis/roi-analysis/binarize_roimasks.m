%Make ROI masks binary (0 = outside mask, 1 = inside mask)
clear all

cutoff = num2str(0.132);
%cutoff = num2str(0.2); % insula masks after coregistration

matlabbatch{1}.spm.util.imcalc.input = {'D:\fMRI_Karita\fMRIdata_biascorrected\Masks\RawMasks\PAG_ATAG_probatlas_mni_linear_young.nii,1'};
matlabbatch{1}.spm.util.imcalc.output = ['PAG_ATAG_probatlas_mni_linear_young_bin_cutoff' cutoff(3:end)];
%matlabbatch{1}.spm.util.imcalc.input = {'D:\fMRI_Karita\fMRIdata_biascorrected\Masks\RawMasks\r_posteriorinsula_L.nii,1'};
%matlabbatch{1}.spm.util.imcalc.output = 'r_posteriorinsula_L_bin';
matlabbatch{1}.spm.util.imcalc.outdir = {'D:\fMRI_Karita\fMRIdata_biascorrected\Masks\RawMasks'};
matlabbatch{1}.spm.util.imcalc.expression = ['i1 > ' cutoff];
matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
matlabbatch{1}.spm.util.imcalc.options.mask = 0;
matlabbatch{1}.spm.util.imcalc.options.interp = 1;
matlabbatch{1}.spm.util.imcalc.options.dtype = 4;

spm defaults fmri
spm_jobman initcfg
spm_get_defaults('cmdline',true)
spm_jobman('run', matlabbatch);