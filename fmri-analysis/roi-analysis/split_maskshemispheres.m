%Split masks into right and left hemispheres
clear all
mainpath = fullfile(cd,'..','..','..');
fpath = fullfile(mainpath,'fMRIdata_biascorrected');
rawPath = fullfile(fpath,'Masks\RawMasks');
dataPath = fullfile(fpath,'Masks\ReslicedMasks');

hemisMasks = {'lefthemisphere' 'righthemisphere'};
hemispheres = {'L' 'R'};
maskNames = {'striatum_highres_bin'...
    'dorsalstriatum_highres_bin' 'ventralstriatum_highres_bin'...
    'substantianigra_highres_bin' 'substantianigraVTA_highres_bin'...
    'probability_map_basolateral_bin_thresh05'...
    'probability_map_centrocortical_bin_thresh05'};
    %'probability_map_amygdala_bin_thresh05'};...

for n = 1:length(maskNames)
    maskList{n} = fullfile(rawPath,[maskNames{n} '.nii']); %#ok<SAGROW>
end

%Reslice to EPI space
% matlabbatch{1}.spm.spatial.coreg.write.ref = {[fpath,'PR02010_VF180893_20160803_123831155\EPI\swmeanuabc20160803_110747almepi2d3mmdropouts005a001.nii,1']};
% matlabbatch{1}.spm.spatial.coreg.write.source = maskList(:);
% matlabbatch{1}.spm.spatial.coreg.write.roptions.interp = 4;
% matlabbatch{1}.spm.spatial.coreg.write.roptions.wrap = [0 0 0];
% matlabbatch{1}.spm.spatial.coreg.write.roptions.mask = 0;
% matlabbatch{1}.spm.spatial.coreg.write.roptions.prefix = 'r_';
% spm_jobman('run',matlabbatch)
% 
% clear matlabbatch

% Split into the two hemispheres
for mask = 1:length(maskList)
    
    for hemis = 1:2
        
        matlabbatch{1}.spm.util.imcalc.input = {fullfile(dataPath,['r_' maskNames{mask} '.nii'])
                                                fullfile(dataPath,['r_' hemisMasks{hemis} '.nii'])};
        matlabbatch{1}.spm.util.imcalc.output = ['r_' maskNames{mask} '_' hemispheres{hemis}];
        matlabbatch{1}.spm.util.imcalc.outdir = {fullfile(dataPath)};
        matlabbatch{1}.spm.util.imcalc.expression = 'i1.*i2';
        matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
        matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
        matlabbatch{1}.spm.util.imcalc.options.mask = 0;
        matlabbatch{1}.spm.util.imcalc.options.interp = 1;
        matlabbatch{1}.spm.util.imcalc.options.dtype = 4;
       
        spm_jobman('run',matlabbatch)

    end
    
end