function extract_ROIbetas_persub(version,sublist,models,phase,rois)
%Extract average beta values from each ROI, for each subject and condition
%for categorical axiomatic GLM

opts = get_options(); % Get general options

fpath = opts.fpath; % Main analysis path

% Subject list
subj = opts.subj;
subj = subj(sublist);

% Steps:
% Load ROI coordinates
%   - Load a contrast and select a created ROI
%   - Set corrected p-value threshold to 1 (every voxel in ROI "active"),
%     recorded in xSPM
%   - xSPM.XYZ contains coordinates for each voxel within the ROI
%
% Create an array of beta images to extract data from
%   - Load 2nd level SPM.mat
%
% ROI analysis with spm_get_data
%   - Extract data from each voxel in the ROI for each subject for each
%     condition
%   - Average across all of the voxels in the ROI

% ROIs
ROIfolder = opts.roifolders(rois);
ROInames = opts.roinames(rois);

for model = models % Loop over GLMs
    
    model_inp = get_modelconfig(opts,model); % Get GLM configuration
    
    for p = phase % Loop over experimental phases
        
        for roitype = 1:length(ROIfolder) % Loop over ROI types
            
            roipath = fullfile(fpath,'Masks','ReslicedMasks','CurrentMasks',ROIfolder{roitype}); % ROI mask path
            roifiles = spm_select('FPList',roipath); % Find ROI mask files
            roinames = spm_select('List',roipath); % ROI mask names for checking purposes
            
            clear meanbetas
            meanbetas = nan(length(subj),6,size(roifiles,1)); % Initialize mean betas matrix
            meanbetas_runs = nan(length(subj),6,4,size(roifiles,1));
            % 21 subjects, 6 conditions, x ROIs
            
            for roi = 1:size(roifiles,1) % Loop over ROIs within ROI type
                
                for sub = 1:length(subj) % 1st level condition betas for each subject
                    
                    clear US1 US2 US3 US4 US5 US6 condbetas cond_bfiles betas
                    
                    % Find beta files with SPM file selection function
                    betapath = fullfile(fpath,subj(sub),'1stlevel',['Version_' num2str(version)],['NoiseCorr_' opts.noisecorr_name],model_inp.modelname,['First_Level_' opts.phasenames{p}]);
                    betafiles = spm_select('FPList',betapath,'^beta.*nii');
                    
                    % Select beta files corresponding to the 6 US conditions
                    if p == 1 || p == 3 % Acquisition or Acquisition 2
                        US1 = betafiles(5,:); % First 4 beta files are for CS conditions
                        US2 = betafiles(6,:);
                        US3 = betafiles(7,:);
                        US4 = betafiles(8,:);
                        US5 = betafiles(9,:);
                        US6 = betafiles(10,:);
                    elseif p == 2 % Maintenance
                        if strncmp(subj{sub},'PR02121',7) % This subject has only 3 (vs. 4) runs of maintenance phase
                            US1 = betafiles([5,39,73],:);
                            US2 = betafiles([6,40,74],:);
                            US3 = betafiles([7,41,75],:);
                            US4 = betafiles([8,42,76],:);
                            US5 = betafiles([9,43,77],:);
                            US6 = betafiles([10,44,78],:);
                        else
                            US1 = betafiles([5,39,73,107],:);
                            US2 = betafiles([6,40,74,108],:);
                            US3 = betafiles([7,41,75,109],:);
                            US4 = betafiles([8,42,76,110],:);
                            US5 = betafiles([9,43,77,111],:);
                            US6 = betafiles([10,44,78,112],:);
                        end
                    elseif p == 4 % All phases
                        US1 = betafiles([5,39,73,107,141,175],:);
                        US2 = betafiles([6,40,74,108,142,176],:);
                        US3 = betafiles([7,41,75,109,143,177],:);
                        US4 = betafiles([8,42,76,110,144,178],:);
                        US5 = betafiles([9,43,77,111,145,179],:);
                        US6 = betafiles([10,44,78,112,146,180],:);
                    elseif p == 5 % Both acquisitions
                        US1 = betafiles([5,39],:);
                        US2 = betafiles([6,40],:);
                        US3 = betafiles([7,41],:);
                        US4 = betafiles([8,42],:);
                        US5 = betafiles([9,43],:);
                        US6 = betafiles([10,44],:);
                    end
                    
                    condbetas = {US1 US2 US3 US4 US5 US6};
                    
                    % Loop over US conditions
                    for cond = 1:6
                        
                        cond_bfiles = condbetas{cond}; % Beta files for the condition
                        
                        % ROI coordinates
                        Y = spm_read_vols(spm_vol(roifiles(roi,:)),1); % Y is 4D matrix of image data
                        indx = find(Y>0);
                        [x,y,z] = ind2sub(size(Y),indx);
                        XYZ = [x y z]';
                        
                        % Extract betas from the ROI for this condition
                        % Save in a matrix of format [subject,condition,ROI]
                        meanbetas(sub,cond,roi) = nanmean(nanmean(spm_get_data(cond_bfiles,XYZ),2));
                        meanbetas_runs(sub,cond,run,roi) = nanmean(spm_get_data(cond_bfiles,XYZ),2);
                        
                    end
                    
                end
                
            end
            
            % Save mean betas per sub per condition per ROI
            save(fullfile(fpath,'ROIanalysis',['Version_' num2str(version)],'MeanBetas',['MeanBetas_' ROInames{roitype} '_' num2str(size(roifiles,1)) 'ROIs_allconds.mat']),'meanbetas','meanbetas_runs','roinames')
            
        end
    end
end
%% Beta files
%     US1 = betafiles([5,41,77,113],:); % PhysIO correction
%     US2 = betafiles([6,42,78,114],:);
%     US3 = betafiles([7,43,79,115],:);
%     US4 = betafiles([8,44,80,116],:);
%     US5 = betafiles([9,45,81,117],:);
%     US6 = betafiles([10,46,82,118],:);

%     US1 = betafiles([5,21,37,53],:); % Head motion only
%     US2 = betafiles([6,22,38,54],:);
%     US3 = betafiles([7,23,39,55],:);
%     US4 = betafiles([8,24,40,56],:);
%     US5 = betafiles([9,25,41,57],:);
%     US6 = betafiles([10,26,42,58],:);

%% Test code
% roipath = fullfile(fpath,'Masks','ROImasks');
% load(fullfile(roipath,'MeanBetas_13rois_allconds.mat'))
% xvec = [0 1/3 2/3 1];
%
% for cond = 1:size(meanbetas,2)
%
%     for sub = 1:21
%         betamatrix(cond,:,sub) = meanbetas{sub,cond};
%     end
%
%     condmean(cond,:) = mean(betamatrix(cond,:,sub),3);
%
% end

% betas_SN = betamatrix(:,12,:);
% betas_SN = squeeze(betas_SN)';
% betas_Amyg = betamatrix(:,5,:);
% betas_Amyg = squeeze(betas_Amyg)';
% betas_DStr = betamatrix(:,7,:);
% betas_DStr = squeeze(betas_DStr)';

% [h_SN{1},p_SN{1},ci_SN{1},stats_SN{1}] = ttest(betas_SN(:,1),betas_SN(:,3));
% [h_SN{2},p_SN{2},ci_SN{2},stats_SN{2}] = ttest(betas_SN(:,3),betas_SN(:,5));
% [h_SN{3},p_SN{3},ci_SN{3},stats_SN{3}] = ttest(betas_SN(:,2),betas_SN(:,4));
% [h_SN{4},p_SN{4},ci_SN{4},stats_SN{4}] = ttest(betas_SN(:,4),betas_SN(:,6));
% [h_SN{5},p_SN{5},ci_SN{5},stats_SN{5}] = ttest(betas_SN(:,2),betas_SN(:,3));
% [h_SN{6},p_SN{6},ci_SN{6},stats_SN{6}] = ttest(betas_SN(:,4),betas_SN(:,5));
% [h_SN{7},p_SN{7},ci_SN{7},stats_SN{7}] = ttest(betas_SN(:,1),betas_SN(:,6));
% % Axiom 2 fulfilled, Axioms 1 and 3 failed
%
% [h_Amyg{1},p_Amyg{1},ci_Amyg{1},stats_Amyg{1}] = ttest(betas_Amyg(:,1),betas_Amyg(:,3));
% [h_Amyg{2},p_Amyg{2},ci_Amyg{2},stats_Amyg{2}] = ttest(betas_Amyg(:,3),betas_Amyg(:,5));
% % Axiom 1 failed
%
% [h_DStr{1},p_DStr{1},ci_DStr{1},stats_DStr{1}] = ttest(betas_DStr(:,2),betas_DStr(:,4));
% [h_DStr{2},p_DStr{2},ci_DStr{2},stats_DStr{2}] = ttest(betas_DStr(:,4),betas_DStr(:,6));
% Axiom 1 failed

%% Old code
% Beta files
%betapath = fullfile(fpath,'2ndlevel',['NoiseCorr_' physiocorr_name],modelname,phasename,'conjunction_axioms12_6cons');
%betafiles = spm_select('FPListRec',betapath,'^beta.*nii'); % search recursively through contrast subfolders
%vols = spm_vol(betafiles);

%spmfiles = spm_select('FPListRec',betapath,'SPM.mat'); % search recursively through contrast subfolders
%spmfiles = spmfiles(3:end,:);

%roifile = spm_select('FPList',roipath,'ROIcoords_voxelspace.mat');
%load(roifile);

% for cond = 1:6
%
%     conrois = ROIcoords(cond+2,:);
%
%     for r = 1:size(conrois,2)
%
%         clear SPM
%         % extract_betas
%         load(spmfiles(cond,:));
%         betas = mean(spm_get_data(SPM.xY.P,conrois{r}),2);
%
%         cons = length(betas)/21;
%
%         % average betas over subjects
%
%         istart = 1;
%
%         for c = 1:cons
%
%             conbetas(c,r) = mean(betas(istart:istart+20));
%             istart = istart+21;
%
%         end
%
%     end
%
%     meanbetas{cond} = conbetas;
%
% end