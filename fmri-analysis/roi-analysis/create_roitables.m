function create_roitables(version,roitype,roilist)
%Write ROI mean beta and error bar data in Excel and MATLAB table format

opts = get_options();

roipath = opts.roipath;
meanbetapath = fullfile(roipath,['Version_' num2str(version)],'MeanBetas');

% ROI beta value files
ROIfiles = cellstr(ls(fullfile(meanbetapath,'MeanBetas*allconds.mat')));

% ROI error bar files
EBfiles = cellstr(ls(fullfile(meanbetapath,'MeanBetas*ERRORBARS.mat')));

roiname = opts.roinames{roitype}; % ROI type name
roi_ind = strfind(ROIfiles, ['_' roiname '_']);
roi_ind2 = not(cellfun('isempty', roi_ind)); %#ok<STRCL1>

mbfile = load(fullfile(meanbetapath,ROIfiles{roi_ind2})); % Mean betas file
ebfile = load(fullfile(meanbetapath,EBfiles{roi_ind2})); % Error bars file

meanbetas = mbfile.meanbetas(:,:,roilist); % Matrix format [subject,condition,ROI]
roinames = mbfile.roinames(roilist,:,:); % ROI names
errorbars = ebfile.errorbars(:,roilist); % Error bars

roi_no = size(meanbetas,3); % Number of ROIs

for roi = 1:roi_no % Loop over ROIs
    
    roibetas = nan(1,6); % Initialize ROI beta value array as NaNs
    
    for cond = 1:6 % Loop over conditions
        roibetas(cond) = mean(meanbetas(:,cond,roi)); % Get mean beta for this condition and ROI
    end
    
    roi_betas(roi,:) = roibetas; %#ok<AGROW> % Save condition-wise mean betas for current ROI
    roi_errors(roi,:) = errorbars(:,roi); %#ok<AGROW> % Save condition-wise error bars for current ROI
    
end

if roitype == 1 % All anatomical ROIs, hemispheres separate
    ROI = opts.roinames_mvpa'; % Get ROI names from the MVPA list
elseif roitype == 3 % Functional cluster ROIs
    for name = 1:size(roinames,1)
        name_end = strfind(roinames(name,:), '_');
        ROI{name} = roinames(name,1:name_end(2)-1); %#ok<AGROW> % ROI name
    end
    ROI = ROI';
end

% Mean betas
Cond1 = roi_betas(:,1); % CS(0%)US-
Cond2 = roi_betas(:,2); % CS(33%)US+
Cond3 = roi_betas(:,3); % CS(33%)US-
Cond4 = roi_betas(:,4); % CS(66%)US+
Cond5 = roi_betas(:,5); % CS(66%)US-
Cond6 = roi_betas(:,6); % CS(100%)US+
betatable = table(ROI,Cond1,Cond2,Cond3,Cond4,Cond5,Cond6); % Matlab table

summarytablefile = fullfile(roipath,['ROI_' roiname '_summarytable_betas_' num2str(length(roilist)) 'rois.xls']); % Save Excel table
writetable(betatable,summarytablefile); % Save Matlab table

% Error bars
Cond1 = roi_errors(:,1);
Cond2 = roi_errors(:,2);
Cond3 = roi_errors(:,3);
Cond4 = roi_errors(:,4);
Cond5 = roi_errors(:,5);
Cond6 = roi_errors(:,6);
errortable = table(ROI,Cond1,Cond2,Cond3,Cond4,Cond5,Cond6);

summarytablefile = fullfile(roipath,['ROI_' roiname '_summarytable_errors_' num2str(length(roilist)) 'rois.xls']);
writetable(errortable,summarytablefile);
    
end