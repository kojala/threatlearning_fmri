function ttest_posthocaxiomatic(version,rois,roilist)
%Post-hoc axiomatic t-tests for mean beta values from ROIs, axioms 1 and 2

opts = get_options();

roipath = opts.roipath; % ROI analysis path
meanbetapath = fullfile(roipath,['Version_' num2str(version)],'MeanBetas');

% ROI beta value files
ROIfiles = cellstr(ls(fullfile(meanbetapath,'MeanBetas*allconds.mat')));

for roitype = rois % Loop over type of ROI
    
    roiname = opts.roinames{roitype}; % ROI type name
    roi_ind = strfind(ROIfiles, ['_' roiname '_']);
    roi_ind2 = not(cellfun('isempty', roi_ind)); %#ok<STRCL1>
    
    mbfile = load(fullfile(meanbetapath,ROIfiles{roi_ind2})); % Mean betas
    meanbetas = mbfile.meanbetas(:,:,roilist); % Matrix format [subject,condition,ROI]
    
    for roi = 1:length(roilist) % Loop over ROIs within ROI type
        
        meanbetas_roi = meanbetas(:,:,roi); % Mean beta values for current ROI
        % Axiomatic paired t-tests (one-sided, right tail)
        % Axiom 1
        [~,p(1),~,stats(1)] = ttest(meanbetas_roi(:,2),meanbetas_roi(:,3),'tail','right'); % CS(33%)US+ > CS(33%)US-
        [~,p(2),~,stats(2)] = ttest(meanbetas_roi(:,4),meanbetas_roi(:,5),'tail','right'); % CS(66%)US+ > CS(66%)US-
        % Axiom 2
        [~,p(3),~,stats(3)] = ttest(meanbetas_roi(:,2),meanbetas_roi(:,4),'tail','right'); % CS(33%)US+ > CS(66%)US+
        [~,p(4),~,stats(4)] = ttest(meanbetas_roi(:,4),meanbetas_roi(:,6),'tail','right'); % CS(66%)US+ > CS(100%)US+
        [~,p(5),~,stats(5)] = ttest(meanbetas_roi(:,1),meanbetas_roi(:,3),'tail','right'); % CS(0%)US- > CS(33%)US-
        [~,p(6),~,stats(6)] = ttest(meanbetas_roi(:,3),meanbetas_roi(:,5),'tail','right'); % CS(33%)US- > CS(66%)US-

        % Save T-values
        t = [stats(1).tstat stats(2).tstat stats(3).tstat stats(4).tstat stats(5).tstat stats(6).tstat];
        roistats_p(roi,:) = p;
        roistats_t(roi,:) = t;
        %roistats_d(roi,:) = d; Cohen's D calculated in R instead
        roinames(roi,:) = mbfile.roinames(roi,:); % Save ROI names
        
    end
    
    save(fullfile(meanbetapath,['MeanBetas_axiomatic_posthoc_ttest_' opts.roinames{roitype} '.mat']),'roistats_p', 'roistats_t', 'roinames')
    %writetable(roistats_table,fullfile(meanbetapath,['MeanBetas_axiomatic_posthoc_ttest_table_' opts.roinames{roitype} '.xls']))
    
end

end