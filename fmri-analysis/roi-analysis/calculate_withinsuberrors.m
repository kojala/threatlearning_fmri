function calculate_withinsuberrors(version,rois)
%Calculate and save within-subject errors for mean beta values of each condition for each ROI, for later plotting

opts = get_options();

roipath = opts.roipath; % ROI analysis path
meanbetapath = fullfile(roipath,['Version_' num2str(version)],'MeanBetas'); % Mean beta values data path
flist = cellstr(ls(fullfile(meanbetapath,'MeanBetas*_allconds.mat'))); % List of all mean beta value files

subjects = length(opts.subj); % Number of subjects

for roi = 1:length(rois) % Loop over ROI type
    
    roiname = opts.roinames{rois(roi)}; % ROI name
    
    roi_ind = strfind(flist, ['_' roiname '_']); % ROI index
    roi_ind2 = not(cellfun('isempty', roi_ind)); %#ok<STRCL1>
    
    clear meanbetas roidata meanremoved grandavg subavg newvalues
    
    mbdata = load(fullfile(meanbetapath,flist{roi_ind2})); % Load data    
    roidata = mbdata.meanbetas;
    
    % Calculate within-subject error bars according to Cousineau (2005)
    subavg = mean(roidata,2); % Mean over conditions for each sub for each ROI
    grandavg = mean(subavg); % Mean over subjects and conditions for each ROI
    
    newvalues = nan(size(roidata)); % Initialize matrix of NaNs for new values
    
    % Normalization of subject values
    for cond = 1:6 % Loop over conditions
        meanremoved = roidata(:,cond,:)-subavg; % Remove mean of conditions from each condition value for each sub for each roi
        newvalues(:,cond,:) = meanremoved+repmat(grandavg,[subjects 1 1]); % Add grand average over subjects to the values where individual sub average was removed
    end
    
    newvar = (cond/(cond-1))*var(newvalues); % Morey (2005) fix
    tvalue = tinv(1-0.05, subjects-1); % Find T-value with alpha level 0.05, for degrees of freedom N-1
    errorbars = squeeze(tvalue*(sqrt(newvar)./sqrt(subjects))); % Calculate error bars according to Cousineau (2005) with Morey (2005) fix
    
    save(fullfile(meanbetapath,[flist{roi_ind2}(1:end-4) '_ERRORBARS.mat']),'errorbars') % Save error bar values
    
end

end