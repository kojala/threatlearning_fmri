clear all

% Combine left and right hemisphere masks
dataPath = 'D:\fMRI_Karita\fMRIdata_biascorrected\Masks\ReslicedMasks\CurrentMasks\Anatomical\BothHemispheres';
outPath = fullfile(dataPath,'Combined');

maskNames = cellstr(ls(char(fullfile(dataPath,'*_L.nii'))));

for n = 1:length(maskNames)
    mask = maskNames{n}(1:end-6);
    maskList{n} = mask;
end

for mask = [11 13]%1:length(maskList)
    
    matlabbatch{1}.spm.util.imcalc.input = {fullfile(dataPath,[maskList{mask} '_L.nii'])
        fullfile(dataPath,[maskList{mask} '_R.nii'])};
    matlabbatch{1}.spm.util.imcalc.output = maskList{mask};
    matlabbatch{1}.spm.util.imcalc.outdir = {fullfile(outPath)};
    matlabbatch{1}.spm.util.imcalc.expression = 'i1+i2';
    matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
    matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
    matlabbatch{1}.spm.util.imcalc.options.mask = 0;
    matlabbatch{1}.spm.util.imcalc.options.interp = 1;
    matlabbatch{1}.spm.util.imcalc.options.dtype = 4;
    
    spm_jobman('run',matlabbatch)
    
end