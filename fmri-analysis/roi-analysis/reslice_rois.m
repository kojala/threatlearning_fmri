%Reslice ROIs into the correct space
mainpath = fullfile(cd,'..','..','..');
fpath = fullfile(mainpath,'fMRIdata_biascorrected');
roipath = fullfile(fpath,'Masks\RawMasks');
roilist = spm_select('FPList',roipath,'.*nii');

roilist_cell = cellstr(roilist); % to check indices
rois_toreslice = [38 39 53 54]; % new PAG mask binarized

roilist = roilist(rois_toreslice,:);

for r = 1:size(roilist,1)
    
    matlabbatch{r}.spm.spatial.coreg.write.ref = {[fpath,'\PR02010_VF180893_20160803_123831155\EPI\swmeanuabc20160803_110747almepi2d3mmdropouts005a001.nii,1']}; %#ok<*SAGROW>
    matlabbatch{r}.spm.spatial.coreg.write.source = cellstr(roilist(r,:));
    matlabbatch{r}.spm.spatial.coreg.write.roptions.interp = 4;
    matlabbatch{r}.spm.spatial.coreg.write.roptions.wrap = [0 0 0];
    matlabbatch{r}.spm.spatial.coreg.write.roptions.mask = 0;
    matlabbatch{r}.spm.spatial.coreg.write.roptions.prefix = 'r_';
    
end

spm defaults fmri
spm_jobman initcfg
spm_get_defaults('cmdline',true)
spm_jobman('run', matlabbatch);