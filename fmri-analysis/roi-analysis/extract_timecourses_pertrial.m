function extract_timecourses_pertrial(roitype,rois,trialwindow)

opts = get_options();

roipath = fullfile(opts.roipath,'RawROItimecourses');

% ROI timecourse files for the specified ROI type
roinames = spm_select('List',fullfile(opts.roimaskpath,opts.roifolders{roitype}));
roinames = roinames(rois,:);

% Data indicating which scans are too short for the trial time window
shortscansfile = fullfile(opts.fpath,['tooshortscans_USresp' num2str(opts.USresptime) 'sec.mat']);
tooshort_data = load(shortscansfile);
        
for roi = 1:length(rois) % Loop over ROIs
    
    roiname = strtrim(roinames(roi,:));
    roiname = roiname(:,1:end-4);
    
    for sub = 1:length(opts.subj_beh) % Loop over subjects
        
        % Load behavioral data to get condition info for each trial
        sub_bhvpath = fullfile(opts.bhvpath, ['S' num2str(opts.subj_beh(sub))]);
        bhvfile = fullfile(sub_bhvpath, ['Behavior_S' num2str(opts.subj_beh(sub)) '.mat']);
        bhvdata = load(bhvfile);
        
        for run = opts.runs % Loop over scanner runs
            
            roidata_CS = []; roidata_US = []; roidata_trial = [];
            trialtype = [];
            
            CStype = bhvdata.cond(run).CS; % CS type
            CSon = bhvdata.tim(run).CS_on; % CS onset
            UStype = bhvdata.cond(run).US; % US type
            USon = bhvdata.tim(run).US_on; % US onset
            
            % Change Acquisition 2 different CS types to the same ones as
            % Acquistion 1 and Maintenance
            CStype(CStype==6) = 1;
            CStype(CStype==7) = 2;
            CStype(CStype==8) = 3;
            CStype(CStype==5) = 4;
            
            % Save trial type (CS-US combination)
            trialtype(UStype==0 & CStype==1) = 1; %#ok<AGROW>
            trialtype(UStype==1 & CStype==2) = 2; %#ok<AGROW>
            trialtype(UStype==0 & CStype==2) = 3; %#ok<AGROW>
            trialtype(UStype==1 & CStype==3) = 4; %#ok<AGROW>
            trialtype(UStype==0 & CStype==3) = 5; %#ok<AGROW>
            trialtype(UStype==1 & CStype==4) = 6; %#ok<AGROW>
            
            % Load and extract ROI timecourse data averaged over voxels
            if roitype == 3
                if strcmp(roiname(1:6),'fullPE'); roiname = roiname(5:end); 
                elseif strcmp(roiname(1:10),'negativePE'); roiname = roiname([1:3 9:end]); 
            %elseif strcmp(roiname(1:10),'unsignedPE'); roiname = roiname(1:10); 
                end
            elseif roitype == 5
                %roiname = roiname(3:end);
            end
            
            subdata = load(fullfile(roipath,['timecourse_' roiname '_run' num2str(run) '_subject' num2str(sub) '.mat']));
            %roidata{run,sub} = mean(subdata.timecourseRoi);
            
            data = subdata.timecourseRoi';
            TR = 3.2; % Repetition time 3.2 s
            x = (1:size(data,1))'; % Data point indices
            querypoints = 0:(1/(TR*10)):size(data,1); % Get data at each TR in the right sampling rate (10 Hz)
            %querypoints(1) = [];
            % Interpolate voxelwise signals to 10 Hz
            interp_data = interp1(x,data,querypoints,'linear','extrap');
            
            % Average over voxels
            roidata = nanmean(interp_data,2);
            
            % Assign ROI data to conditions
            % Think about what is sensible time window!!
%             HDSL = 6*10; % haemodynamic signal lag in 10 Hz
            HDSL = 0; % no lag for now
            CSdur = (6+2)*10; % CS duration in 10 Hz and additional processing time with not too much overlap with the US response
            durafterUS = trialwindow-6.5; % Trial window minus CS+US time
            USdur = (0.5+durafterUS)*10; % US duration in 10 Hz and additional processing time
            
            CSon = round(CSon/100+HDSL); % 10 Hz granularity + HDSL
            CStime = [CSon CSon+CSdur]; % CS onsets and offsets
            
            USon = round(USon/100+HDSL); % US onsets at 10 Hz + HDSL
            UStime = [USon USon+USdur]; % US onsets and offsets
            
            trialtime = [CSon USon+USdur]; % Trial onset and offset
            
            % Is the run too short for the last US to be included?
            tooshort = tooshort_data.tooshortscans(sub,run+1);
            
            for trial = 1:length(CSon) % Loop over trials
                
                if (trial == length(CSon) && tooshort > 0) || (sub == 14 && run == 3) % Last US too short / subject 14 has no usable data in run 3
                    roidata_CS(trial,:) = NaN; %#ok<AGROW> % CS trials
                    roidata_US(trial,:) = NaN; %#ok<AGROW> % US trials
                    roidata_trial(trial,:) = NaN;  %#ok<AGROW> % Whole trial
                else
                    try
                        roidata_CS(trial,:) = roidata(CStime(trial,1):CStime(trial,2)); %#ok<AGROW>
                        roidata_US(trial,:) = roidata(UStime(trial,1):UStime(trial,2)); %#ok<AGROW>
                        roidata_trial(trial,:) = roidata(trialtime(trial,1):trialtime(trial,2)); %#ok<AGROW>
                    catch
                        roidata_CS(trial,:) = NaN; %#ok<AGROW> % CS trials
                        roidata_US(trial,:) = NaN; %#ok<AGROW> % US trials
                        roidata_trial(trial,:) = NaN;  %#ok<AGROW> % Whole trial
                        difference = (trialtime(trial,2) - length(roidata)) / 10;
                        warning(['Data ' num2str(difference) ' s too short: Subject ' num2str(sub) ' - Run ' num2str(run) ' - Trial ' num2str(trial)])
                    end
                end
                
            end
            
            roidata_out.CS(sub).subject(run).timecourse = roidata_CS;
            roidata_out.CS(sub).subject(run).type = CStype;
            
            roidata_out.US(sub).subject(run).timecourse = roidata_US;
            roidata_out.US(sub).subject(run).type = UStype;
            
            roidata_out.trial(sub).subject(run).timecourse = roidata_trial;
            roidata_out.trial(sub).subject(run).type = trialtype';
                
        end
    end
    
%     save(fullfile(opts.roipath,'RawROItimecourses','TrialData',['timecourses_trialdata_' roiname '_timewindow' num2str(trialwindow) 's_HDSL' num2str(HDSL/10) 's.mat']),'roidata_out')
    save(fullfile(opts.roipath,'RawROItimecourses','TrialData',['timecourses_trialdata_' roiname '_timewindow' num2str(trialwindow) 's.mat']),'roidata_out')

end

end