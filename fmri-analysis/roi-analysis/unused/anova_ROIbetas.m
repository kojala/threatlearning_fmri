function anova_ROIbetas(version,roitypes)

opts = get_options();

mbpath = fullfile(opts.fpath,'ROIanalysis',['Version_' num2str(version)],'MeanBetas');
mbfiles = cellstr(ls(fullfile(mbpath,'MeanBetas*allconds.mat')));

for roitype = 1:length(roitypes)
    
    mbdata = load(fullfile(mbpath,mbfiles{roitype}));
    
    for roi = 1:size(mbdata.meanbetas,3)
        
        betas = mbdata.meanbetas(:,:,roi);
        
        condition = 1:6;
        
        % Roy et al.: 
        %   axiom 1: shock effect tested with t-test between averaged shock and no shock conditions
        %   axiom 2: expectation effect tested with a linear regression slope
        %   significance test for the different expetation levels,
        %   non-parametric multilevel sign permutation test
        %   axiom 3: equivalence of fully predicted shock and no shock tested with a
        %   difference test -> no diff -> equivalent (which is wrong!)
        % repeated-measures anova for 3 levels of expectation for shock and
        % no shock conditions separately
        rmtable = table(betas(:,1),betas(:,2),betas(:,3),betas(:,4),betas(:,5),betas(:,6),...
            'VariableNames',{'betas1','betas2','betas3','betas4','betas5','betas6'});
        Betas = table([1 2 3 4 5 6]','VariableNames',{'Betas'});
        rm = fitrm(rmtable,'betas1-betas6~1','WithinDesign',Betas);
        
        ranovatbl = ranova(rm);
        
    end
    
end

end