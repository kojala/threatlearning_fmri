function imcalc_voxels_fulfillaxioms(version,sublist,phase)
%Exploratory analysis to create beta images with only values matching the
%axiomatic criteria for each different prediction error model, including
%only grey matter and averaging over subjects

opts = get_options();

addpath(opts.spmpath) % Add SPM path

direction = opts.direction_2ndlvl_ttest; % Direction of contrast
fpath   = opts.fpath;
subj    = opts.subj;
subj    = subj(sublist);

for p = phase % Experimental phase

    % 1st (subject) level data path
    firstpath = ['1stlevel\Version_' num2str(version) '\NoiseCorr_' opts.noisecorr_name '\Model_Cat_Axiomatic\First_Level_' opts.phasenames{p}];
    
    % Loop over subjects
    for subInd = 1:length(subj)

        subpath   = fullfile(fpath,subj{subInd});
        sespath   = fullfile(subpath,firstpath);
        
        cd(sespath)
        betaList      = dir(fullfile(sespath,'beta*'));
        betaList      = {betaList(:).name}';
        
        % 17 beta files
        % 1-4:    CS type
        % 5-10:   US/outcome type (predictive probability & shock outcome)
        %         5: US1, 6: US2, 7: US3, 8: US4, 9: US5, 10: US6
        % 11-16:  motion regressors
        % 17:     constant

        % Choose only US files
        % Maintenance all 4 runs
        if p == 2
            run1 = 5:10;
            run2 = run1+24;
            run3 = run2+24;
            run4 = run3+24;
            betaList = betaList([run1 run2 run3 run4]); % 4x6 = 24 beta files
        else
            betaList = betaList(5:10); % 6 beta files for Acq and Acq2
        end
        
        %--------------------------------------------------------------------------
        % Image Calculator
        %--------------------------------------------------------------------------
        PEmodels = {'Fullmodel' 'PositivePE' 'NegativePE' 'UnsignedPE'}; % Prediction error models to include
        
        % i1: US1, i2: US2, i3: US3, i4: US4, i5: US5, i6: US6
        expression1 = '(i1>i3>i5)&(i2>i4>i6)&(i4>i5)&(i2>i3)'; % Full axiomatic model direction 1
        expression2 = '(i2>i4>i6)'; % Positive PE direction 1
        expression3 = '(i1>i3>i5)'; % Negative PE direction 1
        expression4 = '(i2>i4>i6)&(i5>i3>i1)'; % Unsigned PE direction 1
        
        % For maintenance, we need average over scanning runs
        if p == 2
            avg_i1 = '((i1+i7+i13+i19)/4)'; % US1 / CS-(0)US-
            avg_i2 = '((i2+i8+i14+i20)/4)'; % US2 / CS+(1/3)US+
            avg_i3 = '((i3+i9+i15+i21)/4)'; % US3 / CS+(1/3)US-
            avg_i4 = '((i4+i10+i16+i22)/4)'; % US4 / CS+(2/3)US+
            avg_i5 = '((i5+i11+i17+i23)/4)'; % US5 / CS+(2/3)US-
            avg_i6 = '((i6+i12+i18+i24)/4)'; % US6 / CS+(1)US+
            expression1 = ['(' avg_i1 '>' avg_i3 ')&(' avg_i3 '>' avg_i5 ')&'... % Full axiomatic model direction 1
                '(' avg_i2 '>' avg_i4 ')&(' avg_i4 '>' avg_i6 ')&'...
                '(' avg_i4 '>' avg_i5 ')&(' avg_i2 '>' avg_i3 ')'];
            
            expression2 = ['(' avg_i2 '>' avg_i4 ')&(' avg_i4 '>' avg_i6 ')']; % Positive PE direction 1
            expression3 = ['(' avg_i1 '>' avg_i3 ')&(' avg_i3 '>' avg_i5 ')']; % Negative PE direction 1
            expression4 = ['(' avg_i5 '>' avg_i3 ')&(' avg_i3 '>' avg_i1 ')&'... % Unsigned PE direction 1
                '(' avg_i2 '>' avg_i4 '>' avg_i6 ')&(' avg_i4 '>' avg_i6 ')'];
            
            %expression = ['(' avg_i1 '>' avg_i3 '>' avg_i5 ')&(' avg_i2 '>' avg_i4 '>' avg_i6 ')&(' avg_i4 '>' avg_i5 ')&(' avg_i2 '>' avg_i3 ')'];
        end

        if direction == -1 % If contrast in the opposite direction
            strrep(expression1,'>','<') % Replace 'larger than' with 'smaller than'
            strrep(expression2,'>','<')
            strrep(expression3,'>','<')
            strrep(expression4,'>','<')
        end
        
        expressions = {expression1 expression2 expression3 expression4};
        
        imcalc.input = cellstr(betaList);
        imcalc.output = ['VoxelsAxiom_S' num2str(subInd) '_dir' num2str(direction)]; % Output file name
        imcalc.var = struct('name', {}, 'value', {});
        imcalc.options.dmtx = 0;
        imcalc.options.mask = 0;
        imcalc.options.interp = 1;
        imcalc.options.dtype = 4;
        
        outdir = fullfile(fpath,'ImCalc_Voxels_Axiomatic',['Version_' num2str(version)], opts.phasenames{p}); % Output directory
            
        % Run imcalc for all PE models
        for m = 1:length(PEmodels)
            
            outdir_m = fullfile(outdir,PEmodels{m}); % Output directory
            
            if ~exist(outdir_m,'dir') % Create directory if does not exist
                mkdir(outdir_m)
            end
            
            imcalc.outdir = {outdir_m};
            imcalc.expression = expressions{m};
            
            matlabbatch{1}.spm.util.imcalc = imcalc;
            
            % RUN matlabbatch
            spm_jobman('run',matlabbatch)
            clear matlabbatch
            
        end
        
        %--------------------------------------------------------------------------
        % Image Calculator
        %--------------------------------------------------------------------------
        % File selection
        % c1 (GM = Grey Matter) segmentation
        c1_img = cellstr(spm_select('ExtFPList',fullfile(fpath,subj{subInd},'T1'),'c1.*.nii$',1));
        
        % Create expression for GM mask
        expression = 'i1>0.5'; % Sensible threshold here 0.5

        imcalc.input = c1_img;
        imcalc.output = ['explicitmask_GM_S' int2str(subInd)];
        imcalc.outdir = {outdir};
        imcalc.expression = expression;
        imcalc.var = struct('name', {}, 'value', {});
        imcalc.options.dmtx = 0;
        imcalc.options.mask = 0;
        imcalc.options.interp = 1;
        imcalc.options.dtype = 4;

        matlabbatch{1}.spm.util.imcalc = imcalc;
        
        spm_jobman('run', matlabbatch)
        clear matlabbatch imcalc
            
        % Reslice into beta image space
        estwrite.ref = cellstr(betaList{1});
        estwrite.source = cellstr(fullfile(outdir,['explicitmask_GM_S' int2str(subInd) '.nii']));
        estwrite.other = {''};
        estwrite.eoptions.cost_fun = 'nmi';
        estwrite.eoptions.sep = [4 2];
        estwrite.eoptions.tol = [0.02 0.02 0.02 0.001 0.001 0.001 0.01 0.01 0.01 0.001 0.001 0.001];
        estwrite.eoptions.fwhm = [7 7];
        estwrite.roptions.interp = 7;
        estwrite.roptions.wrap = [0 0 0];
        estwrite.roptions.mask = 0;
        estwrite.roptions.prefix = 'r_';

        matlabbatch{1}.spm.spatial.coreg.estwrite = estwrite;
        
        spm_jobman('run', matlabbatch)
        clear matlabbatch estwrite
                        
        % Grey matter image
        GMimage = cellstr(spm_select('ExtFPList',fullfile(outdir),['r_explicitmask_GM_S' num2str(subInd) '.nii$'],1));
        
        expression = 'i1.*i2'; % Intersection of grey matter and axiomatic voxels
        imcalc.expression = expression;
        imcalc.output = ['VoxelsAxiom_GM_S' num2str(subInd) '_dir' num2str(direction)];
        imcalc.var = struct('name', {}, 'value', {});
        imcalc.options.dmtx = 0;
        imcalc.options.mask = 0;
        imcalc.options.interp = 1;
        imcalc.options.dtype = 4;
        
        for m = 1:length(PEmodels)
            % Axioms fulfilled subject image
            axiomimage = cellstr(spm_select('ExtFPList',fullfile(outdir,PEmodels{m}),['VoxelsAxiom_S' num2str(subInd) '_dir' num2str(direction) '.nii$'],1));
            imcalc.input = cellstr([axiomimage; GMimage]);
            outdir_m = fullfile(outdir,PEmodels{m});
            imcalc.outdir = {outdir_m};
            
            matlabbatch{1}.spm.util.imcalc = imcalc;
            
            % RUN matlabbatch
            spm_jobman('run',matlabbatch)
            clear matlabbatch
        end
        
    end

    clear imcalc
    expression = '(i1+i2+i3+i4+i5+i6+i7+i8+i9+i10+i11+i12+i13+i14+i15+i16+i17+i18+i19+i20+i21)/21';
    imcalc.expression = expression;
    imcalc.var = struct('name', {}, 'value', {});
    imcalc.options.dmtx = 0;
    imcalc.options.mask = 0;
    imcalc.options.interp = 1;
    imcalc.options.dtype = 4;
    imcalc.output = ['VoxelsAxiom_GM_MEAN_dir' num2str(direction)]; % Only with grey matter included
    
    for m = 1:length(PEmodels)
        
        outdir_m = fullfile(outdir,PEmodels{m});
        % Average over subjects
        ResultPath   = fullfile(outdir_m);
        cd(ResultPath)
        niiList      = dir(fullfile(ResultPath,['VoxelsAxiom_GM_S*_dir' num2str(direction) '.nii'])); % Only with grey matter included
        niiList      = {niiList(:).name}';
        
        
        %--------------------------------------------------------------------------
        % Image Calculator
        %--------------------------------------------------------------------------
        imcalc.input = cellstr(niiList);
        imcalc.outdir = {outdir_m};
        
        matlabbatch{1}.spm.util.imcalc = imcalc;
        
        % RUN matlabbatch
        spm_jobman('run',matlabbatch)
        clear matlabbatch
    end
    
end

end