function plot_roidata_violin(version,rois,models,con2plot)
%Violin plot of ROI data

opts = get_options();

roipath = opts.roipath;
meanbetapath = fullfile(roipath,['Version_' num2str(version)],'MeanBetas');
plotpath = fullfile(roipath,['Version_' num2str(version)],'Plots');

for model = models
    
    % ROI beta value files
    ROIfiles = cellstr(ls(fullfile(meanbetapath,['MeanBetas*param_Model' num2str(model) '.mat'])));

    for roitype = rois
        
        roiname = opts.roinames{roitype};
        roi_ind = strfind(ROIfiles, ['_' roiname '_']);
        roi_ind2 = not(cellfun('isempty', roi_ind)); %#ok<STRCL1>
        
        mbfile = load(fullfile(meanbetapath,ROIfiles{roi_ind2})); % Mean betas
        
        meanbetas = mbfile.meanbetas;
        roinames = mbfile.roinames;
        
        roi_no = size(meanbetas,3);
        
        %roifig = figure('Position', [80, 80, 1600, 950]);
        
        for roi = 1:roi_no
            
            roibetas(:,roi) = meanbetas(:,con2plot,roi); %#ok<*AGROW>
            
            % Define title for the figure: ROI name
            roititle = strtrim(roinames(roi,:));
            if roitype == 1 || roitype == 4;
                roititle = roititle(3:end-4);
                roititle = strrep(roititle,'bin',''); roititle = strrep(roititle,'highres',''); roititle = strrep(roititle,'2ndlvlmasked','');
                roititle = strrep(roititle,'withoutVS',''); roititle = strrep(roititle,'withoutDS','');
            elseif roitype == 2; roititle = [roititle(3:6) ' ' roititle(17)];
            else roititle = roititle(1:end-4);
            end
            roititles{roi} = strrep(roititle,'_',' ');
            
        end
        
        if roitype == 1
            roititles([4:7 12:15 18:19 21]) = [];
            roibetas(:,[4:7 12:15 18:19 21]) = [];
            roititles{3} = roititles{3}(1:3);
            roititles{4} = 'Amyg L';
            roititles{5} = 'Amyg R';
            roititles{6} = 'AInsula L';
            roititles{7} = 'AInsula R';
            roititles{8} = 'PInsula L';
            roititles{9} = 'PInsula R';
            roititles{10} = 'SN/VTA';
            roititles{11}(1) = 'T';
            roititles{12}(1) = 'T';
            roititles{13} = 'VStr L';
            roititles{14} = 'VStr R';
        elseif roitype == 2
            
        end
        % Draw violin plot with one plot for each ROI
        %violin(roibetas(con2plot),'x',[-1 .7 3.4 8.8],'facecolor',[1 1 0;0 1 0;.3 .3 .3;0 0.3 0.1],'edgecolor','none',...
        %    'bw',0.3,'mc','k','medc','r-.')
        %colors = [1 1 0;0 1 0;.3 .3 .3];
        roifig = figure();
        line([0 length(roititles)+1],[0 0],'Color',[0.7 0.7 0.7],'LineWidth',2)
        hold on
        violin(roibetas,'facecolor',0,'edgecolor','none',...
            'mc','k','medc','r-.');
        set(gca,'XTick',1:length(roititles))
        set(gca,'XTickLabel',roititles)
        ylabel('Beta coefficient')
        ylim([-500 500])
        set(gca,'YTick',-500:250:500)
%         subplot(1,3,1)
%         line([0 11],[0 0],'Color',[0.7 0.7 0.7],'LineWidth',2)
%         hold on
%         violin(roibetas(:,1:5),'facecolor',0,'edgecolor','none',...
%             'mc','k','medc','r-.','xlabel',roititles(1:5));
%         set(gca,'XTick',1:5)
%         set(gca,'XTickLabel',roititles(1:5))
%         ylabel('Beta coefficient')
%         ylim([-500 500])
%         set(gca,'YTick',-500:250:500)
%         
%         subplot(1,3,2)
%         line([0 11],[0 0],'Color',[0.7 0.7 0.7],'LineWidth',2)
%         hold on
%         violin(roibetas(:,6:10),'facecolor',0,'edgecolor','none',...
%             'mc','k','medc','r-.','xlabel',roititles(6:10));
%         set(gca,'XTick',1:5)
%         set(gca,'XTickLabel',roititles(6:10))
%         ylabel('Beta coefficient')
%         ylim([-500 500])
%         set(gca,'YTick',-500:250:500)
%         
%         subplot(1,3,3)
%         line([0 6],[0 0],'Color',[0.7 0.7 0.7],'LineWidth',2)
%         hold on
%         violin(roibetas(:,11:end),'facecolor',0,'edgecolor','none',...
%             'mc','k','medc','r-.','xlabel',roititles(11:end));
%         set(gca,'XTick',1:5)
%         set(gca,'XTickLabel',roititles(11:end))
%         ylabel('Beta coefficient')
%         ylim([-500 500])
%         set(gca,'YTick',-500:250:500)
        
        savefig(fullfile(plotpath,['ROIplot_' roiname '_param_model' num2str(model) '_con' num2str(con2plot)]))
        saveas(roifig,fullfile(plotpath,['ROIplot_' roiname '_param_model' num2str(model) '_con' num2str(con2plot)]),'png')
        
    end
    
end

end