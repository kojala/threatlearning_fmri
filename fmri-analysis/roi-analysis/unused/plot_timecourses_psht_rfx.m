% Event-related peri-stimulus time histogram plotting
clear all

addpath('D:\spm12\toolbox\rfxplot')
% rfx plot input arguments:
% 1. manually created opts struct
% 2. the rfx SPM struct
% 3. the xSPM struct holding the information about the current contrast
% 4. the rfx coordinates (in world space (mm)) on which to build the rfx
% search volume

% 2nd level SPM folder
mainpath = fullfile(cd,'..','..','..');
fpath = fullfile(mainpath,'fMRIdata_biascorrected');
rfxdir = fullfile(fpath,'2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1\Maint\1sttest_parmod_US_PE');
% rfxdir = fullfile(fpath,'2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_2\Acq\1sttest_parmod_CS_exptpshock');
% rfxdir = fullfile(fpath,'2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_2\BothAcq\1sttest_parmod_CS_volatility');
% rfxdir = fullfile(fpath,'2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1b\Acq2\1sttest_parmod_US_BayesPE');

SPMfile = fullfile(rfxdir,'SPM.mat');
load(SPMfile)

% create xSPM structure with results batch to give to rfxplot
contrast = 1; % only 1 contrast in 2nd level t-test SPM
matlabbatch{1}.spm.stats.results.spmmat = {SPMfile};
matlabbatch{1}.spm.stats.results.conspec.titlestr = 'Prediction error';%'Volatility'; %Expected p(shock)
matlabbatch{1}.spm.stats.results.conspec.contrasts = contrast;
matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'none';
matlabbatch{1}.spm.stats.results.conspec.thresh = 0.001;
matlabbatch{1}.spm.stats.results.conspec.extent = 0; % voxels extent for cluster correction
matlabbatch{1}.spm.stats.results.conspec.conjunction = 1;
matlabbatch{1}.spm.stats.results.conspec.mask.none = 1;
matlabbatch{1}.spm.stats.results.units = 1;
matlabbatch{1}.spm.stats.results.print = false;
matlabbatch{1}.spm.stats.results.write.none = 1;
spm('defaults', 'FMRI');
spm_jobman('initcfg');
spm_get_defaults('cmdline',true)
spm_jobman('run',matlabbatch);
%xSPM = results_nogui('val',rfxdir,contrast);

rfxxyz = [-6 -22 -14]; % coordinate of your cluster of interest
%[8 -18 6]; Volatility Both Acq
%[28 -10 -16]; Expected p(shock) Acq
% mask = fullfile(fpath,'Masks\ReslicedMasks\CurrentMasks\Anatomical\r_amygdala_bin_R.nii');
% mask = fullfile(fpath,'Masks\ReslicedMasks\CurrentMasks\ModelChecks\Acq_exptpshock_amygdalaR_p001uncorr.nii');
% mask = fullfile(fpath,'Masks\ReslicedMasks\CurrentMasks\ModelChecks\BothAcq_volatility_allclusters_p001cluscorr.nii');
mask = fullfile(fpath,'Masks\ReslicedMasks\CurrentMasks\ModelChecks\Acq2_modelPE_SNVTA_p001uncorr.nii');

%% define opts structure
opts = rfx_setup_opts; % default opts
opts.rfxdir = rfxdir;
opts.xyz = rfxxyz; % coordinates of the RFX voxel on which to build the search volume
opts.task = 'psth';
opts.prefix = 'beta'; % prefix for 1st level analyses from which to extract and plot the data
opts.space = 'image'; % search volume
%opts.dim = ''; % dimension of search volume
opts.mask = {mask}; % names of the anatomical mask images
opts.ngroup = 1;
opts.group = {1:21}; % subjects
%opts.limit = 0; % limit voxel selection to supra-threshold voxels: 1 = yes, 0 = no
%opts.limtype = 'allrfx'; % all voxels, maskrfx = anatomical mask
%otps.ffxsphere = ''; % radius of the individual sphere around the peak voxel in the search volume
opts.select = 5;%[3 37]; % indices for the 1st level images from which to extract the data, leave empty for manual spm window check
opts.regtype = {'pmod'};%{'pmod' 'pmod'}; % type of regressor, reg = onset regressor, pmod = parametric modulation
opts.repl = 1;%[1 1]; % vector indicating which images should be averaged (those with the same number in opts.repl)
opts.reglabel = {'Prediction error'};%{'Volatility'}; % short description, label in the plot
opts.nbin = {1};%{1 1}; % number of bins into which split a regressor
opts.binthresh = {sprintfc('%d',1:44)};% {sprintfc('%d',1:24)}};%{1:24}; % threshold for bins, threshold of each image are themselves cell arrays with 2-element vector (upper, lower bound) or a string specifying a trial range
opts.bintype = {'val'};% 'val'}; % 'val'
opts.binnames = {{'Bin1'} {''}};% {'Bin2'}}; %{{''} {''}}; % for each regressor a cell array of nbin names, used as label in the plot
opts.winbound = [0 15]; % boundaries of time window for plotting PSHTs
opts.winlength = 15; % length of PSHT window
opts.binwidth = 3.2; % width of each PSHT bin (default 1 TR)
opts.rescale = 1; % mean-correct PSHT at stimulus onset
%opts.scale = 'es'; % scale of the plotted data, pcnt = % signal change, es = effect size
opts.where = 'figure'; % where to plot the data, spm or axis command
%opts.plottype = 'dotline'; % plot type for plotting effects
%opts.overlay = 1; % overlay individual data
opts.error = 'sem'; % type of error bar, sem = standard error of the mean
%opts.errcol = 's'; % color of the error bar, s = same, k = black
opts.errtype = 'area'; % type of error, bar = error bar, area = error area
opts.coltype = 'bin'; % how to specify the colors for the plot
opts.color = {'black' 'blue' 'green' 'yellow' 'red'};
opts.colgrad = 1; % color gradient for different bins
%opts.xtick = []; %  XTick positions for the different effects
opts.name = 'phstplot'; % name for the plot config to save

%% rfx plot

rfxplot(rfxxyz,opts,SPM,xSPM)
hold on

%% get hrf
RT = 3.2; % TR
p = []; % defaults
T = 40; % microtime resolution
[hrf,p] = spm_hrf(RT,p,T);
%x = 1:11;
xq = 1:(1/3):11;
hrf2 = interp1(hrf,xq,'spline');
plot(hrf2(1:16),'--r','LineWidth',2)
legend({'Prediction error' 'HRF'})