function calculate_betaLinreg(version,rois,roilist)
%Linear regression for US expectation against objective US rates/conditions
%for axiom 2 (alternative to paired t-tests)

opts = get_options();

subs = opts.roi_subj; % Subject list
roipath = opts.roipath; % ROI analysis path
meanbetapath = fullfile(roipath,['Version_' num2str(version)],'MeanBetas'); % Mean beta path

% ROI beta value files
ROIfiles = cellstr(ls(fullfile(meanbetapath,'MeanBetas*allconds.mat')));

for roitype = rois % Loop over ROI type
    
    roiname = opts.roinames{roitype}; % ROI type name
    roi_ind = strfind(ROIfiles, ['_' roiname '_']);
    roi_ind2 = not(cellfun('isempty', roi_ind)); %#ok<STRCL1>
    
    mbfile = load(fullfile(meanbetapath,ROIfiles{roi_ind2})); % Mean betas
    
    for roi = 1:length(roilist) % ROI list within ROI type
        
        meanbetas = mbfile.meanbetas(subs,:,roilist); % Matrix format [subject,condition,ROI]
        meanbetas_oversubs = squeeze(meanbetas(:,:,roi)); % Reformat matrix with current ROI data only
        
        expectation = [0 1/3 2/3; 1/3 2/3 1]; % US expectation for each condition based on objective US rate
        UScond = [1 3 5; 2 4 6]; % Condition number for US- (first 3) and US+ (last 3)
        
        for US = 1:2 % Loop for US- and US+
            
            expectation_rep = repmat(expectation(US,:),[size(meanbetas_oversubs,1) 1]); % Repeat US expectation levels
            
            meanbetas_US = meanbetas_oversubs(:,UScond(US,:),:); % Get mean beta values for the current US type conditions
            betas_reg = meanbetas_US(:); % Format beta values for regression
            expectation_reg = expectation_rep(:); % Format US expectation levels for regression
            % Linear regression for ROI betas
            % betas ~ expectation (axiom 2)
            lm = fitlm(expectation_reg,betas_reg);
            
            % Save results for ROI
            axiom2.intercept(roi,US) = lm.Coefficients{1,1}; % Intercept
            axiom2.slope(roi,US) = lm.Coefficients{2,1}; % Slope
            axiom2.tvalue(roi,US) = lm.Coefficients{2,3}; % T-value
            axiom2.pvalue(roi,US) = lm.Coefficients{2,4}; % p-value
            axiom2.Rsq(roi,US) = lm.Rsquared.Ordinary; % R squared

        end
        
        % Paired t-test between US+ and US- (axiom 1)
        % CS(33%)
        USp_CS2 = meanbetas_oversubs(:,2,:); % CS(33%)US+
        USm_CS2 = meanbetas_oversubs(:,3,:); % CS(33%)US-
        [~,p,~,stats] = ttest(USp_CS2,USm_CS2,'Tail','right'); % One-sided paired t-test
        axiom1.p(roi,1) = p; % p-value
        axiom1.t(roi,1) = stats.tstat; % T-value
        % CS(66%)
        USp_CS3 = meanbetas_oversubs(:,4,:); % CS(66%)US+
        USm_CS3 = meanbetas_oversubs(:,5,:); % CS(66%)US-
        [~,p,~,stats] = ttest(USp_CS3,USm_CS3,'Tail','right'); % One-sided paired t-test
        axiom1.p(roi,2) = p; % p-value
        axiom1.t(roi,2) = stats.tstat; % T-value
        
        roinames(roi,:) = mbfile.roinames(roi,3:end); %#ok<AGROW> % Save ROI name
        
    end
    
    % Save results
    if opts.roi_subj == 21 % If all subjects included
        save(fullfile(meanbetapath,['MeanBetas_ExptLinReg_' opts.roinames{roitype} '.mat']),'axiom1','axiom2','roinames');
    else % If only subjects with monotonical CS-US contingency learning included
        save(fullfile(meanbetapath,['MeanBetas_ExptLinReg_' opts.roinames{roitype} '_monotsubs.mat']),'axiom1','axiom2','roinames');
    end
    
end

end