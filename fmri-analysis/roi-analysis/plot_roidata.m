function plot_roidata(version,rois,roilist)
%Plot of averaged beta values extracted from each ROI, one value for each
%condition, including error bars (Figure 5 in the article)

opts = get_options();

subs = opts.roi_subj; % Subjects for the ROI analysis
linreg = opts.roiplot_linreg; % Plotting option: which statistical results shown on the plot

roipath = opts.roipath; % ROI analysis path
meanbetapath = fullfile(roipath,['Version_' num2str(version)],'MeanBetas'); % ROI averaged beta values data
plotpath = fullfile(roipath,['Version_' num2str(version)],'Plots'); % Plot path

% ROI beta value files
ROIfiles = cellstr(ls(fullfile(meanbetapath,'MeanBetas*allconds.mat')));

% ROI error bar files
EBfiles = cellstr(ls(fullfile(meanbetapath,'MeanBetas*ERRORBARS.mat')));

probs = [0 1/3 1/3 2/3 2/3 1]; % Objective US rates for each condition
offset = 0.025; % Offset for the markers to avoid overlap
probs_offset = [0 1/3+offset 1/3+offset 2/3+offset 2/3+offset 1]; % Setting the markers on correct US rates on the x-axis with offsets

for roitype = rois % Loop over type of ROI
    
    roiname = opts.roinames{roitype}; % ROI name
    roi_ind = strfind(ROIfiles, ['_' roiname '_']); % ROI index
    roi_ind2 = not(cellfun('isempty', roi_ind)); %#ok<STRCL1>
    
    mbfile = load(fullfile(meanbetapath,ROIfiles{roi_ind2})); % Mean beta file
    roi_ind = strfind(EBfiles, ['_' roiname '_']);
    roi_ind2 = not(cellfun('isempty', roi_ind)); %#ok<STRCL1>
    ebfile = load(fullfile(meanbetapath,EBfiles{roi_ind2})); % Error bar file
    
    meanbetas = mbfile.meanbetas(:,:,roilist); % Extract mean betas for the current ROIs, matrix in format [subject,condition,ROI]
    roinames = mbfile.roinames(roilist,:); % Extract ROI names for the current ROIs
    errorbars = ebfile.errorbars(:,roilist); % Extract error bars for the current ROIs
    
    % Which statistical results to plot
    if linreg == 1 % US expectation linear regression slope separately for US+ and US-
        if length(opts.roi_subj) == 21 % If all subjects included in the analysis
            LinRegfile = fullfile(meanbetapath,['MeanBetas_ExptLinReg_' opts.roinames{roitype} '.mat']);
        else % If only subjects with monotonic explicit CS-US contingency learning included
            LinRegfile = fullfile(meanbetapath,['MeanBetas_ExptLinReg_' opts.roinames{roitype} '_monotsubs.mat']);
        end
        load(LinRegfile) %#ok<LOAD> Load file
        intercept = axiom2.intercept(roilist,:); % Take intercept values
        slope = axiom2.slope(roilist,:); % Slope values
        pvalue_ax2 = axiom2.pvalue(roilist,:); % Axiom 2 p-values
        pvalue_ax1 = axiom1.p(roilist,:); % Axiom 1 p-values
        
    elseif linreg == 2 % All axiomatic t-tests
        if length(opts.roi_subj) == 21
            LinRegfile = fullfile(meanbetapath,['MeanBetas_axiomatic_posthoc_ttest_' opts.roinames{roitype} '.mat']);
        else
            LinRegfile = fullfile(meanbetapath,['MeanBetas_axiomatic_posthoc_ttest_' opts.roinames{roitype} '_monotsubs.mat']);
        end
        load(LinRegfile) %#ok<LOAD>
        pvalue_ax1 = roistats_p(roilist,1:2); % Axiom 1 p-values (tests 1-2)
        pvalue_ax2 = roistats_p(roilist,3:6); % Axiom 2 p-values (tests 3-6)
        
        % Retrieve Holm-Bonferroni corrected p-values
        [c_pvalue1, ~, h1] = fwer_holmbonf(pvalue_ax1,0.05); % Calculate with alpha level 0.05 for axiom 1
        c_pvalue1 = reshape(c_pvalue1,17,2); %#ok<NASGU> % Corrected p-values
        h1 = reshape(h1,17,2); %#ok<NASGU> % Alternative hypothesis supported (1) or not (0)
        [c_pvalue2, ~, h2] = fwer_holmbonf(pvalue_ax2,0.05); % Axiom 2
        c_pvalue2 = reshape(c_pvalue2,17,4);
        h2 = reshape(h2,17,4); %#ok<NASGU>
    end
    
    roi_no = size(meanbetas,3); % Number of ROIs
    
    % Figure dimensions for printing
    if roitype == 3 % Functional ROIs
        figwidth = 15; % 1 pdf column = 8.5 cm, 1.5 pdf column = 11.6, 2 pdf columns = 17.6
        figheight = 25;
    elseif roitype == 5 % Combined hemispheres anatomical ROIs
        figwidth = 17.6; 
        figheight = 17.6;
    end
    
    if roitype == 5; label_roi = 17; else; label_roi = 1; end % Which ROI subplot has the labels
    
    % Create figure
    roifig = figure('DefaultAxesFontSize', 12, 'DefaultAxesFontName', 'Arial', 'DefaultTextFontName', 'Arial');
    set(roifig, 'Units', 'Centimeters',...
        'PaperUnits', 'Centimeters', ...
        'PaperPositionMode','auto')
    set(roifig,'Position', [0, 0, figwidth, figheight])
    
    % Figure axes columns and rows
    if roitype == 3 % Functional ROIs
        axcolumns = 2; % Number of columns
        axrows = ceil(roi_no/axcolumns); % Number of rows (depending on total number of ROIs and number of columns)
        pos = axpos(axrows, axcolumns, 0.1, 0.03, 0.1, 0.1, 0.1); %rownum,colnum,leftmarg,rightmarg,upmarg,downmarg,inmarg
    elseif roitype == 5 % Combined hemispheres anatomical ROIs
        axcolumns = 4; 
        axrows = ceil(roi_no/axcolumns);
        pos = axpos(axrows, axcolumns, 0.07, 0.01, 0.04, 0.08, 0.05); %rownum,colnum,leftmarg,rightmarg,upmarg,downmarg,inmarg
    end

    roi_ind = 1; % Counter for ROI index
    
    % ROI plotting order
    if roitype == 3; rois_to_plot = 1:roi_no; elseif roitype == 5; rois_to_plot = [2:9 1 11 12 14 13 17 10 15 16]; end
    
    for roi = rois_to_plot
        
        % Retrieve beta coefficients and error bars for the ROI for each of
        % the 6 conditions
        roibetas = nan(1,6);
        for cond = 1:6
            roibetas(cond) = mean(meanbetas(subs,cond,roi));
        end
        roierrors = errorbars(:,roi);
        
        % Retrieve data for current ROI if slope line or t-tests plotted
        if linreg == 1
            clear roi_intercept roi_slope roi_pvalue roi_pvalue2
            roi_intercept = intercept(roi,:);
            roi_slope = slope(roi,:);
            roi_pvalue2 = pvalue_ax2(roi,:);
            roi_pvalue1 = pvalue_ax1(roi,:);
        elseif linreg == 2
            clear roi_pvalue roi_pvalue2
            roi_pvalue2 = pvalue_ax2(roi,:);
            roi_pvalue1 = pvalue_ax1(roi,:);
        end
        
        % Set axes for the ROI subplot
        ax = axes('Position', pos(roi_ind,:));
        set(gca,'LineWidth',1) % Set overall line width to 1
        
        % Zero line on y-axis
        xmin = -0.1; % Markers between 0 and 1, leave a little bit of space outside to accommodate markers
        xmax = 1.1;
        line([xmin xmax],[0 0],'Color',[128 128 128]./255,'HandleVisibility','off','LineWidth',1.5)
        hold on
        
        if roitype == 3; ballsize = 9; % Size of markers for beta coefficients for each condition
        elseif roitype == 5; ballsize = 7; end
        
        % Draw plot contents
        USp_color = [220 50 32]./255; % Color for US+ markers (red)
        USm_color = [0 90 181]./255; % Color for US- markers (blue)
        USp_vs_USm_color = [245 145 5]./255; % Color for US+ > US- comparison
        %nonsigcolor = [128 128 128]./255; % Color for non-significant test
        
        % Significance marker and line positions
        x_pos_1 = [0.1 0.3]-0.03; % Comparison line x position CS(0%)-CS(33%)
        x_pos_2 = [0.4 0.6]+0.02; % Comparison line x position CS(33%)-CS(66%)
        x_pos_3 = [0.7 0.9]+0.04; % Comparison line x position CS(66%)-CS(100%)
        y_pos1 = [1.1 1.1]; % Comparison line y position US+
        y_pos2 = [0.9 0.9]; % Comparison line y position US-
        
        % Plot US+ conditions
        if linreg == 1 % Linear regression slope line for axiom 2
            
            X = [1 1/3; 1 2/3; 1 1]; % Helper values for plotting linear regression line on y-axis
            b = [roi_intercept(2) roi_slope(2)]'; % Intercept and slope from linear regression
            x = [1/3 2/3 1]; % Objective US rate for plotting x-axis
            plot(x,X*b,'-','Color',USp_color,'LineWidth',1.5) % Plot line
            % Plot significance markers for the linear regression line
            
            if roi_pvalue2(2)*roi_no*2 < 0.05; text(2/3,0.7,'*','Color',USp_color,'FontSize',14,'FontWeight','bold'); % Star if Bonferroni-corrected p < 0.05
            elseif roi_pvalue2(2) < 0.05; text(2/3,0.7,'\circ','Color',USp_color,'FontSize',14,'FontWeight','bold'); end % Circle if uncorrected p < 0.05
            
        elseif linreg == 2 % Axiomatic t-tests for axiom 2
            
            % Significance markers and lines
            % CS(33%) > CS(66%)
            if c_pvalue2(roi,1) < 0.05 % If corrected p-value for axiom 2 test 1 is less than 0.05 (significant)
                line(x_pos_2,y_pos1,'Color',USp_color,'LineWidth',1.7); % Draw the comparison line
                text(x_pos_2(1)+0.06,y_pos1(1)+0.3,'*','Color',USp_color,'FontSize',14,'FontWeight','bold'); % Add a star/asterisk to denote significance
            elseif roi_pvalue2(1) < 0.05 % If the uncorrected p-value is less than 0.05 (without multiple comparisons correction)
                line(x_pos_2,y_pos1,'Color',USp_color,'LineWidth',1.7);
                text(x_pos_2(1)+0.06,y_pos1(1)+0.3,'\circ','Color',USp_color,'FontSize',14,'FontWeight','bold'); % Add a circle to denote trend
            end
            
            % CS(66%) > CS(100%)
            if c_pvalue2(roi,2) < 0.05 % If corrected p-value for axiom 2 test 2 is less than 0.05 (significant)
                line(x_pos_3,y_pos1,'Color',USp_color,'LineWidth',1.7);
                text(x_pos_3(1)+0.06,y_pos1(1)+0.3,'*','Color',USp_color,'FontSize',14,'FontWeight','bold'); % Asterisk
            elseif roi_pvalue2(2) < 0.05 % If the uncorrected p-value is less than 0.05 (without multiple comparisons correction)
                line(x_pos_3,y_pos1,'Color',USp_color,'LineWidth',1.7);
                text(x_pos_3(1)+0.06,y_pos1(1)+0.3,'\circ','Color',USp_color,'FontSize',14,'FontWeight','bold'); % Circle
            end
            
        end
        
        % Draw mean beta value markers and error bars for the different conditions
        if linreg < 0 % If old style plot without connecting lines between markers
            USp_plot = errorbar(probs([2 4 6]),roibetas([2 4 6]),roierrors([2 4 6]),... % US+
                'ok','MarkerFaceColor',USp_color,... % red color
                'MarkerSize',ballsize,'LineStyle','none','LineWidth',1.5);
        elseif linreg >= 0 % If new style plot with colored lines connecting markers
            USp_plot = errorbar(probs([2 4 6]),roibetas([2 4 6]),roierrors([2 4 6]),... % US+
                'Marker','o','Color',USp_color,'MarkerFaceColor',USp_color,... % red color
                'MarkerEdgeColor','k','MarkerSize',ballsize,'LineStyle','-','LineWidth',1.5);
        end
        
        % Plot US- conditions
        if linreg == 1 % Linear regression slope line for axiom 2
            
            X = [1 0; 1 1/3; 1 2/3]; % Helper values for plotting linear regression line on y-axis
            b = [roi_intercept(1) roi_slope(1)]'; % Intercept and slope from linear regression in R
            x = [0 1/3 2/3]; % Objective US rate for plotting x-axis
            plot(x,X*b,'-','Color',USm_color,'LineWidth',1.5) % Plot line
            % Plot significance markers for the linear regression line
            if roi_pvalue2(1)*roi_no*2 < 0.05; text(1/3,-0.7,'*','Color',USm_color,'FontSize',14,'FontWeight','bold'); % Star if Bonferroni-corrected p < 0.05
            elseif roi_pvalue2(1) < 0.05; text(1/3,-0.7,'\circ','Color',USm_color,'FontSize',14,'FontWeight','bold'); end % Circle if uncorrected p < 0.05
            
        elseif linreg == 2 % Axiomatic t-tests for axiom 2
            
            % Significance markers and lines
            % CS(0%) > CS(33%)
            if c_pvalue2(roi,3) < 0.05 % If corrected p-value for axiom 2 test 2 is less than 0.05 (significant)
                line(x_pos_1,-y_pos2,'Color',USm_color,'LineWidth',1.7); % Draw the comparison line
                text(x_pos_1(1)+0.06,-y_pos2(1)-0.45,'*','Color',USm_color,'FontSize',14,'FontWeight','bold'); % Add a star/asterisk to denote significance
            elseif roi_pvalue2(3) < 0.05 % If the uncorrected p-value is less than 0.05 (without multiple comparisons correction)
                line(x_pos_1,-y_pos2,'Color',USm_color,'LineWidth',1.7);
                text(x_pos_1(1)+0.06,-y_pos2(1)-0.45,'\circ','Color',USm_color,'FontSize',14,'FontWeight','bold'); % Add a circle to denote trend
            end
            % CS(33%) > CS(66%)
            if c_pvalue2(roi,4) < 0.05 % If corrected p-value for axiom 2 test 2 is less than 0.05 (significant)
                line(x_pos_2,-y_pos2,'Color',USm_color,'LineWidth',1.7);
                text(x_pos_2(1)+0.06,-y_pos2(1)-0.45,'*','Color',USm_color,'FontSize',14,'FontWeight','bold');
            elseif roi_pvalue2(4) < 0.05 % If the uncorrected p-value is less than 0.05 (without multiple comparisons correction)
                line(x_pos_2,-y_pos2,'Color',USm_color,'LineWidth',1.7);
                text(x_pos_2(1)+0.06,-y_pos2(1)-0.45,'\circ','Color',USm_color,'FontSize',14,'FontWeight','bold'); 
            end
            
        end
        
        % Draw mean beta value markers and error bars for the different conditions
        if linreg < 0 % If old style plot without connecting lines between markers
            USm_plot = errorbar(probs_offset([1 3 5]),roibetas([1 3 5]),roierrors([1 3 5]),... % US-
                'ok','MarkerFaceColor',USm_color,... % blue color
                'MarkerSize',ballsize,'LineStyle','none','LineWidth',1.5);
        elseif linreg >= 0 % If new style plot with colored lines connecting markers
            USm_plot = errorbar(probs_offset([1 3 5]),roibetas([1 3 5]),roierrors([1 3 5]),... % US-
                'Marker','o','Color',USm_color,'MarkerFaceColor',USm_color,... % blue color
                'MarkerEdgeColor','k','MarkerSize',ballsize,'LineStyle','-','LineWidth',1.5);
        end
        
        % If plotting statistical results, axiom 1: US+ vs. US- difference
        if linreg > 0
            x_pos_1 = [0.2 0.2]; % Comparison line x position CS(33%)
            x_pos_2 = [0.8 0.8]; % Comparison line x position CS(66%)
            y_1 = mean([roibetas(2) roibetas(3)]); % CS(33%)
            y_2 = mean([roibetas(4) roibetas(5)]); % CS(66%)
            y_pos_1 = [y_1-0.5 y_1+0.5]; % Comparison line y position CS(33%)
            y_pos_2 = [y_2-0.5 y_2+0.5]; % Comparison line y position CS(66%)
            
            if roi_pvalue1(1)*roi_no*2 < 0.05 % Bonferroni-corrected p < 0.05
                line(x_pos_1,y_pos_1,'Color',USp_vs_USm_color,'LineWidth',1.7); % Draw comparison line
                text(x_pos_1(1)-0.1,y_1-0.07,'*','Color',USp_vs_USm_color,'FontSize',14,'FontWeight','bold'); % Star
            elseif roi_pvalue1(1) < 0.05 % Uncorrected p < 0.05
                line(x_pos_1,y_pos_1,'Color',USp_vs_USm_color,'LineWidth',1.7);
                text(x_pos_1(1)-0.1,y_1-0.07,'\circ','Color',USp_vs_USm_color,'FontSize',14,'FontWeight','bold'); % Circle
            end
            if roi_pvalue1(2)*roi_no*2 < 0.05  % Bonferroni-corrected p < 0.05
                line(x_pos_2,y_pos_2,'Color',USp_vs_USm_color,'LineWidth',1.7);
                text(x_pos_2(1)+0.04,y_2-0.07,'*','Color',USp_vs_USm_color,'FontSize',14,'FontWeight','bold');
            elseif roi_pvalue1(2) < 0.05 % Uncorrected p < 0.05
                line(x_pos_2,y_pos_2,'Color',USp_vs_USm_color,'LineWidth',1.7);
                text(x_pos_2(1)+0.04,y_2-0.07,'\circ','Color',USp_vs_USm_color,'FontSize',14,'FontWeight','bold');
            end
        end
        
        % Set axis limits
        y_max = 2.5; % Suitable limit for the mean beta values across ROIs and conditions
        y_min = -2.5;
        xlim([xmin xmax])
        ylim([y_min y_max])
        
        % Define and set title for the figure: ROI name
        roititle = strtrim(roinames(roi,:));
        if roitype == 1 || roitype == 4 % If anatomical (separate hemispheres) or voxels fulfilling axioms ROIs
            roititle = roititle(3:end-4); % Remove '.nii' file type
            roititle = strrep(roititle,'bin',''); roititle = strrep(roititle,'highres',''); roititle = strrep(roititle,'2ndlvlmasked',''); % Remove other useless parts
            roititle = strrep(roititle,'withoutVS',''); roititle = strrep(roititle,'withoutDS','');
        elseif roitype == 2; roititle = [roititle(3:6) ' ' roititle(17)]; % If anatomical frontal Brodmann area ROIs, keep hemisphere label (L/R)
        elseif roitype == 5; roititle = opts.roilist_comb(roi); % Anatomical combined hemispheres ROIs
        elseif roitype == 3; roititle = roititle(1:end-17); % Functional significant clusters ROIs
        else; roititle = roititle(3:end-4); % Otherwise just removi '.nii'
        end
        roititle = strrep(roititle,'_',' '); % Replace underscores with spaces
        title(roititle,'FontSize',12,'FontWeight','normal')
        
        % Figure properties depending on the ROI type
        if roi_ind == label_roi; set(gca,'XTick',[0 1/3 2/3 1]); set(gca,'XTickLabel',{'0%','33%','66%','100%','FontSize',12}); % Add x-axis ticks labels only to the ROI in the bottom left corner
        else; set(gca,'XTick',[]); end
        if roi_ind == label_roi; ylabel(ax,'BOLD estimate','FontSize',12); end % Add y-axis label only to bottom left corner ROI
        if roi_ind == label_roi; xlabel(ax,'US+ expectation','FontSize',12); end % Add x-axis label only to bottom left corner ROI
        
        % Set US+/US- legend position
        if roitype == 5 % Anatomical combined hemispheres ROIs
            if roi_ind == label_roi % Next to the last ROI
                plot_legend = legend([USp_plot,USm_plot],{'US+','US-'},'FontSize',12);
                set(plot_legend,'Position',pos(roi_ind+1,:))
                legend boxoff
            end
        elseif roitype == 3 % Functional significant clusters ROIs
            set(gca,'XTick',[0 1/3 2/3 1]); set(gca,'XTickLabel',{'0%','33%','66%','100%','FontSize',12});
            ylabel(ax,'BOLD estimate','FontSize',12);
            xlabel(ax,'US+ expectation','FontSize',12);
            legend('US+','US-','FontSize',12,'Location','northeast');
        end
        set(gca, 'box', 'off') % Legend box off
        
        roi_ind = roi_ind + 1;
        
    end
    
    % Set paper size
    PAPER = get(roifig,'Position');
    set(roifig,'PaperSize',[PAPER(3), PAPER(4)]);
    
    % Save figure
    if length(opts.roi_subj) == 21 % All subjects
        savefig(fullfile(plotpath,['ROIplot_' roiname '_' num2str(roi_no) 'ROIs']))
        saveas(gcf,fullfile(plotpath,['ROIplot_' roiname '_' num2str(roi_no) 'ROIs']),'png')
        saveas(gcf,fullfile(plotpath,['ROIplot_' roiname '_' num2str(roi_no) 'ROIs']),'svg')
    else % Only subjects with monotonical CS-US contingency learning
        savefig(fullfile(plotpath,['ROIplot_' roiname '_' num2str(roi_no) 'ROIs_MonotSubs']))
        saveas(gcf,fullfile(plotpath,['ROIplot_' roiname '_' num2str(roi_no) 'ROIs_MonotSubs']),'png')
        saveas(gcf,fullfile(plotpath,['ROIplot_' roiname '_' num2str(roi_no) 'ROIs_MonotSubs']),'svg')
    end
    
end

end