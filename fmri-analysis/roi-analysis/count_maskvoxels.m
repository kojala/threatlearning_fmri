%Count the number of voxels within a mask
mainpath = fullfile(cd,'..','..','..');
dataPath = fullfile(mainpath,'fMRIdata_biascorrected\Masks\ReslicedMasks');

maskList = {'' 'BL' 'CC'};
hemis = {'L_099' 'R_099'};

for m = 1:length(maskList)
    
    for h = 1:2
    filename_mask = fullfile(dataPath,['r_amygdala' maskList{m} '_bin_' hemis{h} '.nii']);
    
    masknifti = nifti(filename_mask);
    %masknifti = spm_vol(filename_mask);
    
    %maskarray = spm_read_vols(masknifti);
    maskarray = masknifti.dat(:,:,:);
    
    voxels_mask(m,h) = length(find(maskarray(:,:,:)>0.99)); %#ok<SAGROW>
    
    end
    
end

voxelsize = 2;

volume = voxels_mask*(voxelsize^3);