function meanbetas_wideformat(version,rois,roilist)
%Save mean beta values extracted from ROIs in long format suitable for R,
%including columns ROI, Subject, Condition, Outcome, Expectation, Full
%signed PE, Positive PE, Negative PE, Unsigned PE

opts = get_options();

roipath = opts.roipath;
meanbetapath = fullfile(roipath,['Version_' num2str(version)],'MeanBetas');

% ROI beta value files
ROIfiles = cellstr(ls(fullfile(meanbetapath,'MeanBetas*allconds.mat')));

for roitype = rois % Loop over ROI types
    
    roiname = opts.roinames{roitype}; % ROI type name
    roi_ind = strfind(ROIfiles, ['_' roiname '_']);
    roi_ind2 = not(cellfun('isempty', roi_ind)); %#ok<STRCL1>
    
    mbfile = load(fullfile(meanbetapath,ROIfiles{roi_ind2})); % Mean betas
    
    % Create ROI, subject and condition columns
    roino = length(roilist); % Number of ROIs
    subno = length(opts.roi_subj); % Number of subjects
    condno = 6; % US/PE conditions
    
    rois = 1:roino; % ROI indices
    ROI = repmat(rois,[subno*condno 1]); % ROI column
    ROI = ROI(:);
    data.ROI = ROI; % Save in data structure

    subject = repmat(1:subno,[1 condno*roino])'; % Subject column
    data.subject = subject;
    cond_per_roi = [ones(subno,1)*1;ones(subno,1)*2;ones(subno,1)*3;ones(subno,1)*4;ones(subno,1)*5;ones(subno,1)*6];
    condition = repmat(cond_per_roi,[roino 1]); % Condition column
    data.condition = condition;
    
    % US outcome
    outcome = [0 1 0 1 0 1];
    outcome_per_roi = [];
    for outc = outcome
        outcome_per_roi = [outcome_per_roi; repmat(outc,[subno 1])]; %#ok<AGROW>
    end
    Outcome = repmat(outcome_per_roi,[roino 1]); 
    data.Outcome = Outcome;
    
    % US expectation
    expectation = [0 1/3 1/3 2/3 2/3 1];
    expt_per_roi = [];
    for expt = expectation
        expt_per_roi = [expt_per_roi; repmat(expt,[subno 1])]; %#ok<AGROW>
    end
    Expectation = repmat(expt_per_roi,[roino 1]); 
    data.Expectation = Expectation;
    
    % Full signed prediction error
    fullPE_init = outcome-expectation;
    fullPE_per_roi = [];
    for PE = fullPE_init
        fullPE_per_roi = [fullPE_per_roi; repmat(PE,[subno 1])]; %#ok<AGROW>
    end
    fullPE = repmat(fullPE_per_roi,[roino 1]); 
    data.fullPE = fullPE;
    
    % Positive prediction error
    posPE_init = nan(1,condno);
    posPE_init(outcome==1) = 1-expectation(outcome==1);
    posPE_init(outcome==0) = nanmean(posPE_init); 
    posPE_per_roi = [];
    for PE = posPE_init
        posPE_per_roi = [posPE_per_roi; repmat(PE,[subno 1])]; %#ok<AGROW>
    end
    posPE = repmat(posPE_per_roi,[roino 1]); 
    data.posPE = posPE;
    
    % Negative prediction error
    negPE_init = nan(1,condno);
    negPE_init(outcome==0) = 0-expectation(outcome==0);
    negPE_init(outcome==1) = nanmean(negPE_init); 
    negPE_per_roi = [];
    for PE = negPE_init
        negPE_per_roi = [negPE_per_roi; repmat(PE,[subno 1])]; %#ok<AGROW>
    end
    negPE = repmat(negPE_per_roi,[roino 1]); 
    data.negPE = negPE;

    % Unsigned/absolute prediction error
    absPE_init = abs(outcome-expectation); % absolute values
    absPE_per_roi = [];
    for PE = absPE_init
        absPE_per_roi = [absPE_per_roi; repmat(PE,[subno 1])]; %#ok<AGROW>
    end
    absPE = repmat(absPE_per_roi,[roino 1]); 
    data.absPE = absPE;
    
    % Create betas column
    meanbetas = mbfile.meanbetas(:,:,roilist); % Format [subject,condition,ROI]
    betas = meanbetas(:);
    data.betas = betas;


    if roitype == 5 % Combined hemispheres anatomical ROIs
        save(fullfile(meanbetapath,'MeanBetas_LongFormat_allPE_anatROIs.mat'),'data');
        csvwrite(fullfile(meanbetapath,'MeanBetas_LongFormat_allPE_anatROIs.csv'),data);
    elseif roitype == 3 % Functional significant clusters ROIs
        save(fullfile(meanbetapath,'MeanBetas_LongFormat_allPE_funcROIs.mat'),'data');
        csvwrite(fullfile(meanbetapath,'MeanBetas_LongFormat_allPE_funcROIs.csv'),data);
    end
    % csvwrite does not work in MATLAB2016a -> need to use MATLAB2019+ to transform files into tables and save into R readable format
     
end

end