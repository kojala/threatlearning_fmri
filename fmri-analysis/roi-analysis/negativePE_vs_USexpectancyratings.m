clear all

addpath(genpath(fullfile(cd,'..')))

opts = get_options();

% Behavioural data subject list
sList = opts.subj_beh;

%% Retrieve subject rating data
behavFile = fullfile(opts.bhvpath,'subjective_pshock_AcqMaint.mat');
load(behavFile)

% CSsubj_allcond(:,1) = CSsubj(:,1);
% CSsubj_allcond(:,2) = CSsubj(:,2);
% CSsubj_allcond(:,3) = CSsubj(:,2); % CS2 repeated
% CSsubj_allcond(:,4) = CSsubj(:,3);
% CSsubj_allcond(:,5) = CSsubj(:,3); % CS3 repeated
% CSsubj_allcond(:,6) = CSsubj(:,4);

% True CS contingency
CSobj = [0 1/3 2/3 1]*100;
% CSobj = [0 1/3 1/3 2/3 2/3 1];
% CSobj = repmat(CSobj,[21 1]);

% Find slope of each participant's contingency ratings
for sub = 1:size(CSsubj,1)
    b = polyfit(CSobj, CSsubj(sub,:), 1);
    contingency_slopes(sub,:) = b(1);
end

save('C:\Data\PCF_fMRI\Behavior\CSUS_contingency_slopes_allsubs_MaintAcq.mat','contingency_slopes')

%% Retrieve negative PE data
load('C:\Data\PCF_fMRI\fMRIdata_biascorrected\ROIanalysis\Version_11062018\MeanBetas\MeanBetas_Func_23ROIs_allconds.mat')

negPE1 = meanbetas(:,:,14);
negPE2 = meanbetas(:,:,15);
negPE3 = meanbetas(:,:,16);

corr1 = CSsubj_allcond(:,1)\negPE1(:,1);



