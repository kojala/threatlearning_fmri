function axiomtests_correctedp_HB(version,rois,roilist)
%Calculate Holm-Bonferroni-corrected p-values for axioms 1 and 2

opts = get_options();

roipath = opts.roipath;
meanbetapath = fullfile(roipath,['Version_' num2str(version)],'MeanBetas');

for roitype = rois
    
    if length(opts.roi_subj) == 21 % All subjects
        LinRegfile = fullfile(meanbetapath,['MeanBetas_axiomatic_posthoc_ttest_' opts.roinames{roitype} '.mat']);
        savedfile = fullfile(meanbetapath,['MeanBetas_axiomatic_posthoc_ttest_' opts.roinames{roitype} '_correctedp.mat']);
    else % Only subjects with monotoical CS-US contingency learning
        LinRegfile = fullfile(meanbetapath,['MeanBetas_axiomatic_posthoc_ttest_' opts.roinames{roitype} '_monotsubs.mat']);
        savedfile = fullfile(meanbetapath,['MeanBetas_axiomatic_posthoc_ttest_' opts.roinames{roitype} '_correctedp_monotsubs.mat']);
    end
    
    load(LinRegfile) %#ok<LOAD> Load linear regression results
    
    % Retrieve Holm-Bonferroni corrected p-values
    if roitype == 3 % Significant functional clusters ROIs
        
        roi_no = length(roilist);
        
        pvalue_ax1 = roistats_p(roilist,1:2); % P-values axiom 1, tests 1-2
        pvalue_ax2 = roistats_p(roilist,3:6); % P-values axiom 2, tests 3-6
        
        [c_pvalue1, c_alpha1, h1] = fwer_holmbonf(pvalue_ax1,0.05); % alpha level 0.05
        axiom1.c_pvalue = reshape(c_pvalue1,roi_no,2); % 2 tests
        axiom1.c_alpha = reshape(c_alpha1,roi_no,2);
        axiom1.h = reshape(h1,roi_no,2);
        [c_pvalue2, c_alpha2, h2] = fwer_holmbonf(pvalue_ax2,0.05);
        axiom2.c_pvalue = reshape(c_pvalue2,roi_no,4); % 4 tests
        axiom2.c_alpha = reshape(c_alpha2,roi_no,4);
        axiom2.h = reshape(h2,roi_no,4);
        
        save(savedfile,'axiom1','axiom2')
        
    elseif roitype == 5 % Combined hemispheres anatomical ROIs
        
        roi_no = length(roilist);
            
        pvalue_ax1 = roistats_p(roilist,1:2); % P-values axiom 1, tests 1-2
        pvalue_ax2 = roistats_p(roilist,3:6); % P-values axiom 2, tests 3-6
        
        [c_pvalue1, c_alpha1, h1] = fwer_holmbonf(pvalue_ax1,0.05); % alpha level 0.05
        axiom1.c_pvalue = reshape(c_pvalue1,roi_no,2); % 2 tests
        axiom1.c_alpha = reshape(c_alpha1,roi_no,2);
        axiom1.h = reshape(h1,roi_no,2);
        [c_pvalue2, c_alpha2, h2] = fwer_holmbonf(pvalue_ax2,0.05);
        axiom2.c_pvalue = reshape(c_pvalue2,roi_no,4); % 4 tests
        axiom2.c_alpha = reshape(c_alpha2,roi_no,4);
        axiom2.h = reshape(h2,roi_no,4);
        
        save(savedfile,'axiom1','axiom2')
    end
end

end