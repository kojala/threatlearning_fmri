function plot_roidata_BayesFactor(version,roitype)

opts = get_options();

modelnames = {'Outcome', 'Full PE sym.', 'Full PE asym.', 'Positive PE', 'Negative PE', 'Unsigned PE', 'Null'}; % Models in the comparison
no_models = length(modelnames); % Number of models
models2plot = 1:no_models-1; % Remove null model from plotting

roipath = opts.roipath;
meanbetapath = fullfile(roipath,['Version_' num2str(version)],'MeanBetas'); % Data path for averaged beta values for each ROI
plotpath = fullfile(roipath,['Version_' num2str(version)],'Plots'); % Plot path

% ROI beta value files and plotting options
if roitype == 3
    BF_file = fullfile(meanbetapath,'MeanBetas_LongFormat_allPE_funcROIs_BF_results.mat'); % Bayes Factors from the model comparison (analysis in R)
    % Functional ROI: significant clusters from the whole-brain analysis
%     roinames = {'Full PE cluster 1', 'Full PE cluster 2', 'Negative PE cluster 1', 'Negative PE cluster 2', 'Negative PE cluster 3', 'Unsigned PE cluster 1', 'Unsigned PE cluster 2'};
    roinames = {'Full PE cluster 1', 'Full PE cluster 2', 'Negative PE cluster 1', 'Negative PE cluster 2', 'Negative PE cluster 3', 'PE difference cluster 1', 'PE difference cluster 2'};
%     label_roi = 7; % Which ROI subplot has the labels (the one in the bottom left corner)
    label_roi = 5;
    axcolumns = 3; % Columns for ROI plot
%     rois_to_plot = 1:7; % ROI order in the plot
    rois_to_plot = [1 2 6 7 3:5]; % ROI order in the plot
    roi_pos = [1 2 4 5 7:9];
    % Figure dimensions for printing
    figwidth = 15; % 1 pdf column = 8.5 cm, 1.5 pdf column = 11.6, 2 pdf columns = 17.6
    figheight = 12.5;
    inmarg = 0.09;
    leftmarg = 0.16;
    rightmarg = 0.04;
    upmarg = 0.05;
    downmarg = 0.08;
    barwidth = 0.7; % Width of the bars in the bar plot
elseif roitype == 5
    BF_file = fullfile(meanbetapath,'MeanBetas_LongFormat_allPE_anatROIs_BF_results.mat');
    roinames = opts.roilist_comb;
    label_roi = 17;
    axcolumns = 4;
    rois_to_plot = [2:9 1 11 12 14 13 17 10 15 16];
    roi_pos = 1:numel(rois_to_plot);
    figwidth = 17.6; % 1 pdf column = 8.5 cm, 1.5 pdf column = 11.6, 2 pdf columns = 17.6
    figheight = 21;
    inmarg = 0.05;
    leftmarg = 0.14;
    rightmarg = 0.02;
    upmarg = 0.03;
    downmarg = 0.08;
    barwidth = 0.7;
end

% ROI/file definitions
roiname = opts.roinames{roitype}; % ROI type name
bf_data = load(BF_file); % Bayes Factor model comparison file
bf_data_bestmodel = bf_data.modelcomp_best; % Results for best model vs. null data
bf_data_all = bf_data.modelcomp_all; % Results for all models
roi_no = length(roinames); % Number of ROIs

% Create figure
roifig = figure('DefaultAxesFontSize', 12, 'DefaultAxesFontName', 'Arial', 'DefaultTextFontName', 'Arial');
set(roifig, 'Units', 'Centimeters',...
    'PaperUnits', 'Centimeters', ...
    'PaperPositionMode','auto')
set(roifig,'Position', [0, 0, figwidth, figheight])

% Figure axes columns and rows
axrows = ceil(roi_no/axcolumns); % Number of rows
if roitype == 3
    model_label_roi = [1 5 3];
else
    model_label_roi = 1:axcolumns:roi_no; % ROIs which get the model labels (ROIs in the first column only)
end
pos = axpos(axrows, axcolumns, leftmarg, rightmarg, upmarg, downmarg, inmarg); % Axes positions

roi_ind = 1; % Counter for ROI index
datarows = 1:(roi_no*no_models); % Rows of data to extract
datarows = reshape(datarows,[no_models roi_no])';
%modelnames = bf_data.ModelName(1:no_models); % Same model names for all ROIs

for roi = rois_to_plot % Loop plotting ROIs in the right order
    
    % Retrieve Bayes Factor (BF) data for all models
    roi_datarows = datarows(roi,:); % Rows of data to extract for the current ROI
    roi_BF = bf_data_all.ModelBF(roi_datarows); % BF for all models for the current ROI
    roi_BF = str2double(roi_BF); % Transform string format to double   
    roi_BF = log(roi_BF); % Transform BF to log BF
    roi_BF(end) = []; % Remove the null model
    % Best model results
    roi_BF_best = bf_data_bestmodel.ModelBF(roi); % Best model BF
    roi_BF_best = str2double(roi_BF_best); % String to double
    roi_BF_best = log(roi_BF_best); % BF to log BF
    %modelname_best = modelnames{bf_data_bestmodel.BestModelNo(roi)}; % Name of the best model
    
    % Set axes for the ROI subplot
    ax = axes('Position', pos(roi_pos(roi_ind),:));
    set(gca,'LineWidth',1)
    
    % Log BF 1 line -> Not used (denotes weak/insignificant evidence)
%     ymin = 1-0.5;
%     ymax = length(models2plot)+0.5;
%     xline = 1;
%     if opts.roiBFplot_direction > 0; line([xline xline],[ymin ymax],'Color',[128 128 128]./255,'HandleVisibility','off','LineWidth',1.5); end
%     hold on
%     line([-xline -xline],[ymin ymax],'Color',[128 128 128]./255,'HandleVisibility','off','LineWidth',1.5); 
    
    % Log BF 3 line
    ymin = 1-0.5; % Minimum for the line on the y-axis (0.5 for space)
    ymax = length(models2plot)+0.5; % Maximum for the line on the y-axis (number of models to plot plus 0.5 for space)
    xline = 3; % Line position on x-axis
    % Plot positive side only in the case of certain BF plot type
    % See get_options() for the plot types
    if opts.roiBFplot_direction > 0; line([xline xline],[ymin ymax],'Color',[128 128 128]./255,'HandleVisibility','off','LineWidth',1.5,'LineStyle',':'); end %[82 82 82]
    hold on
    % Negative side
    line([-xline -xline],[ymin ymax],'Color',[128 128 128]./255,'LineWidth',1.5,'LineStyle',':'); %'HandleVisibility','off'
    
    % Evidence threshold line vs. best model
    if opts.roiBFplot_direction == 1 && roi_BF_best > 3
        xline = roi_BF_best-3; % Line at best model log F minus log BF 3 (moderate evidence)
        line([xline xline],[ymin ymax],'Color',[255 128 0]./255,'LineWidth',1.5); % Alternative colors: [82 82 82] [255 102 102]
    end %'HandleVisibility','off'
    hold on
    
    % Plot colors
    barcolors = [51 34 136;... % dark blue
        115 34 135; ... % purple
        170 68 153; ... % pink
        185 30 30; ... % red
        0 102 204; ... % medium blue
        0 204 204; ... % teal
        17 119 51; ... % green
        68 170 153; ... % blue green
        136 204 238; ... % light blue
        204 102 119; ... % orange-red
        136 34 85; ... % dark pink
        221 204 119; ... % yellow
        ]./255; 
    %barcolor = [180 180 180]./255; % lighter grey
    %barcolor_bestmodel = [100 100 100]./255; % darker grey
    edgecolor = [50 50 50]./255; % Bar edges
    
    % Bayes Factor horizontal bar graph
    if opts.roiBFplot_direction == 1 && roi_BF_best > 0; bestmodel_ind = find(roi_BF == roi_BF_best); % Find best model index (highest log BF)
    elseif roi_BF_best == 0; bestmodel_ind = 0; % If the null model is the best model
    else; bestmodel_ind = find(roi_BF == 0); % Otherwise best model is the one that has value 0 (reference model)
    end
    
    % Loop over models to draw in the bar graph of the current ROI
    for i = 1:length(models2plot)
        barcolor_i = barcolors(models2plot(i),:); % Get bar color (same for each ROI but different for each model)
        if (i == bestmodel_ind) && (opts.roiBFplot_direction > 0) % If plotting best model and plotting positive right side
            barh(i,roi_BF_best,... % Horizontal bar
                'FaceColor',barcolor_i,...
                'BarWidth',barwidth,...
                'EdgeColor',edgecolor,...
                'LineWidth',1.5);
            hold on
        elseif i == bestmodel_ind % If plotting best model but only negative left side
            barh(i,0); % Set best model to 0 (and later manually add star cartoon image on it with Inkscape)
            hold on
        else % Plotting other models
            barh(i,roi_BF(models2plot(i)),...
                'FaceColor',barcolor_i,...
                'BarWidth',barwidth,...
                'EdgeColor',edgecolor,...
                'LineWidth',1.5);
            hold on
        end
    end

    % Set axis limits
    x = max(abs(roi_BF));
    
    if max(abs(roi_BF)) < 20 % If largest absolute log BF value is less than 20
        if roitype == 5; xmax = ceil(x/5)*5; else; xmax = 10; end % If ROI type is anatomical combined hemispheres, set x-axis maximum to closest (upper) value rounded to 5, otherwise (for functional ROIs) set to 10
    elseif max(abs(roi_BF)) >= 20 % If largest absolute log BF value is at least 20
        xmax = ceil(x/10)*10; % Set x-axis maximum to closest (upper) value rounded to 10
    else
        xmax = 10; 
    end
    xmin = -xmax; % x-axis minimum is the negative of the maximum
    if opts.roiBFplot_direction > 0; xlim([xmin xmax]); else; xlim([xmin 0]); end % If plotting positive side, set x-axis max and min, if only plotting negative side, set x-axis maximum to 0
    ylim([ymin ymax]) % y-axis minimum and maximum as defined above for log BF lines
    
    % Axis labels
    modelnames2plot = modelnames(models2plot); % Retrieve names of the models being plotted
    if ismember(roi_ind,model_label_roi); set(gca,'YTick',1:1:no_models); set(gca,'YTickLabel',modelnames2plot','FontSize',10); % If current ROI is one of the ROIs getting the model plot (in the first column)
    else; set(gca,'YTick',[]); end
    set(gca, 'YDir','reverse') % Reverse y-axis direction for correct model order
    if opts.roiBFplot_direction > 0 % Set x-axis labels if plotting also the positive side
        set(gca,'XTick',[xmin xmax]); set(gca,'XTickLabel',{num2str(xmin),num2str(xmax)},'FontSize',10);
    else % If only plotting the negative side, limit ticks and labels to 0
        set(gca,'XTick',[xmin 0]); set(gca,'XTickLabel',{num2str(xmin) 0},'FontSize',10);
    end

    % If the current ROI is the one receiving the rest of the labels (bottom left corner)
    if roi_ind == label_roi
        xlabel(ax,{'log Bayes Factor'},'FontSize',10); % x-axis label for log BF
        pos_text = pos(roi_ind+1,:); % Position for the text boxes next to the ROI plot
        if roitype == 5 && opts.roiBFplot_direction == 2 % Combined anatomical ROIs & plotting option 2
            dim = [pos_text(1) 0 .3 .3]; % Dimensions for the annotation box
            annotation('textbox',dim,'String',{'Right side: Best model vs. null model',...
                'Left side: Other models vs. best model','More extreme values = more support for the best model'},...
            'FitBoxToText','on','VerticalAlignment','middle');
        elseif roitype == 5 && opts.roiBFplot_direction < 2 % Combined anatomical ROIs & plotting option 1 or -1
            dim = [pos_text(1) 0.08 .37 .3]; % Dimensions for the annotation box
            annotation('textbox',dim,'String',{'+ Evidence for the alternative model','- Evidence for the null model'},...
            'FitBoxToText','on','VerticalAlignment','bottom');
            legend({'Moderate evidence threshold compared to null model',...
                'Moderate evidence threshold compared to best model'},'Position',[pos_text(1)+0.05 .15 .30 .05],'FontSize',10)
        elseif roitype == 3 && opts.roiBFplot_direction == 2 % Functional ROIs & plotting option 2
            dim = [pos_text(1) 0.05 .3 .3]; % Dimensions for the annotation box
            annotation('textbox',dim,'String',{'Right side: Best model vs. null model',...
                'Left side: Other models vs. best model','More extreme values = more support for the best model'},...
            'FitBoxToText','on','VerticalAlignment','middle');
        elseif roitype == 3 && opts.roiBFplot_direction < 2 % Functional ROIs & plotting option 1 or -1
            dim = [pos_text(1) 0.04 .3 .3];
            annotation('textbox',dim,'String',{'+ Evidence for the alternative model','- Evidence for the null model'},...
            'FitBoxToText','off','VerticalAlignment','middle');
            legend({'Moderate evidence threshold compared to null model',...
                'Moderate evidence threshold compared to best model'},'Position',[pos_text(1)+0.05 .15 .30 .05],'FontSize',10) % Dimensions off here
        end
    end

    % Define and set title for the figure: ROI name
    roititle = roinames{roi};
    title(roititle,'FontSize',12,'FontWeight','normal')
    
    roi_ind = roi_ind + 1;
    
end

% Set paper size and save figure (Matlab figure, png, svg formats)
PAPER = get(roifig,'Position');
set(roifig,'PaperSize',[PAPER(3), PAPER(4)]);
savefig(fullfile(plotpath,['ROIplot_' roiname '_' num2str(roi_no) 'ROIs_PE_BayesFactors_dir' num2str(opts.roiBFplot_direction)]))
saveas(gcf,fullfile(plotpath,['ROIplot_' roiname '_' num2str(roi_no) 'ROIs_PE_BayesFactors_dir' num2str(opts.roiBFplot_direction)]),'png')
saveas(gcf,fullfile(plotpath,['ROIplot_' roiname '_' num2str(roi_no) 'ROIs_PE_BayesFactors_dir' num2str(opts.roiBFplot_direction)]),'svg')

end