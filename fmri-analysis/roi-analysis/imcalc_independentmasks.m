%Make sure there is no overlap between masks
mainpath = fullfile(cd,'..','..','..');
fpath = fullfile(mainpath,'fMRI_biascorrected');
roipath = fullfile(fpath,'Masks','ReslicedMasks','CurrentMasks','Anatomical','FrontalBA');
roiList = spm_select('FPList',roipath);
roiNames = spm_select('List',roipath);

for r = 1:size(roiList,1)
    clear matlabbatch
    
    if r > 1
        matlabbatch{1}.spm.util.imcalc.input = {
            [roiList(r,:) ',1']
            [roiList(r-1,:) ',1']
            };
    end
    matlabbatch{1}.spm.util.imcalc.output = fullfile(roipath,[roiNames(r,1:end-4) '_brainmasked.nii']);
    matlabbatch{1}.spm.util.imcalc.outdir = {roipath};
    matlabbatch{1}.spm.util.imcalc.expression = 'i1-i2';
    spm_jobman('run',matlabbatch)
end