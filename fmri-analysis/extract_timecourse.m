function extract_timecourse(sublist,roitypes)
%Extract BOLD signal time courses for each ROI
%Original code 2011 - Frank Scharnowski - Swiss Institute of Technology

opts = get_options();

ROIfolder = opts.roifolders(roitypes);

for roitype = 1:length(ROIfolder) % Loop over ROI type
    
    roipath = fullfile(opts.fpath,'Masks','ReslicedMasks','CurrentMasks',ROIfolder{roitype}); % ROI mask folder
    roifiles = spm_select('FPList',roipath);
    roinames = spm_select('List',roipath); % ROI names for checking purposes
    
    for roi_ind = 14:size(roifiles,1) % Loop over ROI files (first one excluded due to folder structure, not an actual ROI file)
        
        % Read ROI volume data
        roiName = roinames(roi_ind,:);
        roi = spm_select('FPList',roipath,['^' roiName]);
        roi = spm_read_vols(spm_vol(roi));
        
        for sub = 1:length(sublist) % Loop over subjects
            
            % Find EPI files for each subject
            subpath = fullfile(opts.fpath, opts.subj(sublist(sub)), 'EPI');
            im_range = get_imagerange(opts.subj(sublist(sub))); % EPI image range = scanner run file names for current subject
            
            for run = 1:length(im_range) % Loop over EPI range for the subject
                
                if im_range(run) >= 10 % Correct number of zeros in the name
                    volno = ['0' num2str(im_range(run))];
                else
                    volno = ['00' num2str(im_range(run))];
                end
                
                % Extract ROI time course from the preprocessed EPI data
                epi = spm_select('ExtFPList',subpath,['^swuabc.*' volno '.*nii'],Inf);
                epi = cellstr(epi);
                nfiles = size(epi,1); % Number of EPI files
                findvoxels = find(roi>0); % Find voxels within the ROI mask
                nvoxels = size(findvoxels,1); % Number of voxels in the ROI
                timecourseRoi = zeros(nvoxels,nfiles); % Initialize time course data as zeros for each voxel and run
                for n = 1:nfiles % Loop over EPI files
                    epivol = spm_read_vols(spm_vol(cell2mat(epi(n,:)))); % EPI volume
                    timecourseRoi(:,n) = epivol(findvoxels); % Time course for each voxel in the VOI (ROI mask)
                end
                
                % Save time course data for each subject and run for the
                % current ROI
                roiName = strtrim(roiName); % Trim ROI name
                save(fullfile(opts.roipath,'RawROItimecourses',['timecourse_' roiName(1:end-4) '_run' num2str(run) '_subject' num2str(sub) '.mat']),'timecourseRoi')
                
            end
            
        end
        
    end
    
end

end