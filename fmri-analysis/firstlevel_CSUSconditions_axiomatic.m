function firstlevel_CSUSconditions_axiomatic(opts,model,phase)
%Create and run first level GLMs for the categorical axiomatic model

%% Initialize variables and folders
model_inp = get_modelconfig(opts,model);

% File with information about scans that are too short to include the
% last US of each run
load(opts.shortscansfile)

% Loop over subjects (1st level analysis for each subject separately)
for subInd = 1:length(opts.subj)
    
    clear matlabbatch
    
    % Subject's path
    subpath = fullfile(opts.fpath, char(opts.subj(subInd)));
    cd(subpath)
    
    % 1st level path
    fpath_first = fullfile(subpath, '1stlevel', ['Version_' opts.version], ['NoiseCorr_' opts.noisecorr_name], model_inp.modelname, ['First_Level_' opts.phasenames{phase}]);
    
    % Create the folder if it does not exist
    if ~exist(fpath_first, 'dir')
        mkdir(fpath_first)
    end
    
    % Range of images for EPIs (in the the file name)
    im_range = get_imagerange(opts.subj(subInd));
    
    %% Specify 1st level model
   
    % Load behavioural file with timings
    bhvdata = load(fullfile(opts.mainpath, 'Behavior', ['S' num2str(opts.subj_beh(subInd))], ['Behavior_S', num2str(opts.subj_beh(subInd))]),'cond','tim');
    
    % Runs
    runs = opts.phase_runs{phase};
    % All runs kept for AllPhases
    
    % Run 3 excluded for subject PR2121 due to bad head motion and artefact
    if phase == 3 && strncmp(opts.subj(subInd),'PR02121',7); runs = [2 4 5]; end
    
    % Condition onset times and types
    for n = 1:length(runs)
        CS_on(:,n) = bhvdata.tim(runs(n)).CS_on; %#ok<*AGROW>
        US_on(:,n) = bhvdata.tim(runs(n)).US_on;
        CS_type(:,n) = bhvdata.cond(runs(n)).CS;
        
        CS_type(CS_type(:,n)==6,n) = 1;
        CS_type(CS_type(:,n)==7,n) = 2;
        CS_type(CS_type(:,n)==8,n) = 3;
        CS_type(CS_type(:,n)==5,n) = 4;
        
        US_type(:,n) = bhvdata.cond(runs(n)).US;
    end

    sesi = 0; % Session index for matlabbatch
    
    for run = runs
        
        clear EPI episcans co1 co2
        
        sesi = sesi + 1;
        
        % Volume number for the current block
        volno = num2str(im_range(run));
        if length(volno) < 2
            volno = ['0' volno];
        end
        
        % Select EPI files
        EPIpath = fullfile(subpath,'EPI');
        cd(EPIpath)
        EPI.epiFiles = spm_vol(spm_select('ExtList',fullfile(subpath,'EPI'),['^swuabc2.*0',volno,'a001.nii$'])); % Bias corrected EPIs
        
        for epino = 1:size(EPI.epiFiles,1)
            episcans{epino} = [EPI.epiFiles(epino).fname, ',',num2str(epino)];
        end
        
        % Noise correction files
        if opts.noisecorr == 1 % Only head motion
            noisefile = ls(fullfile(EPIpath, ['rp_abc*', volno, 'a001.txt']));
        elseif opts.noisecorr > 1 % RETROICOR or PhysIO
            noisefile = ls(fullfile(EPIpath, ['multiple_regressors_session', num2str(run), '_' opts.noisecorr_name '.txt']));
        end
        
        disp(['...Block ' num2str(run), ' out of ' num2str(length(im_range)), '. Found ', num2str(epino), ' EPIs...' ])
        disp(['Found ', num2str(size(noisefile,1)), ' noise correction file(s).'])
        disp('................................')
      
        % Conditions
        % -----------------------------------------------------------------
        
        SR = 1000; % Sampling rate
        
        % Find CS events and create conditions:
        nCS = model_inp.cond_CS; % Number of different CS:
        % CS1: p(US+) = 0
        % CS2: p(US+) = 1/3
        % CS3: p(US+) = 2/3
        % CS4: p(US+) = 1
        
        % CS onsets and types for the current run
        CS_on_blk = CS_on(:,sesi);
        CS_type_blk = CS_type(:,sesi);
        
        for c = 1:nCS % Loop over CS types to define onsets, durations etc.
            
            matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(c).name = ['CS', num2str(c)];
            matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(c).onset = CS_on_blk(CS_type_blk == (c))/SR;
            matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(c).duration = 0; % Zero duration stick function
            matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(c).tmod = 0; % Temporal derivatives - none
            matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(c).pmod = struct('name', {}, 'param', {}, 'poly', {}); % No parametric modulation
            matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(c).orth = 1; % Orthogonalization - on
            
            co1(c) = length(CS_on_blk(CS_type_blk == (c))/SR); % Number of events found
            
        end
        
        % Find US events and create conditions:
        nUS = model_inp.cond_US; % Number of different US/outcome conditions
        
        USleng = length(US_on(:,sesi));
        
        if tooshortscans(subInd,run+1) > 0 % If scan cut too short to include last US response
            USleng = USleng-1;
        end

        % US onsets and types for the current run
        US_on_blk   = US_on(1:USleng,sesi); 
        US_type_blk = US_type(1:USleng,sesi);
        CS_type_blk = CS_type_blk(1:USleng,:);
        
        % US1: CS1/US-
        USind{1,:} = find((CS_type_blk == 1) & (US_type_blk == 0));
        % US2: CS2/US+
        USind{2,:} = find((CS_type_blk == 2) & (US_type_blk == 1));
        % US3: CS2/US-
        USind{3,:} = find((CS_type_blk == 2) & (US_type_blk == 0));
        % US4: CS3/US+
        USind{4,:} = find((CS_type_blk == 3) & (US_type_blk == 1));
        % US5: CS3/US-
        USind{5,:} = find((CS_type_blk == 3) & (US_type_blk == 0));
        % US6: CS4/US+
        USind{6,:} = find((CS_type_blk == 4) & (US_type_blk == 1));
            
        for c = 1:nUS
            
            matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(c+nCS).name = ['US', num2str(c)];
            matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(c+nCS).onset = US_on_blk(USind{c})/SR;
            matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(c+nCS).duration = 0; % Zero duration stick function
            matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(c+nCS).tmod = 0; % Temporal derivatives - none
            matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(c+nCS).pmod = struct('name', {}, 'param', {}, 'poly', {}); % No parametric modulation
            matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(c+nCS).orth = 1; % Orthogonalization - on
            
            co2(c) = length(US_on_blk(USind{c})/SR); % Number of events found
            
        end
        
        disp(['Found ', num2str(sum(co1)), ' CS and ', num2str(sum(co2)), ' US events.'])
        disp(['Found ', num2str(size(noisefile,1)), ' noise correction file(s).'])
        disp('................................')
        
        matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).scans = episcans';
        matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).multi = {''};
        matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).regress = struct('name', {}, 'val', {});
        matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).multi_reg = {fullfile(EPIpath, noisefile)}; % Physiological and head motion noise correction files for nuisance regressors
        matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).hpf = 128; % High-pass filter 128 Hz
        
    end
     
    matlabbatch{1}.spm.stats.fmri_spec.dir = {fpath_first};
    matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
    matlabbatch{1}.spm.stats.fmri_spec.timing.RT = 3.2; % Repetition time in seconds
    matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = 40; % Total slices
    matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = 20; % Reference slice
    
    matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
    matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0]; % Hemodynamic response function derivatives - none
    matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
    matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
    matlabbatch{1}.spm.stats.fmri_spec.mthresh = 0.5; % Mask threshold, original 0.8
    matlabbatch{1}.spm.stats.fmri_spec.mask = {opts.brainmask}; % Brain mask to exclude e.g. eyes
    matlabbatch{1}.spm.stats.fmri_spec.cvi = opts.tempautocorr; % Temporal autocorrelation removal algorithm to use
    
    %% Estimate 1st level model
    matlabbatch{2}.spm.stats.fmri_est.spmmat = {fullfile(fpath_first, 'SPM.mat')};
    matlabbatch{2}.spm.stats.fmri_est.write_residuals = 0;
    matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;
    
    %% Run matlabbatch
    spm_jobman('run', matlabbatch);
    
    save(fullfile(fpath_first,'batch_s_First'), 'matlabbatch')
    
end

end