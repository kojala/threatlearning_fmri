function create_behavfiles(opts)
%Create behavioural data files that can be used later to construct GLMs
%with trial timings, condition types etc.

subpath = fullfile(opts.mainpath, 'Behavior'); % Behavioural data path
SOA = opts.SOA*1000; % Stimulus onset asynchrony in milliseconds

for subInd = opts.subj_beh % Loop over subjects with their behavioural data IDs
    
    s_id = num2str(subInd); % Subject ID
    subpath_s = fullfile(subpath, ['S' s_id]); % Subject data path
    
    for run = opts.runs % Loop over scanning runs/blocks
        
        clear indata condTime bl t0
        
        if run < 6 % Acquisition 1 and Maintenance
            load(fullfile(subpath_s, ['PCF_05_' s_id, '_Block' num2str(run) '.mat'])) % Load data info file
            t0_b(run) = t0;
        elseif run == 6 % Acquisition 2
            load(fullfile(subpath_s, ['PCF_05_' s_id, '_Block' num2str(run) '_Part2.mat']))
            t0_b(run) = NaN;
        end
        
        tim(run).CS_on = indata(:,5);     % CS onset
        tim(run).US_on = indata(:,5)+SOA; % US onset
        tim(run).CS_off = indata(:,7);    % CS offset
        
        cond(run).CS = indata(:,2);       % CS type
        cond(run).US = indata(:,3);       % US type
        
        resp(run).corr = indata(:,11);    % Response correct
        resp(run).rts = indata(:,10);     % Response time
        
    end
    
    save(fullfile(subpath_s, ['Behavior_S' s_id '.mat']), 'tim', 'cond', 'resp', 't0_b')
    
end
    
end