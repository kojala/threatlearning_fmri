function firstlevel_CSUSconditions_parmod_extravolsout(opts,model,phase)

%% Initialize variables and folders

% Number of blocks
no_runs = 6; % All blocks
runs = 1:no_runs;

% Phase information
if phase == 1 % Acq
    runs = runs(1); % Take only first acquisition block
elseif phase == 2 % Maint
    runs = runs(2:5); % Take only maintenance blocks
elseif phase == 3 % Acq2
    runs = runs(6); % Take only last acquisition block
elseif phase == 4 % AllPhases
    runs = runs;
elseif phase == 5 % BothAcq
    runs = runs([1 6]);
elseif phase == 6 % AllPhases Scaled
    runs = runs;
else
    error('Invalid phase number.')
end

% Loop over subjects (1st level analysis for each subject separately)
for subInd = 1:length(opts.subj)
    
    clear matlabbatch
    
    % Subject's path
    subpath = fullfile(opts.fpath, char(opts.subj(subInd)));
    cd(subpath)
    
    % 1st level path
    fpath_first = fullfile(subpath, '1stlevel', ['Version_' opts.version], ['NoiseCorr_' opts.noisecorr_name], opts.modelname{model}, ['First_Level_' opts.phasename{phase}]);
    
    % Create the folder if does not exist
    if ~exist(fpath_first, 'dir')
        mkdir(fpath_first)
    end
    
    % Range of images for EPIs (in the the file name)
    im_range = get_imagerange(opts.subj(subInd));
    
    %% Specify 1st level model
    sesi = 0; % Session (phase) index for matlabbatch
    
    % Run 3 excluded for subject PR2121 due to bad head motion and artefact
    if strncmp(opts.subj(subInd),'PR02121',7) && phase == 2
        runs = [2 4 5];
    elseif strncmp(opts.subj(subInd),'PR02121',7) && phase == 6
        runs = [1 2 4:6];
    end
    
    for run = runs
        
        clear EPI episcans
        
        sesi = sesi + 1; % Increase session number
        
        % Volume number for the current block
        volno = num2str(im_range(run));
        if length(volno) < 2
            volno = ['0' volno]; %#ok<AGROW>
        end
        
        % Select EPI files
        EPIpath = fullfile(subpath,'EPI');
        EPI.epiFiles = spm_vol(spm_select('ExtList',fullfile(subpath,'EPI'),['^swuabc2.*0',volno,'a001.nii$'])); % Bias corrected EPIs
        
        % Conditions
        % -----------------------------------------------------------------
        % Multiple conditions file created with preparebehav
        condfile = fullfile(opts.mainpath, 'Behavior', ['S' num2str(opts.subj_beh(subInd))], ['S' num2str(opts.subj_beh(subInd)) '_block' num2str(run) '_CSUSconds_' opts.modelname{model} '.mat']);
        
        volumes = find_volumes(condfile);
        
        if volumes <= size(EPI.epiFiles,1)
%             episcans = episcans(1:volumes);
%         else
            tooshortscans(subInd,1) = opts.subj_beh(subInd);
            tooshortscans(subInd,run+1) = run;
        end
        
    end
    
end

if exist('tooshortscans','var')
    save(fullfile(opts.fpath,'tooshortscans.mat'),'tooshortscans');
end

end