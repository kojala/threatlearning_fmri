function combine_echoes(opts)
% this function combines the multiple echoes of an fMRI time-series. The
% echo combination is done using a simple summation.
% All image volumes for each echo number have to be entered together.

%Echo1 = spm_vol(spm_select('ExtList','image','Select 1st echo images',[],[],'.*004a001.(img|nii)$'));

for subInd = 1:length(opts.subj)
    
    subpath = fullfile(opts.mainpath, opts.subj(subInd));

    im_range = get_imagerange(opts.subj(subInd));

    for img = 1:length(im_range)
        
        volno = num2str(im_range(img));
        if length(volno) < 2
            volno = ['0', volno]; %#ok<*AGROW>
        end
        
        Echo1 = spm_vol(spm_select('ExtList',subpath,['^2.*0',volno,'a001.(img|nii)$']));
        
        volno = num2str(im_range(img)+1);
        if length(volno) < 2
            volno = ['0', volno];
        end
        
        Echo2 = spm_vol(spm_select('ExtList',subpath,['^2.*0',volno,'a001.(img|nii)$']));
        
        volno = num2str(im_range(img)+2);
        if length(volno) < 2
            volno = ['0', volno];
        end
        
        Echo3 = spm_vol(spm_select('ExtList',subpath,['^2.*0',volno,'a001.(img|nii)$']));
        
        % Load nifti echo files
        
        for runcounter = 1:min([size((Echo1),1), size((Echo2),1), size((Echo3),1)])
            
            SaveStruct = Echo1(runcounter);
            SaveStruct.fname = fullfile(subpath,['c' spm_str_manip(SaveStruct.fname,'t')]);
            spm_write_vol(SaveStruct,squeeze(spm_read_vols(Echo1(runcounter)) + spm_read_vols(Echo2(runcounter)) + spm_read_vols(Echo3(runcounter))));
        
        end
        
    end
    
    
end

end