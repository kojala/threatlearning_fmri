function firstlevel_MVPA_trialwise(opts,sublist)

%% Initialize variables and folders

% File with information about scans that are too short to include the
% last US of each run
shortscansfile = fullfile(opts.fpath_mvpa,['tooshortscans_USresp' num2str(opts.USresptime) 'sec.mat']);
load(shortscansfile)

% Loop over subjects (1st level analysis for each subject separately)
for subInd = sublist
    
    clear matlabbatch
    
    % Subject's path
    subpath = fullfile(opts.fpath_mvpa, char(opts.subj(subInd)));
    cd(subpath)
    
    % 1st level path
    fpath_first = fullfile(subpath, '1stlevel', ['Version_' opts.version_mvpa], ['NoiseCorr_' opts.noisecorr_name]);
    
    % Create the folder if does not exist
    if ~exist(fpath_first, 'dir')
        mkdir(fpath_first)
    end
    
    % Range of images for EPIs (in the the file name)
    im_range = get_imagerange(opts.subj(subInd));

    %% Specify 1st level model
    sesi = 0; % Session (phase) index for matlabbatch
    
    % Runs for this phase
    runs = opts.runs;
    
    % Run 3 excluded for subject PR2121 due to bad head motion and artefact
    if strncmp(opts.subj(subInd),'PR02121',7); runs = [1 2 4:6]; end
    
    % Load behavioural file with timings
    bhvdata = load(fullfile(opts.bhvpath, ['S' num2str(opts.subj_beh(subInd))], ['Behavior_S', num2str(opts.subj_beh(subInd))]),'cond','tim');
    
    alltrials = 0; % Counter for trials over sessions
    
    for run = runs
        
        CS_on = bhvdata.tim(run).CS_on;
        US_on = bhvdata.tim(run).US_on;
        
        clear EPI episcans
        
        sesi = sesi + 1; % Increase session number
        
        % Volume number for the current block
        volno = num2str(im_range(run));
        if length(volno) < 2
            volno = ['0' volno]; %#ok<AGROW>
        end
        
        % Select EPI files
        EPIpath = fullfile(subpath,'EPI');
        cd(EPIpath)
        EPI.epiFiles = spm_vol(spm_select('ExtList',fullfile(subpath,'EPI'),['^wuabc2.*0',volno,'a001.nii$'])); % Bias corrected EPIs without normalization and smoothing
        
        for epino = 1:size(EPI.epiFiles,1)
            episcans{epino} = [EPI.epiFiles(epino).fname, ',',num2str(epino)]; %#ok<AGROW>
        end
        
        % Noise correction files
        if opts.noisecorr == 1 % Only head motion
            noisefile = ls(fullfile(EPIpath, ['rp_abc*', volno, 'a001.txt']));
        elseif opts.noisecorr > 1 % RETROICOR or PhysIO
            noisefile = ls(fullfile(EPIpath, ['multiple_regressors_session', num2str(run), '_' opts.noisecorr_name '.txt']));
        end

        disp(['...Run ' num2str(run), ' out of ' num2str(length(im_range)), '. Found ', num2str(epino), ' EPIs...' ])
        disp(['Found ', num2str(size(noisefile,1)), ' noise correction file(s).'])
        disp('................................')
      
        % Conditions
        % -----------------------------------------------------------------
        
        SR = 1000; % Sampling rate
        
        % CS onsets and types for the current run
        nTrials = length(CS_on);
        
        allevents = 0;
        
        for trial = 1:nTrials
            
            alltrials = alltrials + 1; % Counter for trials over sessions
            
            allevents = allevents + 1; % CS event added
            matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(allevents).name = ['Trial', num2str(alltrials), 'CS'];
            matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(allevents).onset = CS_on(trial)/SR;
            matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(allevents).duration = 0;
            matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(allevents).tmod = 0;
            matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(allevents).pmod = struct('name', {}, 'param', {}, 'poly', {});
            matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(allevents).orth = 0;
            
            if (trial < nTrials) || (trial == nTrials && tooshortscans(subInd,run+1) == 0) % Add last US event of the run only if scan is long enough
                allevents = allevents + 1; % US event added
                matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(allevents).name = ['Trial', num2str(alltrials), 'US'];
                matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(allevents).onset = US_on(trial)/SR;
                matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(allevents).duration = 0;
                matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(allevents).tmod = 0;
                matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(allevents).pmod = struct('name', {}, 'param', {}, 'poly', {});
                matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond(allevents).orth = 0;
            end
            
        end
        
        % Multiple conditions file created with preparebehav
        %condfile = fullfile(mainpath, 'Behavior', ['S' num2str(subj_beh(subInd))], ['S' num2str(subj_beh(subInd)) '_block' num2str(run) '_MVPAconds.mat']);
        
        % Matlabbatch
        matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).scans = episcans';
        %matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
        %matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).multi = {condfile};
        matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).multi = {''};
        matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).regress = struct('name', {}, 'val', {});
        matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).multi_reg = {fullfile(EPIpath, noisefile)};
        matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).hpf = 128;
        
    end
     
    matlabbatch{1}.spm.stats.fmri_spec.dir = {fpath_first};
    matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
    matlabbatch{1}.spm.stats.fmri_spec.timing.RT = 3.2;
    matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = 40;
    matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = 20;
    
    matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
    matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0];
    matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
    matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
    matlabbatch{1}.spm.stats.fmri_spec.mthresh = 0.5;
    matlabbatch{1}.spm.stats.fmri_spec.mask = {''};
    matlabbatch{1}.spm.stats.fmri_spec.cvi = 'FAST';
    
    %% Estimate 1st level model
    matlabbatch{2}.spm.stats.fmri_est.spmmat = {fullfile(fpath_first, 'SPM.mat')};
    matlabbatch{2}.spm.stats.fmri_est.write_residuals = 0;
    matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;
    
    %% Run matlabbatch
    spm('defaults', 'FMRI');
    spm_jobman('initcfg');
    spm_jobman('run', matlabbatch);
    
    save(fullfile(fpath_first,'batch_s_First'), 'matlabbatch')
    
end

end