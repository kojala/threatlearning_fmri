function firstlevel_CSUSconditions_parmod(opts,model,phase)
% Create and run first level GLMs for parametric models

%% Initialize variables and folders

model_inp = get_modelconfig(opts,model);

% Loop over subjects (1st level analysis for each subject separately)
for subInd = 1:length(opts.subj)
    
    clear matlabbatch
    
    % Subject's path
    subpath = fullfile(opts.fpath, char(opts.subj(subInd)));
    cd(subpath)
    
    % 1st level path
    fpath_first = fullfile(subpath, '1stlevel', ['Version_' opts.version], ['NoiseCorr_' opts.noisecorr_name], model_inp.modelname, ['First_Level_' opts.phasenames{phase}]);
    
    % Create the folder if does not exist
    if ~exist(fpath_first, 'dir')
        mkdir(fpath_first)
    end
    
    % Range of images for EPIs (in the the file name)
    im_range = get_imagerange(opts.subj(subInd));

    %% Specify 1st level model
    sesi = 0; % Session (phase) index for matlabbatch
    
    % Runs for this phase
    runs = opts.phase_runs{phase};
    
    % Run 3 excluded for subject PR2121 due to bad head motion and artefact
    if strncmp(opts.subj(subInd),'PR02121',7) && phase == 2; runs = [2 4 5];
    elseif strncmp(opts.subj(subInd),'PR02121',7) && phase == 6; runs = [1 2 4:6]; end
    
    for run = runs
        
        clear EPI episcans
        
        sesi = sesi + 1; % Increase session number
        
        % Volume number for the current block
        volno = num2str(im_range(run));
        if length(volno) < 2
            volno = ['0' volno]; %#ok<AGROW>
        end
        
        % Select EPI files
        EPIpath = fullfile(subpath,'EPI');
        cd(EPIpath)
        EPI.epiFiles = spm_vol(spm_select('ExtList',fullfile(subpath,'EPI'),['^swuabc2.*0',volno,'a001.nii$'])); % Bias corrected EPIs
        
        for epino = 1:size(EPI.epiFiles,1)
            episcans{epino} = [EPI.epiFiles(epino).fname, ',',num2str(epino)]; %#ok<AGROW>
        end
        
        % Noise correction files
        if opts.noisecorr == 1 % Only head motion
            noisefile = ls(fullfile(EPIpath, ['rp_abc*', volno, 'a001.txt']));
        elseif opts.noisecorr > 1 % RETROICOR or PhysIO
            noisefile = ls(fullfile(EPIpath, ['multiple_regressors_session', num2str(run), '_' opts.noisecorr_name '.txt']));
        end
        
        disp(['...Run ' num2str(run), ' out of ' num2str(length(im_range)), '. Found ', num2str(epino), ' EPIs...' ])
        disp(['Found ', num2str(size(noisefile,1)), ' noise correction file(s).'])
        disp('................................')
      
        % Conditions
        % -----------------------------------------------------------------
        % Multiple conditions file created with preparebehav
        condfile = fullfile(opts.mainpath, 'Behavior', ['S' num2str(opts.subj_beh(subInd))], ['S' num2str(opts.subj_beh(subInd)) '_block' num2str(run) '_CSUSconds_' model_inp.modelname '.mat']);
        
        % Matlabbatch
        matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).scans = episcans';
        matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
        matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).multi = {condfile};
        matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).regress = struct('name', {}, 'val', {});
        matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).multi_reg = {fullfile(EPIpath, noisefile)}; % Physiological and head motion noise correction files for nuisance regressors
        matlabbatch{1}.spm.stats.fmri_spec.sess(sesi).hpf = 128; % High-pass filter 128 Hz
        
    end
     
    matlabbatch{1}.spm.stats.fmri_spec.dir = {fpath_first};
    matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
    matlabbatch{1}.spm.stats.fmri_spec.timing.RT = 3.2; % Repetition time in seconds
    matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = 40; % Total slices
    matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = 20; % Reference slice
    
    matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
    matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0]; % Hemodynamic response function derivatives - none
    matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
    matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
    matlabbatch{1}.spm.stats.fmri_spec.mthresh = 0.5; % Mask threshold, original 0.8
    matlabbatch{1}.spm.stats.fmri_spec.mask = {opts.brainmask}; % Brain mask to exclude e.g. eyes
    matlabbatch{1}.spm.stats.fmri_spec.cvi = opts.tempautocorr; % Temporal autocorrelation removal algorithm
    
    %% Estimate 1st level model
    matlabbatch{2}.spm.stats.fmri_est.spmmat = {fullfile(fpath_first, 'SPM.mat')};
    matlabbatch{2}.spm.stats.fmri_est.write_residuals = 0;
    matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;
    
    %% Run matlabbatch
    spm_jobman('run', matlabbatch);
    
    save(fullfile(fpath_first,'batch_s_First'), 'matlabbatch')
    
end

end