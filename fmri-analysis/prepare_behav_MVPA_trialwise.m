function prepare_behav_MVPA_trialwise(opts)

% Sampling rate
SR = 1000;

% Number of blocks
no_runs = 6; % All blocks
runs = 1:no_runs;

% File with information about scans that are too short to include the
% last US of each run
shortscansfile = fullfile(opts.fpath,['tooshortscans_USresp' num2str(opts.USresptime) 'sec.mat']);
load(shortscansfile)

for subInd = 1:length(opts.subj)
    
    behavDir = fullfile(opts.mainpath,'Behavior',['S' num2str(opts.subj(subInd))]);
    
    alltrials = 1; % Counter for trials over sessions for naming purposes
    
    for iSess = runs
        
        behavFile = sprintf('Behavior_S%d.mat',opts.subj(subInd));
        behavFile = fullfile(behavDir,behavFile);
        behavData = load(behavFile);
        
        % Timings
        tim = behavData.tim;
        
        events = length(tim(iSess).CS_on);
        allevents = 0;
        
        for event = 1:events
            
            allevents = allevents + 1; % CS event added
            namelist{allevents} = ['Trial' num2str(alltrials) 'CS'];
            timings{allevents} = tim(iSess).CS_on(event)/SR;
            durations{allevents} = 0;
            orths{allevents} = 0;
            
            if tooshortscans(subInd,iSess+1) == 0 % Add US event only if scan is long enough
                
                allevents = allevents + 1; % US event added
                namelist{allevents} = ['Trial' num2str(alltrials) 'US'];
                timings{allevents} = tim(iSess).US_on(event)/SR;
                durations{allevents} = 0;
                
            end
            
            alltrials = alltrials + 1; % Counter for trials over sessions
            
        end
        
        names        = namelist;
        onsets       = timings;
        durations    = durations;
        orth         = orths;
        
        outputFile = sprintf('S%d_block%d_MVPAconds.mat',opts.subj(subInd),iSess);
        outputFile = fullfile(behavDir,outputFile);
        save(outputFile,'names','onsets','durations','orth');
        
    end
end

end