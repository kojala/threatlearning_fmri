function firstlevel_contrasts_parmod(opts,model,phase)

%% Initialize variables and folders

% Retrieve model name and number of condition and parametric regressors
model_inp = get_modelconfig(opts,model);

for subInd = 1:length(opts.subj)
    
    clear matlabbatch
    
    subpath = fullfile(opts.fpath, char(opts.subj(subInd)));
    cd(subpath)
    
    fpath_first = fullfile(subpath, '1stlevel', ['Version_' opts.version], ['NoiseCorr_' opts.noisecorr_name], model_inp.modelname, ['First_Level_' opts.phasename{phase}]);
    
    %% Create contrasts and matlabbatch for contrast manager:
    
    matlabbatch{1}.spm.stats.con.spmmat = {fullfile(fpath_first, 'SPM.mat')};
    
    for con = 1:model_inp.no_contr
        
        matlabbatch{1}.spm.stats.con.consess{con}.tcon.name = model_inp.connames{con};
        matlabbatch{1}.spm.stats.con.consess{con}.tcon.weights = model_inp.conweights(:,con);
        matlabbatch{1}.spm.stats.con.consess{con}.tcon.sessrep = model_inp.option_con_sessrep; % Contrasts are replicated and scaled across runs
        matlabbatch{1}.spm.stats.con.delete = model_inp.option_con_delete;
        
    end
    
    %% Run matlabbatch
    spm_jobman('run', matlabbatch);
    
    save(fullfile(fpath_first,'batch_s_Contrasts'), 'matlabbatch')
    
end

end