addpath(fullfile(cd,'utils'))

clear all

opts = get_options();

subj = opts.subj;

inpath = opts.fpath;

%% Mask based on WM and GM for each subject
spm_jobman('initcfg');

for s = 1:length(subj)

    %% Create matlabbatch
    clear matlabbatch

    WMGM_img = cellstr(fullfile(inpath,'Masks','T1_WMGM',['T1_WMGM_S' num2str(s) '.nii']));

    % Normalise to MNI space
    matlabbatch{1}.spm.spatial.normalise.estwrite.subj.vol = WMGM_img;
    matlabbatch{1}.spm.spatial.normalise.estwrite.subj.resample = WMGM_img;
    matlabbatch{1}.spm.spatial.normalise.estwrite.eoptions.biasreg = 0.0001;
    matlabbatch{1}.spm.spatial.normalise.estwrite.eoptions.biasfwhm = 60;
    matlabbatch{1}.spm.spatial.normalise.estwrite.eoptions.tpm = {'D:\spm12\tpm\TPM.nii'};
    matlabbatch{1}.spm.spatial.normalise.estwrite.eoptions.affreg = 'mni';
    matlabbatch{1}.spm.spatial.normalise.estwrite.eoptions.reg = [0 0.001 0.5 0.05 0.2];
    matlabbatch{1}.spm.spatial.normalise.estwrite.eoptions.fwhm = 0;
    matlabbatch{1}.spm.spatial.normalise.estwrite.eoptions.samp = 3;
    matlabbatch{1}.spm.spatial.normalise.estwrite.woptions.bb = [-78 -112 -70
        78 76 85];
    matlabbatch{1}.spm.spatial.normalise.estwrite.woptions.vox = [2 2 2];
    matlabbatch{1}.spm.spatial.normalise.estwrite.woptions.interp = 4;
    matlabbatch{1}.spm.spatial.normalise.estwrite.woptions.prefix = 'normalized_';

    spm_jobman('run', matlabbatch)

end