function prepare_behav(rootDir,subj,phase,models)

% File with model predictions for Model 2 (and expected shock for Model 1b)
if ismember(6,models) || ismember(3,models); load(fullfile(rootDir,'Behavior','AllSubs_behvecs_Predictions_v3.mat')); end
% File with subjective shock probability ratings for Model 1c
if ismember(4,models); load(fullfile(rootDir,'Behavior','subjective_pshock_AcqMaint.mat')); end
% Sampling rate
SR = 1000;

% Number of blocks
no_runs = 6; % All blocks
runs = 1:no_runs;

if phase == 1
    runs = runs(1); % Take only first acquisition block
elseif phase == 2
    runs = runs(2:5); % Take only maintenance blocks
elseif phase == 3
    runs = runs(6);
end

% First trial of the block
ntrials = [6 11 11 11 11 6];
trial1 = [1 1+6 1+6+11 1+6+11+11 1+6+11+11+11 1];

for subInd = 1:length(subj)
    
    behavDir = fullfile(rootDir,'Behavior',['S' num2str(subj(subInd))]);
    
    for iSess = runs
        behavFile = sprintf('Behavior_S%d.mat',subj(subInd));
        behavFile = fullfile(behavDir,behavFile);
        behavData = load(behavFile);
        
        % Determine block, general CS indices for all runs
        tim = behavData.tim;
        conddata = behavData.cond;
        CS = conddata(iSess).CS;
        if iSess < 6 % Acquisition 1 or Maintenance
            if ismember(6,models) || ismember(3,models); modelpred = Pred1; end % Model prediction structure
            CS1 = CS==1;
            CS2 = CS==2;
            CS3 = CS==3;
            CS4 = CS==4;
        elseif iSess == 6 % Acquisition 2          
            if ismember(6,models) || ismember(3,models); modelpred = Pred2; end % Model prediction structure
            CS1 = CS==6; % CS code
            CS2 = CS==7;
            CS3 = CS==8;
            CS4 = CS==5;
        end
        
        % US
        US = conddata(iSess).US;
        
        names        = {'CS','US'};
        onsets       = {tim(iSess).CS_on/SR,tim(iSess).US_on/SR};
        durations    = {zeros(length(onsets{1}),1), zeros(length(onsets{2}),1)};
        orth         = {1,1};
        tmod         = {0,0};
        
        %% Model 1a
        
        if ismember(2,models)
            
            clear pmod shockProb
            shockProb(CS1) = 0;
            shockProb(CS2) = 1/3;
            shockProb(CS3) = 2/3;
            shockProb(CS4) = 1;
            
            pmod(1).name  = {'True p(shock)'};
            pmod(1).param = {shockProb'};
            pmod(1).poly  = {1};
            
            clear predictionError
            predictionError = US-shockProb';
            pmod(2).name  = {'US type','Prediction error'};
            pmod(2).param = {US,predictionError};
            pmod(2).poly  = {1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1.mat',subj(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
        end
        
        %% Model 1b
        
        if ismember(3,models)
            
            clear pmod BayesPredictionError exptShockProb
            
            sesstrials = trial1(iSess):(trial1(iSess)+ntrials(iSess)-1);
            
            % Expected shock probability
            exptShockProb(CS1) = modelpred{1,subInd}.BM(1,sesstrials);
            exptShockProb(CS2) = modelpred{1,subInd}.BM(2,sesstrials);
            exptShockProb(CS3) = modelpred{1,subInd}.BM(3,sesstrials);
            exptShockProb(CS4) = modelpred{1,subInd}.BM(4,sesstrials);
            
            pmod(1).name  = {'Expected p(shock)'};
            pmod(1).param = {exptShockProb'};
            pmod(1).poly  = {1};
            
            BayesPredictionError = US-exptShockProb';
            
            pmod(2).name  = {'US type','Model prediction error'};
            pmod(2).param = {US,BayesPredictionError};
            pmod(2).poly  = {1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1b.mat',subj(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
            
        end
        
        %% Model 1c
        
        if ismember(4,models)

            clear pmod SubjPredictionError subj_exptShockProb
            
            % Subjective expected shock probability based on ratings
            subj_exptShockProb(CS1) = CSsubj(subInd,1);
            subj_exptShockProb(CS2) = CSsubj(subInd,2);
            subj_exptShockProb(CS3) = CSsubj(subInd,3);
            subj_exptShockProb(CS4) = CSsubj(subInd,4);
            
            pmod(1).name  = {'Subjective expected p(shock)'};
            pmod(1).param = {subj_exptShockProb'};
            pmod(1).poly  = {1};
            
            % Prediction error, outcome-subjective rated expectation
            SubjPredictionError = US-subj_exptShockProb';
            
            pmod(2).name  = {'US type','Subj rating prediction error'};
            pmod(2).param = {US,SubjPredictionError};
            pmod(2).poly  = {1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1c.mat',subj(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
            
        end
        
        %% Model 1d
        
        if ismember(5,models)
            
            clear pmod weightedPredictionError exptShockProb
            
            % Expected shock probability
            exptShockProb(CS1) = modelpred{1,subInd}.BM(1,sesstrials);
            exptShockProb(CS2) = modelpred{1,subInd}.BM(2,sesstrials);
            exptShockProb(CS3) = modelpred{1,subInd}.BM(3,sesstrials);
            exptShockProb(CS4) = modelpred{1,subInd}.BM(4,sesstrials);

            pmod(1).name  = {'Expected p(shock)'};
            pmod(1).param = {exptShockProb'};
            pmod(1).poly  = {1};
            
            % Weighted prediction error based on the Bayesian model
            weightedPredictionError(CS1) = modelpred{1,subInd}.PE(1,sesstrials);
            weightedPredictionError(CS2) = modelpred{1,subInd}.PE(2,sesstrials);
            weightedPredictionError(CS3) = modelpred{1,subInd}.PE(3,sesstrials);
            weightedPredictionError(CS4) = modelpred{1,subInd}.PE(4,sesstrials);
            
            pmod(2).name  = {'US type','Weighted prediction error'};
            pmod(2).param = {US,weightedPredictionError};
            pmod(2).poly  = {1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1d.mat',subj(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
            
        end
        
        %% Model 2
        
        if ismember(6,models)
            clear pmod exptShockProb priorEntrShockProb KLdivPrevTrial ...
                surprUSPrevTrial KLdivCurrTrial surprUSCurrTrial signedPE ...
                sesstrials volatilityPrevTrial
            
            sesstrials = trial1(iSess):(trial1(iSess)+ntrials(iSess)-1);
            
            % Expected shock probability
            exptShockProb(CS1) = modelpred{1,subInd}.BM(1,sesstrials);
            exptShockProb(CS2) = modelpred{1,subInd}.BM(2,sesstrials);
            exptShockProb(CS3) = modelpred{1,subInd}.BM(3,sesstrials);
            exptShockProb(CS4) = modelpred{1,subInd}.BM(4,sesstrials);
            
            % Volatility
            volatilityPrevTrial(CS1) = modelpred{1,subInd}.VO(1,sesstrials);
            volatilityPrevTrial(CS2) = modelpred{1,subInd}.VO(2,sesstrials);
            volatilityPrevTrial(CS3) = modelpred{1,subInd}.VO(3,sesstrials);
            volatilityPrevTrial(CS4) = modelpred{1,subInd}.VO(4,sesstrials);
            
            % Prior entropy of shock probability
            priorEntrShockProb(CS1) = modelpred{1,subInd}.BE(1,sesstrials);
            priorEntrShockProb(CS2) = modelpred{1,subInd}.BE(2,sesstrials);
            priorEntrShockProb(CS3) = modelpred{1,subInd}.BE(3,sesstrials);
            priorEntrShockProb(CS4) = modelpred{1,subInd}.BE(4,sesstrials);
            
            % KL divergence between prior and posterior from previous trial
            KLdivPrevTrial(CS1) = modelpred{1,subInd}.KL(1,sesstrials); % Value here based on prev. trial (outcome not yet observed at this point for current trial)
            KLdivPrevTrial(CS2) = modelpred{1,subInd}.KL(2,sesstrials); % And KL div used both prior and posterior so cannot use current trial's KL div here
            KLdivPrevTrial(CS3) = modelpred{1,subInd}.KL(3,sesstrials);
            KLdivPrevTrial(CS4) = modelpred{1,subInd}.KL(4,sesstrials);
            
            % Surprise about US from previous trial
            surprUSPrevTrial(CS1) = modelpred{1,subInd}.SO(1,sesstrials); % Value here based on prev. trial
            surprUSPrevTrial(CS2) = modelpred{1,subInd}.SO(2,sesstrials); % But surprise based only on prior?
            surprUSPrevTrial(CS3) = modelpred{1,subInd}.SO(3,sesstrials);
            surprUSPrevTrial(CS4) = modelpred{1,subInd}.SO(4,sesstrials);
            
            pmod(1).name  = {'Expected p(shock)','Volatility',...
                'Prior entropy p(shock)','KL div prior-posterior previous trial',...
                'Surprise on US previous trial'};
            pmod(1).param = {exptShockProb',volatilityPrevTrial',priorEntrShockProb',KLdivPrevTrial',surprUSPrevTrial'};
            pmod(1).poly  = {1,1,1,1,1};
            
            % KL divergence between prior and posterior from current trial
            KLdivCurrTrial(CS1) = modelpred{1,subInd}.KL(1,sesstrials+1); % Value here based on prev. trial so current trial = +1
            KLdivCurrTrial(CS2) = modelpred{1,subInd}.KL(2,sesstrials+1);
            KLdivCurrTrial(CS3) = modelpred{1,subInd}.KL(3,sesstrials+1);
            KLdivCurrTrial(CS4) = modelpred{1,subInd}.KL(4,sesstrials+1);
            
            % Surprise about US from current trial
            surprUSCurrTrial(CS1) = modelpred{1,subInd}.SO(1,sesstrials+1); % Value here based on prev. trial so current trial = +1
            surprUSCurrTrial(CS2) = modelpred{1,subInd}.SO(2,sesstrials+1); 
            surprUSCurrTrial(CS3) = modelpred{1,subInd}.SO(3,sesstrials+1);
            surprUSCurrTrial(CS4) = modelpred{1,subInd}.SO(4,sesstrials+1);
            
            pmod(2).name  = {'US type','KL div prior-posterior current trial','Surprise on US current trial'};
            pmod(2).param = {US,KLdivCurrTrial',surprUSCurrTrial'};
            pmod(2).poly  = {1,1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_2.mat',subj(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
            
        end
        
    end
end

end