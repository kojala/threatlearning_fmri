clear all

opts = get_options();

% Find out which trials US+, which US- and what CS
for sub = 1:length(opts.subj_beh)
    
    clear bhvdata
    bhvdata = load(fullfile(opts.bhvpath,['S' num2str(opts.subj_beh(sub))],['Behavior_S' num2str(opts.subj_beh(sub)) '.mat']));
    
    US(sub,:) = [bhvdata.cond(1).US; bhvdata.cond(2).US; bhvdata.cond(3).US; bhvdata.cond(4).US; bhvdata.cond(5).US; bhvdata.cond(6).US];
    CS(sub,:) = [bhvdata.cond(1).CS; bhvdata.cond(2).CS; bhvdata.cond(3).CS; bhvdata.cond(4).CS; bhvdata.cond(5).CS; bhvdata.cond(6).CS];    
    
end

modelopts.roiname = 'Maint_trialxPE_PAG_p001uncorr';
condname = 'US';
US_betas = load(fullfile(opts.roimaskpath,'ModelChecks',['MeanBetas_' modelopts.roiname '_' condname '.mat']));

modelopts.roiname = 'Maint_trialxPE_PAG_p001uncorr';
condname = 'CS';
CS_betas = load(fullfile(opts.roimaskpath,'ModelChecks',['MeanBetas_' modelopts.roiname '_' condname '.mat']));

plotopts.trials = 25:200;%1:48;%[1:24; 201:224]';

US = US(:,plotopts.trials);
CS = CS(:,plotopts.trials);

% CS(CS==1) = 0;
% CS(CS==2) = 1/3;
% CS(CS==3) = 2/3;
% CS(CS==4) = 1;

betas_US_all = US_betas.betas_allsubs;
betas_CS_all = CS_betas.betas_allsubs;

US_betas_trials = betas_US_all(:,plotopts.trials(:));
CS_betas_trials = betas_CS_all(:,plotopts.trials(:));

USp = US_betas_trials(US==1); % US+ responses
USp = reshape(USp,[21,length(plotopts.trials)/2]);
CS_USp = CS_betas_trials(US==1);
CS_USp = reshape(CS_USp,[21,length(plotopts.trials)/2]);
PE_USp = ones(size(CS_USp,1),size(CS_USp,2))-CS_USp; % Positive PE

USm = US_betas_trials(US==0); % US- responses
USm = reshape(USm,[21,length(plotopts.trials)/2]);
CS_USm = CS_betas_trials(US==0);
CS_USm = reshape(CS_USm,[21,length(plotopts.trials)/2]);
PE_USm = zeros(size(CS_USm,1),size(CS_USm,2))-CS_USm; % Negative PE

USp_avg = nanmean(USp,2); % Mean US+ response over trials
USm_avg = nanmean(USm,2); % Mean US- response over trials
PE_USp_avg = nanmean(PE_USp,2);
PE_USm_avg = nanmean(PE_USm,2);

for sub = 1:21
    USp_meancorr(sub,:) = USp(sub,:)-USp_avg(sub); % Mean correction
    USm_meancorr(sub,:) = USm(sub,:)-USm_avg(sub);
    PE_USp_meancorr(sub,:) = PE_USp(sub,:)-PE_USp_avg(sub);
    PE_USm_meancorr(sub,:) = PE_USm(sub,:)-PE_USm_avg(sub);
end

USp_resp = USp_meancorr./PE_USp_meancorr;
USm_resp = USm_meancorr./PE_USm_meancorr;

USp_resp_mean = nanmedian(USp_resp);
USm_resp_mean = nanmedian(USm_resp);

% Plotting
% USpcolor = [144, 202, 249]./255;
% USmcolor = [25, 118, 210]./255;

%trials = repmat(plotopts.trials(:),size(betas_trials,1),1);
figure
% plot(USp_meancorr','o','Color','r');
%plot(USp_resp','o','Color','r');
plot(USp_resp_mean,'Color','r');
hold on
% plot(USm_meancorr','o','Color','b');
%plot(USm_resp','o','Color','b');
plot(USm_resp_mean,'Color','b');
ylim([-2 2])
%mean_trial = nanmean(betas_trials);
%scatter(1:48,mean_trial,'MarkerEdgeColor',binscatter_face,'MarkerFaceColor',binscatter_face)
% xL = get(gca,'XLim');
%meanX = nanmean(betas_trials(:));
%line(xL,[meanX meanX],'Color','b');
xlabel('Trials')
ylabel('US response / prediction error')
% title('Whole experiment')
title('Maintenance')
legend({'US+' 'US-'})
%set(gca,'XTick',25:176)