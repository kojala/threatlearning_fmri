function firstlevel_contrasts_axiomatic(opts,phase)

%% Initialize variables and folders
model_inp = get_modelconfig(opts,model);

% Loop over subjects (1st level analysis for each subject separately)
for subInd = 1:length(opts.subj)
    
    clear matlabbatch
    
    % Subject's path
    subpath = fullfile(opts.fpath, char(opts.subj(subInd)));
    cd(subpath)
    
    % 1st level path
    fpath_first = fullfile(subpath, '1stlevel', ['Version_' opts.version], ['NoiseCorr_' opts.noisecorr_name], model_inp.modelname, ['First_Level_' opts.phasenames{phase}]);
    
    % Create the folder if does not exist
    if ~exist(fpath_first, 'dir')
        mkdir(fpath_first)
    end
    
    %% Create contrasts and matlabbatch for contrast manager:
    
    matlabbatch{1}.spm.stats.con.spmmat = {fullfile(fpath_first, 'SPM.mat')};
    
    for con = 1:model_inp.no_contr
        
        matlabbatch{1}.spm.stats.con.consess{con}.tcon.name = model_inp.connames{con};
        matlabbatch{1}.spm.stats.con.consess{con}.tcon.weights = model_inp.conweights(:,con);
        matlabbatch{1}.spm.stats.con.consess{con}.tcon.sessrep = opts.con_sessrep; % Contrasts are replicated and scaled across runs
        matlabbatch{1}.spm.stats.con.delete = opts.con_delete;
    
    end

%% Run matlabbatch
    spm_jobman('run', matlabbatch);
    
    save(fullfile(fpath_first,'batch_s_Contrasts'), 'matlabbatch')
end

end