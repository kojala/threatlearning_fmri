%% fMRI analysis pipeline

clear variables

% Folders
mainpath   = 'D:\fMRI_Karita\';
fpath      = fullfile(mainpath, 'fMRIdata_biascorrected');
fpath_mvpa = fullfile(mainpath, 'fMRIdata_MVPA');
codepath   = fullfile(mainpath, 'GitLabRepository');
physiopath = fullfile(mainpath, 'PhysioRegressors');

% Add paths
addpath(mainpath)
addpath(fpath)
addpath(genpath(codepath))
addpath 'D:\spm12'

% Set SPM settings
spm('defaults', 'FMRI');
spm_jobman initcfg
spm_get_defaults('cmdline',true) % No graphical elements during analysis

%% Main script options
% Run different scripts
run_createbehav     = false;    % Creating behavioural files
run_checklearning   = false;    % Check learning of CS-US contingencies
run_createphysioreg = false;    % Create noise correction regressors (only needed for RETROICOR & full PhysIO)
run_unzipnii        = false;    % Unzipping NIFTI files
run_fmripreproc     = false;    % fMRI preprocessing
run_checkreg        = false;    % Check preprocessed images
run_checkheadmotion = false;    % Check head motion
run_fmri1stlvl      = true;    % fMRI 1st level (within-subject) analysis
run_preparebehav    = true;    % Multiple conditions file for parametric models
run_1stlvlmodel     = true;
run_1stlvlcontrasts = false;
run_copycontrasts   = false;    % Copy all subjects' contrasts and masks into 2nd level folder
run_createexplmask  = false;    % Create explicit mask for 2nd lvl from all subjects' individual masks
run_fmri2ndlvl      = false;    % fMRI 2nd level (group) analysis
printresults        = false;    % Print results of 2nd level analyses

run_mvpa_prepbehav  = false;    % Multiple conditions file for MVPA
run_mvpa_1stlevel   = false;    % MVPA 1st level trialwise GLM (within-subject)

% Version number
version = '11062018';

% Noise correction level
noisecorr = 2;
plotting  = false;
% 1 = only head motion correction (6 regressors)
% 2 = head motion and RETROICOR physiological noise correction (6 + 18)
% 3 = head motion and full PhysIO physiological noise correction (6 + 20)

if noisecorr == 1;
    noisecorr_name = 'HeadMotion';
    % nNoiseReg = 6;
elseif noisecorr == 2;
    noisecorr_name = 'RETROICOR';
    % nNoiseReg = 24;
elseif noisecorr == 3;
    noisecorr_name = 'PhysIO';
    % nNoiseReg = 26;
end

% Phase of the experiment
phase = 1:3; %[5 6];
phasename = {'Acq','Maint','Acq2','AllPhases','BothAcq','AllPhases_Scaled'};
% 1 = Acquisition
% 2 = Maintenance
% 3 = Re-learning / Acquisition 2
% 4 = All phases together
% 5 = Both acquisition phases
% 6 = All phases together, scaled according to run length

% Models included in this analysis
models = 2:5;%[2:5 7:15];
modelname = {'Model_Cat_Axiomatic' 'Model_Par_1' 'Model_Par_1b' 'Model_Par_1c'...
    'Model_Par_1d' 'Model_Par_1e' 'Model_Par_2'...
    'Model_Par_2_reg1' 'Model_Par_2_reg2' 'Model_Par_2_reg3' 'Model_Par_2_reg4'...
    'Model_Par_2_reg5' 'Model_Par_2_reg6' 'Model_Par_2_reg7' 'Model_Par_2_reg8'};
modelcons = [13 5 5 5 5 7 10 3 3 3 3 3 3 3 3]; % Number of contrasts for each model
% 1 = Axiomatic approach
% 2 = Simple prediction error model
% 3 = Prediction error model with expected p(shock) from the Bayesian model
% 4 = Bayesian learning model
% 1  = Contrast weights for 2nd level t-tests set as 
direction_2ndlvl_ttest = 1;
% -1 = Contrast weights for 2nd level t-tests set at -1

% Subject list
subj      = dir(fpath);
subj      = {subj(:).name}';
subj(1:2) = [];
subj      = subj(not(cellfun('isempty', strfind(subj,'PR'))));

subj_beh = [159:162 164:167 169:177 179:182];

% if noisecorr > 1
%     subj = subj(2:end); % PR1905 does not have physio data
%     subj_beh = subj_beh(2:end);
% end

% Running a specific list of subjects
% startsubj = 1;
% endsubj   = 1;
% 
% subj = subj(startsubj:endsubj);
% subj_beh = subj_beh(startsubj:endsubj);

% Compiled options
opts.mainpath = mainpath;
opts.fpath = fpath;
opts.codepath = codepath;
opts.physiopath = physiopath;
opts.version = version;
opts.noisecorr = noisecorr;
opts.noisecorr_name = noisecorr_name;
opts.plotting = plotting;
opts.phase = phase;
opts.phasename = phasename;
opts.models = models;
opts.modelname = modelname;
opts.modelcons = modelcons;
opts.subj = subj;
opts.subj_beh = subj_beh;

%% Behaviour
%-------------------------------------------------------------------------
if run_createbehav %#ok<*UNRCH>
    create_behavfiles(opts) % Create behavioural files
end

if run_checklearning
    check_CSUSlearning(opts) % Check conscious awareness of CS-US contingency
end

%% Psychophysiological data
%-------------------------------------------------------------------------
if run_createphysioreg
    % PhysIO toolbox modelling psychophysiological noise
    % Create a nuisance regressor file for SPM fMRI analysis
    % 155 included in the fMRI analysis sample but no physio file
    % 157, 168 and 178 out because not included in the fMRI sample
    create_physioreg(opts)
end

%% fMRI
%-------------------------------------------------------------------------

%% 0. Unzipping
if run_unzipnii
    % Need DICOM to NIFTI converted files for unzipping
    unzip_niftiis(opts)
end

%% 1. Preprocessing
if run_fmripreproc
    
    %---------------------------------------------------
    % 1.1 Combine multiple echoes of an fMRI time series
    %---------------------------------------------------
    combine_echoes(opts)
    
    %----------------------------------------------------------------
    % 1.2 Preprocessing including EPI bias correction and field maps
    %---------------------------------------------------------------
    preprocessing_withFieldmaps(opts)

end

%-----------------------
% 1.3 Data quality check
%-----------------------
if run_checkreg
    check_registration(opts)
end

%----------------------
% 1.4 Head motion check
%----------------------
if run_checkheadmotion
    check_headmotion(opts)
end

%% 2. First level analysis
if run_fmri1stlvl
    
    %-------------------------------------------------
    % 2.1 1st level model specification and estimation
    %-------------------------------------------------
    
    for p = phase
        
        if run_preparebehav
            % Create multiple conditions files for parametric models
            prepare_behav(opts,p)
            prepare_behav_Model2_allregressors_separatemodels(opts,p)
        end
        
        if run_1stlvlmodel
            for m = models
                if m == 1 % Axiomatic categorical model
                    firstlevel_CSUSconditions_axiomatic(opts,p)
                else % Parametric models
                    firstlevel_CSUSconditions_parmod(opts,m,p)
                end
            end
        end
        
        %------------------------
        % 2.2 1st level contrasts
        %------------------------
        if run_1stlvlcontrasts
            for m = models
                if m == 1
                    firstlevel_contrasts_axiomatic(opts,p)
                else
                    firstlevel_contrasts_parmod(opts,m,p)
                end
            end
        end
        
    end

end

if run_copycontrasts
    
    %-------------------------------------------------
    % 2.3 Copy contrasts and masks to 2nd level folder
    %-------------------------------------------------
    
    for m = models
        for p = phase
            copy_contrasts(opts,m,p)
        end
    end

end

%% 3. Second level analysis
if run_fmri2ndlvl
    
    %-------------------------------------------------
    % 3.1 2nd level model specification and estimation
    %-------------------------------------------------
    for m = models
        
        for p = phase
            
            %------------------------------------------
            % Create explicit mask from subjects' masks
            %------------------------------------------
            
            seclvlpath = fullfile(fpath,'2ndlevel',['Version_' version],['NoiseCorr_' noisecorr_name],modelname{m},phasename{p});
            
            if run_createexplmask
                create_explicitmask(fullfile(seclvlpath,'data','masks'),seclvlpath,subj) % This script is not correct!
            end
            
            %--------------------
            % Run 2nd level tests
            %--------------------
            secondlevel_tests(seclvlpath,m,direction_2ndlvl_ttest,printresults)

        end
        
    end

end

%% 4. MVPA
if run_mvpa_prepbehav
    
    for p = phase
    
        prepare_behav_MVPA_trialwise(mainpath,subj_beh,p);
    
    end
    
end

if run_mvpa_1stlevel
    
    for p = phase
        
        firstlevel_MVPA_trialwise(mainpath,fpath_mvpa,subj,subj_beh,p,noisecorr,noisecorr_name)
    
    end
end