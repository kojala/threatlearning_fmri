%Find correct cluster extent
mainpath = fullfile(cd,'..','..','..');
fpath = fullfile(mainpath,'fMRIdata_biascorrected');
SPMpath = fullfile(fpath,'2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1\Maint\1sttest_parmod_US_PE');
SPMfile = fullfile(SPMpath,'SPM.mat'); 
SPM = load(SPMfile);
SPM = SPM.SPM;

u = 0.0005;
alpha = 0.025;
guess = NaN;

[k, Pc] = CorrClusTh(SPM,u,alpha,guess);