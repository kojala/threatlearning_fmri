
model = 2; % Model 1a
phase = 2; % Maintenance
runs = 2:5;

opts = get_options();
model_inp = get_modelconfig(opts,model);

sublist = 1:21;
subj = opts.subj(sublist);

mripath = opts.fpath;

roifile = fullfile(opts.roimaskpath,'Functional','PE_cluster1_p001cluscorr.nii');
roiname = 'frontalPE';

spm('defaults', 'FMRI');
spm_jobman initcfg
spm_get_defaults('cmdline',opts.spm_cmdline) % No graphical elements during analysis

for sub = 1:length(subj)
    
     sub_mripath = fullfile(mripath, subj(sub), '1stlevel', ['Version_' opts.version], ['NoiseCorr_' opts.noisecorr_name], model_inp.modelname, ['First_Level_' opts.phasenames{phase}]);
     spmfile = char(fullfile(sub_mripath,'SPM.mat'));
        
    if strncmp(subj{sub},'PR02121',7)
        runs = [2 4 5];
    end
    
    for run = runs

        voi.spmmat = {spmfile};
        voi.adjust = 0;
        voi.session = run;
        voi.name = ['sub' num2str(sub) '_run' num2str(run) '_' roiname];
        voi.roi{1}.mask.image = {roifile};
        voi.roi{1}.mask.threshold = 0.5;
        voi.expression = 'i1';
        
        matlabbatch{1}.spm.util.voi = voi;
        
        spm_jobman('run', matlabbatch);

    end
    
end