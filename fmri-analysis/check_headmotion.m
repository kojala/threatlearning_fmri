function check_headmotion(opts)
%Manually check head motion for each subject

head_path = fullfile(opts.mainpath,'HeadMotion'); % Head motion files

for subInd = 1:length(opts.subj)
    
    subpath = fullfile(opts.mainpath,char(opts.subj(subInd)));
    cd(subpath) % Go to subject's directory
    
    im_range = get_imagerange(opts.subj(subInd)); % Get range of EPIs
    
    figure % Initialize head motion figure
    set(gcf, 'Units', 'Normalized', 'Position', [0 0.1 0.5 0.9]);
    
    for img = 1:length(im_range) % Loop over EPI scans
        
        volno = num2str(im_range(img));
        if length(volno) < 2
            volno = ['0', volno]; %#ok<AGROW>
        end
        
        headfile = ls(fullfile(subpath,'EPI',['*ac*',volno, 'a001.txt'])); % Head motion file
        
        headmotion = importdata(fullfile(subpath,'EPI',headfile)); % Import head motion data
        
        % Translation
        subplot(6,2,2*img-1)
        plot(headmotion(:,1:3)) % Columns 1-3
        if img == 1
            title('Translation')
        end
        
        ylabel(['Block ', num2str(img)])
        
        % Rotation
        subplot(6,2,2*img)
        plot(headmotion(:,4:6)) % Columns 4-6
        if img == 1
            title('Rotation')
        end

        input(['ENTER:motion parameters, Block: ', num2str(img)])
        
        clear headmotion
    
    end
    
    print([head_path, headfile,'.tiff'],'-dtiff') % Save head motion plots
    close
    input('ENTER: Proceed with next subject')
    
end

end