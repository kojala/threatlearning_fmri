function modelcheck_extractbetas(opts,sublist,modelopts)
%Extract trial-wise beta values for model checking purposes

mripath = opts.fpath_mvpa; % Data path (MVPA path due to trial-wise models originally created for exploratory MVPA)
subj = opts.subj(sublist);

% Masks for the models and contrasts of interest ("ROIs")
roifile = fullfile(opts.roimaskpath,'ModelChecks',[modelopts.roiname '.nii']);

% Initialize beta value matrix for all subjects over the 224 trials of the
% whole experiment
betas_allsubs = nan(length(subj),224);

%% Beta extraction for selected ROI
for sub = 1:length(subj)

    sub_mripath = fullfile(mripath, subj(sub), '1stlevel', ['Version_' opts.version_mvpa], ['NoiseCorr_' opts.noisecorr_name]);

    betafiles = spm_select('FPList',sub_mripath,'^beta.*nii'); % Find beta files
    
    spmfile = char(fullfile(sub_mripath,'SPM.mat')); % Find SPM files
    spmdata = load(spmfile);
    
    eventnames = spmdata.SPM.xX.name; % Names of events
    
    events = zeros(1,length(eventnames)); % Initialize events array
    
    if modelopts.condition == 1 % If CS event
        condname = 'CS';
    elseif modelopts.condition == 2 % If US event
        condname = 'US';
    end
    
    % Find events and track the event (CS or US) and trial number
    eventnum = 0;
    %missingUSind = [];
    for event = 1:length(eventnames)
        % Record event of interest (CS or US)
        if ~isempty(strfind(eventnames{1,event},condname))
            eventnum = eventnum + 1;
            events(event) = eventnum;
        end
        % Record missing US if event of interest is US
        if modelopts.condition == 2 && ~isempty(strfind(eventnames{1,event},'CS')) && isempty(strfind(eventnames{1,event+1},'US'))
            eventnum = eventnum + 1;
            events(event+1) = 0;
            %missingUSind = [missingUSind eventnum]; for checking purposes
        end
    end
    
    % Find beta values for the events
    eventbeta = betafiles(find(events>0),:); %#ok<*FNDSB>

    % ROI coordinates
    Y = spm_read_vols(spm_vol(roifile),1); % Y is 4D matrix of image data
    indx = find(Y>0);
    [x,y,z] = ind2sub(size(Y),indx);
    XYZ = [x y z]';
    
    bfiles = eventbeta; 
    
    % Extract betas from the ROI for this condition
    if strncmp(subj{sub},'PR02121',7) % This subject lacks run 3, padding with nans
        betas = nan(1,224-44); % Leaves NaNs for missing USs
        betas(events(events>0)) = nanmean(spm_get_data(bfiles,XYZ),2);
        betas = [betas(1:68) nan(1,44) betas(69:end)];
    else
        betas = nan(1,224); % Leaves NaNs for missing USs
        betas(events(events>0)) = nanmean(spm_get_data(bfiles,XYZ),2);
    end
    
    % Store mean beta for each trial
    betas_allsubs(sub,:) = betas; 
    
end

save(fullfile(opts.roimaskpath,'ModelChecks',['MeanBetas_' modelopts.roiname '_' condname '.mat']),'betas_allsubs');

% Figure of all trial-wise betas of all subjects for checking purposes (are
% there any aberrant values)
%figure;imagesc(betas_allsubs); title('Betas PAG US'); ylabel('Subjects');xlabel('Trials'); set(gca,'YTick',1:21); colorbar

end