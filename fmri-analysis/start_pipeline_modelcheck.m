%% fMRI analysis pipeline wrapper script - Model checks

% This pipeline is used for article Supplementary Figure S3 C

clear all

%% Subjects to run
startsubj = 1;
endsubj   = 21;
sublist = startsubj:endsubj;

%% Get options
addpath(fullfile(cd,'utils'))
opts = get_options(); % Get general options

%% Define ROI/mask (filename)
% Manually created in SPM by exporting (a) significant cluster(s) as a binary 
% mask at a specific threshold for the contrast-of-interest
% E.g. p < 0.001 uncorrected
modelopts.roiname = 'volatility_BothAcq_cluster1_p001';
% modelopts.roiname = 'BothAcq_volatility_allclusters_p001cluscorr';
% modelopts.roiname = 'BothAcq_exptpshock_amygdalaR_p001uncorr';
% modelopts.roiname = 'Acq_exptpshock_amygdalaR_p001uncorr';
% modelopts.roiname = 'BothAcq_exptpshock_amygdalaR_SVC';
% modelopts.roiname = 'Acq2_modelPE_SNVTA_p001uncorr';
% modelopts.roiname = 'Maint_trialxPE_PAG_p001uncorr';
% modelopts.roiname = 'PAG';
% modelopts.roiname = 'BilateralAmygdala';

%% Model options
modelopts.modelname = 'Model_Par_2'; % Name of the relevant model predictions file
modelopts.condition = 1; % 1 = Conditioned Stimulus (CS), 2 = Uconditioned Stimulus (US)
modelopts.regnum    = 2; % Regressor number, depends on the model. Check from get_modelconfig(). E.g. Model2 CS regum 1 = expected p(shock), 2 = volatility
modelopts.regname   = 'Expectation uncertainty'; % Regressor name for plotting
modelopts.blocks    = [1 6]; % Which scanning runs included

%% Plot options
plotopts.nbins = 15; % Number of bins for plotting values
plotopts.trials = [1:24; 201:224]'; % Trials to include in the plot
plotopts.plotall = false; % Plot individual trial values in the background
plotopts.set_x_reverse = true; % Reverse the x-axis (for certain regressors, e.g. volatility, which decreases over time)

%% Run options
run_checkprepbeh = false; % Check GLM structures created with prepare_behav() function
run_extractbetas = false; % Extract trial-wise beta (BOLD signal) values
run_binplot      = false; % Plot model check with binned beta values

%% Run

if run_checkprepbeh
    check_prepbehfiles(opts,sublist,modelopts); %#ok<UNRCH>
end

if run_extractbetas
    modelcheck_extractbetas(opts,sublist,modelopts); %#ok<UNRCH>
end

if run_binplot
    modelcheck_binplot(opts,sublist,modelopts,plotopts); %#ok<UNRCH>
end