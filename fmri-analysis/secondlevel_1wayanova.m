function secondlevel_1wayanova(fpath,connums,conjname,actualnames)
%2nd level analysis 1-way ANOVA (used for conjunction analysis only)

contrasts = length(connums);

%% 2nd level model specification
for c = 1:contrasts
    factorial_design.dir = {fullfile(fpath,conjname)};
    factorial_design.des.anova.icell(c).scans = cellstr(spm_select('ExtFPList',fullfile(fpath,'data','contrasts'),['con_00' connums{c} '.*.nii$'],1));
end
factorial_design.des.anova.dept = 0;
factorial_design.des.anova.variance = 1;
factorial_design.des.anova.gmsca = 0;
factorial_design.des.anova.ancova = 0;
factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
factorial_design.masking.tm.tm_none = 1;
factorial_design.masking.im = 1;
factorial_design.masking.em = {''}; %cellstr(spm_select('ExtFPList',fullfile(fpath),'explicitmask.nii$',1));
factorial_design.globalc.g_omit = 1;
factorial_design.globalm.gmsca.gmsca_no = 1;
factorial_design.globalm.glonorm = 1;

matlabbatch{1}.spm.stats.factorial_design = factorial_design;

%% 2nd level model estimation
fmri_est.spmmat(1) = cellstr(fullfile(fpath,conjname,'SPM.mat'));
fmri_est.write_residuals = 0;
fmri_est.method.Classical = 1;

matlabbatch{2}.spm.stats.fmri_est = fmri_est;

%% Contrasts

con.spmmat = {fullfile(fpath,conjname,'SPM.mat')};

for c = 1:contrasts
    conweights = zeros(1,contrasts);
    %conweights(1:22) = 1:22; Need to add subjects?
    conweights(c) = 1;
    
    if strcmp(conjname,'conjunction_axioms_unsignedPE') && (strcmp(connums{c},'05') || strcmp(connums{c},'06'))
        conweights(c) = -1;
    end
    
    con.consess{c}.fcon.name = actualnames{c};
    con.consess{c}.fcon.weights = conweights;
    con.consess{c}.fcon.sessrep = 'none';
end

con.delete = 1;

matlabbatch{3}.spm.stats.con = con;

%% Run matlabbatch
spm_jobman('initcfg')
spm_jobman('run', matlabbatch)

end