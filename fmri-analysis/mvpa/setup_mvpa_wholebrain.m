function setup_mvpa_wholebrain(opts,sublist,version)

data = struct([]);

for sub = 1:length(sublist)
    
    % SPM
    SPMpath = fullfile(opts.fpath_mvpa,char(opts.subj(sublist(sub))),'1stlevel', ['Version_' version], ['NoiseCorr_' opts.noisecorr_name], 'SPM.mat');
    SPMfile = load(SPMpath);
    
    % Find betas for the US events in the Maintenance phase only
    trial_cs_ind = strfind(SPMfile.SPM.xX.name,'CS'); % Take beta values associated with CS responses
    trial_cs_ind_cell = not(cellfun('isempty', trial_cs_ind));
    trial_us_ind = strfind(SPMfile.SPM.xX.name,'US'); % Take beta values associated with US responses
    trial_us_ind_cell = not(cellfun('isempty', trial_us_ind));
    % Compare CS and US indices to find out Maintenance US indices
    maintbetas = nan(length(trial_cs_ind_cell),1);
    cs = 0;
    us = 0;
    for beta = 1:length(trial_cs_ind_cell)
        if trial_cs_ind_cell(beta) == 1 && trial_us_ind_cell(beta+1) == 1
            cs = cs+1;
            us = us+1;
            if (cs > 24) && (cs < 201); maintbetas(beta) = beta; end
        elseif trial_cs_ind_cell(beta) == 1 && trial_us_ind_cell(beta+1) == 0
            cs = cs+1;
        end
    end
    
    betalist = spm_select('FPList',fullfile(opts.fpath_mvpa,char(opts.subj(sublist(sub))),'1stlevel', ['Version_' version], ['NoiseCorr_' opts.noisecorr_name]), 'beta*');
    SPM.xY.VY = cellstr(betalist(maintbetas(~isnan(maintbetas)),:)); % filenames char array OR struct array of images
    SPM.VM = [];
    
    data(sub).SPM = SPM;
    
    % SVM function to use
    data(sub).fun = '@spm_svm_v3_fmelinscak';
    
    % Search options
    searchopt.def = 'sphere';
    voxelsize = 3;
    searchopt.spec = 3*voxelsize; % mm radius (setting from Kahnt et al., 2014 PNAS)
    searchopt.name = 'truePE_wholebrainMVPA';
    data(sub).searchopt = searchopt;
    
    % Target values
    targetpath = fullfile(opts.bhvpath,'Model_Par_1_TruePredictionError.mat');
    targetfile = load(targetpath);
    
    targetvalues = targetfile.regdata_all(sublist(sub),:)';
    targetvalues = targetvalues(25:200); % maintenance only
    targetvalues_final = targetvalues(~isnan(targetvalues)); % remove NaN trials
    
    % Further input arguments
    varargin.target = targetvalues_final;   % n x 1 vector values for SVR
    k = 4; % number of folds
    repetition = repmat(1:k,ceil(length(targetvalues))/k,1);
    %repetition = [1*ones(44,1) 2*ones(44,1) 3*ones(44,1) 4*ones(44,1)];
    %repetition = repmat(1:k,1,ceil(length(targetvalues)/k));
    %repetition = repetition(1:length(targetvalues))';
    repetition_final = repetition(~isnan(targetvalues)); % remove the fold numbers that are for NaN target values
    varargin.repetition = repetition_final; % k-fold for cross-validation
    varargin.featnum    = 300;  % 0 (no feature selection) or number of features
    % do mass-univariate feature selection by computing voxel-wise
    % shared variance between training data and training target
    varargin.permtest   = 0; % number of permutations
    data(sub).varargin = varargin;
    
end

mvpa_opts = data;

save(fullfile(opts.fpath_mvpa,'mvpa_svr_wholebrain_options.mat'),'mvpa_opts','-v7.3')

end