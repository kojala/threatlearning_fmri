function A = spm_svm_v2(M, L, options)
% this function implements a linear binomial C-SVM, or epsilon-SVR, with LIBSVM
% this function can e.g. be called by spm_searchlight
% FORMAT A = spm_svm(M, L, options)
%       with M: a [n x v] matrix (nb images x nb voxels within searchlight)
%            L: a [3 x v] matrix of voxels location within searchlight
%               {vox} (ignored)
%            options
%               .classlabel: a n x 1 vector containing 0 and 1 defining 
%                   classes for a binomial SVM, data points labeled NaN will be
%                   ignored 
%               OR
%               .target: a n x 1 vector containing continouos values for an
%                   SVR. 
%               .repetition (1..k for k-fold leave-one-out)
%               .featnum (do mass-univariate feature selection? 0 or feature number, default 0)
%               .permtest (do permutation test? 0 or number of repetitions)
%            A: classification accuracy. (1) raw accuracy/EV
%                                        (2) for SVR: Fisher-transformed cc
%                                        if doing permutation test:
%                                        (3) baseline-corrected accuracy/EV 
%                                        (4) baseline-corrected Ft-cc (SVR)
%                                        (5)/(6) permutation test p-values
%__________________________________________________________________________
% 13.05.2010 Dominik R Bach (version used for HRA - Bach Weiskopf Dolan 2011)
% 18.07.2014 Dominik R Bach (version used for AOV)
% 01.10.2014 Dominik R Bach (version used for AOV MEG - excludes constant
% voxels/channels)
% 22.04.2016 Saurabh Khemka (vesrion modified to correct p-value
% calculation)
A = [];

% check input & initialise options structure
% -------------------------------------------------------------------------
narginchk(3, 3);
if isfield(options, 'classlabel')
    mod = 1;
    target = options.classlabel(:);
    callstring = '-s 1 -t 0';
elseif isfield(options, 'target')
    mod = 2;
    target = options.target(:);
    callstring = '-s 3 -t 0 -c 0.01'; % Obtained from Kahnt et. al 2015 
else
    warning('\noptions.classlabel OR options.target must be given'); return;
end;
try repetition = options.repetition(:); catch warning('options.repetition is a mandatory field.'); return; end;
try featnum = options.featnum; catch featnum = 0; end;
try permtest = options.permtest; catch permtest = 0; end;
    
% check input arguments
% -------------------------------------------------------------------------
exclude = all(reshape(isnan(target), size(target)),2);
if mod == 1
    if any(~ismember(target(~exclude), [0 1])), warning('Only 0 and 1 are valid class labels (use NaN for data points to be ignored).'); return; end;
end;
if numel(unique(repetition(~exclude))) ~= max(repetition(~exclude)), warning('Repetitions should be numbered 1..k.'); return; end;
k = max(repetition(~exclude)); % i.e. k-fold cross-validation

% exclude datapoints
% -------------------------------------------------------------------------
M(exclude, :) = [];
target(exclude, :) = [];
repetition(exclude) = [];

% exclude voxels with NaN, inf or constant values & adjust feature number
% -------------------------------------------------------------------------
M(:, sum(isinf(M)|isnan(M)) + all(diff(M, 1)==0) > 0) = [];
featnum = min([size(M, 2), featnum]);

% do the job
% -------------------------------------------------------------------------
 for cv = 1:k
     % define training & test dataset
     trainselect = repetition ~= cv;
     testselect  = repetition == cv;
     % define training & test labels
     traintarget = target(trainselect, :);
     testtarget  = target(testselect, :);
     % define training & test data
     fulltraindata = M(trainselect, :);
     fulltestdata = M(testselect, :);
     % rescale data voxel-wise
     trainmean = mean(fulltraindata, 1);
     trainstd  = std(fulltraindata, 1);
     fulltraindata = (fulltraindata - repmat(trainmean, size(fulltraindata, 1), 1))./repmat(trainstd, size(fulltraindata, 1), 1); 
     fulltestdata  = (fulltestdata -  repmat(trainmean, size(fulltestdata, 1), 1)) ./repmat(trainstd, size(fulltestdata, 1), 1); 
     % define train data
     if featnum > 0
         % do mass-univariate feature selection by computing voxel-wise 
         % shared variance between training data and training target
         cc = (corrcoef([traintarget, fulltraindata])).^2;
         [vx, ind] = sort(cc(2:end, 1), 1, 'descend');
         featselect = ind(1:featnum);
         traindata = fulltraindata(:, featselect);
     else
         featselect = 1:size(fulltraindata, 2);
         traindata = fulltraindata;
     end;
     % define test data
     testdata = fulltestdata(:, featselect);
     % train & test
     for n = 1:(permtest + 1)
%          if mod(n, 100) == 0 || n == 1
%             fprintf('Permutation # %03.0f\n', n);
%          end
         % permute labels both in training & test data
        if n > 1
            traintarget = traintarget(randperm(numel(traintarget)));
            testtarget = testtarget(randperm(numel(testtarget)));
        end;
        % train
        [T, model] = evalc('svmtrain_m(traintarget, traindata, callstring);');
        % test
        [T, foo, bar, foobar] = evalc('svmpredict_m(testtarget, testdata, model);');

        if mod == 1
            acc(n, cv) = bar(1); % accuracy
        elseif mod == 2
            acc(n, cv) = 1 - bar(2)/var(testtarget, 1); % EV = (MSStotal - MSE)/MSStotal
            % note: LIBSVM also outputs R2, but this is for a regression
            % from predicted to observed, involving two more parameters
            
            % here I just specify the Fisher-transformed correlation
            % coefficient predicted/observed as in e. g. Kahnt et al. PNAS 2014
            Ftcc(n, cv) = atanh(corr(testtarget, foo));
            
            if n == 1
                rsquare(n, cv) = bar(3);
            end
        end;
    end;
end;

% average accuracy across k-fold procedure
acc = mean(acc, 2);
if mod == 2
    Ftcc = mean(Ftcc, 2);
end;

% extract mean accuracy/MSE
A(1) = acc(1);
if mod == 2
    A(2) = Ftcc(1);
end;
% and the corrected accuracy
if permtest > 0
    A(3)  = acc(1) - mean(acc(2:end));
    if mod == 2
        A(4) = Ftcc(1) - mean(Ftcc(2:end));
    end;
end;

% and the p-value
if permtest > 0
    A(5)  = 1- sum(acc(1) > (acc(2:end)))/permtest; % Modified to obtain accurate p values
    if mod == 2
        A(6) =  1 - sum(Ftcc(1) > (Ftcc(2:end)))/permtest; % Modified to obtain accurate p values
    end; 
end;

return;
