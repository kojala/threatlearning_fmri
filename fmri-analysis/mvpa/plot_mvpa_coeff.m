function plot_mvpa_coeff(opts,rois)

for roi = 1:length(rois)
    
    MVPAdatafile = fullfile(opts.fpath_mvpa,['mvpa_svr_results_' opts.roinames_mvpa{roi} '.mat']);
    MVPAdata = load(MVPAdatafile);
    
    for sub = 1:size(MVPAdata.data_out,2)
        coeffs(sub) = MVPAdata.data_out(sub).classification(2);
    end
    
%     figure
%     histogram(coeffs,10)
%     title(['Histogram of Fisher-transformed corr coeff: ' opts.roinames_mvpa{roi}]);
    
    [h,p] = ztest(coeffs,0,1);
    
    fprintf([opts.roinames_mvpa{roi} '\n'])
    coeffs'
    h
    p
    fprintf('--- \n')
    
    results(1:size(MVPAdata.data_out,2),roi) = coeffs';
    results(size(MVPAdata.data_out,2)+1,roi) = p;
    results(size(MVPAdata.data_out,2)+2,roi) = h;
%     results(roi).coeffs = coeffs;
%     results(roi).h = h;
%     results(roi).p = p;
    
end

save(fullfile(opts.fpath_mvpa,['mvpa_svr_results_ztest.mat']),'results')

end