function run_mvpa_svr_searchlight(opts,sublist)

%data_out = struct([]);

% SPM
optspath = fullfile(opts.fpath_mvpa,'mvpa_svr_wholebrain_options.mat');
optsfile = load(optspath);

for sub = 1:length(sublist)
    
    subopts = optsfile.mvpa_opts(sublist(sub));
    
    resultsdir = fullfile(opts.fpath_mvpa,['Subjects' num2str(sublist(1)) '-' num2str(sublist(end))]);
    if ~exist(resultsdir,'dir'); mkdir(resultsdir); cd(resultsdir); end
    fprintf(['Subject ' num2str(sublist(sub)) '\n'])
    %data_out(sub).classification = spm_searchlight_m(subopts.SPM,subopts.searchopt,subopts.fun,subopts.varargin);
    data_out = spm_searchlight_m(subopts.SPM,subopts.searchopt,subopts.fun,subopts.varargin);
   
    save(fullfile(opts.fpath_mvpa,resultsdir,['mvpa_svr_wholebrain_results_subject' num2str(sublist(sub)) '.mat']),'data_out')
    
end

end