function run_mvpa_svr(opts,sublist,rois)

data_out = struct([]);

% SPM
optspath = fullfile(opts.fpath_mvpa,'mvpa_svr_options.mat');
optsfile = load(optspath);

for roi = 1:length(rois)
    
    for sub = 1:length(sublist)
        
        subopts = optsfile.mvpa_opts(roi).subdata(sub);
        
        fprintf(['Subject ' num2str(sublist(sub)) '\n'])
        data_out(sub).classification = spm_maskonly(subopts.SPM,subopts.fun,subopts.varargin);
        
    end
    
    save(fullfile(opts.fpath_mvpa,['mvpa_svr_results_' opts.roinames_mvpa{rois(roi)} '.mat']),'data_out')
    
end

end