% Intended usage of MVPA functions for project AAA MRI (check whether it works)
% -------------------------------------------------------------------------
% Dominik R Bach 23.11.2017

% for all analyses, create
% - a vector of beta images (a) per trial or (b) per condition within run
% - a vector of the same size of target values: (a) 1/0/NaN (options.classlabel) or (b) continuous (options.target) 
% - a vector of the same size of k-fold leave-1-out cross validation: (a)
% for trial-wise beta images they should be numbered throughout, e. g. 1 2
% 3 4 1 2 3 4 ... (b) for condition-within-run wise images, all conditions
% within one run should get the same number, each run should get a
% different number

% the output from both functions is
% for maskonly: a vector of 6 numbers
% for searchlight: 6 images with one number per voxel
% and the 6 numbers are:
%                                        (1) raw accuracy/EV
%                                        (2) for SVR: Fisher-transformed cc
%                                        if doing permutation test:
%                                        (3) baseline-corrected accuracy/EV 
%                                        (4) baseline-corrected Ft-cc (SVR)
%                                        (5)/(6) permutation test p-values

%__________________________________________________________________________
% spm_maskonly(SPM, fun, options)  returns a vector of MVPA accuracy 
% measures for a mask image
% input:
% SPM       - structure with fields:
%    .xY.VY - n beta images: filenames as char array or spm_vol struct array of images
%    .VM    - filename or spm_vol structure to a mask (binary) image.
%             Orientation & resolutin don't matter for the function, should ideally
%             have original (T1 space) resolution
% fun       - should be @spm_svm_v2
% options   - EITHER .classlabel: a n x 1 vector containing 0 and 1 defining 
%                   classes for a binomial SVM, data points labeled NaN will be
%                   ignored (for example, high threat 1, low threat 0,
%                   medium threat NaN)
%              OR .target: a n x 1 vector containing parametric target for an
%                   SVR (i. e. low threat -1, medium threat 0, high threat
%                   1)
%             .repetition (1..k for k-fold leave-one-out)
%             .featnum (do mass-univariate feature selection? 0 or feature number, default 0)
%             .permtest (do permutation test? 0 or number of repetitions)

% suggested sanity check: save significant result from mass-univariate
% analysis as cluster, and use as mask for this SVM, should ideally return
% a "significant" result. do the same for a region outside the brain.

%__________________________________________________________________________
% spm_searchlight_m(SPM, fun, options) writes images of MVPA accuracy for
% searchlight within a mask image
% input:
% SPM       - structure with fields:
%    .xY.VY - n beta images: filenames as char array or spm_vol struct array of images
%    .VM    - filename or spm_vol structure to a mask (binary) image.
%             Orientation & resolutin don't matter for the function, should ideally
%             have original (T1 space) resolution
% searchopt - searchlight options using VOI structure (xY) from spm_ROI
%    .def   - searchlight definition {['sphere'] 'box'}
%    .spec  - searchlight parameters [sphere radius {mm}]
%    .name  - name trunk for output images
% fun       - should be @spm_svm_v2
% options   - EITHER .classlabel: a n x 1 vector containing 0 and 1 defining 
%                   classes for a binomial SVM, data points labeled NaN will be
%                   ignored (for example, high threat 1, low threat 0,
%                   medium threat NaN)
%              OR .target: a n x 1 vector containing parametric target for an
%                   SVR (i. e. low threat -1, medium threat 0, high threat
%                   1)
%             .repetition (1..k for k-fold leave-one-out)
%             .featnum (do mass-univariate feature selection? 0 or feature number, default 0)
%             .permtest (do permutation test? 0 or number of repetitions)

% suggested sanity check: same as above

%__________________________________________________________________________
