function run_mvpa_prevalence_inference(opts,rois)

data_out = struct([]);

for roi = 1:length(rois)
    
    fprintf(['ROI ' num2str(roi) '\n'])
    
    data_in = load(fullfile(opts.fpath_mvpa,['mvpa_svr_results_' opts.roinames_mvpa{rois(roi)} '.mat']),'data_out');
    
    subs = length(data_in.data_out);
    perms = length(data_in.data_out(1).classification)-6;
    
    data1st = nan(1,subs,perms);
    
    for sub = 1:length(data_in.data_out)
        
        data1st(1,sub,:) = data_in.data_out(sub).classification(7:end);
        
    end
    
    a = data1st;  % three-dimensional array of test statistic values
                  % (voxels x subjects x first-level permutations)
                  % a(:, :, 1)
    P2 = 1e6;     % number of permutations
    alpha = 0.05; % significance level
    [data_out.results, data_out.params] = prevalenceCore(a, P2, alpha);
   
    save(fullfile(opts.fpath_mvpa,['mvpa_previnf_results_' opts.roinames_mvpa{rois(roi)} '.mat']),'data_out')
    
end

end