%% fMRI analysis pipeline starting script
%restoredefaultpath
cd(fileparts(mfilename('fullpath')))
%cd(fprintf('%s\n',matlab.desktop.editor.getActiveFilename))

%% Version
% version = '28092021';
version = '11062018';

%% Subjects to run
startsubj = 1;
endsubj   = 21;%21;
sublist = startsubj:endsubj;
% sublist = [1:7 11:14 17 19]; % monotonic learners only

%% Models to run
% 1 = Axiomatic approach
% 2 = Model 1a: Simple prediction error model
% 3 = Model 1b: Prediction error model with expected p(shock) from the Bayesian model
% 4 = Model 1c: Prediction error model with subjective rated p(shock)
% 5 = Model 1d: Prediction error model with weighted PE from Bayesian model
% 6 = Model 1e: Prediction error and time interaction model
% 7 = Model 1f: Simple positive prediction error model
% 8 = Model 1g: Simple negative prediction error model
% 9 = Model 1h: Simple unsigned prediction error model
% 10 = Various quantities from Bayesian learning model
% 11-18 = Each quantity in its own model
% 19 = Expected p(shock) quadratic model
% 20 = Model 1a modified: Acq vs. Acq2
% 21 = Model 1a modified 1: Added modulator for CS - Prediction error at t-1
% 22 = Model 1a modified 2: Added modulator for CS - Outcome at t-1
% 23 = Model 1i: Expected p(shock) positive prediction error model
% 24 = Model ij: Expected p(shock) negative prediction error model
% 25 = Model 1k: Expected p(shock) unsigned prediction error model
% 26 = Model 1g modified: Acq vs. Acq2
% 27 = Model 1g modified: CS+US- only for true p(shock)
% 28 = Model 1l: Expectation and time interaction model
% 29 = Model 1m: Risk prediction error model
models = 8;%[7:9 23:25];

%% Phases to run
phase = 2;%[1 2 3 5];%[3 5];
% 1 = Acquisition
% 2 = Maintenance
% 3 = Re-learning / Acquisition 2
% 4 = All phases together
% 5 = Both acquisition phases
% 6 = All phases together, scaled according to run length
% 7 = Last two runs of Maintenance

%% Which parts of the pipeline to run
actions.run_createbehav     = false;    % Creating behavioural files
actions.run_checklearning   = false;    % Check learning of CS-US contingencies
actions.run_createphysioreg = false;    % Create noise correction regressors (only needed for RETROICOR & full PhysIO)
actions.run_unzipnii        = false;    % Unzipping NIFTI files
actions.run_fmripreproc     = false;    % fMRI preprocessing
actions.run_checkreg        = false;    % Check preprocessed images
actions.run_checkheadmotion = false;    % Check head motion
actions.run_fmri1stlvl      = false;    % fMRI 1st level (within-subject) analysis
actions.run_findshortscans  = false;     % Find runs that have too short scans to include the last US
actions.run_preparebehav    = false;    % Multiple conditions file for parametric models
actions.run_1stlvlmodel     = false;
actions.run_1stlvlcontrasts = false;
actions.run_copycontrasts   = false;    % Copy all subjects' contrasts and masks into 2nd level folder
actions.run_createexplmask  = false;    % Create explicit mask for 2nd lvl from all subjects' individual masks
actions.run_fmri2ndlvl      = true;    % fMRI 2nd level (group) analysis
actions.printresults        = false;    % Print results of 2nd level analyses

%% Run pipeline
pipeline_analysis_within(version,sublist,models,phase,actions);