%Normalize T1 images of all subjects (helper function as normalization not
%done at any point in the preprocessing pipeline)
clear all
addpath(fullfile(cd,'utils'))
opts = get_options();
subj = opts.subj;
inpath = opts.fpath;

spm_jobman('initcfg');

for s = 2:length(subj)

    %% Create matlabbatch
    clear matlabbatch

    T1_img = cellstr(spm_select('ExtFPList',fullfile(inpath,subj(s),'T1'),'^2016.*.nii$',1));

%     if s == 1
%         matlabbatch{1}.spm.spatial.normalise.est.subj.vol = T1_img;
%         matlabbatch{1}.spm.spatial.normalise.est.eoptions.biasreg = 0.0001;
%         matlabbatch{1}.spm.spatial.normalise.est.eoptions.biasfwhm = 60;
%         matlabbatch{1}.spm.spatial.normalise.est.eoptions.tpm = {'D:\spm12\tpm\TPM.nii'};
%         matlabbatch{1}.spm.spatial.normalise.est.eoptions.affreg = 'mni';
%         matlabbatch{1}.spm.spatial.normalise.est.eoptions.reg = [0 0.001 0.5 0.05 0.2];
%         matlabbatch{1}.spm.spatial.normalise.est.eoptions.fwhm = 0;
%         matlabbatch{1}.spm.spatial.normalise.est.eoptions.samp = 3;
%         
%         spm_jobman('run', matlabbatch)
%         clear matlabbatch
%     end

    % Normalise to MNI space
    deform_field_img = spm_select('FPList',fullfile(inpath,subj(s),'T1'),'^y_2016.*.nii');
    matlabbatch{1}.spm.spatial.normalise.write.subj.def = {deform_field_img};
    matlabbatch{1}.spm.spatial.normalise.write.subj.resample = T1_img;
    matlabbatch{1}.spm.spatial.normalise.write.woptions.bb = [-78 -112 -70
        78 76 85];
    matlabbatch{1}.spm.spatial.normalise.write.woptions.vox = [2 2 2];
    matlabbatch{1}.spm.spatial.normalise.write.woptions.interp = 4;
    matlabbatch{1}.spm.spatial.normalise.write.woptions.prefix = 'w';

    spm_jobman('run', matlabbatch)
    
    file2copy = spm_select('FPList',fullfile(inpath,subj(s),'T1'),'^w2016.*.nii');
    outfile = fullfile(inpath,'Masks','T1_normalized',['normalized_T1_S' num2str(s) '.nii']);
    copyfile(file2copy,outfile)
end