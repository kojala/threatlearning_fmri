function create_physioreg(opts)
%%CREATE_PHYSIOREG Create regressors for physiological noise correction -
%%matlab batch for SPM with RETROICOR method in PhysIO toolbox

for subInd = 1:length(opts.subj) % Loop over subjects
    
    EPIpath = fullfile(opts.fpath,opts.subj{subInd},'EPI'); % EPI path
    savedir = EPIpath;
    
    % Get range of EPIs
    im_range = get_imagerange(opts.subj(subInd));
    
    % Define start times for each scanning run
    fname = fullfile(opts.mainpath,'SCR',['pspm_S' num2str(opts.subj_beh(subInd)),'.mat']); % Physio data file
    load(fname) %#ok<LOAD>
    markers = data{5,1}.data; %#ok<USENS> % Get markers for events
    runind = [1 25 25+44 25+44+44 25+44+44+44 25+44+44+44+44]; % Indices for trials within each run (1st run: 24, 2nd to 5th run: 44 each, 6th run: 24)
    % When each scan starts according to markers, in each run
    scanstarts = [markers(runind(1)) markers(runind(2)) markers(runind(3)) markers(runind(4)) markers(runind(5)) markers(runind(6))];
    
    for run = 1:length(im_range) % Loop over EPIs
        
        % Realignment file
        rpfile = dir(fullfile(EPIpath,'rp*'));
        rpfile = rpfile(run).name;
        
        % EPI image identifier
        volno = num2str(im_range(run));
        if length(volno) < 2
            volno = ['0' volno]; %#ok<AGROW>
        end
        
        % Number of volumes for this run
        EPI(run).epiFiles = spm_select('ExtList',EPIpath,['^swuabc2.*0',volno,'a001.nii$']); %#ok<AGROW>
        nvols = size(EPI(run).epiFiles,1);
        
        % Basic settings
        physio.save_dir                              = {savedir}; % Output saving directory        
        physio.log_files.vendor                      = 'Biopac_Mat'; % Physiological data file type (BIOPAC Matlab imported file)
        physio.log_files.cardiac                     = {fullfile(opts.physiopath,['S' num2str(opts.subj_beh(subInd)) '.mat'])}; % Cardiac log file
        physio.log_files.respiration                 = {fullfile(opts.physiopath,['S' num2str(opts.subj_beh(subInd)) '.mat'])}; % Respiration log file
        physio.log_files.scan_timing                 = {''};
        physio.log_files.sampling_interval           = []; % In seconds, sampling rate 2000 Hz
        physio.log_files.relative_start_acquisition  = scanstarts(run); % Physio file and scans aligned from start
        physio.log_files.align_scan                  = 'first'; % Aligning last scan to the end of the physio log file?
        physio.scan_timing.sqpar.Nslices             = 40; % Number of brain volume slices
        physio.scan_timing.sqpar.NslicesPerBeat      = [];
        physio.scan_timing.sqpar.TR                  = 3.2; % fMRI repetition time
        physio.scan_timing.sqpar.Ndummies            = 0; % Number of dummy scans
        physio.scan_timing.sqpar.Nscans              = nvols; % Number of scans (volumes)
        physio.scan_timing.sqpar.onset_slice         = 20; % Slice to which regressors are temporally aligned, slice-time correction realignment slice
        physio.scan_timing.sqpar.time_slice_to_slice = [];
        physio.scan_timing.sqpar.Nprep               = [];
        physio.scan_timing.sync.nominal              = struct([]);
       
        % Thresholding parameters for de-noising and timing - cardiac
        physio.preproc.cardiac.modality                                = 'PPU'; % Cardiac data modality: photoplethysmograph / pulse oximetry
        physio.preproc.cardiac.initial_cpulse_select.auto_matched.min  = 0.4; % Method for initial detection of heartbeats
        physio.preproc.cardiac.initial_cpulse_select.auto_matched.file = 'initial_cpulse_kRpeakfile.mat';
        physio.preproc.cardiac.posthoc_cpulse_select.off               = struct([]);
        
        physio.model.output_multiple_regressors = ['multiple_regressors_session' num2str(run) '_' opts.noisecorr_name '.txt']; % Regressor output file name
        physio.model.output_physio              = ['physio_session' num2str(run) '_' opts.noisecorr_name '.mat']; % Output file with extracted physiological time series, detected peaks and created regressors
        physio.model.orthogonalise              = 'none'; % Orthogonalization of regressors, only for triggered/gated acquisition sequences
        
        % RETROICOR settings
        physio.model.retroicor.yes.order.c  = 3; % Order of cardiac regressors, 3
        physio.model.retroicor.yes.order.r  = 4; % Order of respiratory regressors, 4
        physio.model.retroicor.yes.order.cr = 1; % Order of cardiac x respiratory regressors, 1
        
        if opts.noisecorr == 3 % PhysIO extra regressors
            physio.model.rvt.yes.delays = 0; % Respiratory volume
            physio.model.hrv.yes.delays = 0; % Heart rate variability
        %else
            %physio.model.rvt.no = struct([]);
            %physio.model.hrv.no = struct([]);
        end
        
        % Principal components of time series of all voxels in given
        % regions of localized noise, e.g. CSF, vessels, white matter
        %physio.model.noise_rois.no = struct([]);
        
        % Motion regressors
        physio.model.movement.yes.file_realignment_parameters = {fullfile(EPIpath,rpfile)}; % Realignment motion regressors file
        physio.model.movement.yes.order                       = 6; % Number of head motion regressors
        physio.model.movement.yes.outlier_translation_mm      = Inf;
        physio.model.movement.yes.outlier_rotation_deg        = Inf;
        %physio.model.other.no                                 = struct([]);
        
        % Plotting options
        if opts.plotting == 1 && opts.noisecorr == 2 % If plotting on and RETROICOR method
            physio.verbose.level           = 2;
            physio.verbose.fig_output_file = ['physio_fig_session' num2str(run) '_' opts.noisecorr_name '.fig']; % Output file for figures, no saving if empty
            physio.verbose.use_tabs        = false;
        elseif opts.plotting == 1 && opts.noisecorr == 3 % If plotting on and PhysIO method
            physio.verbose.level           = 3;
            physio.verbose.fig_output_file = ['physio_fig_session' num2str(run) '_' opts.noisecorr_name '.fig'];
            physio.verbose.use_tabs        = false;
        else % If no physiological regressors, only head motion
            physio.verbose.level = 0;
        end
        
        % Set matlab batch
        matlabbatch{1}.spm.tools.physio = physio;
        
        %% Run matlabbatch
        spm_jobman('run', matlabbatch);
        
        clear EPI physio matlabbatch
        close all
    end
end

end