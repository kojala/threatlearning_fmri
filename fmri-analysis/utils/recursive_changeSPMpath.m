function recursive_changeSPMpath(currpath)
%Helper script to change path names in SPM files
% NOT FUNCTIONAL due to missing recursive_searchpath() function

files = dir(currpath);
filenames = {files(3:end).name}';
dirstate = {files(3:end).isdir};
fullpaths = fullfile(currpath,filenames);

for f = 1:length(filenames)
   
    if strcmp(filenames{f},'SPM.mat')
        load(fullpaths{f})
        newpath = fullpaths{f}(1:end-7);
        spm_changepath(fullpaths{f},SPM.swd,newpath)
        
    elseif dirstate{f} == 1
        recursive_searchpath(fullpaths{f})
    end
    
end

end