function unzip_niftiis(opts)
%Unzip NIFTI .gz files to .nii files

for subInd = 1:size(opts.subj,1) % Loop over subjects
    
    subname = char(opts.subj(subInd)); % Subject MR identifier
    
    filesgz = ls([filep, subname, '\*.gz']); % Zipped gz files
    filesMR = ls([filep, subname, '\MR.*']); % MR files
    fpathgz = fullfile(opts.fpath, subname, 'OriginalFiles');
    
    for f = 1:size(filesgz,1) % Loop over gz files
        
        gunzip(fullfile(filep,subname,filesgz(f,:))) % Use gunzip() function to unzip the gz files
        
        % Clean the .gz files:
        if ~exist(fpathgz, 'dir') % Create directory if does not exist
            mkdir(fpathgz)
        end
        
        movefile(fullfile(opts.fpath,subname,filesgz(f,:)),fullfile(fpathgz,filesgz(f,:)));  % Move files out of the subject data folder to an archive folder
        
    end
    
    clear f filesgz
    
    for f = 1:size(filesMR,1) % Loop over MR files
        
        if ~exist(fpathgz, 'dir')
            mkdir(fpathgz)
        end
        movefile(fullfile(opts.fpath,subname,filesMR(f,:)), fullfile(fpathgz,filesMR(f,:))); % Move original MR files to an archive folder
        
    end
        
    
end

end