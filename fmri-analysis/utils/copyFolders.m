%Copy folders for MVPA analysis
mainpath = fullfile(cd,'..','..','..');
oldpath = fullfile(mainpath,'fMRIdata_biascorrected');
newpath = fullfile(mainpath,'fMRIdata_MVPA');

subj      = dir(oldpath);
subj      = {subj(:).name}';
subj(1:2) = [];
subj      = subj(not(cellfun('isempty', strfind(subj,'PR')))); %#ok<STRCL1>

dirSubjects = subj;

for subj = 2:length(dirSubjects)
    
    pathNew = fullfile(newpath, dirSubjects{subj});
    pathOld = fullfile(oldpath, dirSubjects{subj});
    
    % Make subject folder and EPI folder inside it
    if ~exist(pathNew,'dir')
        mkdir(pathNew);
        mkdir(fullfile(pathNew,'EPI'));
    end
    
    % Copy T1 folder from old to new subject folder
    T1old = fullfile(pathOld,'T1');
    copyfile(T1old, fullfile(pathNew,'T1'));
    
    % Copy relevant EPI and head motion/physio files to new EPI folder
    copyfile(fullfile(pathOld,'EPI','wuabc*'),fullfile(pathNew,'EPI'));
    copyfile(fullfile(pathOld,'EPI','rp*'),fullfile(pathNew,'EPI'));
    copyfile(fullfile(pathOld,'EPI','multiple_regressors*'),fullfile(pathNew,'EPI'));
    copyfile(fullfile(pathOld,'*o2016*t1mprage*'),fullfile(pathNew));
            
%     epilist = spm_select('FPList',fullfile(pathOld,'EPI'),'^wuabc.*.nii');
%     hmlist = spm_select('FPList',fullfile(pathOld,'EPI'),'^rp.*');
%     physiolist = spm_select('FPList',fullfile(pathOld,'EPI'),'^multiple_regressors.*');
%     colist = spm_select('FPList',fullfile(pathOld),'o2016.*t1mprage.*.nii');
    
end