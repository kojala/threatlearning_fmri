function out = get_modelconfig(opts,model)

% Number of conditions
cond_CS = 1;
cond_US = 1;
pmod_CS = 0;
pmod_US = 0;

switch model
    
    case 1 % Axiomatic approach
        
        modelname = 'Model_Cat_Axiomatic';
        cond_CS = 4;
        % CS1: p(US+) = 0
        % CS2: p(US+) = 1/3
        % CS3: p(US+) = 2/3
        % CS4: p(US+) = 1
        cond_US = 6;
        % US1: CS1/US-
        % US2: CS2/US+
        % US3: CS2/US-
        % US4: CS3/US+
        % US5: CS3/US-
        % US6: CS4/US+
        
        %% 1st level
        % Contrast names
        % Sanity check contrasts
        %   1.  (CS1 CS2 CS3 CS4) - baseline // main effect of CS
        %   2.  (US1 US2 US3 US4 US5 US6) - baseline // main effect of US
        %   3.  (US2 US4 US6) - (US1 US3 US5) // US+ - US-
        %   4.  US6 - US1 // CS(1)+US+ - CS(0)-US-

        % 6 contrasts to test for differences (should see a difference)
        %   5.  US3 - US5 // between US- with p(US+) = 1/3 and 2/3
        %   6.  US1 - US3 // between US- with p(US+) = 0 and 1/3
        %   7.  US4 - US5 // between US+ and US- with p(US+) = 1/3
        %   8.  US2 - US3 // between US+ and US- with p(US+) = 2/3
        %   9.  US4 - US6 // between US+ with p(US+) = 2/3 and 1
        %   10. US2 - US4 // between US+ with p(US+) = 1/3 and 2/3
        % 1 contrast to test for equivalence (should not see a difference)
        %   11. US6 - US1 // between US+ and US- of certain probability

        % Alternative (reduced) contrasts
        %   x. (US5 & US6) - (US1 & US2) -- NOT USED
        %   12. (US3 & US5) - (US2 & US4)
        %   13. US2 - US6 // between US+ with p(US+) = 1/3 and 1
        %   14. US1 - US5 // between US- with p(US+) = 0 and 2/3
        %   15. US2 - US4 // between US+ with p(US+) = 1/3 and 2/3
        %   16. US3 - US5 // between US- with p(US+) = 1/3 and 2/3
        %   17. (US3 & US5) - US1 // CS+US- vs. CS- test
        
        % Further checks
        %   18. CS(2/3)US- vs. CS(0)US- FOR US
        
        connames = {'Main effect of CS' 'Main effect of US' 'US+ - US-' 'CS+US+ - CS-US-'...
            'CS+US- > CS-US-'...
            'CS+(1/3)US- - CS+(2/3)US-' 'CS-(0)US- -CS+(1/3)US-' 'CS+(2/3)US+ - CS+(2/3)US-'...
            'CS+(1/3)US+ - CS+(1/3)US-' 'CS+(2/3)US+ - CS+(1)US+' 'CS+(1/3)US+ - CS+(2/3)US+'...
            'CS+(1)US+ - CS-(0)US-' '(CS+(2/3)US+ & CS+(1/3)US+ - CS+(1/3)US- & CS+(2/3)US-'...
            'CS+(1/3)US+ - CS+(1)US+' 'CS-(0)US- - CS+(2/3)US-'...
            'CS+(1/3)US+ - CS+(2/3)US+' 'CS+(1/3)US- - CS+(2/3)US-'...
            'CS(1/3)US- & CS(2/3)US- - CS(0)US-' 'CS(2/3)US- - CS(0)US-'};
        %'(CS-(0)US- & CS+(1/3)US+ - CS+(1)US+ & CS+(2/3)US-'};
        
        no_contr = length(connames);
        conweights = zeros(cond_CS+cond_US,no_contr); % Initialize a vector of zeros
        cn = 1; % Contrast number
        % Contrast weights for each contrasts for all conditions (sum up to 0)
        %--Sanity contrasts
        conweights(1:4,cn)      =  1/4;  cn = cn+1;                                 % Contrast 1
        conweights(5:10,cn)     =  1/6;  cn = cn+1;                                 % Contrast 2
%         conweights([1 3 5],cn)  =  1/3;  cn = cn+1;
        conweights([6 8 10],cn) =  1/3;  conweights([5 7 9],cn)  = -1/3; cn = cn+1; % Contrast 3
        conweights(5,cn)        = -1;    conweights([6 8 10],cn) =  1/3; cn = cn+1; % Contrast 4
        conweights(1,cn)        = 1;    conweights([6 8 10],cn) =  1/3; cn = cn+1; % Contrast 4
        %--Axiomatic contrasts
        conweights(7,cn)        =  1;    conweights(9,cn)        = -1;   cn = cn+1; % Contrast 5:  CS(1/3)US- vs. CS(2/3)US-
        conweights(5,cn)        =  1;    conweights(7,cn)        = -1;   cn = cn+1; % Contrast 6:  CS(0)US-   vs. CS(1/3)US-
        conweights(8,cn)        =  1;    conweights(9,cn)        = -1;   cn = cn+1; % Contrast 7:  CS(2/3)US+ vs. CS(2/3)US-
        conweights(6,cn)        =  1;    conweights(7,cn)        = -1;   cn = cn+1; % Contrast 8:  CS(1/3)US+ vs. CS(1/3)US-
        conweights(8,cn)        =  1;    conweights(10,cn)       = -1;   cn = cn+1; % Contrast 9:  CS(2/3)US+ vs. CS(1)US+
        conweights(6,cn)        =  1;    conweights(8,cn)        = -1;   cn = cn+1; % Contrast 10: CS(1/3)US+ vs. CS(2/3)US+
        conweights(5,cn)        = -1;    conweights(10,cn)       =  1;   cn = cn+1; % Contrast 11: CS(1)US+   vs. CS(0)US-
        conweights([6 8],cn)    =  1/2;  conweights([7 9],cn)    = -1/2; cn = cn+1; % Contrast 12: CS+(2/3)&CS+(1/3)US+ vs. CS+(1/3)&CS+(2/3)US-
        %conweights([5 6],cn)    =  1/2;  conweights([9 10],cn)   = -1/2;
        conweights(6,cn)        =  1;    conweights(10,cn)       = -1;   cn = cn+1; % Contrast 13: CS(1/3)US+ vs. CS(1)US+
        conweights(5,cn)        =  1;    conweights(9,cn)        = -1;   cn = cn+1; % Contrast 14: CS(0)US-   vs. CS(2/3)US-
        conweights(6,cn)        =  1;    conweights(8,cn)        = -1;   cn = cn+1; % Contrast 15: CS(1/3)US+ vs. CS(2/3)US+
        conweights(7,cn)        =  1;    conweights(9,cn)        = -1;   cn = cn+1; % Contrast 16: CS(1/3)US- vs. CS(2/3)US-
        conweights([7 9],cn)    =  1/2;  conweights(5,cn)        = -1;   cn = cn+1; % Contrast 17: CS(1/3)US-&CS(2/3)US- vs. CS(0)US-
        conweights(9,cn)        =  1;    conweights(5,cn)        = -1;              % Contrast 18: CS(2/3)US- vs. CS(0)US-
        
        %% 2nd level
        connames2 = {'Main effect CS' 'Main effect US' 'US+ > US-' 'CS+US+ > CS-US-'...
            'CS+US- > CS-US-'...
            'CS+(1/3)US- > CS+(2/3)US-' 'CS-(0)US- > CS+(1/3)US-' 'CS+(2/3)US+ > CS+(2/3)US-'...
            'CS+(1/3)US+ > CS+(1/3)US-' 'CS+(2/3)US+ > CS+(1)US+' 'CS+(1/3)US+ > CS+(2/3)US+'...
            'CS+(1)US+ > CS-(0)US-' 'CS+(2/3)&CS+(1/3)US+ > CS+(1/3)&CS+(2/3)US-'...
            'CS+(1/3)US+ > CS+(1)US+' 'CS-(0)US- > CS+(2/3)US-'...
            'CS+(1/3)US+ > CS+(2/3)US+' 'CS-(1/3)US- > CS+(2/3)US-'...
            'CS(1/3)US-&CS(2/3)US- > CS(0)US-' '&CS(2/3)US- > CS(0)US-'};
        conpaths2 = {'sancheck1_CS' 'sancheck2_US' 'sancheck3_USpUSm' 'sancheck4_CSpUSpCSmUSm'...
            'sancheck5_CSpUSmCSmUSm',...
            'ax1_CS2USm-CS3USm' 'ax2_CS1USm-CS2USm' 'ax3_CS3USp-CS3USm' 'ax4_CS2USp-CS2USm'...
            'ax5_CS3USp-CS4USp' 'ax6_CS2USp-CS3USp' 'ax7_CS4USp-CS1USm' 'ax8_CS32USp-CS23USm'...
            'ax9_CS2USp-CS4USp' 'ax10_CS1USm-CS3USm' 'ax11_CS2USp-CS3USp' 'ax12_CS2USm-CS3USm'...
            'ax13_CSpCSm' 'ax14_CS3USm-CSm'};
        connumbers2 = {'01' '02' '03' '04' '05' '06' '07' '08' '09' '10' '11' '12' '13' '14' '15' '16' '17' '18' '19'};
        conjnames = {'conjunction_axioms_posPE' 'conjunction_axioms_negPE'...
            'conjunction_axioms12_6cons' 'conjunction_axioms_unsignedPE'...
            'conjunction_axioms_axiom1' 'conjunction_axioms_axiom2'};
        conrepl = 'replsc';
        
        out.connames2 = connames2;
        out.conjnames = conjnames;

    case 2 % Simple prediction error model
        modelname = 'Model_Par_1';
        pmod_CS = 1; % Parametric modulators of CS activity
        pmod_US = 2; % Parametric modulators of US activity
        no_contr = cond_CS + cond_US + pmod_CS + pmod_US; % Total number of contrasts
        connames = {'Unmodulated effect of CS' 'True p(shock)' 'Unmodulated effect of US'...
            'US type' 'Prediction error'};
        conweights = eye(no_contr);
        conpaths2 = {'unmod_CS' 'parmod_CS_pshock' 'unmod_US' 'parmod_US_type' 'parmod_US_PE'};
        connumbers2 = {'01' '02' '03' '04' '05'};
        conrepl = 'replsc';
        
    case 3 % Prediction error model with expectation from the Bayesian model
        modelname = 'Model_Par_1b';
        pmod_CS = 1;
        pmod_US = 2;
        no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
        connames = {'Unmodulated effect of CS' 'Expected p(shock)' 'Unmodulated effect of US'...
            'US type' 'Model prediction error'};
        conweights = eye(no_contr);
        conpaths2 = {'unmod_CS' 'parmod_CS_exptpshock' 'unmod_US' 'parmod_US_type' 'parmod_US_BayesPE'};
        connumbers2 = {'01' '02' '03' '04' '05'};
        conrepl = 'replsc';
        
    case 4 % Prediction error model with subjective ratings as expectation
        modelname = 'Model_Par_1c';
        pmod_CS = 1;
        pmod_US = 2;
        no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
        connames = {'Unmodulated effect of CS' 'Subj Expected p(shock)' 'Unmodulated effect of US'...
            'US type' 'Subj rating prediction error'};
        conweights = eye(no_contr);
        conpaths2 = {'unmod_CS' 'parmod_CS_subjexptpshock' 'unmod_US' 'parmod_US_type' 'parmod_US_SubjratingPE'};
        connumbers2 = {'01' '02' '03' '04' '05'};
        conrepl = 'replsc';
        
    case 5 % Weighted prediction error model
        modelname = 'Model_Par_1d';
        pmod_CS = 1;
        pmod_US = 2;
        no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
        connames = {'Unmodulated effect of CS' 'Expected p(shock)' 'Unmodulated effect of US'...
            'US type' 'Weighted prediction error'};
        conweights = eye(no_contr);
        conpaths2 = {'unmod_CS' 'parmod_CS_exptpshock' 'unmod_US' 'parmod_US_type' 'parmod_US_wPE'};
        connumbers2 = {'01' '02' '03' '04' '05'};
        conrepl = 'replsc';
        
    case 6 % Time modulation of prediction error learning
        modelname = 'Model_Par_1e';
        pmod_CS = 1;
        pmod_US = 4;
        no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
        connames = {'Unmodulated effect of CS' 'True p(shock)' 'Unmodulated effect of US'...
            'US type' 'Trial number' 'Prediction error' 'Trial x Prediction error'};
        conweights = eye(no_contr);
        conpaths2 = {'unmod_CS' 'parmod_CS_pshock' 'unmod_US' 'parmod_US_type' 'parmod_US_trial' 'parmod_US_PE' 'parmod_US_trialxPE'};
        connumbers2 = {'01' '02' '03' '04' '05' '06' '07'};
        conrepl = 'replsc';
        
    case 7 % Simple positive prediction error model
        modelname = 'Model_Par_1f';
        pmod_CS = 1;
        pmod_US = 2;
        no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
        connames = {'Unmodulated effect of CS' 'True p(shock)' 'Unmodulated effect of US'...
            'US type' 'Positive prediction error'};
        conweights = eye(no_contr);
        conpaths2 = {'unmod_CS' 'parmod_CS_pshock' 'unmod_US' 'parmod_US_type' 'parmod_US_posPE'};
        connumbers2 = {'01' '02' '03' '04' '05'};
        conrepl = 'replsc';
        
    case 8 % Simple negative prediction error model
        modelname = 'Model_Par_1g';
        pmod_CS = 1;
        pmod_US = 2;
        no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
        connames = {'Unmodulated effect of CS' 'True p(shock)' 'Unmodulated effect of US'...
            'US type' 'Negative prediction error'};
        conweights = eye(no_contr);
        conpaths2 = {'unmod_CS' 'parmod_CS_pshock' 'unmod_US' 'parmod_US_type' 'parmod_US_negPE'};
        connumbers2 = {'01' '02' '03' '04' '05'};
        conrepl = 'replsc';
        
    case 9 % Simple unsigned prediction error model
        modelname = 'Model_Par_1h';
        pmod_CS = 1;
        pmod_US = 2;
        no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
        connames = {'Unmodulated effect of CS' 'True p(shock)' 'Unmodulated effect of US'...
            'US type' 'Unsigned prediction error'};
        conweights = eye(no_contr);
        conpaths2 = {'unmod_CS' 'parmod_CS_pshock' 'unmod_US' 'parmod_US_type' 'parmod_US_unsigPE'};
        connumbers2 = {'01' '02' '03' '04' '05'};
        conrepl = 'replsc';
        
    case 10 % Bayesian learning model
        modelname = 'Model_Par_2';
        pmod_CS = 5;
        pmod_US = 3;
        no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
        connames = {'Unmodulated effect of CS' 'Expected p(shock)' 'Volatility'...
            'Prior entropy p(shock)' 'KL div prior-posterior previous trial'...
            'Surprise on US previous trial' 'Unmodulated effect of US' 'US type'...
            'KL div prior-posterior current trial' 'Surprise on US current trial'};
        conweights = eye(no_contr);
        conpaths2 = {'unmod_CS' 'parmod_CS_exptpshock' 'parmod_CS_volatility' 'parmod_CS_priorentropypshock'...
            'parmod_CS_KLdivprevtrial' 'parmod_CS_surprUSprevtrial' 'unmod_US' 'parmod_US_type'...
            'parmod_US_KLdivcurrtrial' 'parmod_US_surprUScurrtrial'};
        connumbers2 = {'01' '02' '03' '04' '05' '06' '07' '08' '09' '10'};
        conrepl = 'replsc';
        
    case 19 % Bayesian learning model, expected p(shock) quadratic
        modelname = 'Model_Par_2_reg2_poly2';
        pmod_CS = 2;
        pmod_US = 0;
        no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
        connames = {'Unmodulated effect of CS' 'Expected p(shock)' 'Expected p(shock) quadratic' 'Unmodulated effect of US'};
        conweights = eye(no_contr);
        conpaths2 = {'unmod_CS' 'parmod_CS_exptpshock' 'parmod_CS_exptpshock_quadr' 'unmod_US'};
        connumbers2 = {'01' '02' '03' '04'};
        conrepl = 'replsc';
        
    case 20 % Prediction error model 1a, Acq vs. Acq2
        modelname = 'Model_Par_1_Acq_vs_Acq2';
        pmod_CS = 1;
        pmod_US = 2;
        no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
        connames = {'Unmodulated effect of CS' 'True p(shock)' 'Unmodulated effect of US'...
            'US type' 'Prediction error'};
        conweights = eye(no_contr+24+no_contr+24,no_contr);
        conweights(31,2) = -1; % p(shock) Acq vs. Acq2
        conweights(33,4) = -1; % US type Acq vs. Acq2
        conweights(34,5) = -1; % PE Acq vs. Acq2
        conpaths2 = {'unmod_CS' 'parmod_CS_pshock' 'unmod_US' 'parmod_US_type' 'parmod_US_PE'};
        connumbers2 = {'01' '02' '03' '04' '05'};
        conrepl = 'none'; % For this model need to change contrast replication to none
        
    case 21 % Simple prediction error model with added CS modulator: outcome from previous trial of same type
        modelname = 'Model_Par_1_CS-US';
        pmod_CS = 2;
        pmod_US = 2;
        no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
        connames = {'Unmodulated effect of CS' 'True p(shock)' 'Previous US'...
            'Unmodulated effect of US' 'US type' 'Prediction error'};
        conweights = eye(no_contr);
        conpaths2 = {'unmod_CS' 'parmod_CS_pshock' 'parmod_CS_prevUS' 'unmod_US' 'parmod_US_type' 'parmod_US_PE'};
        connumbers2 = {'01' '02' '03' '04' '05' '06'};
        conrepl = 'replsc';
        
    case 22 % Simple prediction error model with added CS modulator: prediction error from previous trial of same type
        modelname = 'Model_Par_1_CS-PE';
        pmod_CS = 2;
        pmod_US = 2;
        no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
        connames = {'Unmodulated effect of CS' 'True p(shock)' 'Previous prediction error'...
            'Unmodulated effect of US' 'US type' 'Prediction error'};
        conweights = eye(no_contr);
        conpaths2 = {'unmod_CS' 'parmod_CS_pshock' 'parmod_CS_prevPE' 'unmod_US' 'parmod_US_type' 'parmod_US_PE'};
        connumbers2 = {'01' '02' '03' '04' '05' '06'};
        conrepl = 'replsc';

    case 23 % Expected p(shock) positive prediction error model
        modelname = 'Model_Par_1i';
        pmod_CS = 1;
        pmod_US = 2;
        no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
        connames = {'Unmodulated effect of CS' 'Expected p(shock)' 'Unmodulated effect of US'...
            'US type' 'Positive model prediction error'};
        conweights = eye(no_contr);
        conpaths2 = {'unmod_CS' 'parmod_CS_exptpshock'  'unmod_US' 'parmod_US_type' 'parmod_US_posBayesPE'};
        connumbers2 = {'01' '02' '03' '04' '05'};
        conrepl = 'replsc';
        
    case 24 % Expected p(shock) negative prediction error model
        modelname = 'Model_Par_1j';
        pmod_CS = 1;
        pmod_US = 2;
        no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
        connames = {'Unmodulated effect of CS' 'Expected p(shock)' 'Unmodulated effect of US'...
            'US type' 'Negative model prediction error'};
        conweights = eye(no_contr);
        conpaths2 = {'unmod_CS' 'parmod_CS_exptpshock'  'unmod_US' 'parmod_US_type' 'parmod_US_negBayesPE'};
        connumbers2 = {'01' '02' '03' '04' '05'};
        conrepl = 'replsc';
        
    case 25 % Expected p(shock) unsigned prediction error model
        modelname = 'Model_Par_1k';
        pmod_CS = 1;
        pmod_US = 2;
        no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
        connames = {'Unmodulated effect of CS' 'Expected p(shock)' 'Unmodulated effect of US'...
            'US type' 'Unsigned model prediction error'};
        conweights = eye(no_contr);
        conpaths2 = {'unmod_CS' 'parmod_CS_exptpshock'  'unmod_US' 'parmod_US_type' 'parmod_US_unsigBayesPE'};
        connumbers2 = {'01' '02' '03' '04' '05'};
        conrepl = 'replsc';
    
    case 26 % Prediction error model 1g, Acq vs. Acq2
        modelname = 'Model_Par_1g_Acq_vs_Acq2';
        pmod_CS = 1;
        pmod_US = 2;
        no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
        connames = {'Unmodulated effect of CS' 'True p(shock)' 'Unmodulated effect of US'...
            'US type' 'Negative prediction error'};
        conweights = eye(no_contr+24+no_contr+24,no_contr);
        conweights(31,2) = -1; % p(shock) Acq vs. Acq2
        conweights(33,4) = -1; % US type Acq vs. Acq2
        conweights(34,5) = -1; % PE Acq vs. Acq2
        conpaths2 = {'unmod_CS' 'parmod_CS_pshock' 'unmod_US' 'parmod_US_type' 'parmod_US_negPE'};
        connumbers2 = {'01' '02' '03' '04' '05'};
        conrepl = 'none'; % For this model need to change contrast replication to none
    
    case 27 % Negative prediction error model 1g, CS+US- included only
        modelname = 'Model_Par_1g_CS+US-';
        pmod_CS = 1;
        pmod_US = 2;
        no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
        connames = {'Unmodulated effect of CS' 'True p(shock)' 'Unmodulated effect of US'...
            'US type' 'Negative prediction error'};
        conweights = eye(no_contr);
        conpaths2 = {'unmod_CS' 'parmod_CS_pshock' 'unmod_US' 'parmod_US_type' 'parmod_US_negPE'};
        connumbers2 = {'01' '02' '03' '04' '05'};
        conrepl = 'replsc';
        
    case 28 % Time modulation of US expectation/shock probability learning
        modelname = 'Model_Par_1l';
        pmod_CS = 3;
        pmod_US = 1;
        no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
        connames = {'Unmodulated effect of CS' 'True p(shock)' 'Trial number' 'Trial no. x True p(shock)' 'Unmodulated effect of US'...
            'US type'};
        conweights = eye(no_contr);
        conpaths2 = {'unmod_CS' 'parmod_CS_pshock' 'parmod_CS_trial' 'parmod_CS_trialxpshock' 'unmod_US' 'parmod_US_type'};
        connumbers2 = {'01' '02' '03' '04' '05' '06'};
        conrepl = 'replsc';
        
    case 29 % Time modulation of US expectation/shock probability learning
        modelname = 'Model_Par_1m';
        pmod_CS = 1;
        pmod_US = 2;
        no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
        connames = {'Unmodulated effect of CS' 'Risk prediction' 'Unmodulated effect of US'...
            'US type' 'Risk prediction error'};
        conweights = eye(no_contr);
        conpaths2 = {'unmod_CS' 'parmod_CS_riskpred' 'unmod_US' 'parmod_US_type' 'parmod_US_riskPE'};
        connumbers2 = {'01' '02' '03' '04' '05'};
        conrepl = 'replsc';

%     case 29 % Positive PE second half of Maintenance only
%         modelname = 'Model_Par_1f_lasthalf';
%         pmod_CS = 1;
%         pmod_US = 2;
%         no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
%         connames = {'Unmodulated effect of CS' 'True p(shock)' 'Unmodulated effect of US'...
%             'US type' 'Positive prediction error'};
%         conweights_run1 = zeros(no_contr+24,no_contr);
%         conweights_run2 = zeros(no_contr+24,no_contr);
%         conweights_run3 = eye(no_contr+24,no_contr);
%         conweights_run4 = eye(no_contr+24,no_contr);
%         conweights = [conweights_run1; conweights_run2; conweights_run3; conweights_run4];
%         conpaths2 = {'unmod_CS' 'parmod_CS_pshock' 'unmod_US' 'parmod_US_type' 'parmod_US_posPE'};
%         connumbers2 = {'01' '02' '03' '04' '05'};
%         conrepl = 'none';
        
    otherwise % Bayesian learning model, 1 regressor only
        regnames = {'Expected p(shock)' 'Volatility' 'Prior entropy p(shock)'...
            'KL div prior-posterior previous trial', 'Surprise on US previous trial',...
            'US type' 'KL div prior-posterior current trial' 'Surprise on US current trial'};
        pathnames = {'parmod_CS_exptpshock' 'parmod_CS_volatility' 'parmod_CS_priorentropypshock'...
            'parmod_CS_KLdivprevtrial' 'parmod_CS_surprUSprevtrial' 'parmod_US_type'...
            'parmod_US_KLdivcurrtrial' 'parmod_US_surprUScurrtrial'};
        
        modelorder = model-(opts.no_models-length(regnames));
        
        modelname = ['Model_Par_2_reg' num2str(modelorder)];
        
        if model <= (opts.no_models-3) % CS modulators: 5
            pmod_CS = 1;
            pmod_US = 0;
            connames = {'Unmodulated effect of CS' regnames{modelorder} 'Unmodulated effect of US'};
            no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
            conweights = eye(no_contr);
            conpaths2 = {'unmod_CS' pathnames{modelorder} 'unmod_US'};
        else % US modulators: 3
            pmod_CS = 0;
            pmod_US = 1;
            no_contr = cond_CS + cond_US + pmod_CS + pmod_US;
            connames = {'Unmodulated effect of CS' 'Unmodulated effect of US'  regnames{modelorder}};
            conweights = eye(no_contr);
            conpaths2 = {'unmod_CS' 'unmod_US' pathnames{modelorder}};
        end
        
        connumbers2 = {'01' '02' '03'};
        conrepl = 'replsc';
        
end

% Output
out.cond_CS = cond_CS;
out.cond_US = cond_US;
out.no_contr = no_contr;
out.modelname = modelname;
out.pmod_CS = pmod_CS;
out.pmod_US = pmod_US;
out.connames = connames;
out.conweights = conweights;
out.conpaths2 = conpaths2;
out.connumbers2 = connumbers2;
out.conrepl = conrepl;

dirnames = {'_' '_dir2_'};
out.dirnames = dirnames;

end