function check_prepbehfiles(opts,sublist,modelopts)
%Check the correctness of values in the GLM structures created with
%prepare_behav() function

subj = opts.subj(sublist); % Subjects on the current subject list
subj_beh = opts.subj_beh(sublist); % Subjects' behavioural data IDs
bhvpath = fullfile(opts.mainpath,'Behavior'); % Behavioural data main path

% Load data for which runs have a too short last trial to include US BOLD
% responses
shortscanfile = fullfile(opts.fpath,['tooshortscans_USresp' num2str(opts.USresptime) 'sec.mat']);
shortscandata = load(shortscanfile);

for block = 1:6 % Loop over scanning runs
    
    for sub = 1:length(subj) % Loop over subjects
        
        shortscans = shortscandata.tooshortscans(sublist(sub),2:end) > 0; % Find runs that have too short last trial for US response
        
        sub_bhvpath = fullfile(bhvpath, ['S' num2str(subj_beh(sub))]); % Subject's behavioural data including timing and condition information
        if block == 1 % Acquisition 1 phase
            spmfile = fullfile(opts.fpath, subj(sub), '1stlevel', 'Version_11062018', 'NoiseCorr_RETROICOR', modelopts.modelname, 'First_Level_Acq', 'SPM.mat');
            sessind = 1;
        elseif block == 6 % Acquisition 2
            spmfile = fullfile(opts.fpath, subj(sub), '1stlevel', 'Version_11062018', 'NoiseCorr_RETROICOR', modelopts.modelname, 'First_Level_Acq2', 'SPM.mat');
            sessind = 1;
        else % Maintenance
            spmfile = fullfile(opts.fpath, subj(sub), '1stlevel', 'Version_11062018', 'NoiseCorr_RETROICOR', modelopts.modelname, 'First_Level_Maint', 'SPM.mat');
            sessind = block-1;
        end
        spmdata = load(char(spmfile)); % Load SPM file
        
        % Load model structure
        modelfile = fullfile(sub_bhvpath, ['S' num2str(subj_beh(sub)) '_block' num2str(block) '_CSUSconds_' modelopts.modelname '.mat']);
        modeldata = load(modelfile);
        
        results(sub,1) = sub; %#ok<*AGROW> % Subject number
        results(sub,2) = shortscans(block); % Too short scans
        %results(sub,3) = length(modeldata.pmod(2).param{2});
        CSlength1 = length(modeldata.onsets{1}); % How many CS
        USlength1 = length(modeldata.onsets{2}); % How many US
        results(sub,3) = USlength1;
        
        % Check that equal number of CS and US in the model file, if this run does not have
        % too short last trial (when US excluded)
        if (CSlength1-USlength1) == 0 && shortscans(block) == 1
            results(sub,4) = 0; % Save info: equal number of CS and US
        else
            results(sub,4) = 1; % Unequal number of CS and US -> Bad!
        end
        
        % Retrieve number of CS and US from SPM file to check correctness of GLM structure
        if block == 3 && strncmp(subj{sub},'PR02121',7) % Exception subject with no run 3
            CSlength = 0;
            USlength = 0;
        elseif block > 3 && block < 6 && strncmp(subj{sub},'PR02121',7) % Exception subject with runs 4-5 wrong index
            sessind = block-2;
            CSlength = length(spmdata.SPM.Sess(sessind).U(1).ons);
            USlength = length(spmdata.SPM.Sess(sessind).U(2).ons);
        else
            CSlength = length(spmdata.SPM.Sess(sessind).U(1).ons);
            USlength = length(spmdata.SPM.Sess(sessind).U(2).ons);
        end
        
        % Check for equal number of CS and US
        if (CSlength-USlength) == 0 && shortscans(block) == 1
            results(sub,5) = 0;
        else
            results(sub,5) = 1;
        end
        
    end
    
    fprintf(['Run ' num2str(block) '\n']) % Print run number to keep track
    results %#ok<NOPRT> % Print results matrix in the command window
    
end

end