function recursive_deleteSPM(fpath)
%Delete SPM files, recursively searches for all SPM in the main folder
%Input: 
%   input_path      - root path for the SPM to be deleted
%   models          - GLMs to delete by in cellstr {} by their model name
%   phases          - experimental phases to delete
%   version         - version of the analysis to delete

version = '11062018';
noisecorr = 'NoiseCorr_RETROICOR';
models = {'Model_Cat_Axiomatic' 'Model_Par_1' 'Model_Par_1b' 'Model_Par_2' 'Model_Par_2_reg1' 'Model_Par_2_reg2' 'Model_Par_2_reg3' 'Model_Par_2_reg4' 'Model_Par_2_reg5' 'Model_Par_2_reg6' 'Model_Par_2_reg7' 'Model_Par_2_reg8'}; 
phases = {'Acq' 'Maint' 'Acq2' 'BothAcq' 'AllPhases_Scaled'};

spmpaths =  cellstr(spm_select('FPListRec',fpath,'SPM.mat')); % 1st level paths
spmpaths2 = cellstr(spm_select('FPListRec',fullfile(fpath,'2ndlevel'),'SPM.mat')); % 2nd level paths

for m = 1:length(models)
    
    for p = 1:length(phases)
        
        string2find = ['1stlevel\Version_' version noisecorr '\' models{m} '\First_Level_' phases{p}];
        string2find2 = [noisecorr '\' models{m} '\' phases{p}];
            
        matches = strfind(spmpaths,string2find);
        matches2 = strfind(spmpaths2,string2find2);
        
        spm2delete = spmpaths(~cellfun(@isempty,matches)); %#ok<STRCLFH>
        spm2delete2 = spmpaths2(~cellfun(@isempty,matches2)); %#ok<STRCLFH>
        
        for i = 1:length(spm2delete)
            delete(spm2delete{i})
        end
        
        for j = 1:length(spm2delete2)
            delete(spm2delete2{j})
        end
        
    end
end

% files = dir(currpath);
% filenames = {files(3:end).name}';
% dirstate = {files(3:end).isdir};
% fullpaths = fullfile(currpath,filenames);
% 
% for f = 1:length(filenames)
%    
%     if strcmp(filenames{f},'SPM.mat')
%         %delete(filenames{f},'SPM.mat')
%         
%     elseif dirstate{f} == 1
%         recursive_searchpath_deleteSPM(fullpaths{f})
%     end
%     
% end

%end