%Check how correlated regressors are with physiological noise regressors
mainpath = fullfile(cd,'..','..','..');
fpath    = fullfile(mainpath, 'fMRIdata_biascorrected');

phase = 'AllPhases_Scaled';
numruns = 6;
model = 'Model_Par_2';

subj      = dir(fpath);
subj      = {subj(:).name}';
subj(1:2) = [];
subj      = subj(not(cellfun('isempty', strfind(subj,'PR')))); %#ok<STRCL1>
subj      = subj(2:end); % PR1905 does not have physio data

for subInd = 1:length(subj)
    
    subpath = fullfile(fpath,subj(subInd),'NoiseCorr_PhysIO',model,['First_Level_' phase]);
    spmfile = char(fullfile(subpath,'SPM.mat'));
    
    load(spmfile)
    
    numcon = length(SPM.xCon);
    numvol = SPM.nscan;
    
    regvals = SPM.xX.X;
    regnames = SPM.xX.name;
    
    iStart = 1;
    volStart = 1;
    
    for run = 1:numruns
      
        clear runvals_EOI runvals_physreg
        
        runvals_EOI = regvals(:,iStart:iStart+9);
        runvals_physreg = regvals(:,iStart+10:iStart+35);
        
        iStart = iStart+36;
        
        runvals_EOI = runvals_EOI(volStart:volStart+numvol(run)-1,:);
        runvals_physreg = runvals_physreg(volStart:volStart+numvol(run)-1,:);
        
        volStart = volStart+numvol(run);
        
        for effect = 1:10
            effectcorrmat(:,effect) = corr(runvals_EOI(:,effect),runvals_physreg); %#ok<*SAGROW>
        end
        
        corrs_allsubs{subInd} = effectcorrmat;
    end
        
end

for sub = 1:length(subj)
   
    corrs(:,sub) = corrs_allsubs{1,sub}(:,10);
    
end