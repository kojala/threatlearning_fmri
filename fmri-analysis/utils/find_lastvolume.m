function lastvol = find_lastvolume(condfile, USresp)
%%Find those volumes that contain experimental activations

cond = load(condfile);

% Timings of CSs
allCS = cond.onsets{1};

% Last CS timing
lastCS = allCS(end);

% TR 3.2 seconds
TR = 3.2;

% Find volume for last CS time + 6.5s for CS-US presentation time + time US response
endsess = lastCS + 6.5 + USresp;

% Divide by TR to find last volume
lastvol = endsess/TR;

% Round up to include all US activity within the specified time window
lastvol = ceil(lastvol);

end