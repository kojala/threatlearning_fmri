%%COMPARING motion parameters against CS-US timings: can we see whether
%%motion is related to CS or US presentation
clear all

mainpath = fullfile(cd,'..','..','..');
fpath = fullfile(mainpath,'fMRIdata_biascorrected');

% Subject list
subj      = dir(fpath);
subj      = {subj(:).name}';
subj(1:2) = [];
subj      = subj(not(cellfun('isempty', strfind(subj,'PR')))); %#ok<STRCL1>

subj_beh = [159:162 164:167 169:177 179:182];

modelname = 'Model_Par_2';

for subInd = 1:length(subj)
    
    % Correlate unmodulated CS and US regressors with the head motion
    % regressors
    
    % SPM file
    SPMfile = [fpath,'\',subj{subInd},'\1stlevel\Version_11062018\Noisecorr_RETROICOR\Model_Par_2\First_Level_AllPhases_Scaled\SPM.mat'];
    load(SPMfile)
    
    regvals = SPM.xX.X;
    if subInd == 14; numruns = 5; else; numruns = 6; end
    numreg = 10;
    numphysreg = 24;
    numvol = SPM.nscan;
    
    iStart = 1;
    volStart = 1;
    
    for run = 1:numruns
       
        % Get regressors values for each regressor
        runvals_CS = regvals(:,iStart); % CS events
        % 5 regressors between CS and US regressors
        runvals_US = regvals(:,iStart+6); % US events
        runvals_hm = regvals(:,iStart+28:iStart+33); % Head motion regressors
        
        iStart = iStart+numreg+numphysreg; % Next start index after physio regressors for each run
        
        runvals_CS = runvals_CS(volStart:volStart+numvol(run)-1,:);
        runvals_US = runvals_US(volStart:volStart+numvol(run)-1,:);
        runvals_hm = runvals_hm(volStart:volStart+numvol(run)-1,:);
        
        volStart = volStart+numvol(run);
        
        corrmat_CS_hm = corr(runvals_CS,runvals_hm);
        corrmat_US_hm = corr(runvals_US,runvals_hm);
        
        allcorrs_CS(subInd,run,:) = corrmat_CS_hm;
        allcorrs_US(subInd,run,:) = corrmat_US_hm;
        
    end
    % Compare motion files and CS & US onsets (visual inspection)
    % But too difficult to look at all events in all runs in all subjects
    % by eye
%     % Motion file
%     motionpath = fullfile(fpath,subj(subInd),'EPI');
%     motionfiles = cellstr(spm_select('FPList',fullfile(motionpath),'rp_abc*'));
%     
%     for run = 1:6
%        
%         % Load motion file for this run
%         motiondata = importdata(motionfiles{run});
%         
%         % Conditions file for this run
%         condfile = fullfile(mainpath, 'Behavior', ['S' num2str(subj_beh(subInd))], ['S' num2str(subj_beh(subInd)) '_block' num2str(run) '_CSUSconds_' modelname '.mat']);
%         cond = load(condfile);
%         
%         CSon = cond.onsets{1}/3.2;
%         USon = cond.onsets{2}/3.2;
%        
%         figure
%         plot(motiondata)
%         hold on
%         
%         for m = 1:numel(CSon)
%             line([CSon(m) CSon(m)], [min(min(motiondata)) max(max(motiondata))],'Color','b','LineWidth',1)
%         end
%         
%         for n = 1:numel(USon)
%             line([USon(n) USon(n)], [min(min(motiondata)) max(max(motiondata))],'Color','r','LineWidth',1)
%         end
%         
%     end
    
end

figure;imagesc(squeeze(mean(allcorrs_CS,1))); title('CS - Head Motion correlations'); ylabel('Run'); xlabel('Head Motion regressor'); colorbar
figure;imagesc(squeeze(mean(allcorrs_US,1))); title('US - Head Motion correlations'); ylabel('Run'); xlabel('Head Motion regressor'); colorbar