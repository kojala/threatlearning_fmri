function recursive_deletefolders(input_path,models,phases,version,noisecorr,delete_1stlevel,delete_2ndlevel)
%Delete multi-layered folders (e.g. before running new models)
% Input: 
%   input_path      - root path for the folders to be deleted
%   models          - GLMs to delete by in cellstr {} by their model name
%   phases          - experimental phases to delete
%   version         - version of the analysis to delete
%   delete_1stlevel - whether to delete 1st level folders: true/false
%   delete_2ndlevel - whether to delete 2nd level folders: true/false
% E.g. 
% version = '11062018';
% noisecorr = 'NoiseCorr_RETROICOR';
% models = {'Model_Cat_Axiomatic' 'Model_Par_1' 'Model_Par_1b' 'Model_Par_1c' 'Model_Par_1d' 'Model_Par_1e' 'Model_Par_1f' 'Model_Par_1g' 'Model_Par_1h' 'Model_Par_2' 'Model_Par_2_reg1' 'Model_Par_2_reg2' 'Model_Par_2_reg3' 'Model_Par_2_reg4' 'Model_Par_2_reg5' 'Model_Par_2_reg6' 'Model_Par_2_reg7' 'Model_Par_2_reg8'};
% phases = {'Acq' 'Maint' 'Acq2' 'BothAcq' 'AllPhases_Scaled'};

fpath = input_path; 

subj      = dir(fpath);
subj      = {subj(:).name}';
subj(1:2) = [];
subj      = subj(not(cellfun('isempty', strfind(subj,'PR'))));  %#ok<STRCL1>

subjects = 1:length(subj);

%% Delete 1st level folders

if delete_1stlevel
    
    for sub = subjects
        
        for m = 1:length(models)
            
            for p = 1:length(phases)
                
                path = ['1stlevel\Version_' num2str(version) '\' noisecorr '\' models{m}];
                
                paths =  cellstr(spm_select('FPListRec',fullfile(fpath,subj(sub),path),'dir'));
                
                string2find = ['\First_Level_' phases{p}];
                matches = strfind(paths,string2find);
                
                folders2delete = paths(~cellfun(@isempty,matches)); %#ok<STRCLFH>
                
                for i = 1:length(folders2delete)
                    
                    if exist(folders2delete{i},'dir')
                        rmdir(folders2delete{i}, 's')
                    end
                    
                end
                
            end
        end
        
    end
    
end
%% Delete 2nd level folder

if delete_2ndlevel
    
    for m = 1:length(models)
        
        for p = 1:length(phases)
            
            path = ['Version_' num2str(version) '\' noisecorr '\' models{m}];
            paths = cellstr(spm_select('FPListRec',fullfile(fpath,'2ndlevel',path),'dir'));
            
            string2find = phases{p};
            matches = strfind(paths,string2find);
            
            folders2delete = paths(~cellfun(@isempty,matches)); %#ok<STRCLFH>
            
            for i = 1:length(folders2delete)
                
                if exist(folders2delete{i},'dir')
                    rmdir(folders2delete{i}, 's')
                end
                
            end
            
        end
    end
    
end

end