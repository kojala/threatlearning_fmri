%Check the structure of a SPM.mat file

clear all

% modelname = 'Model_Cat_Axiomatic';
noisecorr = opts.noisecorr_name;
modelname = 'Model_Par_1b';

% Folders
mainpath = fullfile(cd,'..','..','..');
fpath1 = fullfile(mainpath,'fMRIdata_biascorrected');

% Subject list
subj      = dir(fpath1);
subj      = {subj(:).name}';
subj(1:2) = [];
subj      = subj(not(cellfun('isempty', strfind(subj,'PR')))); %#ok<STRCL1>

phase = {'Acq','Maint','Acq2','AllPhases_Scaled'};

for subInd = 1:length(subj)
    
    fpath = fullfile(fpath1,char(subj(subInd)));
    
    for p = 1:length(phase)
        
        fpath_first = fullfile(fpath,['NoiseCorr_' noisecorr],modelname,['First_Level_' phase{p}]);
        spmfile = fullfile(fpath_first,'SPM.mat');
        
        load(spmfile)
        
        condnames{p} = SPM.xX.name; %#ok<SAGROW>
        
        %     nCS = 4;
        %     nUS = 6;
        %     nHeadM = 26;
        %     % Contrast CS+(2/3)US+ - CS+(1)US+ (US4 - US6)
        %     convec = [zeros(1,nCS+nUS) zeros(1,nHeadM)];
        %     convec(8) = 1; % CS+(2/3)US+
        %     convec(10) = -1; % CS+(1)US+
        %     indx = find(convec ~= 0);
        %     condnames(indx) %#ok<FNDSB>
        %
        %     % Contrast CS+(1/3)US- - CS+(2/3)US- (US3 - US5)
        %     convec = [zeros(1,nCS+nUS) zeros(1,nHeadM)];
        %     convec(7) = 1; % CS+(1/3)US-
        %     convec(9) = -1; % CS+(2/3)US-
        %     indx = find(convec ~= 0);
        %     condnames(indx)
        
    end
    
end