function find_tooshortscans(opts)
%Find scans where the last trial scan is too short to include full US response

% Initialize variable
tooshortscans = zeros(length(opts.subj),length(opts.runs)+1);

% Loop over subjects (1st level analysis for each subject separately)
for subInd = 1:length(opts.subj)

    % Subject's path
    subpath = fullfile(opts.fpath, char(opts.subj(subInd)));
    
    % Range of images for EPIs (in the the file name)
    im_range = get_imagerange(opts.subj(subInd));
    
    %% Specify 1st level model
    sesi = 0; % Session (phase) index for matlabbatch

    runs = opts.runs; % Retrieve number of runs from the options structure
    
    % Run 3 excluded for subject PR2121 due to bad head motion and artefact
    if strncmp(opts.subj(subInd),'PR02121',7); runs = [1 2 4:6]; end
    
    for run = runs
        
        clear EPI episcans
        
        sesi = sesi + 1; % Increase session number
        
        % Volume number for the current block
        volno = num2str(im_range(run));
        if length(volno) < 2
            volno = ['0' volno]; %#ok<AGROW>
        end
        
        % Select EPI files
        EPIpath = fullfile(subpath,'EPI');
        epiFiles = cellstr(spm_select('ExtFPList',EPIpath,['^swuabc2.*0',volno,'a001.nii$'])); % Bias corrected EPIs
        
        % Conditions
        % -----------------------------------------------------------------
        % Multiple conditions file created with prepare_behav() function
        condfile = fullfile(opts.mainpath, 'Behavior', ['S' num2str(opts.subj_beh(subInd))], ['S' num2str(opts.subj_beh(subInd)) '_block' num2str(run) '_CSUSconds_Model_Par_1.mat']);
        
        % Find the volume for the last US + US response time
        lastvolume = find_lastvolume(condfile, opts.USresptime);
        
        % List whether this run has too short scan to include the last US
        tooshortscans(subInd,1) = opts.subj_beh(subInd);
        if lastvolume > size(epiFiles,1) % Last volume number larger than the length of EPI files
            tooshortscans(subInd,run+1) = run; % Mark that this run as having the last trial too short for full US response to be included in the fMRI data
        end
        
    end
    
end

if exist('tooshortscans','var')
    save(fullfile(opts.fpath,['tooshortscans_USresp' num2str(opts.USresptime) 'sec.mat']),'tooshortscans');
end

end