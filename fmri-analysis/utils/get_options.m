function opts = get_options()

opts = []; 

%% Main analysis
% Version
opts.version = '11062018';

% Path options
opts.mainpath = fullfile(cd,'..','..');
opts.fpath = fullfile(opts.mainpath, 'fMRIdata_biascorrected');
opts.codepath = fullfile(opts.mainpath, 'GitLabRepository', 'fmri-analysis');
opts.physiopath = fullfile(opts.mainpath, 'PhysioRegressors');
opts.bhvpath = fullfile(opts.mainpath, 'Behavior');
opts.spmpath = fullfile(opts.mainpath,'..','Toolboxes','spm12');

opts.roimaskpath = fullfile(opts.fpath,'Masks','ReslicedMasks','CurrentMasks');
opts.roipath = fullfile(opts.fpath,'ROIanalysis');

% SPM options
opts.spm_cmdline = true; % Only command line, no graphics

% Temporal autocorrelation correction option
opts.tempautocorr = 'FAST';

% Physiological noise correction options
opts.noisecorr = 2;
% 1 = only head motion correction (6 regressors)
% 2 = head motion and RETROICOR physiological noise correction (6 + 18)
% 3 = head motion and full PhysIO physiological noise correction (6 + 20)
opts.noisecorr_name = 'RETROICOR'; % HeadMotion, PhysIO
opts.plotting = false;

% Phase options
opts.phasenames = {'Acq','Maint','Acq2','AllPhases','BothAcq','AllPhases_Scaled','MaintLastHalf'};
% 1 = Acquisition
% 2 = Maintenance
% 3 = Re-learning / Acquisition 2
% 4 = All phases together
% 5 = Both acquisition phases
% 6 = All phases together, scaled according to run length

opts.runs_concatenated = 0;

% Subject list
subj      = dir(opts.fpath);
subj      = {subj(:).name}';
subj(1:2) = [];
subj      = subj(not(cellfun('isempty', strfind(subj,'PR'))));
opts.subj = subj;
opts.subj_beh = [159:162 164:167 169:177 179:182];
opts.subj_monot = [1:7 11:14 17 19]; % subs with monotonic explicit CS-US learning
opts.subj_monot_on = false;

% Response time after last US
opts.USresptime = 8;
opts.shortscansfile = fullfile(opts.fpath,['tooshortscans_USresp' num2str(opts.USresptime) 'sec.mat']);

% Number of runs
opts.runs = 1:6;
opts.phase_runs = {1; 2:5; 6; 1:6; [1 6]; 1:6; 4:5};

% Stimulus onset asynchrony (SOA): time between CS and US onsets
opts.SOA = 6; % seconds

% Model options
opts.no_models = 29;
opts.allreg_models = [1:10 20:29];
opts.singlereg_models = 11:19;
opts.Bayes_models = [3 5 6 10 23 24 25];
% 1 = Axiomatic approach
% 2 = Model 1a: Simple prediction error model
% 3 = Model 1b: Prediction error model with expected p(shock) from the Bayesian model
% 4 = Model 1c: Prediction error model with subjective rated p(shock)
% 5 = Model 1d: Prediction error model with weighted PE from Bayesian model
% 6 = Model 1e: Prediction error and time interaction model
% 7 = Model 1f: Simple positive prediction error model
% 8 = Model 1g: Simple negative prediction error model
% 9 = Model 1h: Simple unsigned prediction error model
% 10 = Various quantities from Bayesian learning model
% 11-18 = Each quantity in its own model
% 19 = Expected p(shock) quadratic model
% 20 = Expected p(shock) Model 1b Acq vs. Acq2
% 21 = Model 1a modified 1: Prediction error model with additional CS modulator: US outcome from previous trial of same type
% 22 = Model 1a modified 2: Prediction error model with additional CS modulator: Prediction error from previous trial of same type
% 23 = Model 1i: Expected p(shock) positive prediction error model
% 24 = Model ij: Expected p(shock) negative prediction error model
% 25 = Model 1k: Expected p(shock) unsigned prediction error model
% 26 = Model 1g: Negative prediction error model, Acq vs. Acq2
% 27 = Model 1g: Negative prediction error model, CS+US- included only
% 28 = Model 1l: True p(shock) learning and time interaction model
% 29 = Model 1m: Risk prediction and risk prediction error model

% ROI options
opts.roifolders = {'Anatomical' 'Anatomical\FrontalBA' 'Functional' 'VoxelsFulfillingAxioms' 'Anatomical\BothHemispheres\Combined'};
opts.roinames = {'Anat' 'AnatFrontBA' 'Func' 'VoxFulAx' 'AnatAllComb'};
% opts.roilist_comb = {'ACC' 'BA 8' 'BA 9' 'BA 10' 'BA 11' 'BA 44' 'BA 45' 'BA 46' 'BA 47' 'PAG' 'Amygdala' 'A Insula' ...
%     'D Striatum' 'P Insula' 'SN/VTA' 'Thalamus' 'V Striatum'};
opts.roilist_comb = {'ACC' 'BA 8' 'BA 9' 'BA 10' 'BA 11' 'BA 44' 'BA 45' 'BA 46' 'BA 47' 'PAG' 'Amygdala' 'Anterior insula' ...
    'Dorsal striatum' 'Posterior insula' 'SN/VTA' 'Thalamus' 'Ventral striatum'};
opts.roiplot_linreg = 0; % 0 = nothing on the ROI plots, 1 = linear regression slope axiom 2, t-test result axiom 1, 2 = plot all t-tests
opts.roi_subj = 1:21; % all subs
opts.roiBFplot_direction = 1; % 1: both left and ride side of the graph, -1: only negative side
% opts.roi_subj = [1:7 11:14 17 19];% subs with monotonic explicit CS-US learning

% Direction of 2nd level t-tests
opts.covariates_on_2ndlvl = false;
opts.direction_2ndlvl_ttest = 1;
% 1  = Contrast weights for 2nd level t-tests set as 1
% -1 = Contrast weights for 2nd level t-tests set at -1

% Brain mask
opts.brainmask = fullfile(opts.fpath,'Masks','r_brainmask_binarized.nii');

% Contrast options
opts.contrasts = {1:18; 1:5; 1:5; 1:5; 1:5; 1:7; 1:5; 1:5; 1:5; 1:10;...
    1:3; 1:3; 1:3; 1:3; 1:3; 1:3; 1:3; 1:3; 1:4; 1:5; 1:6; 1:6; 1:5; ...
    1:5; 1:5; 1:5; 1:5; 1:6; 1:5}; % Contrasts to run
opts.conjunctions = 0;%1:6; % Conjunctions (only for axiomatic model)
opts.contrasts_conj = {[9 10]; [5 6]; [5 6 7 8 9 10]; [5 6 9 10]; [13 14]; [7 8]};
opts.con_delete = 1;
%opts.con_sessrep = 'replsc'; % defined in get_modelcondig for each model

% Betas for each phase option
opts.betas{1}.modelbetas = opts.contrasts;                                 % acquisition
opts.betas{2}.modelbetas = {1:18; 1:5; 1:5; 1:5; 1:5; 1:7; 1:5; 1:5;...
    1:5; 1:10; 1:3; 1:3; 1:3; 1:3; 1:3; 1:3; 1:3; 1:3; 1:4; 1:5; 1:6;...   % maintenance
    1:6; 1:5; 1:5; 1:5; 1:5; 1:5; 1:6; 1:5};
opts.betas{3}.modelbetas = opts.contrasts;                                 % acquisition 2
opts.betas{4}.modelbetas = {};                                             % all phases no scaling, not used
opts.betas{5}.modelbetas = {[1:18 43:61]; [1:5 30:34]; [1:5 30:34]; [1:5 30:34];...% both acquisitions
    [1:5 30:34]; [1:7 32:38]; [1:5 30:34]; [1:5 30:34]; [1:5 30:34];...
    [1:10 35:44]; [1:3 28:30]; [1:3 28:30]; [1:3 28:30]; [1:3 28:30];...
    [1:3 28:30]; [1:3 28:30]; [1:3 28:30]; [1:3 28:30]; [1:4 29:31];...
    [1:5 30:34]; [1:6 31:36]; [1:6 31:36]; [1:5 30:34]; [1:5 30:34];...
    [1:5 30:34]; [1:5 30:34]; [1:5 30:34]; [1:6 30:35]; [1:5 30:34];}; 
opts.betas{6}.modelbetas = {};                                             % all phases scaled
opts.betas{7}.modelbetas = {};                                             % maintenance last two runs (others not included)

% Copy individual contrasts/masks/betas to 2nd level folder
opts.copy_contrasts = true;
opts.copy_masks = true;
opts.copy_betas = true;
opts.copy_spmTs = false;

if opts.subj_monot_on
    opts.confolder = 'MonotSub';
else
    opts.confolder = '';
end

%% MVPA

opts.version_mvpa = '15062018';
opts.sublist_mvpa = 1:21;
opts.fpath_mvpa = fullfile(opts.mainpath,'fMRIdata_MVPA');
opts.roinames_mvpa = {'ACC_L' 'ACC_R' 'amygdala_L' 'amygdala_R'...
    'dorsalstriatum_L' 'dorsalstriatum_R' 'anteriorinsula_L' 'anteriorinsula_R'...
    'posteriorinsula_L' 'posteriorinsula_R' 'PAG' 'substantianigraVTA'...
    'thalamus_L' 'thalamus_R' 'ventralstriatum_L' 'ventralstriatum_R'};

end