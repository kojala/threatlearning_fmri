%CS and US regressor correlations in trial-by-trial design matrix

mainpath = fullfile(cd,'..','..','..');
filepath = fullfile(mainpath,'fMRIdata_MVPA\PR02010_VF180893_20160803_123831155\1stlevel\Version_07062018\NoiseCorr_RETROICOR\First_Level_AllPhases_Scaled');
load(fullfile(filepath,'SPM.mat'));

regvals = SPM.xX.X;
regnames = SPM.xX.name;

numruns = 6;
numtrials = [24 44 44 44 44 24];
numphysreg = 24;
numvol = SPM.nscan;

iStart = 1;
volStart = 1;

for run = 1:numruns
    
    clear runvals_EOI runvals_physreg
    
    % Get regressors values for each regressor (trial in this case)
    % 2 event types per trial: CS and US
    runvals_CS = regvals(:,iStart:2:iStart+2*numtrials(run)-1); % CS events
    runvals_US = regvals(:,iStart+1:2:iStart+2*numtrials(run)-2); % US events
    %runvals_physreg = regvals(:,iStart+2*numtrials(run):iStart+2*numtrials(run)+24); % Physio regressors
    
    iStart = iStart+2*numtrials(run)-1+numphysreg; % Next start index after physio regressors for each run
    
    runvals_CS = runvals_CS(volStart:volStart+numvol(run)-1,:);
    runvals_US = runvals_US(volStart:volStart+numvol(run)-1,:);
    
    runvals_US(:,end+1) = NaN; %#ok<*SAGROW>
    %runvals_physreg = runvals_physreg(volStart:volStart+numvol(run)-1,:);
    
    volStart = volStart+numvol(run);
    
    corrmat = corr(runvals_CS,runvals_US);
    
    corrs_allruns{run} = corrmat;
end

figure

for run = 1:numruns
    
    subplot(3,2,run)
    imagesc(corrs_allruns{run})
    colorbar
    ylabel('CS trials')
    xlabel('US trials')
    title(['Run ' num2str(run)])
end

suptitle('Correlation CS vs. US regressors')