%Check scanner triggers against physiological data markers
mainpath = fullfile(cd,'..','..','..');
ExpPath = fullfile(mainpath,'Psychophysiology','PCF_05_fmri','Data');

sList = [159:162 164:167 169:177 179:182]; % Behavioural data subject IDs

sub = 1;

for s = sList
    
    load([ExpPath '\Physio\Data\Trimmed\tpspm_S' num2str(s) '.mat'])
    
    sr = 2000; % sampling rate
    markers = data{5,1}.data; % marker channel data
    t0 = round(markers*sr); % experiment onset
    scantrig = data{4,1}.data; % scanner triggers
    markersvec = NaN(1,numel(scantrig));
    markersvec(t0) = t0;
    figure;plot(scantrig,'k')
    hold on
    
    blocks = 1:6;
    condit = [];
    
    for block = blocks
        eventtypes  = fullfile(ExpPath, 'EventTimings', ['event_types_S' num2str(s) '_Block' num2str(block) '.mat']);
        load(eventtypes)
        condit = [condit allconditions]; %#ok<AGROW>
    end
    
    trial = 1;
    for m = 1:numel(markersvec)
        if ~isnan(markersvec(m))
            line([markersvec(m) markersvec(m)], [0 max(scantrig)],'Color','b')
            if condit(trial) == 2 || condit(trial) == 4 || condit(trial) == 6 % If US trial
                line([markersvec(m)+6*sr markersvec(m)+6*sr], [0 max(scantrig)],'Color','r')
            end
            
            if trial == 200 || trial == 224 % block/run 5 or 6 last trial
                lastCS = markersvec(m);
                lastUS = markersvec(m)+6*2000;
                
               % Find the next 0 scanner trig in the 20 s after last CS
%                for trig = lastUS:m+20*2000
%                    if scantrig(trig) == 0
%                        lasttrig = trig;
%                    end
%                end
                
%                 lasttrig = input('Last trigger: ');
%                 time_afterUS = (lasttrig-lastUS)/2000; % in seconds
%                 fprintf(['Time between last US onset and last scanner trigger: ' num2str(time_afterUS) ' s\n'])
%                 
%                 timeUS(1,sub) = s;
%                 timeUS(2,sub) = trial;
%                 timeUS(3,sub) = lasttrig;
%                 timeUS(4,sub) = time_afterUS;
                
            end
            
            trial = trial+1;
            
        end
        
    end
    
    % Check that ITIs correct
    for t = 1:numel(markers)-1
        iti(t) = markers(t+1)-markers(t)-6.5; %#ok<SAGROW> % 6.5 s duration of the trial
    end
    
    sub = sub + 1;
    
end

save(fullfile(ExpPath,'time_afterLastUS.mat'),'timeUS')