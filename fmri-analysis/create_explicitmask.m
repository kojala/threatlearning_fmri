function create_explicitmask(inpath,outpath,subj)

%% Create expression
expression = [];
for i = 1:length(subj)
    if i == length(subj)
        expression = [expression 'i' int2str(i)];
    else
        expression = [expression 'i' int2str(i) '+'];
    end
end
expression = ['(' expression ')/' int2str(length(subj))];

%% Create matlabbatch
matlabbatch{1}.spm.util.imcalc.input = cellstr(spm_select('ExtFPList',fullfile(inpath),'mask.*.nii$',1));
matlabbatch{1}.spm.util.imcalc.output = 'explicitmask';
matlabbatch{1}.spm.util.imcalc.outdir = cellstr(fullfile(outpath));
matlabbatch{1}.spm.util.imcalc.expression = expression;
matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
matlabbatch{1}.spm.util.imcalc.options.mask = 0;
matlabbatch{1}.spm.util.imcalc.options.interp = 1;
matlabbatch{1}.spm.util.imcalc.options.dtype = 4;

%% Run batch
spm_jobman('initcfg');
spm_jobman('run', matlabbatch)

end