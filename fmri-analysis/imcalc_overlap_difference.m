
calculation = 1;

mainpath = fullfile(cd,'..','..','..');
fpath = fullfile(mainpath,'fMRIdata_biascorrected');
contrasts = [];

for con = 1:length(contrasts)
    
    clear matlabbatch
    
    images = spm_select('FPList',fullfile(fpath),[contrasts{con} '_Acq.*.nii']);
    
    matlabbatch{1}.spm.util.imcalc.input = cellstr(images);
    matlabbatch{1}.spm.util.imcalc.outdir = {fpath};
    
    if calculation == 1 % Overlap = intersection
        matlabbatch{1}.spm.util.imcalc.output = ['overlap_Acq_Acq2_' contrasts{con}];
        matlabbatch{1}.spm.util.imcalc.expression = '(i1.*i2)>0.1';
    elseif calculation == 2 % Difference Acq-Acq2 (Acq without Acq2)
        matlabbatch{1}.spm.util.imcalc.output = ['difference_Acq-Acq2_' contrasts{con}];
        matlabbatch{1}.spm.util.imcalc.expression = '(i1-i2)>0.1';
    elseif calculation == 3  % Difference Acq2-Acq (Acq2 without Acq)
        matlabbatch{1}.spm.util.imcalc.output = ['difference_Acq2-Acq_' contrasts{con}];
        matlabbatch{1}.spm.util.imcalc.expression = '(i2-i1)>0.1';
    end
    
    spm_jobman('run', matlabbatch)

end

%end