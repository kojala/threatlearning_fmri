function preprocessing_withFieldmaps(opts)
%Preprocessing pipeline for PCF fMRI study
%   0. Create folders and move files for EPI, T1
%   1. Bias correction of EPIs for intensity differences across the images
%   2. Create field maps from the phase and magnitude images
%   3. Segmentation of the different tissues based on T1 anatomical image
%   4. Slice-time correction: 40 slices, reference slice 20, ascending
%   5. Realignment and unwarping, including field maps from Step 2
%   6. Co-registration of functional EPI to anatomical T1
%   7. Normalisation to MNI space
%   8. Smoothing: 8 mm kernel
%   9. Run the whole Matlab batch
%   10. Move files to another folder - if necessary

for subInd = 1:length(opts.subj)
    
    %% Create folders and move files
    subpath = fullfile(opts.fpath, char(opts.subj(subInd)));
    cd(subpath)
    
    % Create folder for preprocessed images
    P.path.subFolder = fullfile(subpath,'preprocessed');
    if ~exist(P.path.subFolder, 'dir')
        mkdir(P.path.subFolder)
    end
    
    % Create folder for EPI files
    P.path.epiImg = fullfile(subpath,'EPI');
    if ~exist(P.path.epiImg, 'dir')
        mkdir(P.path.epiImg)
    end
    
    % Move EPI files to their folder
    %movefile('c2*', P.path.epiImg);
    
    % Create folder for T1 images
    P.path.t1Img = fullfile(subpath,'T1');
    if ~exist(P.path.t1Img, 'dir')
        mkdir(P.path.t1Img)
        
        % Move T1 files to their folder
        t1File = ls(fullfile(subpath,'*t1*.nii'));
        t1File = t1File(1,:);
        copyfile(fullfile(subpath,t1File), fullfile(P.path.t1Img,t1File));
    else
        T1files = ls(fullfile(P.path.t1Img,'*t1*.nii'));
        t1File = T1files(1,:);
    end
    
    % Range of the EPI images, e.g. 4 for the first EPI
    % For every block 3 EPIs (1,2,3 together, 4,5,6 together etc.)
    im_range = get_imagerange(opts.subj(subInd));
    
    %% 1. Bias correction EPIs
    
    cd(P.path.epiImg)
    
    for img = 1:length(im_range)
        
        volno = num2str(im_range(img));
        if length(volno) < 2
            volno = ['0', volno]; %#ok<AGROW>
        end
        
        EPIFiles = cellstr(spm_select('ExtList',fullfile(P.path.epiImg),['^c2016.*0',volno,'.*\.nii$']));
        spm_biascorrect(EPIFiles,volno)
        
    end
    
    %% 2. Fieldmaps
    
    % Collect EPIs
    for img = 1:length(im_range)
        
        volno = num2str(im_range(img));
        if length(volno) < 2
            volno = ['0', volno]; %#ok<AGROW>
        end
        
        epis = spm_select('ExtList',P.path.epiImg,['^bc2.*0',volno,'a001.nii$']);
        EPI(img).epiFiles = spm_vol(epis); %#ok<AGROW> % Bias corrected EPIs
        session(img).epi = {[EPI(img).epiFiles(1).fname,',1']}; %#ok<AGROW>
            
    end
    
    % Field map path
    Fieldmapspath = fullfile(subpath,'FieldMaps');
    
    % Find field maps and run the batch
    PhaseIm = ls(fullfile(Fieldmapspath,'2*003a2*.nii'));
    MagIm = ls(fullfile(Fieldmapspath,'2*002a1*.nii'));
            
    spm.tools.fieldmap.presubphasemag.subj.phase = {fullfile(Fieldmapspath,[PhaseIm ',1'])};
    spm.tools.fieldmap.presubphasemag.subj.magnitude = {fullfile(Fieldmapspath, [MagIm ',1'])};
    spm.tools.fieldmap.presubphasemag.subj.defaults.defaultsfile = {fullfile(opts.codepath,'pm_defaults_Prisma_3mm_PI.m')};
    spm.tools.fieldmap.presubphasemag.subj.matchvdm = 1;
    spm.tools.fieldmap.presubphasemag.subj.sessname = 'session';
    spm.tools.fieldmap.presubphasemag.subj.writeunwarped = 0;
    spm.tools.fieldmap.presubphasemag.subj.anat = '';
    spm.tools.fieldmap.presubphasemag.subj.matchanat = 0;
    
    if strncmp(opts.subj,'PR02018',7) % This subj has two field maps
        PhaseIm2 = ls(fullfile(Fieldmapspath,'2*018a2*.nii'));
        MagIm2 = ls(fullfile(Fieldmapspath,'2*017a1*.nii'));
        
        % 1st field map for the first four blocks
        spm.tools.fieldmap.presubphasemag.subj.session(1).epi = session(1).epi;
        spm.tools.fieldmap.presubphasemag.subj.session(2).epi = session(2).epi;
        spm.tools.fieldmap.presubphasemag.subj.session(3).epi = session(3).epi;
        spm.tools.fieldmap.presubphasemag.subj.session(4).epi = session(4).epi;
        matlabbatch{1}.spm = spm;
        
        % 2nd field map for the last two blocks
        spm.tools.fieldmap.presubphasemag.subj.session = struct(); % clear session data
        spm.tools.fieldmap.presubphasemag.subj.phase = {fullfile(Fieldmapspath,[PhaseIm2 ',1'])};
        spm.tools.fieldmap.presubphasemag.subj.magnitude = {fullfile(Fieldmapspath, [MagIm2 ',1'])};
        spm.tools.fieldmap.presubphasemag.subj.session(1).epi = session(5).epi;
        spm.tools.fieldmap.presubphasemag.subj.session(2).epi = session(6).epi;
        matlabbatch{2}.spm = spm;
    else
        spm.tools.fieldmap.presubphasemag.subj.session = session;
        matlabbatch{1}.spm = spm;
    end
    
    spm_jobman('run', matlabbatch);
        
    save('Batch_Fieldmaps.mat', 'matlabbatch')
    
    clear matlabbatch spm
    
    %% 3. Segmentation
    
    preproc.channel.vols = {[strtrim(fullfile(P.path.t1Img, t1File)) ',1']};
    preproc.channel.biasreg = 0.001;
    preproc.channel.biasfwhm = 60;
    preproc.channel.write = [0 0];
    preproc.tissue(1).tpm = {[opts.spmpath '\tpm\TPM.nii,1']};
    preproc.tissue(1).ngaus = 1;
    preproc.tissue(1).native = [1 0];
    preproc.tissue(1).warped = [0 0];
    preproc.tissue(2).tpm = {[opts.spmpath '\tpm\TPM.nii,2']};
    preproc.tissue(2).ngaus = 1;
    preproc.tissue(2).native = [1 0];
    preproc.tissue(2).warped = [0 0];
    preproc.tissue(3).tpm = {[opts.spmpath '\tpm\TPM.nii,3']};
    preproc.tissue(3).ngaus = 2;
    preproc.tissue(3).native = [1 0];
    preproc.tissue(3).warped = [0 0];
    preproc.tissue(4).tpm = {[opts.spmpath '\tpm\TPM.nii,4']};
    preproc.tissue(4).ngaus = 3;
    preproc.tissue(4).native = [1 0];
    preproc.tissue(4).warped = [0 0];
    preproc.tissue(5).tpm = {[opts.spmpath '\tpm\TPM.nii,5']};
    preproc.tissue(5).ngaus = 4;
    preproc.tissue(5).native = [1 0];
    preproc.tissue(5).warped = [0 0];
    preproc.tissue(6).tpm = {[opts.spmpath '\tpm\TPM.nii,6']};
    preproc.tissue(6).ngaus = 2;
    preproc.tissue(6).native = [0 0];
    preproc.tissue(6).warped = [0 0];
    preproc.warp.mrf = 1;
    preproc.warp.cleanup = 1;
    preproc.warp.reg = [0 0.001 0.5 0.05 0.2];
    preproc.warp.affreg = 'mni';
    preproc.warp.fwhm = 0;
    preproc.warp.samp = 3;
    preproc.warp.write = [1 1];
    
    matlabbatch{1}.spm.spatial.preproc = preproc;
    
    %% 4. Slice time correction:
   
    for img = 1:length(im_range)
        
        volno = num2str(im_range(img));
        if length(volno) < 2
            volno = ['0', volno]; %#ok<AGROW>
        end
        
        EPI(img).epiFiles = spm_vol(spm_select('ExtList',P.path.epiImg,['^bc2.*0',volno,'a001.nii$'])); % Bias corrected EPIs
        clear episcans
        for epi = 1:size(EPI(img).epiFiles,1)
            episcans{epi} = [EPI(img).epiFiles(epi).fname, ',',num2str(epi)]; %#ok<AGROW>
        end
        
        st.scans{1,img} = episcans';
        
        %also, retrieve VDM maps, will be used later on:
        VDM{img} = ls(fullfile(Fieldmapspath,'vdm*_session',num2str(img),'.nii')); %#ok<AGROW>
        
    end
    
    nslices = 40;
    TR = 3.2;
    ta = TR - TR/nslices;
    st.nslices = nslices;
    st.tr = TR;
    st.ta = ta;
    st.so = 1:nslices;
    st.refslice = 20;
    st.prefix = 'a';
    
    matlabbatch{2}.spm.temporal.st = st;
    
    %% 5. Realign + Unwarp
    
    % Get parameters for slice time correction
    for ind = 1:size(EPI,2)
        realignunwarp.data(ind).scans(1) = cfg_dep(['Slice Timing: Slice Timing Corr. Images (Sess ', num2str(ind), '1)'], substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('()',{ind}, '.','files'));
        realignunwarp.data(ind).pmscan = {fullfile(Fieldmapspath, char(VDM(ind)))};
    end
    realignunwarp.eoptions.quality = 0.9;
    realignunwarp.eoptions.sep = 4;
    realignunwarp.eoptions.fwhm = 5;
    realignunwarp.eoptions.rtm = 0;
    realignunwarp.eoptions.einterp = 2;
    realignunwarp.eoptions.ewrap = [0 0 0];
    realignunwarp.eoptions.weight = '';
    realignunwarp.uweoptions.basfcn = [12 12];
    realignunwarp.uweoptions.regorder = 1;
    realignunwarp.uweoptions.lambda = 100000;
    realignunwarp.uweoptions.jm = 0;
    realignunwarp.uweoptions.fot = [4 5];
    realignunwarp.uweoptions.sot = [];
    realignunwarp.uweoptions.uwfwhm = 4;
    realignunwarp.uweoptions.rem = 1;
    realignunwarp.uweoptions.noi = 5;
    realignunwarp.uweoptions.expround = 'Average';
    realignunwarp.uwroptions.uwwhich = [2 1];
    realignunwarp.uwroptions.rinterp = 4;
    realignunwarp.uwroptions.wrap = [0 0 0];
    realignunwarp.uwroptions.mask = 1;
    realignunwarp.uwroptions.prefix = 'u';

    matlabbatch{3}.spm.spatial.realignunwarp = realignunwarp;
    
    %% 6. Coregistration of EPI to T1:
    
    for img = 1:length(im_range)
        
        volno = num2str(im_range(img));
        if length(volno) < 2
            volno = ['0', volno]; %#ok<AGROW>
        end
        
        EPI(img).epiFiles = spm_vol(spm_select('FPList',P.path.epiImg,['^uabc2.*0',volno,'a001.nii$'])); 
        clear episcans
        for epi = 1:size(EPI(img).epiFiles,1)
            episcans{epi} = [EPI(img).epiFiles(epi).fname, ',',num2str(epi)];
        end
        
        coreg.estimate.other{1,img} = episcans';
        
    end
    
    sourcefile = cellstr(spm_select('FPList',P.path.epiImg,'^meanuabc2*')); % Mean functional image
    coreg.estimate.ref = {[strtrim(fullfile(P.path.t1Img, t1File)) ',1']};  % T1 image
    coreg.estimate.source(1) = sourcefile; % Source image = mean functional image
    coreg.estimate.other = episcans'; % Other files to coregister according to the parameters based on the mean image coregistration to T1: EPI scans
    coreg.estimate.source(1) = cfg_dep('Realign & Unwarp: Unwarped Mean Image', substruct('.','val', '{}',{3}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','meanuwr'));
     for ind = 1:size(EPI,2)
         coreg.estimate.other(ind) = cfg_dep(['Realign & Unwarp: Unwarped Images (Sess ', num2str(ind), ')'], substruct('.','val', '{}',{3}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','sess', '()',{ind}, '.','uwrfiles'));
     end
    coreg.estimate.eoptions.cost_fun = 'nmi';
    coreg.estimate.eoptions.sep = [4 2];
    coreg.estimate.eoptions.tol = [0.02 0.02 0.02 0.001 0.001 0.001 0.01 0.01 0.01 0.001 0.001 0.001];
    coreg.estimate.eoptions.fwhm = [7 7];
%     coreg.estimate.roptions.interp = 4;
%     coreg.estimate.roptions.wrap = [0 0 0];
%     coreg.estimate.roptions.mask = 0;
%     coreg.estimate.roptions.prefix = 'r';

%     matlabbatch{1}.spm.spatial.coreg = coreg;
    matlabbatch{4}.spm.spatial.coreg = coreg;
    
    %% 7. Normalisation:
    
    estwrite.subj.vol = {[strtrim(fullfile(P.path.t1Img, t1File)) ',1']}; % T1 image
    estwrite.subj.resample(1) = cfg_dep('Coregister: Estimate & Reslice: Coregistered Images', substruct('.','val', '{}',{4}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','cfiles'));
    estwrite.eoptions.biasreg = 0.0001;
    estwrite.eoptions.biasfwhm = 60;
    estwrite.eoptions.tpm = {[opts.spmpath '\tpm\TPM.nii']};
    estwrite.eoptions.affreg = 'mni'; % Montreal Neurological Institute space
    estwrite.eoptions.reg = [0 0.001 0.5 0.05 0.2];
    estwrite.eoptions.fwhm = 0;
    estwrite.eoptions.samp = 3;
    estwrite.woptions.bb = [-78 -112 -70; 78 76 85];
    estwrite.woptions.vox = [2 2 2];
    estwrite.woptions.interp = 4;
    
    matlabbatch{5}.spm.spatial.normalise.estwrite = estwrite;
    
    %% 8. Smoothing:
    
    cd(P.path.epiImg)
    
    for img = length(im_range)
        
        volno = num2str(im_range(img));
        if length(volno) < 2
            volno = ['0', volno]; %#ok<AGROW>
        end
        
        EPI(img).epiFiles = spm_vol(spm_select('ExtList',P.path.epiImg,['^wuabc2.*0',volno,'a001.nii$'])); % Bias corrected EPIs
        clear episcans
        for epi = 1:size(EPI(img).epiFiles,1)
            episcans{epi} = [EPI(img).epiFiles(epi).fname, ',',num2str(epi)];
        end
        
        %smooth.data{1,img} = episcans';
        
    end
    
    smooth.data = episcans';
    smooth.data(1) = cfg_dep('Normalise: Estimate & Write: Normalised Images (Subj 1)', substruct('.','val', '{}',{5}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('()',{1}, '.','files'));
    smooth.fwhm = [8 8 8]; % 8 mm FWHM kernel
    smooth.dtype = 0;
    smooth.im = 0;
    smooth.prefix = 's';
    
%     matlabbatch{1}.spm.spatial.smooth = smooth;
    matlabbatch{6}.spm.spatial.smooth = smooth;
    
    %% Run matlabbatch
    
    spm_jobman('run', matlabbatch);
    %clear matlabbatch
    
    save('batch_Preprocessing', 'matlabbatch')
    clear matlabbatch
    
    %% Move resulting preprocessed files into their folder
    %movefile('*c2*', P.path.epiImg);
end

cd(opts.codepath) % Return current directory to the code path

end