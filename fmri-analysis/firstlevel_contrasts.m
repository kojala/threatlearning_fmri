function firstlevel_contrasts(opts,model,phase)
%Create and run first level contrasts for each GLM

%% Initialize variables and folders

% Retrieve model name and number of condition and parametric regressors
model_inp = get_modelconfig(opts,model);

for subInd = 1:length(opts.subj)
    
    clear matlabbatch
    
    subpath = fullfile(opts.fpath, char(opts.subj(subInd))); % Subject data path
    cd(subpath) % Go to subject directory
    
    fpath_first = fullfile(subpath, '1stlevel', ['Version_' opts.version], ['NoiseCorr_' opts.noisecorr_name], model_inp.modelname, ['First_Level_' opts.phasenames{phase}]);
    
    %% Create contrasts and matlabbatch for contrast manager:
    
    matlabbatch{1}.spm.stats.con.spmmat = {fullfile(fpath_first, 'SPM.mat')};
    
    contrasts = opts.contrasts{model}; % Retrieve number of contrasts for current model
    
    for con = contrasts % Loop over contrasts for current model
        
        matlabbatch{1}.spm.stats.con.consess{con}.tcon.name = model_inp.connames{con}; % Contrast name
        matlabbatch{1}.spm.stats.con.consess{con}.tcon.weights = model_inp.conweights(:,con); % Contrast weight
        matlabbatch{1}.spm.stats.con.consess{con}.tcon.sessrep = model_inp.conrepl; % Contrast replication across sessions
      
    end
    
    matlabbatch{1}.spm.stats.con.delete = opts.con_delete; % Deleting old contrast
    
    %% Run matlabbatch
    spm_jobman('run', matlabbatch);
    
    save(fullfile(fpath_first,'batch_s_Contrasts'), 'matlabbatch')
    
end

end