%% fMRI analysis pipeline wrapper script - MPVA

% This exploratory analysis is not included in the main study

clc
close all
clear variables

opts = get_options();

%% Version
version = opts.version_mvpa; % Version of the analysis

%% Subjects to run
% startsubj = 1;
% endsubj   = 21;
% sublist = startsubj:endsubj;
sublist = [1:13 15:21]; % subject 14 has only 3 maintenance runs so excluded

%% ROIs to run
% 1  = ACC_L                2  = ACC_R
% 3  = amygdala_L           4  = amygdala_R
% 5  = dorsalstriatum_L     6  = dorsalstriatum_R
% 7  = ventralstriatum_L    8  = ventralstriatum_R
% 9  = anteriorinsula_L     10 = anteriorinsula_R
% 11 = posteriorinsula_L    12 = posteriorinsula_R
% 13 = PAG                  14 = substantianigraVTA
% 15 = thalamus_L           16 = thalamus_R
% L = left hemisphere, R = right hemisphere
rois = 1:16;

%% Which parts of the pipeline to run
actions.run_mvpa_prepbehav        = false;   % Multiple conditions file for MVPA
actions.run_mvpa_1stlevel         = false;   % MVPA 1st level trialwise GLM (within-subject)

actions.run_mvpa_wholebrain_setup = false;   % Set up structures for ROI support vector regression
actions.run_mvpa_wholebrain       = false;   % Run support vector regression searchlight for the whole brain

actions.run_mvpa_setup            = false;   % Set up structures for ROI support vector regression
actions.run_mvpa_svr              = false;   % Run support vector regression searchlight within an ROI

actions.run_mvpa_preval_infer     = false;   % Run prevalence inference

actions.run_mvpa_plot_coeff       = false;   % Plot results

%% Run pipeline
pipeline_analysis_within_mvpa(version,sublist,rois,actions);

% TO DO: implement second level with prevalenceCore.m