function check_registration(opts)
%Check registration of images on T1

%Load MNI template:
SPM_path = 'D:\spm12\';
t1MNI = fullfile(SPM_path,'canonical','single_subj_T1.nii'); % Single subject example T1 in MNI space

prefixes = {'bc', 'abc', 'uabc', 'wuabc', 'swuabc'}; % Prefixes of the different levels of preprocessed images
%bc = bias corrected
%abc = bc slice-time corrected
%uabc = realigned/unwarped abc
%wuabc = normalized uabc
%swuabc = smoothed wuabc

for subInd = 1:length(opts.subj)
    
    subpath = fullfile(opts.mainpath,char(opts.subj(subInd)));
    cd(subpath) % Go to the subject's directory
    
    %Find T1 anatomical image
    P.path.t1Img = fullfile(subpath,'T1');
    T1files = ls(fullfile(subpath,'*t1*.nii'));
    t1File = fullfile(P.path.t1Img,T1files(1,:));
    
    im_range = get_imagerange(opts.subj(subInd)); % Get EPI range for the subject
    
    for img = 1:length(im_range) % Loop over EPIs
        
        volno = num2str(im_range(img)); % Volume (scan) number
        if length(volno) < 2
            volno = ['0', volno]; %#ok<AGROW>
        end
        
        %EPI(i).epiFiles = ls([fpath,'*c2*0',r,'a001.nii']);
        
        cd(fullfile(subpath,'EPI')) % Go to EPI folder
        clear episcans
        for epi = 1:length(prefixes) % Find slice-time corrected EPIs
            episcans{epi} = fullfile(subpath,'EPI', [ls([char(prefixes(epi)) '*' '0' volno 'a*.nii']) ',1']); %#ok<AGROW>
        end

%         v = spm_read_vols(spm_vol(episcans{2})); % Read volume data     
        
        % Check registration of EPIs to subject's T1
        spm_check_registration(char({t1File; episcans{1}}), char({t1File; episcans{2}}), char({t1File ;episcans{3}}));
        spm_orthviews('reorient','context_init',1); % show wireframe by opening reorient
        %spm_orthviews('Addtruecolourimage',1,fullfile(ll{1}),flipud(hot(1024)),.5,min(v(:)),max(v(:)))
        spm_orthviews('context_menu','mapping_gl','histeq'); % set global mapping to histeq to see more details in T1
%         spm_orthviews('Reposition',[10 -80 -10]); % any coordinates you want {mm}
%         spm_orthviews('Redraw');
        
        % Write block/scanning run number in the command window to track
        % progress
        input(['ENTER: EPIs vs. T1, Block: ', num2str(img)])
        
        clc
        % Check registration of EPI scans to the MNI space example T1
        spm_check_registration(char({t1MNI; episcans{4}}), char({t1MNI; episcans{5}}));
        spm_orthviews('reorient','context_init',1); % show wireframe by opening reorient
        
        %spm_orthviews('Addtruecolourimage',1,fullfile(ll{1}),flipud(hot(1024)),.5,min(v(:)),max(v(:)))
        spm_orthviews('context_menu','mapping_gl','histeq'); % set global mapping to histeq to see more details in T1
%         spm_orthviews('Reposition',[10 -80 -10]); % any coordinates you want {mm}
%         spm_orthviews('Redraw');
        
        input(['ENTER: EPIs vs. MNI, Block: ', num2str(img)])
        
        clear matlabbatch
    end
    
    input('ENTER: Proceed with next subject')
    clc
end

cd(opts.mainpath) % Return to main analysis path

end