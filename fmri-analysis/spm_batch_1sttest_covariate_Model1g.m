matlabbatch{1}.spm.stats.factorial_design.dir = {'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\1sttest_parmod_US_negPE\CovariateModel'};
%%
matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = {
                                                          'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\data\contrasts\con_0005_S1.nii,1'
                                                          'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\data\contrasts\con_0005_S2.nii,1'
                                                          'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\data\contrasts\con_0005_S3.nii,1'
                                                          'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\data\contrasts\con_0005_S4.nii,1'
                                                          'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\data\contrasts\con_0005_S5.nii,1'
                                                          'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\data\contrasts\con_0005_S6.nii,1'
                                                          'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\data\contrasts\con_0005_S7.nii,1'
                                                          'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\data\contrasts\con_0005_S8.nii,1'
                                                          'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\data\contrasts\con_0005_S9.nii,1'
                                                          'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\data\contrasts\con_0005_S10.nii,1'
                                                          'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\data\contrasts\con_0005_S11.nii,1'
                                                          'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\data\contrasts\con_0005_S12.nii,1'
                                                          'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\data\contrasts\con_0005_S13.nii,1'
                                                          'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\data\contrasts\con_0005_S14.nii,1'
                                                          'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\data\contrasts\con_0005_S15.nii,1'
                                                          'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\data\contrasts\con_0005_S16.nii,1'
                                                          'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\data\contrasts\con_0005_S17.nii,1'
                                                          'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\data\contrasts\con_0005_S18.nii,1'
                                                          'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\data\contrasts\con_0005_S19.nii,1'
                                                          'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\data\contrasts\con_0005_S20.nii,1'
                                                          'C:\Data\PCF_fMRI\fMRIdata_biascorrected\2ndlevel\Version_11062018\NoiseCorr_RETROICOR\Model_Par_1g\Maint\data\contrasts\con_0005_S21.nii,1'
                                                          };
%%
%%
matlabbatch{1}.spm.stats.factorial_design.cov.c = [0.9912
                                                   0.9048
                                                   0.9012
                                                   0.9468
                                                   0.936
                                                   0.9948
                                                   0.9588
                                                   0.0875999999999994
                                                   -0.1032
                                                   -0.3924
                                                   0.9936
                                                   0.9756
                                                   1.0176
                                                   0.9
                                                   -0.0624
                                                   0.1308
                                                   0.9
                                                   0.0635999999999998
                                                   0.7836
                                                   0.858
                                                   -0.0276000000000004];
%%
matlabbatch{1}.spm.stats.factorial_design.cov.cname = 'CSUSlearning';
matlabbatch{1}.spm.stats.factorial_design.cov.iCFI = 1;
matlabbatch{1}.spm.stats.factorial_design.cov.iCC = 1;
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {''};
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;