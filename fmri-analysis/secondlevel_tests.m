function secondlevel_tests(opts,model,phase,printresults)

model_inp = get_modelconfig(opts,model);

seclvlpath = fullfile(opts.fpath,'2ndlevel',['Version_' opts.version],['NoiseCorr_' opts.noisecorr_name],model_inp.modelname,opts.phasenames{phase},opts.confolder);

direction = opts.direction_2ndlvl_ttest;

if exist(seclvlpath,'dir')
    
    if any(opts.contrasts{model}) > 0
        % Run selected contrasts
        for con = opts.contrasts{model}
            
            pathname = model_inp.conpaths2{con};
            connum = model_inp.connumbers2{con};
            
            if model == 1
                actualname = model_inp.connames2{con};
            else
                actualname = model_inp.connames{con};
            end
            
            if direction == -1 % Flipped direction (does not yet work for other than axiomatic model)
                pathname = strrep(pathname,'_',model_inp.dirnames{2});
                actualname = strrep(actualname,'>','<');
            end
            
            pathname = strcat('1sttest_',pathname);
            
            if opts.covariates_on_2ndlvl % If covariates are on (currently only contingency learning as a covariate)
                secondlevel_1sttest_covariate(seclvlpath,pathname,connum,actualname,direction,printresults);
            else
                secondlevel_1sttest(seclvlpath,pathname,connum,actualname,direction,printresults);
            end
            
        end
        
    end
    
    if (model == 1) && (any(opts.conjunctions) > 0)
        % Run selected conjunctions if they exist for the model
        for conj = opts.conjunctions(model)
            
            conjcons = opts.contrasts_conj{conj}; % Contrast numbers for this conjunction
            
            connums = model_inp.connumbers2(conjcons);
            conjname = model_inp.conjnames{conj};
            actualnames = model_inp.connames2(conjcons);
            
            secondlevel_1wayanova(seclvlpath,connums,conjname,actualnames);
            
        end
        
    end
    
end

%% Old stuff
% if model == 1 && exist(seclvlpath,'dir')
%     %% Axiomatic model
%     if direction == 1
%         % ------------------
%         % Sanity check tests
%         % ------------------
%
%
%         % 1. Main effect of CS
%         secondlevel_1sttest(seclvlpath,'1sttest_sancheck1_CS','01',{'CS > baseline'},direction,printresults);
%
%         % 2. Main effect of US
%         secondlevel_1sttest(seclvlpath,'1sttest_sancheck2_US','02',{'US > baseline'},direction,printresults);
%
%         % 3. US+ > US-
%         secondlevel_1sttest(seclvlpath,'1sttest_sancheck3_USpUSm','03',{'US+ > US-'},direction,printresults);
%
%         % 4. All CS+US+ > CS-US-
%         secondlevel_1sttest(seclvlpath,'1sttest_sancheck4_CSpUSpCSmUSm','04',{'CS+US+ > CS-US-'},direction,printresults);
%
%         % ----------------
%         % Single contrasts
%         % ----------------
%
%         % 5. CS(1/3)US- > CS(2/3)US-
%         secondlevel_1sttest(seclvlpath,'1sttest_ax1_CS2USm-CS3USm','05',{'CS2US- > CS3US-'},direction,printresults);
%
%         % 6. CS(0)US- > CS(1/3)US-
%         secondlevel_1sttest(seclvlpath,'1sttest_ax2_CS1USm-CS2USm','06',{'CS1US- > CS2US-'},direction,printresults);
%
%         % 7. CS(2/3)US+ > CS(2/3)US-
%         secondlevel_1sttest(seclvlpath,'1sttest_ax3_CS3USp-CS3USm','07',{'CS3US+ > CS3US-'},direction,printresults);
%
%         % 8. CS(1/3)US+ > CS(1/3)US-
%         secondlevel_1sttest(seclvlpath,'1sttest_ax4_CS2USp-CS2USm','08',{'CS2US+ > CS2US-'},direction,printresults);
%
%         % 9. CS(2/3)US+ > CS(1)US+
%         secondlevel_1sttest(seclvlpath,'1sttest_ax5_CS3USp-CS4USp','09',{'CS3US+ > CS4US+'},direction,printresults);
%
%         % 10. CS(1/3)US+ > CS(2/3)US+
%         secondlevel_1sttest(seclvlpath,'1sttest_ax6_CS2USp-CS3USp','10',{'CS2US+ > CS3US+'},direction,printresults);
%
%         % 11. CS(1)US+ > CS(0)US-
%         secondlevel_1sttest(seclvlpath,'1sttest_ax7_CS4USp-CS1USm','11',{'CS4US+ > CS1US-'},direction,printresults);
%
%         % 12. CS+(2/3)US+ & CS+(1/3)US+ > CS+(1/3)US- & CS+(2/3)US-
%         secondlevel_1sttest(seclvlpath,'1sttest_ax8_CS32USp-CS23USm','12',{'CS+(2/3)&CS+(1/3)US+ > CS+(1/3)US- & CS+(2/3)US-'},direction,printresults);
%
%         % 13. CS(1/3)US+ > CS(1)US+
%         secondlevel_1sttest(seclvlpath,'1sttest_ax9_CS2USp-CS4USp','12',{'CS2US+ > CS4US+'},direction,printresults);
%
%         % 14. CS(0)US- > CS(2/3)US-
%         secondlevel_1sttest(seclvlpath,'1sttest_ax10_CS1USm-CS3USm','13',{'CS1US- > CS3US-'},direction,printresults);
%
%         % --------------------
%         % Conjunction analysis
%         % --------------------
%
%         % Positive prediction error (2 contrasts)
%         conjname = 'conjunction_axioms_posPE';
%         connames = {'1sttest_ax5_CS3USp-CS4USp' '1sttest_ax6_CS2USp-CS3USp'};
%         actualnames = {'CS3US+ > CS4US+' 'CS2US+ > CS3US+'};
%         connums = {'09' '10'};
%         secondlevel_1wayanova(seclvlpath,connames,connums,conjname,actualnames);
%
%         % Negative prediction error (2 contrasts)
%         conjname = 'conjunction_axioms_negPE';
%         connames = {'1sttest_ax1_CS2USm-CS3USm' '1sttest_ax2_CS1USm-CS2USm'};
%         actualnames = {'CS2US- > CS3US-' 'CS1US- > CS2US-'};
%         connums = {'05' '06'};
%         secondlevel_1wayanova(seclvlpath,connames,connums,conjname,actualnames);
%
%         % 1. Full model (6 contrasts)
%         conjname = 'conjunction_axioms12_6cons';
%         connames = {'1sttest_ax1_CS2USm-CS3USm' '1sttest_ax2_CS1USm-CS2USm' '1sttest_ax3_CS3USp-CS3USm' '1sttest_ax4_CS2USp-CS2USm' '1sttest_ax5_CS3USp-CS4USp' '1sttest_ax6_CS2USp-CS3USp'};
%         actualnames = {'CS2US- > CS3US-' 'CS1US- > CS2US-' 'CS3US+ > CS3US-' 'CS2US+ > CS2US-' 'CS3US+ > CS4US+' 'CS2US+ > CS3US+'};
%         connums = {'05' '06' '07' '08' '09' '10'};
%         secondlevel_1wayanova(seclvlpath,connames,connums,conjname,actualnames);
%
%         % Unsigned prediction error (4 contrasts)
%         conjname = 'conjunction_axioms_unsignedPE';
%         connames = {'1sttest_ax1_CS3USm-CS2USm' '1sttest_ax2_CS2USm-CS1USm' '1sttest_ax5_CS3USp-CS4USp' '1sttest_ax6_CS2USp-CS3USp'};
%         actualnames = {'CS3US- > CS2US-' 'CS2US- > CS1US-' 'CS3US+ > CS4US+' 'CS2US+ > CS3US+'};
%         connums = {'05' '06' '09' '10'};
%         secondlevel_1wayanova(seclvlpath,connames,connums,conjname,actualnames);
%
%         % Axiom 1 only
%         conjname = 'conjunction_axioms_axiom1';
%         connames = {'1sttest_ax8_CS2USp-CS4USp' '1sttest_ax9_CS1USm-CS3USm'};
%         actualnames = {'CS2US+ > CS4US+' 'CS1US- > CS3US-'};
%         connums = {'12' '13'};
%         secondlevel_1wayanova(seclvlpath,connames,connums,conjname,actualnames);
%
%         % Axiom 2 only
%         conjname = 'conjunction_axioms_axiom2';
%         connames = {'1sttest_ax3_CS3USp-CS3USm' '1sttest_ax4_CS2USp-CS2USm'};
%         actualnames = {'CS3US+ > CS3US-' 'CS2US+ > CS2US-'};
%         connums = {'07' '08'};
%         secondlevel_1wayanova(seclvlpath,connames,connums,conjname,actualnames);
%
%     elseif direction == -1
%
%         % ------------------
%         % Sanity check tests
%         % ------------------
%
%         % 3. US- > US+
%         secondlevel_1sttest(seclvlpath,'1sttest_sancheck3_dir2_USpUSm','03',{'US- > US+'},direction,printresults);
%
%         % 4. CS-US- > CS+US+
%         secondlevel_1sttest(seclvlpath,'1sttest_sancheck4_dir2_CSpUSpCSmUSm','04',{'CS-US- > CS+US+'},direction,printresults);
%
%         % ----------------
%         % Single contrasts
%         % ----------------
%
%         % 1. CS(2/3)US- > CS(1/3)US-
%         secondlevel_1sttest(seclvlpath,'1sttest_ax1_dir2_CS2USm-CS3USm','05',{'CS3US- > CS2US-'},direction,printresults);
%
%         % 2. CS(1/3)US- > CS(0)US-
%         secondlevel_1sttest(seclvlpath,'1sttest_ax2_dir2_CS1USm-CS2USm','06',{'CS2US- > CS1US-'},direction,printresults);
%
%         % 3. CS(2/3)US- >  CS(2/3)US+
%         secondlevel_1sttest(seclvlpath,'1sttest_ax3_dir2_CS3USp-CS3USm','07',{'CS3US- > CS3US+'},direction,printresults);
%
%         % 4. CS(1/3)US- > CS(1/3)US+
%         secondlevel_1sttest(seclvlpath,'1sttest_ax4_dir2_CS2USp-CS2USm','08',{'CS2US- > CS2US+'},direction,printresults);
%
%         % 5. CS(1)US+ > CS(2/3)US+
%         secondlevel_1sttest(seclvlpath,'1sttest_ax5_dir2_CS3USp-CS4USp','09',{'CS4US+ > CS3US+'},direction,printresults);
%
%         % 6. CS(2/3)US+ > CS(1/3)US+
%         secondlevel_1sttest(seclvlpath,'1sttest_ax6_dir2_CS2USp-CS3USp','10',{'CS3US+ > CS2US+'},direction,printresults);
%
%         % 7. CS(0)US- > CS(1)US+
%         secondlevel_1sttest(seclvlpath,'1sttest_ax7_dir2_CS4USp-CS1USm','11',{'CS1US- > CS4US+'},direction,printresults);
%
%         % 8. CS(1)US+ > CS(1/3)US+
%         secondlevel_1sttest(seclvlpath,'1sttest_ax8_dir2_CS2USp-CS4USp','12',{'CS4US+ > CS2US+'},direction,printresults);
%
%         % 9. CS(2/3)US- > CS(0)US-
%         secondlevel_1sttest(seclvlpath,'1sttest_ax9_dir2_CS1USm-CS3USm','13',{'CS3US- > CS1US-'},direction,printresults);
%
%         % Conjunction analyses F-tests so in both directions already
%
%     end
%
% elseif model == 2 && exist(seclvlpath,'dir')
%     %% Parametric Model 1a
%
%     % 1. Unmodulated effect of CS
%     secondlevel_1sttest(seclvlpath,'1sttest_unmod_CS','01',{'Unmodulated effect of CS'},direction,printresults);
%
%     % 2. Effect of p(shock)
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_CS_pshock','02',{'Effect of p(shock)'},direction,printresults);
%
%     % 3. Unmodulated effect of US
%     secondlevel_1sttest(seclvlpath,'1sttest_unmod_US','03',{'Unmodulated effect of US'},direction,printresults);
%
%     % 4. Effect of US type
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_type','04',{'Effect of US type'},direction,printresults);
%
%     % 5. Effect of prediction error
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_PE','05',{'Prediction error'},direction,printresults);
%
% elseif model == 3 && exist(seclvlpath,'dir')
%     %% Parametric Model 1b
%
%     % 1. Unmodulated effect of CS
%     secondlevel_1sttest(seclvlpath,'1sttest_unmod_CS','01',{'Unmodulated effect of CS'},direction,printresults);
%
%     % 2. Effect of p(shock)
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_CS_exptpshock','02',{'Effect of Expected p(shock)'},direction,printresults);
%
%     % 3. Unmodulated effect of US
%     secondlevel_1sttest(seclvlpath,'1sttest_unmod_US','03',{'Unmodulated effect of US'},direction,printresults);
%
%     % 4. Effect of US type
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_type','04',{'Effect of US type'},direction,printresults);
%
%     % 5. Effect of Bayesian prediction error
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_BayesPE','05',{'Bayesian prediction error'},direction,printresults);
%
% elseif model == 4 && exist(seclvlpath,'dir')
%     %% Parametric Model 1c
%
%     % 1. Unmodulated effect of CS
%     secondlevel_1sttest(seclvlpath,'1sttest_unmod_CS','01',{'Unmodulated effect of CS'},direction,printresults);
%
%     % 2. Effect of p(shock)
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_CS_subjexptpshock','02',{'Effect of Subj Expected p(shock)'},direction,printresults);
%
%     % 3. Unmodulated effect of US
%     secondlevel_1sttest(seclvlpath,'1sttest_unmod_US','03',{'Unmodulated effect of US'},direction,printresults);
%
%     % 4. Effect of US type
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_type','04',{'Effect of US type'},direction,printresults);
%
%     % 5. Effect of Bayesian prediction error
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_SubjratingPE','05',{'Subj rating prediction error'},direction,printresults);
%
% elseif model == 5 && exist(seclvlpath,'dir')
%     %% Parametric Model 1d
%
%     % 1. Unmodulated effect of CS
%     secondlevel_1sttest(seclvlpath,'1sttest_unmod_CS','01',{'Unmodulated effect of CS'},direction,printresults);
%
%     % 2. Effect of p(shock)
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_CS_exptpshock','02',{'Effect of Expected p(shock)'},direction,printresults);
%
%     % 3. Unmodulated effect of US
%     secondlevel_1sttest(seclvlpath,'1sttest_unmod_US','03',{'Unmodulated effect of US'},direction,printresults);
%
%     % 4. Effect of US type
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_type','04',{'Effect of US type'},direction,printresults);
%
%     % 5. Effect of Weighted prediction error
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_wPE','05',{'Weighted prediction error'},direction,printresults);
%
% elseif model == 6 && exist(seclvlpath,'dir')
%     %% Parametric Model 1e
%
%     % 1. Unmodulated effect of CS
%     secondlevel_1sttest(seclvlpath,'1sttest_unmod_CS','01',{'Unmodulated effect of CS'},direction,printresults);
%
%     % 2. Effect of p(shock)
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_CS_pshock','02',{'Effect of True p(shock)'},direction,printresults);
%
%     % 3. Unmodulated effect of US
%     secondlevel_1sttest(seclvlpath,'1sttest_unmod_US','03',{'Unmodulated effect of US'},direction,printresults);
%
%     % 4. Effect of US type
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_type','04',{'Effect of US type'},direction,printresults);
%
%     % 5. Effect of trial number
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_trial','05',{'Trial number'},direction,printresults);
%
%     % 6. Effect of weighted prediction error
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_PE','06',{'Prediction error'},direction,printresults);
%
%     % 7. Effect of trial number x wPE
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_trialxPE','07',{'Trial x Prediction error'},direction,printresults);
%
% elseif model == 7 && exist(seclvlpath,'dir')
%     %% Parametric Model 1f
%
%     if direction == 1
%         % 1. Unmodulated effect of CS
%         secondlevel_1sttest(seclvlpath,'1sttest_unmod_CS','01',{'Unmodulated effect of CS'},direction,printresults);
%
%         % 2. Effect of p(shock)
%         secondlevel_1sttest(seclvlpath,'1sttest_parmod_CS_pshock','02',{'Effect of True p(shock)'},direction,printresults);
%
%         % 3. Unmodulated effect of US
%         secondlevel_1sttest(seclvlpath,'1sttest_unmod_US','03',{'Unmodulated effect of US'},direction,printresults);
%
%         % 4. Effect of US type
%         secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_type','04',{'Effect of US type'},direction,printresults);
%
%         % 5. Effect of Positive prediction error
%         secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_posPE','05',{'Positive prediction error'},direction,printresults);
%
%     elseif direction == -1
%         secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_type_dir2','04',{'Effect of US type'},direction,printresults);
%         secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_posPE_dir2','05',{'Positive prediction error'},direction,printresults);
%     end
%
% elseif model == 8 && exist(seclvlpath,'dir')
%     %% Parametric Model 1g
%
%     if direction == 1
%         % 1. Unmodulated effect of CS
%         secondlevel_1sttest(seclvlpath,'1sttest_unmod_CS','01',{'Unmodulated effect of CS'},direction,printresults);
%
%         % 2. Effect of p(shock)
%         secondlevel_1sttest(seclvlpath,'1sttest_parmod_CS_pshock','02',{'Effect of p(shock)'},direction,printresults);
%
%         % 3. Unmodulated effect of US
%         secondlevel_1sttest(seclvlpath,'1sttest_unmod_US','03',{'Unmodulated effect of US'},direction,printresults);
%
%         % 4. Effect of US type
%         secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_type','04',{'Effect of US type'},direction,printresults);
%
%         % 5. Effect of Negative prediction error
%         secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_negPE','05',{'Negative prediction error'},direction,printresults);
%
%     elseif direction == -1
%         secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_type_dir2','04',{'Effect of US type'},direction,printresults);
%         secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_negPE_dir2','05',{'Negative prediction error'},direction,printresults);
%     end
%
% elseif model == 9 && exist(seclvlpath,'dir')
%     %% Parametric Model 1h
%
%     if direction == 1
%
%         % 1. Unmodulated effect of CS
%         secondlevel_1sttest(seclvlpath,'1sttest_unmod_CS','01',{'Unmodulated effect of CS'},direction,printresults);
%
%         % 2. Effect of p(shock)
%         secondlevel_1sttest(seclvlpath,'1sttest_parmod_CS_pshock','02',{'Effect of p(shock)'},direction,printresults);
%
%         % 3. Unmodulated effect of US
%         secondlevel_1sttest(seclvlpath,'1sttest_unmod_US','03',{'Unmodulated effect of US'},direction,printresults);
%
%         % 4. Effect of US type
%         secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_type','04',{'Effect of US type'},direction,printresults);
%
%         % 5. Effect of Unsigned prediction error
%         secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_unsigPE','05',{'Unsigned prediction error'},direction,printresults);
%
%     elseif direction == -1
%         secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_type_dir2','04',{'Effect of US type'},direction,printresults);
%         secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_unsigPE_dir2','05',{'Unsigned prediction error'},direction,printresults);
%     end
%
% elseif model == 10 && exist(seclvlpath,'dir')
%     %% Parametric Model 2 (version 2 with volatility)
%
%     % 1. Unmodulated effect of CS
%     secondlevel_1sttest(seclvlpath,'1sttest_unmod_CS','01',{'Unmodulated effect of CS'},direction,printresults);
%
%     % 2. Expected p(shock)
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_CS_exptpshock','02',{'Expected p(shock)'},direction,printresults);
%
%     % 3. Volatility
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_CS_volatility','03',{'Volatility'},direction,printresults);
%
%     % 4. Prior entropy p(shock)
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_CS_priorentropypshock','04',{'Prior entropy p(shock)'},direction,printresults);
%
%     % 5. KL divergence prior-posterior from previous trial
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_CS_KLdivprevtrial','05',{'KL div prev trial'},direction,printresults);
%
%     % 6. Suprise about US from previous trial
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_CS_surprUSprevtrial','06',{'Surpr US prev trial'},direction,printresults);
%
%     % 7. Unmodulated effect of US
%     secondlevel_1sttest(seclvlpath,'1sttest_unmod_US','07',{'Unmodulated effect of US'},direction,printresults);
%
%     % 8. US type
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_type','08',{'US type'},direction,printresults);
%
%     % 9. KL divergence prior-posterior for current trial
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_KLdivcurrtrial','09',{'KL div curr trial'},direction,printresults);
%
%     % 10. Suprise about US from previous trial
%     secondlevel_1sttest(seclvlpath,'1sttest_parmod_US_surprUScurrtrial','10',{'Surpr US curr trial'},direction,printresults);
%
% elseif model > 10 && exist(seclvlpath,'dir')
%
%     %% Parametric Model 2 with 1 regressor at a time
%
%     modelnames = {'exptpshock' 'volatility' 'priorentropypshock'...
%         'KLdivprevtrial' 'surprUSprevtrial' 'US_type'...
%         'KLdivcurrtrial' 'surprUScurrtrial'};
%     regnames  = model_inp.connames;
%
%     % Unmodulated effect of CS
%     secondlevel_1sttest(seclvlpath,'1sttest_unmod_CS','01',{'Unmodulated effect of CS'},direction,printresults);
%
%     modelorder = model-(opts.no_models-length(modelnames)); % Number of the individual regressor model to get correct names
%
%     if model <= (opts.no_models-3)
%         % Parametric modulator
%         secondlevel_1sttest(seclvlpath,['1sttest_parmod_CS_' modelnames{modelorder}],'02',regnames(2),direction,printresults);
%         % Unmodulated effect of US
%         secondlevel_1sttest(seclvlpath,'1sttest_unmod_US','03',{'Unmodulated effect of US'},direction,printresults);
%     else
%         secondlevel_1sttest(seclvlpath,'1sttest_unmod_US','02',{'Unmodulated effect of US'},direction,printresults);
%         secondlevel_1sttest(seclvlpath,['1sttest_parmod_US_' modelnames{modelorder}],'03',regnames(3),direction,printresults);
%     end
%
% end

end