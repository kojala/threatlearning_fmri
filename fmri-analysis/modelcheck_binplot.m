function modelcheck_binplot(opts,sublist,modelopts,plotopts)
%Plot model check: trial-wise model values vs. trial-wise BOLD estimates

subj = opts.subj(sublist);
subj_beh = opts.subj_beh(sublist);
bhvpath = opts.bhvpath;

blocks = opts.runs; % Runs
blocktrials = [24 44 44 44 44 24]; % Number of trials in each run

if modelopts.condition == 1; condname = 'CS'; % Looking at CS events
elseif modelopts.condition == 2; condname = 'US'; end % Looking at US events

%% Bin the parametric modulator/model value

regdata_all = nan(length(subj),224); % Initialize matrix for all model values
trialdata_all = nan(length(subj),224); % Initialize matrix for all trial type values

for sub = 1:length(subj) % Loop over subjects
   
    sub_bhvpath = fullfile(bhvpath, ['S' num2str(subj_beh(sub))]); % Current subjects' behavioural data path
    
    regdata = []; 
    trialdata = []; 
    
    % Load file with information on which scans are too short to include
    % the last US
    shortscanfile = fullfile(opts.fpath,['tooshortscans_USresp' num2str(opts.USresptime) 'sec.mat']);     
    shortscandata = load(shortscanfile); 
    shortscans = shortscandata.tooshortscans(sub,2:end) > 0;
        
    for block = blocks % Loop over runs/blocks
        
        if block == 6; bhvfile = fullfile(sub_bhvpath, ['PCF_05_' num2str(subj_beh(sub)) '_Block' num2str(block) '_Part2.mat']); % Acquisition 2 data in a separate file
        else; bhvfile = fullfile(sub_bhvpath, ['PCF_05_' num2str(subj_beh(sub)) '_Block' num2str(block) '.mat']); end % Acquisition 1 and Maintenance data
        
        % File with model structure created for GLMs, including model values
        modelfile = fullfile(sub_bhvpath, ['S' num2str(subj_beh(sub)) '_block' num2str(block) '_CSUSconds_' modelopts.modelname '.mat']);
        
        if exist(modelfile,'file')
            
            % Load data
            bhvdata = load(bhvfile);
            modeldata = load(modelfile);
            
            % Put NaN to the slots of missing USs
            if block == 3 && strncmp(subj{sub},'PR02121',7) % Missing run for this sub, all NaNs
                regdata = [regdata; NaN(blocktrials(block),1)]; %#ok<*AGROW>
            elseif shortscans(block) == 1 && modelopts.condition == 2 % If too short scan and condition is US
                regdata = [regdata; modeldata.pmod(modelopts.condition).param{modelopts.regnum}; NaN]; % Add NaN for missing US
            else
                regdata = [regdata; modeldata.pmod(modelopts.condition).param{modelopts.regnum}];
            end
            
            trialdata = [trialdata; bhvdata.indata(:,2)]; % Accumulate trial type information
            
        else
            
            regdata = [regdata; NaN(blocktrials(block),1)];
            trialdata = [trialdata; NaN(blocktrials(block),1)];
            
        end
        
    end
    
    regdata_all(sub,:) = regdata';
    trialdata_all(sub,:) = trialdata';
    
end

% Save regressor data for use in other applications
save(fullfile(bhvpath,[modelopts.modelname '_' modelopts.regname '.mat']),'regdata_all')

%% Sorting
% Translate acquisition 2 CS types to same numbers as previous runs
% trialdata_all(trialdata_all == 6) = 1; % CS(0)
% trialdata_all(trialdata_all == 7) = 2; % CS(1/3)
% trialdata_all(trialdata_all == 8) = 3; % CS(2/3)
% trialdata_all(trialdata_all == 5) = 4; % CS(1)
% 
% % Sort regressor values according to trial types
% [~,trialdata_ind] = sort(trialdata_all,2); % sort trial types 1 to 4 and output indices
% regdata_all = regdata_all';
% [m,n] = size(regdata_all);
% regdata_sorted_bytrial = regdata_all(sub2ind([m n],trialdata_ind',repmat(1:n,m,1))); % sort values according to trial type indices
% % sorting this way only works columnwise
% 
% % Average over subjects for each trial
% regdata_mean = nanmean(regdata_sorted_bytrial,2);
% 
% % Reorder data to phase order (Acq, Maint, Acq2) regardless of trial type
% % from acq-maint-acq2-acq-maint-acq2... order to overall acq-maint-acq2 order
% % first 6 trials acq, 44 trials maint, 6 trials acq2
% acq_indices = [1:6 57:62 113:118 169:174];
% maint_indices = [7:50 63:106 119:162 175:218];
% acq2_indices = [51:56 107:112 163:168 219:224];
% regdata_mean_ord(1:24) = regdata_mean(acq_indices);
% regdata_mean_ord(25:200) = regdata_mean(maint_indices);
% regdata_mean_ord(201:224) = regdata_mean(acq2_indices);

%% Create bins
% Pick information only for the chosen phase(s)
regdata_trials = regdata_all(:,plotopts.trials(:));
regdata_trials(isnan(regdata_trials)) = [];

% Create bins for the mean parametric modulator/model values
trybins = plotopts.nbins; % Try the specified number of bins (may not work if too few values for each bin, in which case fewer bins used)
[bincounts,~,indices] = histcounts(regdata_trials(:),trybins);

% Sort values into the bins, take mean of each bin
bin_regdata = accumarray(indices(:),regdata_trials(:),[],@median);

%% Bin betas (BOLD activity values)
betas = load(fullfile(opts.roimaskpath,'ModelChecks',['MeanBetas_' modelopts.roiname '_' condname '.mat']));
betas_all = betas.betas_allsubs;

%% Sorting
% % Sort beta values according to trial types
% 
% [m,n] = size(betas_all);
% betas_sorted_bytrial = betas_all(sub2ind([m n],trialdata_ind',repmat(1:n,m,1)));
% 
% % Average over subjects for each trial
% betas_mean = nanmean(betas_sorted_bytrial,2);
% 
% % Reorder data to phase order (Acq, Maint, Acq2) regardless of trial type
% % from acq-maint-acq2-acq-maint-acq2... order to overall acq-maint-acq2 order
% % first 6 trials acq, 44 trials maint, 6 trials acq2
% acq_indices = [1:6 57:62 113:118 169:174];
% maint_indices = [7:50 63:106 119:162 175:218];
% acq2_indices = [51:56 107:112 163:168 219:224];
% betas_mean_ord(1:24) = betas_mean(acq_indices);
% betas_mean_ord(25:200) = betas_mean(maint_indices);
% betas_mean_ord(201:224) = betas_mean(acq2_indices);

%% Create bins
% Pick only the chosen trials
betas_trials = betas_all(:,plotopts.trials(:));
betas_trials(isnan(betas_trials)) = [];

% Bin betas according to parametric modulator bins, take mean of each bin
bin_betas = accumarray(indices(:),betas_trials(:),[],@median);

%% Plot

% Take only non-empty bins
bin_regdata = bin_regdata(bincounts~=0);
bin_betas = bin_betas(bincounts~=0);
%markersizes = bincounts(bincounts>0);
markersizes = 15; % Size of the markers for each bin of beta values

% Colors
% [189,201,225
%             103,169,207
%             28,144,153
%             1,108,89]./255; % add color
        
bgscatter = [28,144,153]./255;%[144, 202, 249]./255; % Background marker color for individual values
binscatter_edge = [0 0 0];%[13, 71, 161]./255; % Binned beta values marker edge color
binscatter_face = [1,108,89]./255;%[25, 118, 210]./255; % Binned beta values marker face color

% Make figure
figure('DefaultAxesFontSize', 12, 'DefaultAxesFontName', 'Arial', 'DefaultTextFontName', 'Arial');
set(gcf, 'Units', 'Centimeters',...
        'PaperUnits', 'Centimeters', ...
        'PaperPositionMode','auto')
set(gcf,'Position', [0, 0, 7, 4])
    
if plotopts.plotall % Plot individual data points in the background
    scatter(regdata_trials(:),betas_trials(:),'MarkerEdgeColor',bgscatter,'MarkerFaceColor',bgscatter); % Plot regressor/model values against beta values
    if plotopts.set_x_reverse; set(gca,'XDir','reverse'); end % Reverse x-axis if specified
    hold on
end

% Plot binned data - regressor/model values vs. beta values
% Size of the circle represents the bin size
scatter(bin_regdata,bin_betas,markersizes*5,'filled',...
    'MarkerEdgeColor',binscatter_edge,'LineWidth',1.5,...
    'MarkerFaceColor',binscatter_face);

set(gca,'XTick',-2.5:0.5:0); % x-axis ticks
set(gca,'YTick',0:1:4); % y-axis ticks
xlim([-2.2 0])
ylim([0 2])

if plotopts.set_x_reverse; set(gca,'XDir','reverse'); end

xlabel(modelopts.regname) % Set regressor name as the x-axis label
ylabel('BOLD estimate','FontSize',12)

if plotopts.plotall; ylim([floor(min(betas_trials(:))) ceil(max(betas_trials(:)))]); end % When plotting all trial values, set y-axis limits according to their minimum and maximum

% if size(plotopts.trials,1) > 1
%     title(['Trials ' num2str(plotopts.trials(1,1)) '-' num2str(plotopts.trials(end,1)) ' & '...
%         num2str(plotopts.trials(1,2)) '-' num2str(plotopts.trials(end,2)) ', ' num2str(length(markersizes)) ' bins'])
% else
%     title(['Trials ' num2str(plotopts.trials(1)) '-' num2str(plotopts.trials(end)) ', ' num2str(length(markersizes)) ' bins'])
% end

set(gca,'LineWidth',1.25) % Set plot line width to thicker
saveas(gcf,fullfile(fullfile(opts.roimaskpath,'ModelChecks',['ModelCheckplot_' modelopts.regname])),'svg') % Save figure in svg format

end

%% Discarded stuff

% regdata_sorted_bytrial = nan(size(trialdata_all,1),size(trialdata_all,2));
% for sub = 1:length(subj)
%     regdata_sorted_bytrial(sub,:) = regdata_all(sub,trialdata_ind(sub,:));
% end

% while any(bincounts == 0)
%     [bincounts,~,indices] = histcounts(regdata_mean,trybins);
%     if trybins == 0
%         disp('Bin size 0!')
%         break
%     end
%     trybins = trybins-1;
% end