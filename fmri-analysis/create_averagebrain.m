function create_averagebrain(opts)
%Create an average T1 of all subjects' T1 images

addpath(fullfile(cd,'utils'))

subj = opts.subj;

inpath = opts.fpath;
outpath = fullfile(inpath,'Masks','T1_WMGM');
if ~exist(outpath,'dir')
    mkdir(outpath)
end

%% Mask based on WM and GM for each subject
spm_jobman('initcfg');

for s = 1:length(subj)
    %% Create expression
    %expression = 'i1>0.001 | i2>0.001';
    expression = '(i3.*(i1+i2))';
    
    %% File selection
    % c1 and c2 (GM and WM) segmentation EPI
    % For each block
    c1_img = cellstr(spm_select('ExtFPList',fullfile(inpath,subj(s),'T1'),'c1.*.nii$',1));
    c2_img = cellstr(spm_select('ExtFPList',fullfile(inpath,subj(s),'T1'),'c2.*.nii$',1));
    T1_img = cellstr(spm_select('ExtFPList',fullfile(inpath,subj(s),'T1'),'^2016.*t1.*.nii$',1));
    
    %c1c2_img = [c1_img; c2_img];
    c1c2T1_img = [c1_img; c2_img; T1_img];
    
    %% Create matlabbatch
    clear matlabbatch
    matlabbatch{1}.spm.util.imcalc.input = c1c2T1_img;
    matlabbatch{1}.spm.util.imcalc.output = ['T1_WMGM_S' int2str(s)];
    matlabbatch{1}.spm.util.imcalc.outdir = cellstr(fullfile(outpath));
    matlabbatch{1}.spm.util.imcalc.expression = expression;
    matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
    matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
    matlabbatch{1}.spm.util.imcalc.options.mask = 0;
    matlabbatch{1}.spm.util.imcalc.options.interp = 1;
    matlabbatch{1}.spm.util.imcalc.options.dtype = 4;
    
    %% Run batch
    spm_jobman('initcfg');
    spm_jobman('run', matlabbatch)
    
    % Normalise to MNI space
    epi_img = cellstr(spm_select('ExtFPList',fullfile(inpath,subj(s),'EPI'),'^swmeanuabc.*.nii$',1));
    estwrite.subj.vol = epi_img;
    estwrite.subj.resample(1) = {strtrim(fullfile(outpath,['T1_WMGM_S' int2str(s) '.nii']))};
    estwrite.eoptions.biasreg = 0.0001;
    estwrite.eoptions.biasfwhm = 60;
    estwrite.eoptions.tpm = {[opts.spmpath '\tpm\TPM.nii']};
    estwrite.eoptions.affreg = 'mni';
    estwrite.eoptions.reg = [0 0.001 0.5 0.05 0.2];
    estwrite.eoptions.fwhm = 0;
    estwrite.eoptions.samp = 3;
    estwrite.woptions.bb = [-78 -112 -70 78 76 85];
    estwrite.woptions.vox = [2 2 2];
    estwrite.woptions.interp = 4;
    
    matlabbatch{1}.spm.spatial.normalise.estwrite = estwrite;

    spm_jobman('run', matlabbatch)
    
end

%% Average over subjects
%% Create expression
expression = [];
for i = 1:length(subj)
    if i == length(subj)
        expression = [expression 'i' int2str(i)]; %#ok<*AGROW>
    else
        expression = [expression 'i' int2str(i) '+'];
    end
end
expression = ['(' expression ')/' int2str(length(subj))];

%% Create matlabbatch
clear matlabbatch

norm_wmgm_img = cellstr(spm_select('ExtFPList',outpath,'normalized_T1_WMGM_S.*.nii$',1));

matlabbatch{1}.spm.util.imcalc.input = norm_wmgm_img;
matlabbatch{1}.spm.util.imcalc.output = 'normalized_T1_WMGM_mean';
matlabbatch{1}.spm.util.imcalc.outdir = cellstr(fullfile(outpath));
matlabbatch{1}.spm.util.imcalc.expression = expression;
matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
matlabbatch{1}.spm.util.imcalc.options.mask = 0;
matlabbatch{1}.spm.util.imcalc.options.interp = 1;
matlabbatch{1}.spm.util.imcalc.options.dtype = 4;

%% Run batch
spm_jobman('initcfg');
spm_jobman('run', matlabbatch)

end