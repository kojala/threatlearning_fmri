function prepare_behav(opts,sublist,models)

% File with model predictions for Model 2 (and expected shock for Model 1b)
if any(ismember(opts.Bayes_models,models))
    load(fullfile(opts.mainpath,'Behavior','AllSubs_behvecs_Predictions_v3.mat'))
end
% File with subjective shock probability ratings for Model 1c
if ismember(4,models); load(fullfile(opts.mainpath,'Behavior','subjective_pshock_AcqMaint.mat')); end
% File with mean-centered variables for Models 1e and 1l
if any(ismember([6 28],models)); load(fullfile(opts.mainpath,'Behavior','meancentered_time_interaction.mat')); end
% File with information about scans that are too short to include the
% last US of each run
shortscansfile = fullfile(opts.fpath,['tooshortscans_USresp' num2str(opts.USresptime) 'sec.mat']);
load(shortscansfile)

% Sampling rate
SR = 1000;

% Number of blocks
no_runs = 6; % All blocks
runs = 1:no_runs;
alltrials_maint = {0; 1:44; 45:88; 89:132; 133:176; 0};

% First trial of the block
ntrials = [6 11 11 11 11 6];
trial1 = [1 1+6 1+6+11 1+6+(2*11) 1+6+(3*11) 1];

for subInd = 1:length(opts.subj_beh)
    
    behavDir = fullfile(opts.mainpath,'Behavior',['S' num2str(opts.subj_beh(subInd))]);
    
    for iSess = runs
        behavFile = sprintf('Behavior_S%d.mat',opts.subj_beh(subInd));
        behavFile = fullfile(behavDir,behavFile);
        behavData = load(behavFile);
        
        % Determine block, general CS indices for all runs
        tim = behavData.tim;
        conddata = behavData.cond;
        CS = conddata(iSess).CS;
        if iSess < 6 % Acquisition 1 or Maintenance
            if any(ismember(opts.Bayes_models,models)); modelpred = Pred1; end % Model prediction structure
            CS1 = CS==1;
            CS2 = CS==2;
            CS3 = CS==3;
            CS4 = CS==4;
        elseif iSess == 6 % Acquisition 2
            if any(ismember(opts.Bayes_models,models)); modelpred = Pred2; end % Model prediction structure
            CS1 = CS==6; % CS code
            CS2 = CS==7;
            CS3 = CS==8;
            CS4 = CS==5;
        end
        
        CS_timings = tim(iSess).CS_on/SR;
        
        % US
        if tooshortscans(sublist(subInd),iSess+1) > 0 % If there is a mark for the scan of this session to be too short to include the last US
            US = conddata(iSess).US(1:end-1); % Then exclude the last US
            USremoved = true;
        else
            US = conddata(iSess).US;
            USremoved = false;
        end
        
        USleng = length(US);
        US_timings = tim(iSess).US_on(1:USleng)/SR;
        % Not including the last US of each session because scanning was
        % interrupted too early in some cases and there was not enough time
        % left for a US BOLD response to develop
        
        names        = {'CS','US'};
        onsets       = {CS_timings,US_timings};
        durations    = {zeros(length(onsets{1}),1), zeros(length(onsets{2}),1)};
        orth         = {1,1};
        tmod         = {0,0};
        
        % Trials for this session
        sesstrials = trial1(iSess):(trial1(iSess)+ntrials(iSess)-1);
        
        %% Model 1a
        
        if ismember(2,models)
            
            clear pmod shockProb
            
            shockProb = nan(1,length(CS1));
            
            shockProb(CS1) = 0;
            shockProb(CS2) = 1/3;
            shockProb(CS3) = 2/3;
            shockProb(CS4) = 1;
            
            pmod(1).name  = {'True p(shock)'};
            pmod(1).param = {shockProb'};
            pmod(1).poly  = {1};
            
            clear predictionError
            predictionError = US-shockProb(1:USleng)';
            pmod(2).name  = {'US type','Prediction error'};
            pmod(2).param = {US,predictionError};
            pmod(2).poly  = {1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1.mat',opts.subj_beh(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
        end
        
        %% Model 1b
        
        if ismember(3,models)
            
            clear pmod BayesPredictionError exptShockProb
            
            exptShockProb = nan(1,length(CS1));
            
            % Expected shock probability
            exptShockProb(CS1) = modelpred{1,sublist(subInd)}.BM(1,sesstrials);
            exptShockProb(CS2) = modelpred{1,sublist(subInd)}.BM(2,sesstrials);
            exptShockProb(CS3) = modelpred{1,sublist(subInd)}.BM(3,sesstrials);
            exptShockProb(CS4) = modelpred{1,sublist(subInd)}.BM(4,sesstrials);
            
            pmod(1).name  = {'Expected p(shock)'};
            pmod(1).param = {exptShockProb'};
            pmod(1).poly  = {1};
            
            BayesPredictionError = US-exptShockProb(1:USleng)';
            
            pmod(2).name  = {'US type','Model prediction error'};
            pmod(2).param = {US,BayesPredictionError};
            pmod(2).poly  = {1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1b.mat',opts.subj_beh(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
            
        end
        
        %% Model 1c
        
        if ismember(4,models)
            
            clear pmod SubjPredictionError subj_exptShockProb
            
            subj_exptShockProb = nan(1,length(CS1));
            SubjPredictionError  = nan(1,length(US));
            
            % Subjective expected shock probability based on ratings
            subj_exptShockProb(CS1) = CSsubj(sublist(subInd),1);
            subj_exptShockProb(CS2) = CSsubj(sublist(subInd),2);
            subj_exptShockProb(CS3) = CSsubj(sublist(subInd),3);
            subj_exptShockProb(CS4) = CSsubj(sublist(subInd),4);
            
            pmod(1).name  = {'Subjective expected p(shock)'};
            pmod(1).param = {subj_exptShockProb'};
            pmod(1).poly  = {1};
            
            % Prediction error, outcome-subjective rated expectation
            SubjPredictionError(CS1(1:USleng)) = US(CS1(1:USleng))-CSsubj(sublist(subInd),1)';
            SubjPredictionError(CS2(1:USleng)) = US(CS2(1:USleng))-CSsubj(sublist(subInd),2)';
            SubjPredictionError(CS3(1:USleng)) = US(CS3(1:USleng))-CSsubj(sublist(subInd),3)';
            SubjPredictionError(CS4(1:USleng)) = US(CS4(1:USleng))-CSsubj(sublist(subInd),4)';
            
            pmod(2).name  = {'US type','Subj rating prediction error'};
            pmod(2).param = {US,SubjPredictionError};
            pmod(2).poly  = {1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1c.mat',opts.subj_beh(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
            
        end
        
        %% Model 1d
        
        if ismember(5,models)
            
            clear pmod weightedPredictionError exptShockProb
            
            exptShockProb = nan(1,length(CS1));
            weightedPredictionError  = nan(1,length(CS1));
            
            % Expected shock probability
            exptShockProb(CS1) = modelpred{1,sublist(subInd)}.BM(1,sesstrials);
            exptShockProb(CS2) = modelpred{1,sublist(subInd)}.BM(2,sesstrials);
            exptShockProb(CS3) = modelpred{1,sublist(subInd)}.BM(3,sesstrials);
            exptShockProb(CS4) = modelpred{1,sublist(subInd)}.BM(4,sesstrials);
            
            pmod(1).name  = {'Expected p(shock)'};
            pmod(1).param = {exptShockProb'};
            pmod(1).poly  = {1};
            
            % Weighted prediction error based on the Bayesian model
            weightedPredictionError(CS1) = modelpred{1,sublist(subInd)}.PE(1,sesstrials);
            weightedPredictionError(CS2) = modelpred{1,sublist(subInd)}.PE(2,sesstrials);
            weightedPredictionError(CS3) = modelpred{1,sublist(subInd)}.PE(3,sesstrials);
            weightedPredictionError(CS4) = modelpred{1,sublist(subInd)}.PE(4,sesstrials);
            if USremoved; weightedPredictionError(end) = []; end
            
            pmod(2).name  = {'US type','Weighted prediction error'};
            pmod(2).param = {US,weightedPredictionError};
            pmod(2).poly  = {1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1d.mat',opts.subj_beh(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
            
        end
        
        %% Model 1e
        
        if ismember(6,models) && (iSess > 1 && iSess < 6) % Only for Maintenance phase
            
            clear pmod shockProb trialNumber predictionError trialNo_x_PE
            
            shockProb = nan(1,length(CS1));
            
            shockProb(CS1) = 0;
            shockProb(CS2) = 1/3;
            shockProb(CS3) = 2/3;
            shockProb(CS4) = 1;
            
            % Time = trial number
            trialNumber = alltrials_maint{iSess}';
            if USremoved; trialNumber(end) = []; end % If last US of each session excluded
            
            pmod(1).name  = {'True p(shock)'};
            pmod(1).param = {shockProb'};
            pmod(1).poly  = {1};
            
            % Prediction error
            predictionError = US-shockProb(1:USleng)';
            
            % Trial number x wPE
            trialNo_x_PE = (trialNumber-mean(trialNumber)).*(predictionError-mean(predictionError));
            % Mean centering and then interaction
            
            pmod(2).name  = {'US type','Trial number','Prediction error','Trial no. x PE'};
            pmod(2).param = {US,trialNumber,predictionError,trialNo_x_PE};
            pmod(2).poly  = {1,1,1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1e.mat',opts.subj_beh(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
            
        end
        
        %% Model 1f
        % Simple positive prediction error model
        
        if ismember(7,models)
            
            clear pmod shockProb predictionError
            
            shockProb = nan(1,length(CS1));
            predictionError = nan(1,USleng);
            
            shockProb(CS1) = 0;
            shockProb(CS2) = 1/3;
            shockProb(CS3) = 2/3;
            shockProb(CS4) = 1;
            
            pmod(1).name  = {'True p(shock)'};
            pmod(1).param = {shockProb'};
            pmod(1).poly  = {1};
            
            USp = US==1;
            USm = US==0;
            predictionError(USp) = 1-shockProb(USp);
            predictionError(USm) = nanmean(predictionError);
            % trials not taken into account (US-) set to session mean to
            % exclude their influence
            
            pmod(2).name  = {'US type','Positive prediction error'};
            pmod(2).param = {US,predictionError'};
            pmod(2).poly  = {1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1f.mat',opts.subj_beh(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
        end
        
        %% Model 1g
        % Simple negative prediction error model
        
        if ismember(8,models)
            
            clear pmod shockProb predictionError
            
            shockProb = nan(1,length(CS1));
            predictionError = nan(1,USleng);
            
            shockProb(CS1) = 0;
            shockProb(CS2) = 1/3;
            shockProb(CS3) = 2/3;
            shockProb(CS4) = 1;
            
            pmod(1).name  = {'True p(shock)'};
            pmod(1).param = {shockProb'};
            pmod(1).poly  = {1};
            
            USp = US==1;
            USm = US==0;
            predictionError(USm) = 0-shockProb(USm);
            predictionError(USp) = nanmean(predictionError);
            % trials not taken into account (US+) set to session mean to
            % exclude their influence
            
            pmod(2).name  = {'US type','Negative prediction error'};
            pmod(2).param = {US,predictionError'};
            pmod(2).poly  = {1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1g.mat',opts.subj_beh(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
        end
        
        %% Model 1h
        % Simple unsigned/absolute prediction error model
        
        if ismember(9,models)
            
            clear pmod shockProb predictionError
            
            shockProb = nan(1,length(CS1));
            
            shockProb(CS1) = 0;
            shockProb(CS2) = 1/3;
            shockProb(CS3) = 2/3;
            shockProb(CS4) = 1;
            
            pmod(1).name  = {'True p(shock)'};
            pmod(1).param = {shockProb'};
            pmod(1).poly  = {1};
            
            predictionError = abs(US-shockProb(1:USleng)');
            
            pmod(2).name  = {'US type','Unsigned prediction error'};
            pmod(2).param = {US,predictionError};
            pmod(2).poly  = {1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1h.mat',opts.subj_beh(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
        end
        
        %% Model 2
        
        if ismember(10,models)
            
            clear pmod exptShockProb priorEntrShockProb KLdivPrevTrial ...
                surprUSPrevTrial KLdivCurrTrial surprUSCurrTrial signedPE ...
                volatilityPrevTrial
            
            % Initialize to Nan
            exptShockProb       = nan(1,length(CS1));
            volatility          = nan(1,length(CS1));
            priorEntrShockProb  = nan(1,length(CS1));
            surprUSPrevTrial    = nan(1,length(CS1));
            KLdivPrevTrial      = nan(1,length(CS1));
            KLdivCurrTrial      = nan(1,length(CS1));
            surprUSCurrTrial    = nan(1,length(CS1));
            
            % Expected shock probability
            exptShockProb(CS1) = modelpred{1,sublist(subInd)}.BM(1,sesstrials);
            exptShockProb(CS2) = modelpred{1,sublist(subInd)}.BM(2,sesstrials);
            exptShockProb(CS3) = modelpred{1,sublist(subInd)}.BM(3,sesstrials);
            exptShockProb(CS4) = modelpred{1,sublist(subInd)}.BM(4,sesstrials);
            
            % Volatility
            volatility(CS1) = modelpred{1,sublist(subInd)}.VO(1,sesstrials);
            volatility(CS2) = modelpred{1,sublist(subInd)}.VO(2,sesstrials);
            volatility(CS3) = modelpred{1,sublist(subInd)}.VO(3,sesstrials);
            volatility(CS4) = modelpred{1,sublist(subInd)}.VO(4,sesstrials);
            
            % Prior entropy of shock probability
            priorEntrShockProb(CS1) = modelpred{1,sublist(subInd)}.BE(1,sesstrials);
            priorEntrShockProb(CS2) = modelpred{1,sublist(subInd)}.BE(2,sesstrials);
            priorEntrShockProb(CS3) = modelpred{1,sublist(subInd)}.BE(3,sesstrials);
            priorEntrShockProb(CS4) = modelpred{1,sublist(subInd)}.BE(4,sesstrials);
            
            % Find indices for each CS type
            CS1_ind = find(CS1);
            CS2_ind = find(CS2);
            CS3_ind = find(CS3);
            CS4_ind = find(CS4);
            
            % KL divergence between prior and posterior from previous trial
            KLdivPrevTrial(CS1_ind(2:end)) = modelpred{1,sublist(subInd)}.KL(1,sesstrials(2:end)-1); % CS modulation possible from 2nd trial
            KLdivPrevTrial(CS2_ind(2:end)) = modelpred{1,sublist(subInd)}.KL(2,sesstrials(2:end)-1); % CS modulated by the value from the previous trial
            KLdivPrevTrial(CS3_ind(2:end)) = modelpred{1,sublist(subInd)}.KL(3,sesstrials(2:end)-1); % Not possible to modulate with value from current trial
            KLdivPrevTrial(CS4_ind(2:end)) = modelpred{1,sublist(subInd)}.KL(4,sesstrials(2:end)-1); % because outcome not observed yet at CS time
            KLdivPrevTrial(isnan(KLdivPrevTrial)) = nanmean(KLdivPrevTrial); % First CS cannot be modulated by KL div based on non-existent outcome
            % Set to mean in order to have no influence on the estimation
            % Other option: remove this CS entirely
            
            % Surprise about US from previous trial
            surprUSPrevTrial(CS1_ind(2:end)) = modelpred{1,sublist(subInd)}.SO(1,sesstrials(2:end)-1);
            surprUSPrevTrial(CS2_ind(2:end)) = modelpred{1,sublist(subInd)}.SO(2,sesstrials(2:end)-1);
            surprUSPrevTrial(CS3_ind(2:end)) = modelpred{1,sublist(subInd)}.SO(3,sesstrials(2:end)-1);
            surprUSPrevTrial(CS4_ind(2:end)) = modelpred{1,sublist(subInd)}.SO(4,sesstrials(2:end)-1);
            surprUSPrevTrial(isnan(surprUSPrevTrial)) = nanmean(surprUSPrevTrial); % First CS cannot be modulated by surprise based on non-existent outcome
            % Set to mean in order to have no influence on the estimation
            
            pmod(1).name  = {'Expected p(shock)','Volatility',...
                'Prior entropy p(shock)','KL div prior-posterior previous trial',...
                'Surprise on US previous trial'};
            pmod(1).param = {exptShockProb',volatility',priorEntrShockProb',KLdivPrevTrial',surprUSPrevTrial'};
            pmod(1).poly  = {1,1,1,1,1};
            
            % KL divergence between prior and posterior from current trial
            KLdivCurrTrial(CS1) = modelpred{1,sublist(subInd)}.KL(1,sesstrials);
            KLdivCurrTrial(CS2) = modelpred{1,sublist(subInd)}.KL(2,sesstrials);
            KLdivCurrTrial(CS3) = modelpred{1,sublist(subInd)}.KL(3,sesstrials);
            KLdivCurrTrial(CS4) = modelpred{1,sublist(subInd)}.KL(4,sesstrials);
            if USremoved; KLdivCurrTrial(end) = []; end% In case last US of each session excluded
            
            % Surprise about US from current trial
            surprUSCurrTrial(CS1) = modelpred{1,sublist(subInd)}.SO(1,sesstrials);
            surprUSCurrTrial(CS2) = modelpred{1,sublist(subInd)}.SO(2,sesstrials);
            surprUSCurrTrial(CS3) = modelpred{1,sublist(subInd)}.SO(3,sesstrials);
            surprUSCurrTrial(CS4) = modelpred{1,sublist(subInd)}.SO(4,sesstrials);
            if USremoved; surprUSCurrTrial(end) = []; end % In case last US of each session excluded
            
            pmod(2).name  = {'US type','KL div prior-posterior current trial','Surprise on US current trial'};
            pmod(2).param = {US,KLdivCurrTrial',surprUSCurrTrial'};
            pmod(2).poly  = {1,1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_2.mat',opts.subj_beh(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
            
        end
        
        %% Model 1a Acq vs. Acq2
        if ismember(20,models)
            
            clear pmod shockProb
            
            shockProb = nan(1,length(CS1));
            
            shockProb(CS1) = 0;
            shockProb(CS2) = 1/3;
            shockProb(CS3) = 2/3;
            shockProb(CS4) = 1;
            
            pmod(1).name  = {'True p(shock)'};
            pmod(1).param = {shockProb'};
            pmod(1).poly  = {1};
            
            clear predictionError
            predictionError = US-shockProb(1:USleng)';
            pmod(2).name  = {'US type','Prediction error'};
            pmod(2).param = {US,predictionError};
            pmod(2).poly  = {1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1_Acq_vs_Acq2.mat',opts.subj_beh(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
        end
        
        %% Model 1a modified 1
        % Additional CS modulator: outcome from previous trial of
        % same type
        
        if ismember(21,models)
            
            clear pmod shockProb prevUS
            
            shockProb = nan(1,length(CS1));
            prevUS = nan(1,length(CS1));
            
            shockProb(CS1) = 0;
            shockProb(CS2) = 1/3;
            shockProb(CS3) = 2/3;
            shockProb(CS4) = 1;
            
            % Find indices for each CS type
            CS1_ind = find(CS1);
            CS2_ind = find(CS2);
            CS3_ind = find(CS3);
            CS4_ind = find(CS4);
            
            % Too short last USs don't matter because not used anyway here
            % (last US doesn't modulate any CS)
            prevUS(CS1_ind(2:end)) = US(CS1_ind(1:end-1)); % CS modulation possible from 2nd trial
            prevUS(CS2_ind(2:end)) = US(CS2_ind(1:end-1)); % CS modulated by the value from the previous trial
            prevUS(CS3_ind(2:end)) = US(CS3_ind(1:end-1)); % Not possible to modulate with value from current trial
            prevUS(CS4_ind(2:end)) = US(CS4_ind(1:end-1)); % because outcome not observed yet at CS time
            prevUS(isnan(prevUS)) = nanmean(prevUS); % First CS cannot be modulated based on non-existent outcome
            % Set to mean in order to have no influence on the estimation
            
            pmod(1).name  = {'True p(shock)' 'Previous US'};
            pmod(1).param = {shockProb' prevUS'};
            pmod(1).poly  = {1,1};
            
            clear predictionError
            predictionError = US-shockProb(1:USleng)';
            pmod(2).name  = {'US type','Prediction error'};
            pmod(2).param = {US,predictionError};
            pmod(2).poly  = {1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1_CS-US.mat',opts.subj_beh(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
        end
        
        %% Model 1a modified 2
        % Additional CS modulator: prediction error from previous trial of
        % same type
        if ismember(22,models)
            
            clear pmod shockProb prevPE predictionError
            
            shockProb = nan(1,length(CS1));
            prevPE = nan(1,length(CS1));
            
            shockProb(CS1) = 0;
            shockProb(CS2) = 1/3;
            shockProb(CS3) = 2/3;
            shockProb(CS4) = 1;
            
            % Find indices for each CS type
            CS1_ind = find(CS1);
            CS2_ind = find(CS2);
            CS3_ind = find(CS3);
            CS4_ind = find(CS4);
            
            % Too short last USs don't matter because not used anyway here
            % (last US doesn't modulate any CS)
            predictionError = US-shockProb(1:USleng)';
            
            prevPE(CS1_ind(2:end)) = predictionError(CS1_ind(1:end-1)); % CS modulation possible from 2nd trial
            prevPE(CS2_ind(2:end)) = predictionError(CS2_ind(1:end-1)); % CS modulated by the value from the previous trial
            prevPE(CS3_ind(2:end)) = predictionError(CS3_ind(1:end-1)); % Not possible to modulate with value from current trial
            prevPE(CS4_ind(2:end)) = predictionError(CS4_ind(1:end-1)); % because outcome not observed yet at CS time
            prevPE(isnan(prevPE)) = nanmean(prevPE); % First CS cannot be modulated based on non-existent outcome
            % Set to mean in order to have no influence on the estimation
            
            pmod(1).name  = {'True p(shock)' 'Previous prediction error'};
            pmod(1).param = {shockProb' prevPE'};
            pmod(1).poly  = {1,1};
            
            pmod(2).name  = {'US type','Prediction error'};
            pmod(2).param = {US,predictionError};
            pmod(2).poly  = {1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1_CS-PE.mat',opts.subj_beh(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
        end
        
        %% Model 1i
        % Positive prediction error with expected p(shock) from the
        % Bayesian model
        
        if ismember(23,models)
            
            clear pmod BayesPredictionError exptShockProb
            
            exptShockProb = nan(1,length(CS1));
            
            % Expected shock probability
            exptShockProb(CS1) = modelpred{1,sublist(subInd)}.BM(1,sesstrials);
            exptShockProb(CS2) = modelpred{1,sublist(subInd)}.BM(2,sesstrials);
            exptShockProb(CS3) = modelpred{1,sublist(subInd)}.BM(3,sesstrials);
            exptShockProb(CS4) = modelpred{1,sublist(subInd)}.BM(4,sesstrials);
            
            pmod(1).name  = {'Expected p(shock)'};
            pmod(1).param = {exptShockProb'};
            pmod(1).poly  = {1};
            
            USp = US==1;
            USm = US==0;
            BayesPredictionError(USp) = 1-exptShockProb(USp);
            BayesPredictionError(USm) = nanmean(BayesPredictionError);
            % trials not taken into account (US-) set to session mean to
            % exclude their influence
            
            pmod(2).name  = {'US type','Positive model prediction error'};
            pmod(2).param = {US,BayesPredictionError};
            pmod(2).poly  = {1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1i.mat',opts.subj_beh(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
            
        end
        
        %% Model 1j
        % Negative prediction error with expected p(shock) from the
        % Bayesian model
        
        if ismember(24,models)
            
            clear pmod BayesPredictionError exptShockProb
            
            exptShockProb = nan(1,length(CS1));
            
            % Expected shock probability
            exptShockProb(CS1) = modelpred{1,sublist(subInd)}.BM(1,sesstrials);
            exptShockProb(CS2) = modelpred{1,sublist(subInd)}.BM(2,sesstrials);
            exptShockProb(CS3) = modelpred{1,sublist(subInd)}.BM(3,sesstrials);
            exptShockProb(CS4) = modelpred{1,sublist(subInd)}.BM(4,sesstrials);
            
            pmod(1).name  = {'Expected p(shock)'};
            pmod(1).param = {exptShockProb'};
            pmod(1).poly  = {1};
            
            USp = US==1;
            USm = US==0;
            BayesPredictionError(USm) = 0-exptShockProb(USm);
            BayesPredictionError(USp) = nanmean(BayesPredictionError);
            % trials not taken into account (US+) set to session mean to
            % exclude their influence
            
            pmod(2).name  = {'US type','Negative model prediction error'};
            pmod(2).param = {US,BayesPredictionError};
            pmod(2).poly  = {1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1j.mat',opts.subj_beh(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
            
        end
        
        %% Model 1k
        % Unsigned prediction error with expected p(shock) from the
        % Bayesian model
        
        if ismember(25,models)
            
            clear pmod BayesPredictionError exptShockProb
            
            exptShockProb = nan(1,length(CS1));
            
            % Expected shock probability
            exptShockProb(CS1) = modelpred{1,sublist(subInd)}.BM(1,sesstrials);
            exptShockProb(CS2) = modelpred{1,sublist(subInd)}.BM(2,sesstrials);
            exptShockProb(CS3) = modelpred{1,sublist(subInd)}.BM(3,sesstrials);
            exptShockProb(CS4) = modelpred{1,sublist(subInd)}.BM(4,sesstrials);
            
            pmod(1).name  = {'Expected p(shock)'};
            pmod(1).param = {exptShockProb'};
            pmod(1).poly  = {1};
            
            BayesPredictionError = abs(US-exptShockProb(1:USleng)');
            
            pmod(2).name  = {'US type','Unsigned model prediction error'};
            pmod(2).param = {US,BayesPredictionError};
            pmod(2).poly  = {1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1k.mat',opts.subj_beh(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
            
        end
        
        %% Model 1g Acq vs. Acq2
        if ismember(26,models)
            
            clear pmod shockProb predictionError
            
            shockProb = nan(1,length(CS1));
            predictionError = nan(1,USleng);
            
            shockProb(CS1) = 0;
            shockProb(CS2) = 1/3;
            shockProb(CS3) = 2/3;
            shockProb(CS4) = 1;
            
            pmod(1).name  = {'True p(shock)'};
            pmod(1).param = {shockProb'};
            pmod(1).poly  = {1};
            
            USp = US==1;
            USm = US==0;
            predictionError(USm) = 0-shockProb(USm);
            predictionError(USp) = nanmean(predictionError);
            % trials not taken into account (US+) set to session mean to
            % exclude their influence
            
            pmod(2).name  = {'US type','Negative prediction error'};
            pmod(2).param = {US,predictionError'};
            pmod(2).poly  = {1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1g_Acq_vs_Acq2.mat',opts.subj_beh(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
        end
        
        %% Model 1g CS with US- only
        if ismember(27,models)
            
            clear pmod shockProb predictionError
            
            shockProb = nan(1,length(CS1));
            predictionError = nan(1,USleng);
            
            shockProb(CS1) = 0;
            shockProb(CS2) = 1/3;
            shockProb(CS3) = 2/3;
            shockProb(CS4) = 1;
            
            USp = US==1;
            USm = US==0;
            predictionError(USm) = 0-shockProb(USm);
            predictionError(USp) = nanmean(predictionError);
            % trials not taken into account (US+) set to session mean to
            % exclude their influence
            shockProb(USp) = nanmean(shockProb);
            % for CS shock prob as well
            
            pmod(1).name  = {'True p(shock)'};
            pmod(1).param = {shockProb'};
            pmod(1).poly  = {1};
            
            pmod(2).name  = {'US type','Negative prediction error'};
            pmod(2).param = {US,predictionError'};
            pmod(2).poly  = {1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1g_CS+US-.mat',opts.subj_beh(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
        end
        
        
        %% Model 1l
        % Interaction of time and shock probability/US expectation
        % (learning effects)
        if ismember(28,models) && (iSess > 1 && iSess < 6) % Only for Maintenance phase
            
            clear pmod shockProb trialNumber trialNo_x_shockProb
            
            shockProb = nan(1,length(CS1));
            
            shockProb(CS1) = 0;
            shockProb(CS2) = 1/3;
            shockProb(CS3) = 2/3;
            shockProb(CS4) = 1;
            
            % Time = trial number
            trialNumber = alltrials_maint{iSess}';
            
            % Trial number x True shock probability
            trialNo_x_shockProb = (trialNumber-mean(trialNumber)).*(shockProb'-mean(shockProb'));
            
            pmod(1).name  = {'True p(shock)','Trial number','Trial no. x True p(shock)'};
            pmod(1).param = {shockProb',trialNumber,trialNo_x_shockProb};
            pmod(1).poly  = {1,1,1};
            
            
            pmod(2).name  = {'US type'};
            pmod(2).param = {US};
            pmod(2).poly  = {1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1l.mat',opts.subj_beh(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
            
        end
        
        if ismember(29,models)
            
            clear pmod riskPrediction RPx
            
            riskPrediction = nan(1,length(CS1));
            
            % coding of the two possible outcomes
            USp = 1;
            USm = 0;
            
            % probability of US (= expected outcome) for 6 conditions
            p = [0 1/3 2/3;
                1/3 2/3 1];
            
            eo = p * USp;
            
            % actual outcome for 6 conditions
            o = [USm, USm, USm;
                USp, USp, USp];
            
            % risk prediction
            RP = (1-p) .* (USm - eo).^2 + p .* (USp - eo).^2;
            RPx = RP([1 2 4 6]);
            
%             RP2_1 = (1-p(3)) * (USm - eo(3))^2;
%             RP2_2 = p(2) * (USp - eo(2))^2;
%             RP2 = RP2_1 + RP2_2;
%             
%             RP3_1 = (1-p(5)) * (USm - eo(5))^2;
%             RP3_2 = p(4) * (USp - eo(4))^2;
%             RP3 = RP3_1 + RP3_2;
            
            riskPrediction(CS1) = RPx(1);
            riskPrediction(CS2) = RPx(2);
            riskPrediction(CS3) = RPx(3);
            riskPrediction(CS4) = RPx(4);
            
            pmod(1).name  = {'Risk prediction'};
            pmod(1).param = {riskPrediction'};
            pmod(1).poly  = {1};
            
            clear riskPredictionError RPE
            
            riskPredictionError = nan(1,USleng);
            
            RPE = (o - eo).^2 - RP;
            USp = US==1;
            USm = US==0;
            
            riskPredictionError(CS1(1:USleng)) = RPE(1);
            riskPredictionError(CS2(1:USleng) & USp) = RPE(2);
            riskPredictionError(CS2(1:USleng) & USm) = RPE(3);
            riskPredictionError(CS3(1:USleng) & USp) = RPE(4);
            riskPredictionError(CS3(1:USleng) & USm) = RPE(5);
            riskPredictionError(CS4(1:USleng)) = RPE(6);
            
            pmod(2).name  = {'US type','Risk prediction error'};
            pmod(2).param = {US,riskPredictionError'};
            pmod(2).poly  = {1,1};
            
            outputFile = sprintf('S%d_block%d_CSUSconds_Model_Par_1m.mat',opts.subj_beh(subInd),iSess);
            outputFile = fullfile(behavDir,outputFile);
            save(outputFile,'names','onsets','durations','orth','pmod','tmod');
            
        end
    end
    
end

end