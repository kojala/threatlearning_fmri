function secondlevel_1sttest_covariate(fpath,contrastname,contrastno,actualname,direction,printresults)

conpath = [fpath '\' contrastname '\CovariateModel'];

covdata = load(fullfile(cd,'..','..','Behavior','CSUS_contingency_slopes_allsubs_MaintAcq.mat'));
contingency_slopes = covdata.contingency_slopes;

%% 2nd level model specification
matlabbatch{1}.spm.stats.factorial_design.dir = {fullfile(conpath)};
matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = cellstr(spm_select('ExtFPList',fullfile(fpath,'data','contrasts'),['con_00' contrastno '.*.nii$'],1));
matlabbatch{1}.spm.stats.factorial_design.cov.c = contingency_slopes';
matlabbatch{1}.spm.stats.factorial_design.cov.cname = 'ContingencyLearning';
matlabbatch{1}.spm.stats.factorial_design.cov.iCFI = 1;
matlabbatch{1}.spm.stats.factorial_design.cov.iCC = 1;
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {''}; %cellstr(spm_select('ExtFPList',fullfile(fpath),'explicitmask.nii$',1));
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;

%% 2nd level model estimation
matlabbatch{2}.spm.stats.fmri_est.spmmat(1) = cellstr(fullfile(conpath,'SPM.mat'));
matlabbatch{2}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;

%% Contrasts
matlabbatch{3}.spm.stats.con.spmmat = {fullfile(conpath,'SPM.mat')};
matlabbatch{3}.spm.stats.con.consess{1}.tcon.name = actualname;
matlabbatch{3}.spm.stats.con.consess{1}.tcon.weights = direction;
matlabbatch{3}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.delete = 1;

%% Print results
if printresults == 1
    matlabbatch{4}.spm.stats.results.spmmat = {fullfile(conpath,'SPM.mat')};
    matlabbatch{4}.spm.stats.results.conspec.titlestr = '';
    matlabbatch{4}.spm.stats.results.conspec.contrasts = Inf;
    matlabbatch{4}.spm.stats.results.conspec.threshdesc = 'none';
    matlabbatch{4}.spm.stats.results.conspec.thresh = 0.001;
    matlabbatch{4}.spm.stats.results.conspec.extent = 0;
    matlabbatch{4}.spm.stats.results.conspec.conjunction = 1;
    matlabbatch{4}.spm.stats.results.conspec.mask.none = 1;
    matlabbatch{4}.spm.stats.results.units = 1;
    matlabbatch{4}.spm.stats.results.print = 'pdf';
    matlabbatch{4}.spm.stats.results.write.none = 1;
end

%% Run matlabbatch
spm_jobman('initcfg')
spm_jobman('run', matlabbatch)

end