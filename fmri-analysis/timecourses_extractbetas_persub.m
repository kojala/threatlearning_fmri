%function timecourses_extractbetas_persub(opts,sublist)
clear all

opts = get_options();
sublist = [1:13 15:21];

%% Settings
mripath = opts.fpath;
subj = opts.subj(sublist);

bhvpath = fullfile(opts.mainpath,'Behavior');
subj_beh = opts.subj_beh(sublist);

opts = get_options();
model = 2; % Model 1a
phase = 2; % Maintenance
roiname = 'PE_cluster1_p001cluscorr';
regname = 'parmod_US_PE';
%regressor = 5; % Order number of the regressor of interest. True p PE
model_inp = get_modelconfig(opts,model);

seclvlpath = fullfile(opts.fpath, '2ndlevel', ['Version_' opts.version], ['NoiseCorr_' opts.noisecorr_name], model_inp.modelname, opts.phasenames{phase});
regpath = fullfile(seclvlpath,['1sttest_' regname]);
roifile = fullfile(regpath,[roiname '.nii']);

% ROI coordinates
Y = spm_read_vols(spm_vol(roifile),1); % Y is 4D matrix of image data
indx = find(Y>0);
[x,y,z] = ind2sub(size(Y),indx);
XYZ = [x y z]';

%% Beta extraction for selected ROI
for sub = 1:length(subj)

    sub_mripath = fullfile(mripath, subj(sub), '1stlevel', ['Version_' opts.version], ['NoiseCorr_' opts.noisecorr_name], model_inp.modelname, ['First_Level_' opts.phasenames{phase}]);

    betafiles = spm_select('FPList',sub_mripath,'^beta.*nii');
   
    filename = ['S' subj_beh(sub)];
    bhvname  = fullfile(ExpPath, 'Behavior', filename, ['Behavior_' filename '.mat']); % Behaviour file
    bhvdata = load(bhvname);
    
    if strncmp(subj{sub},'PR02121',7)
        block2 = tim(2).CS_on;
        block3 = NaN(44,1);
        block4 = tim(4).CS_on;
        block5 = tim(5).CS_on;
        timings = [block 2; block3; block4; block5];
    else
        block2 = tim(2).CS_on;
        block3 = tim(3).CS_on;
        block4 = tim(4).CS_on;
        block5 = tim(5).CS_on;
        timings = [block 2; block3; block4; block5];
    end
    
    % Choose regressor of interest for all runs
%     if strncmp(subj{sub},'PR02121',7) % this sub only has 3 runs of maintenance phase
%         reg1 = betafiles(5,:);
%         reg2 = [];
%         reg3 = betafiles(5+24+5,:);
%         reg4 = betafiles(5+24+5+24+5,:);
%     else
%         reg1 = betafiles(5,:);
%         reg2 = betafiles(5+24+5,:);
%         reg3 = betafiles(5+24+5+24+5,:);
%         reg4 = betafiles(5+24+5+24+5+24+5,:);
%     end
%     
%     betafiles_inp = {reg1 reg2 reg3 reg4};
    
    % Load SPM file
    SPMfile = char(fullfile(sub_mripath,'SPM.mat'));
    load(SPMfile)

    % Get time courses
    y = spm_get_data(SPM.xY.VY, XYZ);

    % Processing    
    y = SPM.xX.W*y; % Prewhitening (sphericity correction)
    y = spm_filter(SPM.xX.K, y); % High-pass filtering
    
    % Extract betas from the ROI for this condition
    timecourse = y(1:689,:);

%     if strncmp(subj{sub},'PR02121',7) % This subject lacks run 3, padding with nans
%         betas = [betas(1:68); nan(44,1); betas(69:end)];
%     end
    
    timecourse_allsubs(sub,:) = mean(timecourse,2); 
    
end

save(fullfile(opts.roimaskpath,'Timecourses','Timecourses_Model1a_PE_frontal_allsubs.mat'),'timecourse_allsubs');

figure
plot(mean(timecourse_allsubs),'-o','LineWidth',1.5,'MarkerFaceColor','b','MarkerEdgeColor','b','MarkerSize',2)
title({'BOLD TIMECOURSE' 'Model 1a: Prediction error cluster (341 voxels) - Maintenance'})
% xlim([0 22])
set(gca,'XTick',0:25:700)
set(gca,'XTickLabel',{round((0:25:700))});
ylabel('BOLD')
xlabel('Seconds')

%end