clear all

model = 2; % Model 1a
phase = 2; % Maintenance
runs = 2:5;

opts = get_options();
model_inp = get_modelconfig(opts,model);

sublist = 1:21;
subj = opts.subj(sublist);
subj_beh = opts.subj_beh;

bhvpath = fullfile(opts.mainpath,'Behavior');
mripath = opts.fpath;

roifile = fullfile(opts.roimaskpath,'Functional','PE_cluster1_p001cluscorr.nii');
roiname = 'frontalPE';

% Retrieve VOIs for all subjects and all runs
for sub = 3%1:length(subj)
    
    sub_mripath = fullfile(mripath, subj(sub), '1stlevel', ['Version_' opts.version], ['NoiseCorr_' opts.noisecorr_name], model_inp.modelname, ['First_Level_' opts.phasenames{phase}]);   
    sub_bhvpath = fullfile(bhvpath, ['S' num2str(subj_beh(sub))]);
    
    if strncmp(subj{sub},'PR02121',7)
        runs = [2 4 5];
    end
    
    runtrials = [25:68; 69:112; 113:156; 157:200];
    
    timecourse_cond1 = nan(4,6);
    timecourse_cond2 = nan(4,6);
    timecourse_cond3 = nan(4,6);
    timecourse_cond4 = nan(4,6);
    timecourse_cond5 = nan(4,6);
    timecourse_cond6 = nan(4,6);
        
    for run = runs
        
        %bhvfile = fullfile(sub_bhvpath, ['S' num2str(subj_beh(sub)) '_block' num2str(run) '_CSUSconds_' model_inp.modelname '.mat']);
        bhvfile = fullfile(sub_bhvpath, ['PCF_05_' num2str(subj_beh(sub)) '_Block' num2str(run) '.mat']);
        bhvdata = load(bhvfile);
        
        voiname = ['VOI_sub' num2str(sub) '_run' num2str(run) '_' roiname '_' num2str(run) '.mat'];
        voifile = char(fullfile(sub_mripath,voiname));
        voidata = load(voifile);
        
        xY = voidata.xY;
        Y = voidata.Y;
        
        Yc  = Y - xY.X0*inv(xY.X0'*xY.X0)*xY.X0'*Y; %#ok<MINV>
        
        voi_eigenvars(run-1,:) = Yc;
        
        %onsets_CS = round(bhvdata.onsets{1}./3.2);
        %offsets_CS = round((bhvdata.onsets{1}+6.5)./3.2);
        %onsets_US = round(bhvdata.onsets{2}./3.2);
        %trialend = round((bhvdata.onsets{1}+12.5)./3.2);
        onsets_CS = bhvdata.indata(:,5);

%         itis = bhvdata.tm;
%         itis_run = itis(runtrials(run-1,:),6);
        
        CStype = bhvdata.indata(1:43,2);
        UStype = bhvdata.indata(1:43,3);
        
        for trial = 1:43
%             trialend = round((onsets_CS(trial)+itis_run(trial)+6500)./3200);
            trialend = round((onsets_CS(trial)+5000+6500)./3200);
            onset_CS = round(bhvdata.indata(trial,5)./3200);
            
            timecourse{trial,:} = voi_eigenvars(onset_CS:trialend);
        end
            
        cond1 = CStype == 1;
        cond2 = CStype == 2 & UStype == 1;
        cond3 = CStype == 2 & UStype == 0;
        cond4 = CStype == 3 & UStype == 1;
        cond5 = CStype == 3 & UStype == 0;
        cond6 = CStype == 4;
        
        timecourse_cond1{run-1} = timecourse(cond1,:);
        timecourse_cond2{run-1,:} = timecourse{cond2,:};
        timecourse_cond3{run-1,:} = timecourse{cond3,:};
        timecourse_cond4{run-1,:} = timecourse{cond4,:};
        timecourse_cond5{run-1,:} = timecourse{cond5,:};
        timecourse_cond6{run-1,:} = timecourse{cond6,:};
        
    end
    
    timecourse_cond1mean(sub) = nanmean(timecourse_cond1);
    timecourse_cond2mean(sub) = nanmean(timecourse_cond2);
    timecourse_cond3mean(sub) = nanmean(timecourse_cond3);
    timecourse_cond4mean(sub) = nanmean(timecourse_cond4);
    timecourse_cond5mean(sub) = nanmean(timecourse_cond5);
    timecourse_cond6mean(sub) = nanmean(timecourse_cond6);
    
end

timecourse_cond1_all = mean(timecourse_cond1mean);
timecourse_cond2_all = mean(timecourse_cond2mean);
timecourse_cond3_all = mean(timecourse_cond3mean);
timecourse_cond4_all = mean(timecourse_cond4mean);
timecourse_cond5_all = mean(timecourse_cond5mean);
timecourse_cond6_all = mean(timecourse_cond6mean);

figure;plot(timecourse_cond1_all);