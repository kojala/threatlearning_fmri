function pipeline_analysis_within_mvpa(version,sublist,rois,actions)
%%MVPA pipeline for fMRI data

opts = get_options();

% Add paths
addpath(opts.mainpath)
addpath(opts.fpath_mvpa)
addpath(genpath(opts.codepath))
addpath(opts.spmpath)

% Set SPM settings
spm('defaults', 'FMRI');
spm_jobman initcfg
spm_get_defaults('cmdline',opts.spm_cmdline) % No graphical elements during analysis

%% Main script options

% Running a specific list of subjects
% opts.subj = opts.subj(sublist);
% opts.subj_beh = opts.subj_beh(sublist);

%% MVPA
% if actions.run_mvpa_prepbehav
%     
%     for p = phase
%     
%         prepare_behav_MVPA_trialwise(opts,p);
%     
%     end
%     
% end

if actions.run_mvpa_1stlevel
        
    firstlevel_MVPA_trialwise(opts,sublist)
end

if actions.run_mvpa_wholebrain_setup

    setup_mvpa_wholebrain(opts,sublist,version)
    
end

if actions.run_mvpa_wholebrain
   
    run_mvpa_svr_searchlight(opts,sublist)
    
end

if actions.run_mvpa_setup
    
    setup_mvpa(opts,sublist,version,rois)
    
end

if actions.run_mvpa_svr
   
    run_mvpa_svr(opts,sublist,rois)
    
end

if actions.run_mvpa_preval_infer
   
    run_mvpa_prevalence_inference(opts,rois)
    
end

if actions.run_mvpa_plot_coeff
    
    plot_mvpa_coeff(opts,rois)
    
end