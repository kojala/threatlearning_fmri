function physio_import(opts)

sList = opts.sList;

for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    
    % File
    file       = fullfile(opts.rawPath, ['S' s_id '.mat']); % Original BIOPAC AcqKnowledge data file
    importfile = fullfile(opts.importPath, ['pspm_PCF' s_id '.mat']); % Imported file
    
    if exist(file,'file') && ~exist(importfile,'file')
        
        datatype = 'acqmat';
        
        import{opts.phys.scr_chan}.type = 'scr';
        import{opts.phys.scr_chan}.channel = opts.phys.scr_chan;
        import{opts.phys.scr_chan}.transfer = 'none';
        
        import{opts.phys.ppu_chan}.type = 'ppu';
        import{opts.phys.ppu_chan}.channel = opts.phys.ppu_chan;
        
        import{opts.phys.resp_chan}.type = 'resp';
        import{opts.phys.resp_chan}.channel = opts.phys.resp_chan;
        
        import{opts.phys.scantrig_chan}.type = 'custom';
        import{opts.phys.scantrig_chan}.channel = opts.phys.scantrig_chan;
        
        options.overwrite = true;
        
        % Due to AcqKnowledge selection window during exporting to MATLAB
        % format some subjects have start_sample =~ which will give a
        % warning in PsPM. Fix this here, original data backed up. 
        rawdata = load(file);
        if rawdata.start_sample ~= 0
            start_sample = 0;
            save(file,'start_sample','-append');
        end
        
        imported_file = pspm_import(file, datatype, import, options); % import
        if ~isempty(imported_file)
            pspm_ren(imported_file{:},importfile); % rename
        else
           sprintf(['Problem importing data for subject ' s_id]) 
        end
        
    end
    
end

end