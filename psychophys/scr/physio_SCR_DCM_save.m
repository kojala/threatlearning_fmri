function physio_SCR_DCM_save(opts)

sList = opts.scr.sList.DCM; 

dcmPath = fullfile(opts.modelPath,'SCR','DCM','DCM_107');

%% Retrieve DCM stats

dcm_est_cs1 = nan(length(sList),1);
dcm_est_cs2 = nan(length(sList),1);
dcm_est_cs3 = nan(length(sList),1);
dcm_est_cs4 = nan(length(sList),1);
dcm_est_cs5 = nan(length(sList),1);
dcm_est_cs6 = nan(length(sList),1);

dcm_est_us1 = nan(length(sList),1);
dcm_est_us2 = nan(length(sList),1);
dcm_est_us3 = nan(length(sList),1);
dcm_est_us4 = nan(length(sList),1);
dcm_est_us5 = nan(length(sList),1);
dcm_est_us6 = nan(length(sList),1);

dcm_est_cs1_trial = nan(length(sList),opts.scr.dcm_trials/4);
dcm_est_cs2_trial = nan(length(sList),opts.scr.dcm_trials/4*0.36);
dcm_est_cs3_trial = nan(length(sList),opts.scr.dcm_trials/4*0.64);
dcm_est_cs4_trial = nan(length(sList),opts.scr.dcm_trials/4*0.64);
dcm_est_cs5_trial = nan(length(sList),opts.scr.dcm_trials/4*0.36);
dcm_est_cs6_trial = nan(length(sList),opts.scr.dcm_trials/4);

dcm_est_us1_trial = nan(length(sList),opts.scr.dcm_trials/4);
dcm_est_us2_trial = nan(length(sList),opts.scr.dcm_trials/4*0.36);
dcm_est_us3_trial = nan(length(sList),opts.scr.dcm_trials/4*0.64);
dcm_est_us4_trial = nan(length(sList),opts.scr.dcm_trials/4*0.64);
dcm_est_us5_trial = nan(length(sList),opts.scr.dcm_trials/4*0.36);
dcm_est_us6_trial = nan(length(sList),opts.scr.dcm_trials/4);

for s = 1:numel(sList)
    
    s_id = num2str(sList(s));
    
    filename = ['dcm_' s_id '.mat'];
    dcmfile = fullfile(dcmPath,filename);
    
    if exist(dcmfile,'file')
        
        dcmdata = load(dcmfile);
        
        trials = 1:size(dcmdata.dcm.stats,1);
        
        dcm_est_cs = dcmdata.dcm.stats(trials,1);
        dcm_est_us = dcmdata.dcm.stats(trials,4);
        
        condition_type = [];
        for block = 1:5
            fname_event = ['event_types_S' s_id '_Block' num2str(block) '.mat'];
            eventfile = fullfile(opts.eventPath, fname_event);
            bhvdata = load(eventfile);
            condition_type = [condition_type bhvdata.allconditions];
        end
        
        cs1 = condition_type == 1; % CS(0)US-
        cs2 = condition_type == 2; % CS(1/3)US+
        cs3 = condition_type == 3; % CS(1/3)US-
        cs4 = condition_type == 4; % CS(2/3)US+
        cs5 = condition_type == 5; % CS(2/3)US-
        cs6 = condition_type == 6; % CS(1)US+
        
        us1 = condition_type == 1; % CS(0)US-
        us2 = condition_type == 2; % CS(1/3)US+
        us3 = condition_type == 3; % CS(1/3)US-
        us4 = condition_type == 4; % CS(2/3)US+
        us5 = condition_type == 5; % CS(2/3)US-
        us6 = condition_type == 6; % CS(1)US+
        
        % Trial-by-trial data
        dcm_est_cs1_trial(s,:) = dcm_est_cs(cs1);
        dcm_est_cs2_trial(s,:) = dcm_est_cs(cs2);
        dcm_est_cs3_trial(s,:) = dcm_est_cs(cs3);
        dcm_est_cs4_trial(s,:) = dcm_est_cs(cs4);
        dcm_est_cs5_trial(s,:) = dcm_est_cs(cs5);
        dcm_est_cs6_trial(s,:) = dcm_est_cs(cs6);
        
        dcm_est_us1_trial(s,:) = dcm_est_us(us1);
        dcm_est_us2_trial(s,:) = dcm_est_us(us2);
        dcm_est_us3_trial(s,:) = dcm_est_us(us3);
        dcm_est_us4_trial(s,:) = dcm_est_us(us4);
        dcm_est_us5_trial(s,:) = dcm_est_us(us5);
        dcm_est_us6_trial(s,:) = dcm_est_us(us6);
        
        trial_data_out.subject(s,:) = sList(s);
        trial_data_out.trial(s,:) = trials;
        trial_data_out.cs(s,:) = dcm_est_cs;
        trial_data_out.us(s,:) = dcm_est_us;
        trial_data_out.cond(s,:) = condition_type;
        
        
        % Condition-wise average
        if opts.scr.summarystat == 1
            dcm_est_cs1(s) = nanmean(dcm_est_cs(cs1));
            dcm_est_cs2(s) = nanmean(dcm_est_cs(cs2));
            dcm_est_cs3(s) = nanmean(dcm_est_cs(cs3));
            dcm_est_cs4(s) = nanmean(dcm_est_cs(cs4));
            dcm_est_cs5(s) = nanmean(dcm_est_cs(cs5));
            dcm_est_cs6(s) = nanmean(dcm_est_cs(cs6));
            
            dcm_est_us1(s) = nanmean(dcm_est_us(us1));
            dcm_est_us2(s) = nanmean(dcm_est_us(us2));
            dcm_est_us3(s) = nanmean(dcm_est_us(us3));
            dcm_est_us4(s) = nanmean(dcm_est_us(us4));
            dcm_est_us5(s) = nanmean(dcm_est_us(us5));
            dcm_est_us6(s) = nanmean(dcm_est_us(us6));
            
        else
            dcm_est_cs1(s) = nanmedian(dcm_est_cs(cs1));
            dcm_est_cs2(s) = nanmedian(dcm_est_cs(cs2));
            dcm_est_cs3(s) = nanmedian(dcm_est_cs(cs3));
            dcm_est_cs4(s) = nanmedian(dcm_est_cs(cs4));
            dcm_est_cs5(s) = nanmedian(dcm_est_cs(cs5));
            dcm_est_cs6(s) = nanmedian(dcm_est_cs(cs6));
            
            dcm_est_us1(s) = nanmedian(dcm_est_us(us1));
            dcm_est_us2(s) = nanmedian(dcm_est_us(us2));
            dcm_est_us3(s) = nanmedian(dcm_est_us(us3));
            dcm_est_us4(s) = nanmedian(dcm_est_us(us4));
            dcm_est_us5(s) = nanmedian(dcm_est_us(us5));
            dcm_est_us6(s) = nanmedian(dcm_est_us(us6));
        end

        
    end
    
end

dcm_data.cs{1} = dcm_est_cs1;
dcm_data.cs{2} = dcm_est_cs2;
dcm_data.cs{3} = dcm_est_cs3;
dcm_data.cs{4} = dcm_est_cs4;
dcm_data.cs{5} = dcm_est_cs5;
dcm_data.cs{6} = dcm_est_cs6;

dcm_data.us{1} = dcm_est_us1;
dcm_data.us{2} = dcm_est_us2;
dcm_data.us{3} = dcm_est_us3;
dcm_data.us{4} = dcm_est_us4;
dcm_data.us{5} = dcm_est_us5;
dcm_data.us{6} = dcm_est_us6;

dcm_data_trial.cs{1} = dcm_est_cs1_trial;
dcm_data_trial.cs{2} = dcm_est_cs2_trial;
dcm_data_trial.cs{3} = dcm_est_cs3_trial;
dcm_data_trial.cs{4} = dcm_est_cs4_trial;
dcm_data_trial.cs{5} = dcm_est_cs5_trial;
dcm_data_trial.cs{6} = dcm_est_cs6_trial;

dcm_data_trial.us{1} = dcm_est_us1_trial;
dcm_data_trial.us{2} = dcm_est_us2_trial;
dcm_data_trial.us{3} = dcm_est_us3_trial;
dcm_data_trial.us{4} = dcm_est_us4_trial;
dcm_data_trial.us{5} = dcm_est_us5_trial;
dcm_data_trial.us{6} = dcm_est_us6_trial;

%% Save data
save(fullfile(dcmPath,'DCM_cond_summary_mean.mat'),'dcm_data');
save(fullfile(dcmPath,'DCM_trial_summary.mat'),'dcm_data_trial');

% For use in JASP (wide format)
Subject = trial_data_out.subject;
SCR_CR_Cond1 = dcm_est_cs1;
SCR_CR_Cond2 = dcm_est_cs2;
SCR_CR_Cond3 = dcm_est_cs3;
SCR_CR_Cond4 = dcm_est_cs4;
SCR_CR_Cond5 = dcm_est_cs5;
SCR_CR_Cond6 = dcm_est_cs6;
datatable = table(Subject,SCR_CR_Cond1,SCR_CR_Cond2,SCR_CR_Cond3,SCR_CR_Cond4,SCR_CR_Cond5,SCR_CR_Cond6);
writetable(datatable,fullfile(dcmPath,'DCM_CR_6cond_summarytable.csv'));
SCR_CR_CS1 = dcm_est_cs1;
SCR_CR_CS2 = mean([dcm_est_cs2,dcm_est_cs3],2);
SCR_CR_CS3 = mean([dcm_est_cs4,dcm_est_cs5],2);
SCR_CR_CS4 = dcm_est_cs6;
datatable = table(Subject,SCR_CR_CS1,SCR_CR_CS2,SCR_CR_CS3,SCR_CR_CS4);
writetable(datatable,fullfile(dcmPath,'DCM_CR_4cond_summarytable.csv'));
SCR_UR_Cond1 = dcm_est_us1;
SCR_UR_Cond2 = dcm_est_us2;
SCR_UR_Cond3 = dcm_est_us3;
SCR_UR_Cond4 = dcm_est_us4;
SCR_UR_Cond5 = dcm_est_us5;
SCR_UR_Cond6 = dcm_est_us6;
datatable = table(Subject,SCR_UR_Cond1,SCR_UR_Cond2,SCR_UR_Cond3,SCR_UR_Cond4,SCR_UR_Cond5,SCR_UR_Cond6);
writetable(datatable,fullfile(dcmPath,'DCM_UR_6cond_summarytable.csv'));

% For use in R (long format)
no_trials = length(trials);
Subject = repmat(trial_data_out.subject,[no_trials 1]);
Subject = Subject(:);
data_out_cs = trial_data_out.cs';
SCR_CR = data_out_cs(:);
data_out_us = trial_data_out.us';
SCR_UR = data_out_us(:);
Trial = repmat(trials,[length(trial_data_out.subject) 1])';
Trial = Trial(:);

cond = trial_data_out.cond';
cond = cond(:);
Condition = cond;
CSprobability(cond==1) = 0;
CSprobability(cond==2 | cond==3) = 1/3;
CSprobability(cond==4 | cond==5) = 2/3;
CSprobability(cond==6) = 1;
CSprobability = CSprobability';
    
datatable = table(Subject,Trial,Condition,CSprobability,SCR_CR,SCR_UR);

summarytablefile2 = fullfile(dcmPath,'DCM_trial_summarytable.csv');
writetable(datatable,summarytablefile2);

end