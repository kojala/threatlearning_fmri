%function physio_SCR_DCM(ExpPath,sList,phase)

clear all

clear matlabbatch
ExpPath = 'D:\fMRI_Karita\';
sList = [159 161:162 164:167 169:177 179:182]; %160 not incl, bad data

for s = 1:length(sList)
    s_id = num2str(sList(s));
    
    % Create batch
    filename = ['S' s_id];
    tfname   = fullfile(ExpPath, 'SCR', ['tpspm_' filename '.mat']); % Trimmed file
    bhvname  = fullfile(ExpPath, 'Behavior', filename, ['Behavior_' filename '.mat']); % Behaviour file
    
    if exist(tfname,'file') && exist(bhvname,'file')
        
        % Separate for CS+ (US+ and US-) and CS-
        %------------------------------------------------------------------
        % Load behavioural file with event IDs
        
        load(bhvname)
        load(tfname)
        
        % File output directory
        outdirname = fullfile(ExpPath,'SCR','DCM','Allblocks');
        if ~exist(outdirname,'dir')
            mkdir(outdirname)
        end
        
        % DCM for SCR amplitude
        %------------------------------------------------------------------
        
        % 1st level non-linear DCM
        clear matlabbatch dcm
        dcm.modelfile = sprintf(['dcm_',filename]);
        dcm.outdir = {outdirname};
        dcm.chan.chan_nr = 6;
            
        blocks = 1:6;
        
        for block = blocks
            
            clear CS2pUSp CS3pUSp CS4pUSp CS2pUSm CS3pUSm CSm CS_onset
            
            if block < 6
                CS2pUSp = cond(block).CS == 2 & cond(block).US == 1;
                CS3pUSp = cond(block).CS == 3 & cond(block).US == 1;
                CS4pUSp = cond(block).CS == 4 & cond(block).US == 1;
                CS2pUSm = cond(block).CS == 2 & cond(block).US == 0;
                CS3pUSm = cond(block).CS == 3 & cond(block).US == 0;
                CSm = cond(block).CS == 1;
            else
                CS2pUSp = cond(block).CS == 7 & cond(block).US == 1;
                CS3pUSp = cond(block).CS == 8 & cond(block).US == 1;
                CS4pUSp = cond(block).CS == 5 & cond(block).US == 1;
                CS2pUSm = cond(block).CS == 7 & cond(block).US == 0;
                CS3pUSm = cond(block).CS == 8 & cond(block).US == 0;
                CSm = cond(block).CS == 6;
            end
            
            dcm.session(block).datafile = cellstr(tfname);
            eventsname = fullfile(ExpPath,'SCR','EventTimings',['event_timings_' filename '_Block' num2str(block) '.mat']); % Event file
            dcm.session(block).timing.timingfile = cellstr(eventsname);
            dcm.session(block).condition(1).name = 'CS-';
            dcm.session(block).condition(1).index = find(CSm==1)';
            dcm.session(block).condition(2).name = 'CS(1/3)+|US-';
            dcm.session(block).condition(2).index = find(CS2pUSm==1)';
            dcm.session(block).condition(3).name = 'CS(1/3)+|US+';
            dcm.session(block).condition(3).index = find(CS2pUSp==1)';
            dcm.session(block).condition(4).name = 'CS(2/3)+|US-';
            dcm.session(block).condition(4).index = find(CS3pUSm==1)';
            dcm.session(block).condition(5).name = 'CS(2/3)+|US+';
            dcm.session(block).condition(5).index = find(CS3pUSp==1)';
            dcm.session(block).condition(6).name = 'CS(1)+|US+';
            dcm.session(block).condition(6).index = find(CS4pUSp==1)';

        end
        
        dcm.data_options.norm = 0;
        dcm.data_options.filter.def = 0;
        dcm.resp_options.crfupdate = 0;
        dcm.resp_options.indrf = 0;
        dcm.resp_options.getrf = 0;
        dcm.resp_options.rf = 0;
        dcm.inv_options.depth = 2;
        dcm.inv_options.sfpre = 2;
        dcm.inv_options.sfpost = 5;
        dcm.inv_options.sffreq = 0.5;
        dcm.inv_options.sclpre = 2;
        dcm.inv_options.sclpost = 5;
        dcm.inv_options.ascr_sigma_offset = 0.1;
        dcm.disp_options.dispwin = 0;
        dcm.disp_options.dispsmallwin = 0;
        matlabbatch{1}.pspm{1}.first_level{1}.scr{1}.dcm = dcm;
        
        save(fullfile(outdirname,'matlabbatchdcm.mat'),'matlabbatch')
        
        %Run batch
        if ~exist(fullfile(outdirname,[dcm.modelfile '.mat']),'file')
            pspm_jobman('run', matlabbatch);
        end
        
    end
    
end