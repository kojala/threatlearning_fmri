function physio_SCR_sensitivity_fix(opts)

sList = opts.scr.sList.included;

% Subject no, first sensitivity, second sensitivity, artefact index for the
% sensitivity change
sensitivity = [162 500 200 27; ...
    169 2000 2000 1];

artefact_file = fullfile(opts.expPath,'scr_markedartefacts_general.mat');
if exist(artefact_file,'file'); artefacts = load(artefact_file); end
artefacts = artefacts.y_artefacts;

for s = 1:length(sList)
    
    % change sensitivity for data if needed
    if ismember(sList(s), sensitivity(:,1))
        
        s_id = num2str(sList(s));
        fname  = fullfile(opts.scr.dataPath,['atpspm_PCF' s_id '.mat']); % Artefact corrected file
        chan = opts.phys.scr_chan;
        [~, ~, data] = pspm_load_data(fname);
        
        %sr = data{chan}.header.sr;
        savedata = data;

        sindx = find(sensitivity(:,1) == sList(s));
        art_ind = sensitivity(sindx,4);
        startpoint = artefacts{s,2}(art_ind,2);
        endpoint = length(savedata{chan}.data);
        savedata{chan}.data(startpoint:endpoint) = savedata{chan}.data(startpoint:endpoint) +1.4;%* sensitivity(sindx,2)/sensitivity(sindx,3);
        sensdata = NaN(length(savedata{chan}.data),1);
        sensdata(startpoint:endpoint) = savedata{chan}.data(startpoint:endpoint);
        figure; plot(savedata{chan}.data,'k'); hold on; plot(sensdata,'r')
        
        saveopt = input('Save data (y/n)? ','s');
        if strcmp(saveopt,'y'); pspm_write_channel(fname,savedata,'replace'); end
            
        close all
        
    end
    
end