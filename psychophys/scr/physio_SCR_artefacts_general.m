function physio_SCR_artefacts_general(opts)

sList = opts.scr.sList.included;

% Define artefacts file
artefact_file = fullfile(opts.expPath,'scr_markedartefacts_general.mat');

if ~exist(artefact_file,'file')
    y_artefacts = cell(length(sList),2);
else
    artefacts = load(artefact_file); y_artefacts = artefacts.y_artefacts;
end

for s = 1:numel(sList)
    
    s_id = num2str(sList(s));
    
    % File name
    fname  = fullfile(opts.scr.dataPath,['atpspm_PCF' s_id '.mat']); % Artefact corrected file
    
    if exist(fname,'file')
        
        clear data
        [~,~,data] = pspm_load_data(fname);
        
        % Remove artefacts
        %------------------------------------------------------------------
        
        fprintf(['Artefact correction subject ' s_id '\n'])
        
        y  = data{opts.phys.scr_chan}.data;
        
        if exist(artefact_file,'file') % edit existing file
            artefacts = load(artefact_file);
            y_artefacts = artefacts.y_artefacts;
        end
        
        if isempty(y_artefacts{s,2})
            y_artefacts{s,1} = sList(s);
            y_artefacts{s,2} = pspm_data_editor(y);
        end
        
        save(artefact_file,'y_artefacts');
        
        % Correct artefacts and save data file if does not exist yet
        if ~isempty(y_artefacts{s,2})
            
            y_edited = y;
            
            % Change values in the marked artefact epochs to NaN
            for i=1:size(y_artefacts{s,2},1)
                y_edited(y_artefacts{s,2}(i,1):y_artefacts{s,2}(i,2)) = NaN;
            end
            
            % Plot unprocessed SCR data
%             figure,plot(y,'r'); title(['Subject ' int2str(sList(s))])
            % Plot artefact removed signal
%             hold on
%             plot(y_edited,'b'); title(['Subject ' int2str(sList(s))])
            
            % Save artefact corrected data
            newdata = data;
            newdata{1,1}.data = y_edited;
            
            pspm_write_channel(fname,newdata,'replace')
            
        end
        
    end
   
    close all
    
end

end