function physio_SCR_DCM_plot(opts)

sList = opts.scr.sList.DCM; 

%condnames = {'CS(0)','CS(1/3)','CS(2/3)','CS(1)'};
condnames = {'CS(0)US-','CS(1/3)US+','CS(1/3)US-','CS(2/3)US+','CS(2/3)US-','CS(1)US+'};

%% Retrieve GLM stats

dcmpath = fullfile(opts.modelPath,'SCR','DCM','DCM_107');
dcmfile = 'DCM_cond_summary_mean.mat';
data = load(fullfile(dcmpath,dcmfile));
dcm_data = data.dcm_data;

dcm_est_cs1 = dcm_data.cs{1};
dcm_est_cs2 = dcm_data.cs{2};
dcm_est_cs3 = dcm_data.cs{3};
dcm_est_cs4 = dcm_data.cs{4};
dcm_est_cs5 = dcm_data.cs{5};
dcm_est_cs6 = dcm_data.cs{6};

dcm_est_us1 = dcm_data.us{1};
dcm_est_us2 = dcm_data.us{2};
dcm_est_us3 = dcm_data.us{3};
dcm_est_us4 = dcm_data.us{4};
dcm_est_us5 = dcm_data.us{5};
dcm_est_us6 = dcm_data.us{6};

%% Error bars
meanresp_cs = [mean(dcm_est_cs1,2) mean(dcm_est_cs2,2) ...
    mean(dcm_est_cs3,2) mean(dcm_est_cs4,2) ...
    mean(dcm_est_cs5,2) mean(dcm_est_cs6,2)];
subavg_cs = mean(meanresp_cs,2); % mean over conditions for each sub
grandavg_cs = mean(subavg_cs); % mean over subjects and conditions

meanresp_us = [mean(dcm_est_us1,2) mean(dcm_est_us2,2) ...
    mean(dcm_est_us3,2) mean(dcm_est_us4,2) ...
    mean(dcm_est_us5,2) mean(dcm_est_us6,2)];
subavg_us = mean(meanresp_us,2); % mean over conditions for each sub
grandavg_us = mean(subavg_us); % mean over subjects and conditions

newvalues_cs = nan(size(meanresp_cs));
newvalues_us = nan(size(meanresp_us));

% normalization of subject values
for cond = 1:size(meanresp_cs,2)
    meanremoved_cs = meanresp_cs(:,cond)-subavg_cs; % remove mean of conditions from each condition value for each sub
    newvalues_cs(:,cond) = meanremoved_cs+repmat(grandavg_cs,[length(sList) 1 1]); % add grand average over subjects to the values where individual sub average was removed
    plotdata_cs(:,cond) = mean(newvalues_cs(:,cond));
    
    meanremoved_us = meanresp_us(:,cond)-subavg_us; % remove mean of conditions from each condition value for each sub
    newvalues_us(:,cond) = meanremoved_us+repmat(grandavg_us,[length(sList) 1 1]); % add grand average over subjects to the values where individual sub average was removed
    plotdata_us(:,cond) = mean(newvalues_us(:,cond));
end

newvar_cs = (cond/(cond-1))*var(newvalues_cs);
errorbars_cs = squeeze(1.96*(sqrt(newvar_cs)./sqrt(length(sList)))); % calculate error bars according to Cousineau (2005) with Morey (2005) fix

newvar_us = (cond/(cond-1))*var(newvalues_us);
errorbars_us = squeeze(1.96*(sqrt(newvar_us)./sqrt(length(sList)))); % calculate error bars according to Cousineau (2005) with Morey (2005) fix

%% Plot

figure('Position',[300,300,800,600]);
hold on
if opts.scr.summarystat == 1
    bar(plotdata_cs)
    errorbar(plotdata_cs,errorbars_cs,'k','LineStyle','none')
    %bar([mean(dcm_est_cs1); mean(dcm_est_cs2); mean(dcm_est_cs3); mean(dcm_est_cs4); mean(dcm_est_cs5); mean(dcm_est_cs6)])
    title(['SCR mean of DCM trial-by-trial CS estimates, N = ' num2str(length(sList))]);
else
    bar([median(dcm_est_cs1); median(dcm_est_cs2); median(dcm_est_cs3); median(dcm_est_cs4); median(dcm_est_cs5); median(dcm_est_cs6)])
    title(['SCR median of DCM trial-by-trial CS estimates, N = ' num2str(length(sList))]);
end
set(gca,'xTick', 1:length(condnames))
set(gca,'xTickLabel', condnames)


figure('Position',[300,300,800,600]);
hold on
if opts.scr.summarystat == 1
     bar(plotdata_us)
    errorbar(plotdata_us,errorbars_us,'k','LineStyle','none')
    %bar([mean(dcm_est_us1); mean(dcm_est_us2); mean(dcm_est_us3); mean(dcm_est_us4); mean(dcm_est_us5); mean(dcm_est_us6)])
    title(['SCR mean of DCM trial-by-trial US estimates, N = ' num2str(length(sList))]);
else
    bar([median(dcm_est_us1); median(dcm_est_us2); median(dcm_est_us3); median(dcm_est_us4); median(dcm_est_us5); median(dcm_est_us6)])
    title(['SCR median of DCM trial-by-trial US estimates, N = ' num2str(length(sList))]);
end
set(gca,'xTick', 1:length(condnames))
set(gca,'xTickLabel', condnames)


%% Individual values plots
%colors = [102 255 255; 153 51 255; 255 0 127;...
%    0 255 128; 255 255 128; 204 204 0]/255;
colors = [0 102 244; 255 153 153; 51 153 255;...
    255 51 51; 153 204 255; 204 0 0]/255;

% CS
figure
hold on
line([0 19],[mean(dcm_est_cs1) mean(dcm_est_cs1)],'Color',colors(1,:),'LineWidth',2)
line([0 19],[mean(dcm_est_cs2) mean(dcm_est_cs2)],'Color',colors(2,:),'LineWidth',2)
line([0 19],[mean(dcm_est_cs3) mean(dcm_est_cs3)],'Color',colors(3,:),'LineWidth',2)
line([0 19],[mean(dcm_est_cs4) mean(dcm_est_cs4)],'Color',colors(4,:),'LineWidth',2)
line([0 19],[mean(dcm_est_cs5) mean(dcm_est_cs5)],'Color',colors(5,:),'LineWidth',2)
line([0 19],[mean(dcm_est_cs6) mean(dcm_est_cs6)],'Color',colors(6,:),'LineWidth',2)
cond1 = plot(dcm_est_cs1,'o','MarkerFaceColor',colors(1,:),'MarkerEdgeColor','k','MarkerSize',8);
cond2 = plot(dcm_est_cs2,'o','MarkerFaceColor',colors(2,:),'MarkerEdgeColor','k','MarkerSize',8);
cond3 = plot(dcm_est_cs3,'o','MarkerFaceColor',colors(3,:),'MarkerEdgeColor','k','MarkerSize',8);
cond4 = plot(dcm_est_cs4,'o','MarkerFaceColor',colors(4,:),'MarkerEdgeColor','k','MarkerSize',8);
cond5 = plot(dcm_est_cs5,'o','MarkerFaceColor',colors(5,:),'MarkerEdgeColor','k','MarkerSize',8);
cond6 = plot(dcm_est_cs6,'o','MarkerFaceColor',colors(6,:),'MarkerEdgeColor','k','MarkerSize',8);
xlim([0 19])
set(gca,'XTick',1:18)
set(gca,'XTickLabel',1:18)
xlabel('Subjects')
ylabel('Coefficient (a.u.)')
legend([cond1 cond2 cond3 cond4 cond5 cond6],condnames,'Location','best')
title('SCR individual DCM CS estimates for all conditions');

% US
figure
hold on
line([0 19],[mean(dcm_est_us1) mean(dcm_est_us1)],'Color',colors(1,:),'LineWidth',2)
line([0 19],[mean(dcm_est_us2) mean(dcm_est_us2)],'Color',colors(2,:),'LineWidth',2)
line([0 19],[mean(dcm_est_us3) mean(dcm_est_us3)],'Color',colors(3,:),'LineWidth',2)
line([0 19],[mean(dcm_est_us4) mean(dcm_est_us4)],'Color',colors(4,:),'LineWidth',2)
line([0 19],[mean(dcm_est_us5) mean(dcm_est_us5)],'Color',colors(5,:),'LineWidth',2)
line([0 19],[mean(dcm_est_us6) mean(dcm_est_us6)],'Color',colors(6,:),'LineWidth',2)
cond1 = plot(dcm_est_us1,'o','MarkerFaceColor',colors(1,:),'MarkerEdgeColor','k','MarkerSize',8);
cond2 = plot(dcm_est_us2,'o','MarkerFaceColor',colors(2,:),'MarkerEdgeColor','k','MarkerSize',8);
cond3 = plot(dcm_est_us3,'o','MarkerFaceColor',colors(3,:),'MarkerEdgeColor','k','MarkerSize',8);
cond4 = plot(dcm_est_us4,'o','MarkerFaceColor',colors(4,:),'MarkerEdgeColor','k','MarkerSize',8);
cond5 = plot(dcm_est_us5,'o','MarkerFaceColor',colors(5,:),'MarkerEdgeColor','k','MarkerSize',8);
cond6 = plot(dcm_est_us6,'o','MarkerFaceColor',colors(6,:),'MarkerEdgeColor','k','MarkerSize',8);
xlim([0 19])
set(gca,'XTick',1:18)
set(gca,'XTickLabel',1:18)
xlabel('Subjects')
ylabel('Coefficient (a.u.)')
legend([cond1 cond2 cond3 cond4 cond5 cond6],condnames,'Location','best')
title('SCR individual DCM US estimates for all conditions');

end