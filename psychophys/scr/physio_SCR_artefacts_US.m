function physio_SCR_artefacts_US(opts)

sList = opts.scr.sList.included;

% Plotting off
if ~opts.scr.artefact_US_plotting; set(0,'DefaultFigureVisible','off'); end

for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    
    filename = ['PCF' s_id];
    tfname   = fullfile(opts.trimPath, ['tpspm_' filename '.mat']); % Trimmed file
    atfname  = fullfile(opts.scr.dataPath,['atpspm_' filename '.mat']); % Artefact corrected file
    [~, ~, data] = pspm_load_data(tfname);
    
    y  = data{opts.phys.scr_chan}.data;
    markers = data{opts.phys.marker_chan}.data;
    sr = data{opts.phys.scr_chan}.header.sr;
    t0 = round(markers*sr);
    
    if ~exist(atfname,'file')
        fprintf(['US artefact correction, subject ' s_id '\n'])
        
        % Find event types
        cond = [];
        for block = opts.runs
            eventtypes  = fullfile(opts.eventPath, ['event_types_S' s_id '_Block' num2str(block) '.mat']);
            eventdata = load(eventtypes);
            cond = [cond eventdata.allconditions];
        end
        
        markersvec = NaN(1,numel(y));
        markersvec(t0) = t0;
        figure;plot(y)
        hold on
        
        soa = opts.soa;
        soa2 = soa+2;
        soa_cust = 7.8;
        
        trial = 1;
        y_artefacts = y;
        for i = 1:numel(markersvec) % go through markers
            if ~isnan(markersvec(i))
                line([markersvec(i) markersvec(i)], [0 max(y)],'Color','g') % trial start
                line([markersvec(i)+soa*2000 markersvec(i)+soa*2000], [0 max(y)],'Color','r') % US
                line([markersvec(i)+soa2*2000 markersvec(i)+soa2*2000], [0 max(y)],'Color','c') % 2 sec after US
                if cond(trial) == 2 || cond(trial) == 4 || cond(trial) == 6 % if US+ condition
                    y_artefacts(markersvec(i)+soa*2000:markersvec(i)+soa_cust*2000) = NaN; % replace artefact period with NaNs
                    line([markersvec(i)+soa*2000 markersvec(i)+soa*2000], [0 max(y)],'Color','m') % replace marking line color
                end
                trial = trial+1;
            end
        end
        
        figure;plot(y)
        hold on
        plot(y_artefacts,'-r')
        newdata = struct('data', y_artefacts(:));
        newdata.header.sr = sr;
        newdata.header.chantype = 'scr';
        newdata.header.units = data{opts.phys.scr_chan}.header.units;
        
        action = 'replace';
        options.chan = opts.phys.scr_chan;
        
        copyfile(tfname,atfname); % copy file to avoid overwriting and have a separate file for SCR
        pspm_write_channel(atfname, newdata, action, options); % save artefact corrected data
        
    end
end

% Plotting back on
set(0,'DefaultFigureVisible','on')

end