function physio_SCR_DCM(opts)

sList = opts.scr.sList.DCM;

for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    
    % Find data files
    filename = ['atpspm_PCF' s_id];
    sessionfiles = ls(fullfile(opts.scr.dataPath,[filename '_sn*.mat']));
    sespath = what(opts.scr.dataPath);
    for ses = 1:size(sessionfiles,1)
        sessionfiles_fp(ses,:) = [sespath.path '\' sessionfiles(ses,:)]; % full paths
    end
    
    % Exclude second acquisition for fMRI data set
    if opts.dataset == 2
        sessionfiles_fp = sessionfiles_fp(1:5,:);
    end
    
    % Find CS and US timings
    fname_bhv = [opts.bhvname s_id '.mat'];
    eventfile = fullfile(opts.bhvPath, fname_bhv);
    
    for blk = 1:size(sessionfiles_fp,1)
        
        [~, onsets, ~] = physio_get_onsets(sessionfiles_fp(blk,:),eventfile,opts); % get onsets for trials
        
        timing{blk}{1} = onsets;
        timing{blk}{1}(:, 2) = onsets + opts.soa - 1.5; % model 107 from Filip's model space
        timing{blk}{2} = onsets + opts.soa;
        
    end
    
    % File output directory
    outdirname = fullfile(opts.modelPath,'SCR','DCM','DCM_107');
    if ~exist(outdirname,'dir'); mkdir(outdirname); end
    
    % DCM options
    %model.missing = artefacts;
    model.datafile = cellstr(sessionfiles_fp);
    model.modelfile = fullfile(outdirname,['dcm_' s_id]);
    model.timing = timing;
    model.timeunits = 'seconds';
    model.constrained = 1;
    options.overwrite = 1;
    options.dispwin = 0;
    
    % Run DCM
    pspm_dcm(model,options)
    
end

end