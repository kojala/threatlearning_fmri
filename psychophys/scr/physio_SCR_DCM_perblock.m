%function physio_SCR_DCM(ExpPath,sList,phase)

clear all

clear matlabbatch
ExpPath = 'D:\fMRI_Karita\';
sList = [159 161:162 164:167 169:177 179:182]; %160 not incl, bad data
%phase = 1:3;
%phase = 2;


% for p = phase
%
%     clear blocks phasename
%     if p == 1 % Acquisition 1
%         blocks = 1;
%         blockstart = 1;
%         phasename = 'Acq';
%     elseif p == 2 % Maintenance
%         blocks = 2:5;
%         blockstart = 25;
%         phasename = 'Maint';
%     else % Acquisition 2
%         blockstart = 201;
%         phasename = 'Acq2';
%         blocks = 6;
%     end

for s = 1:length(sList)
    s_id = num2str(sList(s));
    
    % Create batch
    filename = ['S' s_id];
    tfname   = fullfile(ExpPath, 'SCR', ['tpspm_' filename '.mat']); % Trimmed file
    bhvname  = fullfile(ExpPath, 'Behavior', filename, ['Behavior_' filename '.mat']); % Behaviour file
    
    if exist(tfname,'file') && exist(bhvname,'file')
        
        % Separate for CS+ (US+ and US-) and CS-
        %------------------------------------------------------------------
        % Load behavioural file with event IDs
        
        load(bhvname)
        load(tfname)
        %CS_markers = data{5,1}.data;
        
        % File output directory
        %outdirname = fullfile(ExpPath,'SCR','DCM',phasename);
        outdirname = fullfile(ExpPath,'SCR','DCM','Allblocks');
        if ~exist(outdirname,'dir')
            mkdir(outdirname)
        end
        
        blocks = 1:6;
        blockstart = [1 25 25+44 25+44+44 25+44+44+44 25+44+44+44+44];
        
        for block = blocks
            clear blocks phasename
            %             if block == 1 % Acquisition 1
            %                 %blocks = 1;
            %                 blockstart = 1;
            %                 %phasename = 'Acq';
            %             elseif block == 2 || block == 3 || block == 4 || block == 5 % Maintenance
            %                 %blocks = 2:5;
            %                 blockstart = 25;
            %                 %phasename = 'Maint';
            %             else % Acquisition 2
            %                 blockstart = 201;
            %                 %phasename = 'Acq2';
            %                 %blocks = 6;
            %             end
            
            clear CS2pUSp CS3pUSp CS4pUSp CS2pUSm CS3pUSm CSm CS_onset
            
            CS2pUSp = [];
            CS3pUSp = [];
            CS4pUSp = [];
            CS2pUSm = [];
            CS3pUSm = [];
            CSm = [];
            CS_onset = [];
            
            %blockstart_sub = blockstart(block);
            
            %blktrials = length(cond(block).CS);
            %blockind = blockstart_sub:(blockstart_sub+blktrials)-1;
            
            %                 CS2pUSp = cond(block).CS == 2 & cond(block).US == 1;
            %                 CS3pUSp = cond(block).CS == 3 & cond(block).US == 1;
            %                 CS4pUSp = cond(block).CS == 4 & cond(block).US == 1;
            %                 CS2pUSm = cond(block).CS == 2 & cond(block).US == 0;
            %                 CS3pUSm = cond(block).CS == 3 & cond(block).US == 0;
            %                 CSm = cond(block).CS == 1;
            
            if block < 6 %block == 1 || block == 6
                CS2pUSp = [CS2pUSp; cond(block).CS == 2 & cond(block).US == 1];
                CS3pUSp = [CS3pUSp; cond(block).CS == 3 & cond(block).US == 1];
                CS4pUSp = [CS4pUSp; cond(block).CS == 4 & cond(block).US == 1];
                CS2pUSm = [CS2pUSm; cond(block).CS == 2 & cond(block).US == 0];
                CS3pUSm = [CS3pUSm; cond(block).CS == 3 & cond(block).US == 0];
                CSm = [CSm; cond(block).CS == 1];
            else
                CS2pUSp = [CS2pUSp; cond(block).CS == 7 & cond(block).US == 1];
                CS3pUSp = [CS3pUSp; cond(block).CS == 8 & cond(block).US == 1];
                CS4pUSp = [CS4pUSp; cond(block).CS == 5 & cond(block).US == 1];
                CS2pUSm = [CS2pUSm; cond(block).CS == 7 & cond(block).US == 0];
                CS3pUSm = [CS3pUSm; cond(block).CS == 8 & cond(block).US == 0];
                CSm = [CSm; cond(block).CS == 6];
            end
            
            %                 CS_onset = tim(block).CS_on/1000; % recorded triggers of CS onset in seconds
            %                 CS_onset = [CS_onset; CS_markers(blockind)]; % recorded triggers of CS onset in seconds
            
            % Create an event file
            %                 SOA = 6;
            %                 CS_onset = CS_markers(blockind);
            %                 US_onset = CS_onset + SOA; % US starts 6 seconds after CS
            %                 events{1} = [CS_onset US_onset];
            %                 events{2} = US_onset;
            %                 eventsname  = fullfile(ExpPath,'EventTimings',['event_timings_' filename '_Block' num2str(block) '.mat']); % Event file
            %                 save(eventsname,'events')
            
            %blockstart_sub = blockstart_sub+blktrials;
            
            
            % DCM for SCR amplitude
            %------------------------------------------------------------------
            
            % 1st level non-linear DCM
            clear matlabbatch dcm
            dcm.modelfile = sprintf(['dcm_',filename]);
            dcm.outdir = {outdirname};
            dcm.model.channel = 6;
            %             dcm.session.condition(1).name = 'CS+|US+';
            %             dcm.session.condition(1).index = find(CSpUSp==1)';
            %             dcm.session.condition(2).name = 'CS+|US-';
            %             dcm.session.condition(2).index = find(CSpUSm==1)';
            %             dcm.session.condition(3).name = 'CS-';
            %             dcm.session.condition(3).index = find(CSm==1)';
            %             if p == 1 || p == 3 % Acquisition phases with 1 block each
            %                 dcm.session.datafile = cellstr(tfname);
            %                 eventsname = fullfile(ExpPath,'EventTimings',['event_timings_' filename '_Block' num2str(blocks) '.mat']); % Event file
            %                 dcm.session(block).timing.timingfile = cellstr(eventsname);
            %                 dcm.session(block).condition(1).name = 'CS-';
            %                 dcm.session(block).condition(1).index = find(CSm==1)';
            %                 dcm.session(block).condition(2).name = 'CS(1/3)+|US-';
            %                 dcm.session(block).condition(2).index = find(CS2pUSm==1)';
            %                 dcm.session(block).condition(3).name = 'CS(1/3)+|US+';
            %                 dcm.session(block).condition(3).index = find(CS2pUSp==1)';
            %                 dcm.session(block).condition(4).name = 'CS(2/3)+|US-';
            %                 dcm.session(block).condition(4).index = find(CS3pUSm==1)';
            %                 dcm.session(block).condition(5).name = 'CS(2/3)+|US+';
            %                 dcm.session(block).condition(5).index = find(CS3pUSp==1)';
            %                 dcm.session(block).condition(6).name = 'CS(1)+|US+';
            %                 dcm.session(block).condition(6).index = find(CS4pUSp==1)';
            %             else % Maintenance phase with 4 blocks as sessions for DCM
            %                 blockind = 1:44;
            %                 for block = 1:length(blocks)
            dcm.session(block).datafile = cellstr(tfname);
            eventsname = fullfile(ExpPath,'SCR','EventTimings',['event_timings_' filename '_Block' num2str(block) '.mat']); % Event file
            dcm.session(block).timing.timingfile = cellstr(eventsname);
            dcm.session(block).condition(1).name = 'CS-';
            dcm.session(block).condition(1).index = find(CSm==1)';
            dcm.session(block).condition(2).name = 'CS(1/3)+|US-';
            dcm.session(block).condition(2).index = find(CS2pUSm==1)';
            dcm.session(block).condition(3).name = 'CS(1/3)+|US+';
            dcm.session(block).condition(3).index = find(CS2pUSp==1)';
            dcm.session(block).condition(4).name = 'CS(2/3)+|US-';
            dcm.session(block).condition(4).index = find(CS3pUSm==1)';
            dcm.session(block).condition(5).name = 'CS(2/3)+|US+';
            dcm.session(block).condition(5).index = find(CS3pUSp==1)';
            dcm.session(block).condition(6).name = 'CS(1)+|US+';
            dcm.session(block).condition(6).index = find(CS4pUSp==1)';
            %blockind = blockind+44;
        end
        
        dcm.data_options.norm = 0;
        dcm.data_options.filter.def = 0;
        dcm.resp_options.crfupdate = 0;
        dcm.resp_options.indrf = 0;
        dcm.resp_options.getrf = 0;
        dcm.resp_options.rf = 0;
        dcm.inv_options.depth = 2;
        dcm.inv_options.sfpre = 2;
        dcm.inv_options.sfpost = 5;
        dcm.inv_options.sffreq = 0.5;
        dcm.inv_options.sclpre = 2;
        dcm.inv_options.sclpost = 5;
        dcm.inv_options.ascr_sigma_offset = 0.1;
        dcm.disp_options.dispwin = 0;
        dcm.disp_options.dispsmallwin = 0;
        matlabbatch{1}.pspm{1}.first_level{1}.scr{1}.dcm = dcm;
        
        save(fullfile(outdirname,'matlabbatchdcm.mat'),'matlabbatch')
        
        %Run batch
        if ~exist(fullfile(outdirname,[dcm.modelfile '.mat']),'file')
            pspm_jobman('run', matlabbatch);
        end
        
    end
    
end