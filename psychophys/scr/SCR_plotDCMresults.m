
% Plot DCM results

% Subject list
SUBJ_LABELS = [159 161:162 164:167 169:177 179:182]; % 160 bad SCR, excluded

MAIN_PATH = 'D:\fMRI_Karita';

figure;
suptitle({;'SCR DCM trial-by-trial estimates, N = 20'})
hold on

for iSubj = 1:length(SUBJ_LABELS)

    s_id = num2str(SUBJ_LABELS(iSubj));
    SCR_DATA = fullfile(MAIN_PATH, 'SCR', 'DCM', 'DCM_107', ['dcm_' s_id '.mat']);
    DCM_data = load(SCR_DATA);
    
    SCR = DCM_data.stats(:,1);
    %SCR(iSubj,2,:) % Fix/fix
    CSm = SCR(iSubj,1,:)==1;
    CS2USm = SCR(iSubj,1,:)==3;
    CS3USm = SCR(iSubj,1,:)==5;
    
    USm = squeeze(ismember(SCR(iSubj,1,:),[1 3 5]));
    SCR_CSm = squeeze(SCR(iSubj,3,CSm)); % Fix/lex
    SCR_CS2USm = squeeze(SCR(iSubj,3,CS2USm));
    SCR_CS3USm = squeeze(SCR(iSubj,3,CS3USm));

    SCR_USm = squeeze(SCR(iSubj,3,USm));

    subplot(ceil(sqrt(length(SUBJ_LABELS)))-1, ceil(sqrt(length(SUBJ_LABELS))), iSubj)
    
    p = plot(SCR_USm,'ok','MarkerFaceColor','k','MarkerSize',2);
    
%     plot(SCR_CSm,'-ob','MarkerFaceColor','b','MarkerSize',2)
%     hold on
%     plot(SCR_CS2USm,'-om','MarkerFaceColor','m','MarkerSize',2)
%     plot(SCR_CS3USm,'-or','MarkerFaceColor','r','MarkerSize',2)
    
    title(['Subject ' num2str(SUBJ_LABELS(iSubj))])
    xlabel('Trials')
    ylabel('SCR estimate')
    xlim([1 115])
    
    % Ordinal place of trials within trial type
    SCR_CSm_all(iSubj,1,:) = SCR_CSm;
    SCR_CS2USm_all(iSubj,2,:) = SCR_CS2USm;
    SCR_CS3USm_all(iSubj,3,:) = SCR_CS3USm;
    
end

figure
suptitle({;'SCR: Mean over trial position ordered DCM estimates, N = 20'})
subplot(3,1,1)
plot(squeeze(nanmean(SCR_CSm_all(:,1,:))),'b','LineWidth',2)
title('CS-')
xlabel('Trials')
ylabel('SCR estimate')
xlim([1 56])
ylim([0 1])
hold on
subplot(3,1,2)
plot(squeeze(nanmean(SCR_CS2USm_all(:,2,:))),'m','LineWidth',2)
title('CS+(1/3) US-')
xlabel('Trials')
ylabel('SCR estimate')
xlim([1 56])
ylim([0 1])
subplot(3,1,3)
plot(squeeze(nanmean(SCR_CS3USm_all(:,3,:))),'r','LineWidth',2)
title('CS+(2/3) US-')
hold on
xlabel('Trials')
ylabel('SCR estimate')
xlim([1 56])
ylim([0 1])
%legend('CS-','CS+(1/3)US-','CS+(2/3)US-','Location','northeast')