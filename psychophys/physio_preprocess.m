function physio_preprocess(opts)

sList = opts.sList_physio;

for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    
    filename = ['PCF' s_id];
    fname    = fullfile(opts.importPath, ['pspm_' filename '.mat']); % Imported file
    tfname   = fullfile(opts.trimPath, ['tpspm_' filename '.mat']); % Trimmed file
    bhvname  = fullfile(opts.bhvPath, [opts.bhvname s_id '.mat']); % Behaviour file
    
    if exist(fname,'file') && ~exist(tfname,'file') && exist(bhvname,'file')
        
        [~, ~, data] = pspm_load_data(fname);
        scantrig = data{opts.phys.scantrig_chan}.data; % Scanner triggers
        sr_scantrig = data{opts.phys.scantrig_chan}.header.sr; % Sampling rate
        sr_bhv = 1000;
        
        if length(data) == 4
            
            sprintf(['Processing subject number ' s_id])
            
            % Find scanner triggers by going through data
            a = zeros(length(scantrig)-200,1);
            for t = 200:length(scantrig)
                a(t) = sum(scantrig(t-199:t))>0;
            end
            
            adiff = diff(a); % Find changes in values
            up = find(adiff==1); % find upward changes
            %down = find(adiff==-1); % find downward changes
            
            runs_start = up; % Start of a scanner run according to scanner triggers
            %blocks_end = down;
            
            % Edit runs, some subjects have extras for some reason
            if length(runs_start) > length(opts.runs) % Max 6 runs in the experiment
                sprintf('More than 6 blocks!')
                %figure; plot(scantrig)
                if str2double(s_id) == 161
                    runs_start(6) = [];
                elseif str2double(s_id) == 162
                    runs_start(2) = [];
                    %                 elseif s_id == 172
                    %                     blocks_start(1) = [];
                elseif str2double(s_id) == 182
                    runs_start(5) = [];
                end
            end
            
            %figure; plot(scantrig)
            bhvdata = load(bhvname);
            
            CS_onset = [];
            
            % Find recorded triggers of CS onset in seconds for each run
            for run = opts.runs
                CS_onset = [CS_onset; runs_start(run)/sr_scantrig+bhvdata.tim(run).CS_on/sr_bhv];
            end
            
            % Find markers and save new marker channel
            foundMarker = CS_onset;
            
            newdata = struct('data', foundMarker(:));
            newdata.header.sr        = 1;
            newdata.header.chantype  = 'marker';
            newdata.header.units     = 'events';
            newdata.markerinfo.value = ones(size(foundMarker));
            newdata.markerinfo.name  = num2cell(ones(size(foundMarker)));
            
            action = 'add';
            options.channel = opts.phys.marker_chan;
            pspm_write_channel(fname, newdata, action, options); % write new marker channel
            
        end
        
        % Trim the imported pspm file with regard to first and last
        % scanner triggers
        options.overwrite = true;
        trimmed_file = pspm_trim(fname,opts.scr.trimstart,opts.scr.trimend,'marker',options);
        copyfile(trimmed_file,tfname);
        
    end
    
end

end