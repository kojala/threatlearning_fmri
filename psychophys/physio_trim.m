function physio_trim(ExpPath,sList)

clear matlabbatch

for s = 1:length(sList)
    s_id = num2str(sList(s));
    
    % Create batch
    filename = ['S' s_id];
    importfile = fullfile(ExpPath, 'SCR', ['pspm_' filename '.mat']);
    
    
    matlabbatch{1}.pspm{1}.prep{1}.trim.datafile = {importfile};
    matlabbatch{1}.pspm{1}.prep{1}.trim.ref.ref_mrk.from = -5;
    matlabbatch{1}.pspm{1}.prep{1}.trim.ref.ref_mrk.to = 5;
    matlabbatch{1}.pspm{1}.prep{1}.trim.ref.ref_mrk.mrk_chan.chan_def = 0;
    matlabbatch{1}.pspm{1}.prep{1}.trim.overwrite = true;
    
    % Run batch
    pspm_jobman('run', matlabbatch);
    
    
end

end