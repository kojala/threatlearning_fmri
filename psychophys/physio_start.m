%% Pipeline wrapper script for psychophysiology analysis
%Psychophysiological analysis pipeline for 3T fMRI study PCF05 and its behavioural pilot PCF02
clear variables
close all; clc

%% Options and paths
opts = get_physio_options(); % get options
addpath(genpath(opts.pspmPath)) % PsPM version
addpath(opts.codePath)
addpath(fullfile(opts.codePath,'utils'))
addpath(opts.codePath_bhv)
% addpath(opts.pilot.codePath_athina) % Old scripts for pilot physio data analysis
addpath(genpath(cd)) % new scripts

%% Dataset to run
dataset = 1; % 1 = pilot, 2 = fmri
if dataset == 1; opts_dataset = opts.pilot; else; opts_dataset = opts.fmri; end

%% Modality of physio data to run
modality = 'PSR';
% modality = 'SCR';
% modality = 'RAR';
% modality = 'HPR';

%% Actions to run
actions.run_pupil           = true; % run pupil analysis steps one by one
actions.run_pupil_analysis  = false; % run the entire pupil pipeline
actions.run_pilot_physio    = false; % run pilot physiological data analysis (other than pupil) -> not functional!
actions.run_fmri_physio     = false; % run fMRI physiological data analysis (other than pupil)
actions.run_behav           = false; % run behavioural data extraction (analyses in R)

%% Physio actions to run
actions.run_physio_import               = false; % Import physio data into PsPM
actions.run_physio_markers_trim         = false; % Find markers and trim data
actions.run_physio_SCR_artcorr_US       = false; % SCR artefact correction (US artefacts)
actions.run_physio_SCR_artcorr_gen      = false; % SCR artefact correction (generic other artefacts)
actions.run_physio_SCR_fixsens          = false; % SCR fix sensitivity for some subjects
actions.run_physio_HB_artcorr           = false; % Heartbeat artefact correction
actions.run_physio_HPR_preproc          = false; % HPR preprocessing
actions.run_physio_RAR_preproc          = false; % RAR preprocessing
actions.run_physio_split_sessions       = false; % Split data into sessions
actions.run_physio_trialavg_save        = false; % Save averages over trials
actions.run_physio_trialavg_plot        = false; % Plot averages over trials
actions.run_physio_SCR_DCM              = false; % Run SCR DCM
actions.run_physio_SCR_DCM_save         = false; % Save SCR DCM results
actions.run_physio_SCR_DCM_plot         = false; % Plot SCR DCM results
actions.run_physio_SCR_DCM_plot_trials  = false; % Plot SCR DCM results per trial
actions.run_physio_RAR_GLM              = false; % Run RAR GLM
actions.run_physio_RAR_GLM_plot         = false; % Plot RAR GLM results
actions.run_physio_HPR_GLM              = false; % Run HPR GLM
actions.run_physio_HPR_GLM_plot         = false; % Plot HPR GLM results

%% Pupil actions to run

if actions.run_pupil_analysis
    actions.run_pupil_importtrim    = true; % Import and trim data in PsPM
    actions.run_pupil_validfix      = true; % Find valid fixations
    actions.run_pupil_spikefix      = true; % Fix spikes in the data
    actions.run_pupil_validtrials   = true; % Find trials with valid data
    actions.run_pupil_preproc       = true; % Preprocess data
    actions.run_pupil_plot          = true; % Plot pupil data
    actions.run_pupil_GLMcond       = true; % Run a conditionwise GLM
    actions.run_pupil_GLMcond_plot  = true; % Plot GLM results
    actions.run_pupil_GLMreconresp  = true; % Reconstruct GLM responses
    actions.run_pupil_save_GLMcond  = true; % Save conditionwise GLM data
    actions.run_pupil_GLMtrial      = true; % Run a trialwise GLM
    actions.run_pupil_GLMtrial_plot = true; % Plot GLM Results
    actions.run_pupil_save_GLMtrial = true; % Save trialwise GLM data
    actions.run_pupil_preproc_trace = true;
    actions.run_pupil_permtest_save = true;
    actions.run_pupil_permtest_plot = true; % Plot permuation test results (analysis done in R)
    actions.run_pupil_article_plot  = true; % Make figure for the article
else
    actions.run_pupil_importtrim    = false;
    actions.run_pupil_validfix      = false;
    actions.run_pupil_preproc       = false;
    actions.run_pupil_spikefix      = false;
    actions.run_pupil_validtrials   = false;
    actions.run_pupil_plot          = false;
    actions.run_pupil_GLMcond       = false;
    actions.run_pupil_GLMcond_plot  = false;
    actions.run_pupil_GLMreconresp  = false;
    actions.run_pupil_save_GLMcond  = false;
    actions.run_pupil_GLMtrial      = false;
    actions.run_pupil_GLMtrial_plot = false;
    actions.run_pupil_save_GLMtrial = false;
    actions.run_pupil_preproc_trace = false;
    actions.run_pupil_permtest_save = false;
    actions.run_pupil_permtest_plot = false;
    actions.run_pupil_article_plot  = true;
end

%% Behavior actions to run

actions.run_behav_info          = false; % Descriptives
actions.run_behav_ratings       = false; % CS-US contingency ratings
actions.run_behav_ratings_plot  = false; % CS-US contingency ratings plot
actions.run_behav_ratings_monot = false; % Subjects with monotonic CS-US contingency ratings
actions.run_behav_RTacc         = false; % Reaction time and accuracy data retrieval, saving and plotting

%% PCF 02 pilot PHYSIOLOGICAL DATA: SCR, HPR, RAR, SEBR
if actions.run_pilot_physio
    
    % Implement / edit old scripts to work (not functional):
%     PCF02_physio_import_001
%     PCF02_physio_QA_001
%     PCF02_physio_QA_002
%     PCF02_physio_QA_003_SCR
% 
%     PCF02_analysis_HPR_001
%     PCF02_analysis_RAR_001
%     PCF02_analysis_SEBR_001
%     
%     PCF02_analysis_SCR_001_Karita
%     PCF_DCM_SCR_001
%     
%     PCF_analysis_RT_001
%     PCF_analysis_rating_001
    
end

%% PCF 05 fMRI PHYSIOLOGICAL DATA: SCR, HPR, RAR
if actions.run_fmri_physio
    
    %% Import into PsPM
    if actions.run_physio_import; physio_import(opts_dataset); end
    
    %% Find markers based on scanner triggers and trim data
    if actions.run_physio_markers_trim; physio_preprocess(opts_dataset); end 

    %% Artefact removal
    % Remove US+ artefacts from SCR data automatically
    if actions.run_physio_SCR_artcorr_US; physio_SCR_artefacts_US(opts_dataset); end 
    % Manual removal of other SCR artefacts
    if actions.run_physio_SCR_artcorr_gen; physio_SCR_artefacts_general(opts_dataset); end 
    % Manual fix of sensitivity levels
    if actions.run_physio_SCR_fixsens; physio_SCR_sensitivity_fix(opts_dataset); end
    % Remove artefacts from heartbeat data
    if actions.run_physio_HB_artcorr; physio_HPR_artefacts(opts_dataset); end
    
    %% HPR preprocessing: heartbeat to heart period
    if actions.run_physio_HPR_preproc; physio_preprocess_HPR(opts_dataset); end
    
    %% RAR preprocessing: breathing cycle data into respiratory amplitude
    if actions.run_physio_RAR_preproc; physio_preprocess_RAR(opts_dataset); end
    
    %% Trial type averages
    % Retrieve and save data
    if actions.run_physio_trialavg_save; physio_trialavg(opts_dataset,modality); end
    % Plot data
    if actions.run_physio_trialavg_plot; physio_plot_avgresponse(opts_dataset,modality); end
    
    %% Split data into sessions
    if actions.run_physio_split_sessions; physio_splitsessions(opts_dataset,modality); end
    
    %% DCM for SCR
    if actions.run_physio_SCR_DCM; physio_SCR_DCM(opts_dataset); end
    
    if actions.run_physio_SCR_DCM_save; physio_SCR_DCM_save(opts_dataset); end
    
    if actions.run_physio_SCR_DCM_plot; physio_SCR_DCM_plot(opts_dataset); end
    
    if actions.run_physio_SCR_DCM_plot_trials; physio_SCR_DCM_plot_individtrials(opts_dataset); end
    
    %% GLM for RAR
    if actions.run_physio_RAR_GLM; physio_GLM_RAR(opts_dataset); end
    
    % Plot GLM data for RAR
    if actions.run_physio_RAR_GLM_plot; physio_reconresp(opts_dataset,modality); end
    
    %% GLM for HPR
    if actions.run_physio_HPR_GLM; physio_GLM_HPR(opts_dataset); end
    
    % Plot GLM data for RAR
    if actions.run_physio_HPR_GLM_plot; physio_reconresp(opts_dataset,modality); end
    
end

%% Pupil analysis
if actions.run_pupil
    
    %% Import eyetracking files into PsPm and trim
    if actions.run_pupil_importtrim
        pupil_import_trim(opts_dataset)
    end
    
    %% Find valid fixations
    if actions.run_pupil_validfix
        pupil_validfixation(opts_dataset);
    end
    
    %% Preprocess pupil data with new algorithm
    if actions.run_pupil_preproc
        pupil_preprocess_new(opts_dataset);
    end
%     %% Fix spike artefacts in the pupil data -> done in the above step
%     if actions.run_pupil_spikefix
%         pupil_fixspikes(opts_dataset)
%     end
    
    %% Select valid trials with enough data left
    if actions.run_pupil_validtrials
        pupil_extract_segments(opts_dataset);
    end
    
    %% Preprocess pupil data for plotting and non-PsPM analyses
    if actions.run_pupil_preproc_trace
        pupil_preprocess_trace(opts_dataset)
    end
    
    %% Plot pupil traces
    if actions.run_pupil_plot
        pupil_plot(opts_dataset)
    end
    
    %% Condition-wise GLM
    if actions.run_pupil_GLMcond
        pupil_GLM(opts_dataset,1);
    end
    
    if actions.run_pupil_GLMcond_plot
        physio_simpleplotGLM(opts_dataset, modality);
    end
    
    if actions.run_pupil_GLMreconresp
        physio_reconresp(opts_dataset,modality);
    end
    
    if actions.run_pupil_save_GLMcond
        physio_saveGLM(opts_dataset)
    end
    
    %% Trial-by-trial GLM
    if actions.run_pupil_GLMtrial
        pupil_GLM(opts_dataset,2);
    end
    
    if actions.run_pupil_GLMtrial_plot
       physio_simpleplotGLM_trialwise(opts_dataset); 
    end
    
    if actions.run_pupil_save_GLMtrial
        physio_saveGLM_trial(opts_dataset);
    end
    
    %% Linear regression pupil size ~ CS probability
%     if actions.run_pupil_linreg_pCS
%         pupil_linregression_pCS(opts_dataset)
%     end
%        
%     if actions.run_pupil_linreg_plot
%         pupil_linregression_pCS_plot(opts_dataset)
%     end
%     
    if actions.run_pupil_permtest_save
        pupil_cluster_permutation_CSp(opts_dataset)
    end
    
    % Run permutation test analysis in R
    
    if actions.run_pupil_permtest_plot
        pupil_permtest_plot(opts_dataset)
    end
    
    %% Figure for the article
    if actions.run_pupil_article_plot
        pupil_plot_article(opts_dataset)
    end
    
end

%% Behavioural analysis

if actions.run_behav
    
    %% Get participant information
    if actions.run_behav_info; behav_get_subjectinfo(opts_dataset); end
    
    %% Analyse CS-US ratings
    if actions.run_behav_ratings; behav_get_CSUSratings(opts_dataset); end
    
    %% Plot CS-US ratings
    if actions.run_behav_ratings_plot; behav_plot_CSUSratings(opts_dataset); end
    
    %% Find subjects with monotonic/non-monotonic CS-US ratings
    if actions.run_behav_ratings_monot; behav_find_monotonic_CSUSratings(opts_dataset); end
   
    %% Analyse reaction time and accuracy
    if actions.run_behav_RTacc; behav_analysis_RT_acc(opts_dataset); end
    
end
