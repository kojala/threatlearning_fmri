function opts = get_physio_options()
%General options for psychophysiological analysis

% Dataset
% 1 = pilot
% 2 = fMRI

opts.mainPath = fullfile(cd,'..','..','Psychophysiology');
opts.codePath = fullfile(cd,'..');
opts.codePath_bhv = fullfile(opts.codePath,'behavior');
opts.pspmPath = 'C:\Data\Toolboxes\PsPM\pspm-v4.3.0'; % Psychophysiological Modelling toolbox

%% Pilot study
%% General
opts.pilot.expPath = fullfile(opts.mainPath,'PCF_02_pilot');
opts.pilot.codePath_athina = fullfile(opts.pilot.expPath,'Analysis'); % scripts for pilot data analysis
opts.pilot.bhvPath = fullfile(opts.pilot.expPath,'Data','Behavior');
opts.pilot.bhvname = 'PCF_02_';
foldername = 'EventTimings';
opts.pilot.eventPath = fullfile(opts.pilot.expPath,'Data',foldername);
opts.pilot.modelPath = fullfile(opts.pilot.expPath,'Models');
opts.pilot.sessions = 1;
opts.pilot.sessnames = {''};
opts.pilot.dataset = 1; % dataset ID
opts.pilot.trials_all = 172; % total number of trials
opts.pilot.trials_incl = 164;%1:164; % included trials (8 startle trials excluded)
opts.pilot.trials_incl_CS = 7:41;
opts.pilot.trials_analysis = 25:164; % only maintenance phase
opts.pilot.sessind = {1:24; 25:59; 60:94; 95:129; 130:164}; % trial indices for each block
opts.pilot.sList = [21:27 29:40]; % subject ID list

%% Behavior
opts.pilot.behav.sList = opts.pilot.sList;

%% Skin conductance responses (SCR)
% This data is not used due to prevalent US artefacts / low quality
opts.pilot.scr.dataPath = fullfile(opts.pilot.expPath,'Data','SCR');

%% Pupil size responses (PSR)
% Data path
opts.pilot.psr.dataPath = fullfile(opts.pilot.expPath,'Data','PSR');
opts.pilot.psr.eyelinkPath = fullfile(opts.pilot.psr.dataPath,'Eyelink'); % original EyeLink data
opts.pilot.psr.importPath = fullfile(opts.pilot.psr.dataPath,'Imported_mm'); % imported data in millimeters

% Subject list
opts.pilot.psr.sList.included = opts.pilot.sList;

% Import
opts.pilot.psr.eyeside.incl = 0; 
opts.pilot.psr.distance.eyetracker = 690; % distance of participant's eyes from the eyetracking camera
opts.pilot.psr.distance_unit = 'mm'; % distance in millimeters

% Trim options
opts.pilot.psr.trimstart = -1; % trim file from -1 before onset of the first trial
opts.pilot.psr.trimend = 5; % trim file until 5 seconds after the last trial
opts.pilot.psr.trials = 172; % number of trials

% Channels
opts.pilot.psr.pupil_chan = [1 4]; % original pupil channel
opts.pilot.psr.marker_chan = 7; % marker channel
opts.pilot.psr.gaze_chan_px = [2:3 5:6]; % gaze channels in pixel
opts.pilot.psr.gaze_chan_cm = [8:9 10:11]; % gaze channels in cm
opts.pilot.psr.pupil_chan_valfix = [12 13]; % valid fixations pupil channel
opts.pilot.psr.pupil_chan_pp = 14; % preprocessed pupil channel (best eye)

% Valid fixations
opts.pilot.psr.distance.screen = 700; % eyetracker distance from the subject in mm
opts.pilot.psr.eyelink_width = 31.242; % EyeLink calibration window size in cm
opts.pilot.psr.eyelink_height = 22.689; % pspm pipeline defaults for L008 lab
opts.pilot.psr.box_degrees = 5; % visual degrees to include for valid fixations
opts.pilot.psr.convert_unit = 'cm'; % unit for converting gaze coordinates from pixels (to cm)
opts.pilot.psr.fixation_opt.eyes = 'all'; % include both eyes in valid fixations search
opts.pilot.psr.fixation_opt.missing = 0; % do not create a separate channel for missing data
opts.pilot.psr.fixation_opt.plot_gaze_coords = true; % plot valid fixations with included area and data points (for quality control)
opts.pilot.psr.fixation_opt.overwrite = 1; % overwrite previous valid fixations data channel
%opts.pilot.psr.fixation_opt.channels = opts.pilot.psr.pupil_chan_pp;
opts.pilot.psr.fixation_opt.channel_action = 'add'; % add a new valid fixations data channel
% opts.pilot.psr.fixation_type = 'gazemedian'; % set fixation point to the median of all gaze points
opts.pilot.psr.fixation_type = 'default'; % use default fixation point in the middle of the screen [0.5 0.5]
% opts.pilot.psr.fixation_type = 'biased'; % fixation point is slightly off from the center
opts.pilot.psr.fixation_point_biased = [0.6 0.5]; % biased fixation point for 'biased' valid fixations search option

% Valid trials
opts.pilot.psr.split_sessions_markers = {[25 60 95 130]}; % markers where sessions/blocks/runs are split
time_afterUS = 5; % time in seconds included after US onset
opts.pilot.psr.trialtime = 6+time_afterUS; % total trial time (stimuli duration + time after US offset)
opts.pilot.psr.validtrials.validrunsfn = 'valfix_valid_runs_allsubjects.mat'; % list of runs with valid data for each subject
opts.pilot.psr.validtrials.besteyefn = 'valfix_besteye_allsubjects.mat'; % list of each subject's best eye

% Plotting
opts.pilot.psr.plot.sList = opts.pilot.psr.sList.included;
opts.pilot.psr.plot.valfix = 1; % plot data 0 = before or 1 = after valid fixations
opts.pilot.psr.plot.clr = [9 226 183; 9 136 239; 147 0 226; 226 9 78]./255; % plot colors
opts.pilot.psr.plot.names = {'Acquisition & Maintenance' 'Acquisition 2'}; % plot titles
%clr = [.2 .2 .2; .4 .4 .4; .6 .6 .6; .8 .8 .8];
%clr = [89 247 234; 66 134 244; 164 66 244; 244 66 170];
%clr = [155 211 255; 94 175 247; 43 135 206; 0 83 147];

% General Linear Model (GLM)
valsubfile = fullfile(opts.pilot.psr.dataPath,opts.pilot.psr.validtrials.validrunsfn); % file with valid runs for each subject
if exist(valsubfile,'file'); valsubs = load(valsubfile);
    included = valsubs.validsubs(1).session(valsubs.validsubs(1).session(:,end) == 5,1); % include subjects that have all runs valid
    opts.pilot.psr.sList.GLM = included';
else; opts.pilot.psr.sList.GLM = opts.pilot.psr.sList.included; % all subjects ok
end
%opts.pilot.psr.sList.GLM = opts.pilot.psr.sList.included([1:4 6:end]); % sList for GLM all subjects except 25 has all runs valid
%sList_GLM = sList(validsubs(:,3)==1); % subject list for GLM, those subjects with enough valid trials
opts.pilot.psr.estimated_response = 1;  % for conditionwise GLM
                                        % 1 = CS response + derivative, default
                                        % 2 = CS + US response
                                        % 3 = US response + derivative
opts.pilot.psr.estimated_conditions = 4; % 4 -> CS probability, 6 -> CS probability + US outcome
opts.pilot.psr.glm_bfname = {'CSderiv' 'CSUS' 'USderiv'}; 
opts.pilot.psr.summarystat = 2;         % for trialwise GLM. 1: mean, 2: median

opts.pilot.psr.glmPath.cond = fullfile(opts.pilot.expPath,'Models','PSR','GLM_cond');
opts.pilot.psr.glmPath.trial = fullfile(opts.pilot.expPath,'Models','PSR','GLM_trial');
opts.pilot.psr.glm_filename.cond = 'GLM_PSR_cond_';
opts.pilot.psr.glm_filename.trial = 'GLM_PSR_trial_';

opts.pilot.psr.linregPath = fullfile(opts.pilot.expPath,'Models','PSR','LinearRegression');
opts.pilot.psr.permtest.test_type = 'anova'; % Which test to use, 'ttest' or 'anova'
opts.pilot.psr.permtest.test_no = 1:3;%5; % 2:4 -> tests between conditions, 1 -> only CS+ vs. CS-
opts.pilot.psr.permtest.contrast_type = 'linear'; % 'linear' or 'quadratic' contrast for anova

%% Heart period responses (HPR)
% Not used
opts.pilot.hpr.dataPath = fullfile(opts.pilot.expPath,'Data','Physio');

%% Respiratory amplitude responses (RAR)
% Not used
opts.pilot.rar.dataPath = fullfile(opts.pilot.expPath,'Data','Physio');

%% fMRI study
%% General
opts.fmri.dataset = 2; % Dataset ID
opts.fmri.expPath = fullfile(opts.mainPath,'PCF_05_fmri');
opts.fmri.dataPath = fullfile(opts.fmri.expPath,'Data','Physio','Data');
opts.fmri.rawPath = fullfile(opts.fmri.dataPath,'BIOPAC');
opts.fmri.importPath = fullfile(opts.fmri.dataPath,'Imported');
opts.fmri.trimPath = fullfile(opts.fmri.dataPath,'Trimmed');

opts.fmri.bhvPath = fullfile(opts.fmri.expPath,'Data','Behavior');
opts.fmri.bhvname = 'PCF_05_';
opts.fmri.eventname = 'Behavior_S';
opts.fmri.modelPath = fullfile(opts.fmri.expPath,'Models');
opts.fmri.eventPath = fullfile(opts.fmri.expPath,'Data','EventTimings');

opts.fmri.sessions = 1:2; % Data divided into Acquisition 1 + Maintenance and Acquisition 2
opts.fmri.sessnames = {'','B'};
opts.fmri.runs = 1:6; % Number of runs
opts.fmri.trials_all = 224; % All trials
opts.fmri.trials_incl = 224; % Included trials
opts.fmri.sessind = {1:24; 25:68; 69:112; 113:156; 157:200; 201:224}; % Trial indices for each run
opts.fmri.soa = 6; % Stimulus onset asynchrony (time between CS and US onsets)
opts.fmri.sList = [155 157 159:162 164:182];
opts.fmri.sList_physio = [159:162 164:167 169:177 179:182];

opts.fmri.phys.scr_chan = 1; % Skin conductance data channel
opts.fmri.phys.ppu_chan = 2; % Pulse data channel
opts.fmri.phys.resp_chan = 3; % Breathing data channel
opts.fmri.phys.scantrig_chan = 4; % Scanner trigger channel
opts.fmri.phys.marker_chan = 5; % Trial start marker channel

%% Behavior

opts.fmri.behav.sList = opts.fmri.sList_physio;
opts.fmri.behav.origPath = fullfile(opts.mainPath,'..','Behavior');

%% SCR

opts.fmri.scr.dataPath = fullfile(opts.fmri.dataPath,'SCR');

opts.fmri.scr.sList.orig = opts.fmri.sList;
opts.fmri.scr.sList.excluded = [155 157 160 168 178];
opts.fmri.scr.sList.included = opts.fmri.scr.sList.orig(~ismember(opts.fmri.scr.sList.orig,opts.fmri.scr.sList.excluded));
% 157, 168 and 178 out because not included in the fMRI sample
% 155 fMRI data but no physio file
opts.fmri.scr.sList.excluded_DCM = [opts.fmri.scr.sList.excluded 162 169];
opts.fmri.scr.sList.DCM = opts.fmri.scr.sList.orig(~ismember(opts.fmri.scr.sList.orig,opts.fmri.scr.sList.excluded_DCM));

opts.fmri.scr.scr_chan = 1; % "final" SCR channel
opts.fmri.scr.marker_chan = 5;

opts.fmri.scr.trimstart = -6; 
opts.fmri.scr.trimend = 20;

opts.fmri.scr.dcm_trials = 200; % trials to include in DCM model
opts.fmri.scr.artefact_US_plotting = true; % plot US artefacts
opts.fmri.scr.summarystat = 1; % summary statistic: mean (vs. median = 2)

%% HPR
opts.fmri.hpr.dataPath = fullfile(opts.fmri.dataPath,'HPR');

opts.fmri.hpr.sList.orig = opts.fmri.sList;
opts.fmri.hpr.sList.excluded = [155 157 168 170 178]; % excluded subjects
% 170 completely rubbish PPU data
opts.fmri.hpr.sList.included = opts.fmri.hpr.sList.orig(~ismember(opts.fmri.hpr.sList.orig,opts.fmri.hpr.sList.excluded));
opts.fmri.hpr.sList.GLM = opts.fmri.hpr.sList.included;

opts.fmri.hpr.estimated_conditions = 6; % conditions
opts.fmri.hpr.spike_perc_discarded = 0.1; % percentage largest spikes discarded
opts.fmri.hpr.new_sr = 10; % new sampling rate in Hz for interpolated respiratory amplitude data
opts.fmri.hpr.ppu_preproc_chan = 3; % channel for preprocessed ppu data
opts.fmri.hpr.hb_chan = 4; % heartbear channel
opts.fmri.hpr.hp_chan = 5; % heart period channel
opts.fmri.hpr.marker_chan = 2;

%% RAR

opts.fmri.rar.dataPath = fullfile(opts.fmri.dataPath,'RAR');

opts.fmri.rar.sList.orig = opts.fmri.sList;
opts.fmri.rar.sList.excluded = [155 157 168 178];
opts.fmri.rar.sList.included = opts.fmri.rar.sList.orig(~ismember(opts.fmri.rar.sList.orig,opts.fmri.rar.sList.excluded));
opts.fmri.rar.sList.GLM = opts.fmri.rar.sList.included;

opts.fmri.rar.estimated_conditions = 6;
opts.fmri.rar.new_sr = 10; % new sampling rate in Hz for interpolated respiratory amplitude data
opts.fmri.rar.ra_chan = 3; % respiratory amplitude channel
opts.fmri.rar.marker_chan = 2;

%% PSR
  
% Data path
opts.fmri.psr.dataPath = fullfile(opts.fmri.expPath,'Data','PSR');
opts.fmri.psr.eyelinkPath = fullfile(opts.fmri.psr.dataPath,'Eyelink');
opts.fmri.psr.importPath = fullfile(opts.fmri.psr.dataPath,'Imported_mm');

% Subject list
opts.fmri.psr.sList.orig = [159:162 164:167 169:177 179:182]; % original list
opts.fmri.psr.sList.excluded = [159 160 161 162 165 170 182];
opts.fmri.psr.sList.included = opts.fmri.psr.sList.orig(~ismember(opts.fmri.psr.sList.orig,opts.fmri.psr.sList.excluded));
opts.fmri.psr.sList.incl_ind = find(~ismember(opts.fmri.psr.sList.orig,opts.fmri.psr.sList.excluded));    
% 159, 162, 165, 170, 182 have problems with eyetracking markers session 1
% 160 and 161 don't have Acquisition 2 (B) session data

% separately for session 1 and 2
% excluded_ses1 = [159 162 165 170 182];
% sList_ses1 = sList_orig(~ismember(sList_orig,excluded_ses1));
% incl_ind_ses1 = find(~ismember(sList_orig,excluded_ses1));
% 
% excluded_ses2 = [160 161];
% sList_ses2 = sList_orig(~ismember(sList_orig,excluded_ses2));
% incl_ind_ses2 = find(~ismember(sList_orig,excluded_ses2));
% think whether to include/exclude subjects separately for these 2 sessions

% Import
opts.fmri.psr.distance.eyetracker = 730; % value from R?mi Castella CHUV
% same as screen distance as this is not known, current values at CHUV likely not the same as in this experiment (40 cm to eyetracker, 60 cm to screen)
opts.fmri.psr.distance_unit = 'mm'; % eyetracker camera distance from the subject's eyes in mm
opts.fmri.psr.eyeside.orig = [1 1 1 1 1 1 1 1 2 2 2 2 2 2 1 1 1 2 2 2 2]; % eye to import, % 1 = left, 2 = right eye
opts.fmri.psr.eyeside.incl = opts.fmri.psr.eyeside.orig(opts.fmri.psr.sList.incl_ind); % eye side for included subjects only

% Trim options
opts.fmri.psr.trimstart = -1;
opts.fmri.psr.trimend = 5;
opts.fmri.psr.trials = [200 24];

% Channels
opts.fmri.psr.pupil_chan = 1; % original pupil channel
opts.fmri.psr.marker_chan = 4; % marker channel
opts.fmri.psr.gaze_chan_px = 2:3; % gaze channels in pixel
opts.fmri.psr.gaze_chan_cm = 5:6; % gaze channels in cm
opts.fmri.psr.pupil_chan_valfix = 7; % valid fixations pupil channel

% Valid fixations and trials
opts.fmri.psr.distance.screen = 730; % distance of participant's eyes to the screen - value from CHUV technician afterward -> could be different from actual setup if camera was moved
opts.fmri.psr.fixation_opt.resolution = [1365 767]; % screen resolution
opts.fmri.psr.eyelink_height = 33; % eyelink calibration window heigth in cm
opts.fmri.psr.resolution_screen_x = opts.fmri.psr.fixation_opt.resolution(1);%1024;
opts.fmri.psr.resolution_screen_y = opts.fmri.psr.fixation_opt.resolution(2);%768; % px
opts.fmri.psr.eyelink_width = (opts.fmri.psr.eyelink_height/opts.fmri.psr.resolution_screen_y)*opts.fmri.psr.resolution_screen_x; % eyelink tracking area width
% height of presentation on screen in cm, 1024 px = ?? cm, 768 px = 33 cm
opts.fmri.psr.box_degrees = 7; % visual degrees for valid fixations
% opts.fmri.psr.circle_degree = 7; % visual degrees for valid fixations
opts.fmri.psr.convert_unit = 'cm'; % unit for converting gaze coordinates from pixels (to cm)
opts.fmri.psr.fixation_opt.missing = 0; % channel for missing data
opts.fmri.psr.fixation_opt.plot_gaze_coords = 1; % plot with fix square and data points (for quality control)
opts.fmri.psr.fixation_opt.overwrite = 1;
opts.fmri.psr.fixation_opt.channel_action = 'add';
% opts.fmri.psr.fixation_type = 'gazemedian';
opts.fmri.psr.fixation_type = 'default'; % default: fixation point in the middle of the screen
% opts.fmri.psr.fixation_type = 'biased';
opts.fmri.psr.fixation_point_biased = [];

% Valid trials
time_afterUS = 5; % time in seconds included after US onset
opts.fmri.psr.trialtime = 6+time_afterUS; % total time included
opts.fmri.psr.split_sessions_markers = {[25 69 113 157]; []}; % 5 blocks in one file, 1 in another
opts.fmri.psr.validtrials.validrunsfn = 'valfix_valid_runs_allsubjects.mat';
opts.fmri.psr.validtrials.besteyefn = '';

% Plotting
opts.fmri.psr.plot.valfix = 1; % plot data 0 = before or 1 = after valid fixations
opts.fmri.psr.plot.clr = [9 226 183; 9 136 239; 147 0 226; 226 9 78]./255; % plot colors
opts.fmri.psr.plot.names = {' Acquisition & Maintenance' 'Acquisition 2'};

% GLM
valsubfile = fullfile(opts.fmri.psr.dataPath,opts.fmri.psr.validtrials.validrunsfn); % subjects with all runs valid data
if exist(valsubfile,'file'); valsubs = load(valsubfile);
    included = valsubs.validsubs(1).session(valsubs.validsubs(1).session(:,end) == 5,1); 
    opts.fmri.psr.sList.GLM = included';
else; opts.fmri.psr.sList.GLM = opts.fmri.psr.sList.included; % all subjects ok
end 
opts.fmri.psr.estimated_response = 1;   % for conditionwise GLM
                                        % 1 = CS response + derivative, default
                                        % 2 = CS + US response
                                        % 3 = US response + derivative
opts.fmri.psr.estimated_conditions = 6; % 4 -> CS probability, 6 -> CS probability + US outcome
opts.fmri.psr.glm_bfname = {'CSderiv' 'CSUS' 'USderiv'}; 
opts.fmri.psr.summarystat = 1;         % for trialwise GLM. 1: mean, 2: median

opts.fmri.psr.glmPath.cond = fullfile(opts.fmri.expPath,'Models','PSR','GLM_cond');
opts.fmri.psr.glmPath.trial = fullfile(opts.fmri.expPath,'Models','PSR','GLM_trial');
opts.fmri.psr.glm_filename.cond = 'GLM_PSR_cond_';
opts.fmri.psr.glm_filename.trial = 'GLM_PSR_trial_';

opts.fmri.psr.linregPath = fullfile(opts.pilot.expPath,'Models','PSR','LinearRegression');
opts.fmri.psr.permtest.contrast_type = 'linear';

end