function physio_HPR_artefacts(opts)

% 1. Use pspm data editor to edit out artefacts in the ppu data manually
% 2. Interpolate ppu artefact corrected data for the NaN parts
% 3. Save artefact corrected ppu data

sList = opts.hpr.sList.included;

ppu_chan = opts.phys.ppu_chan;

for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    fname = fullfile(opts.trimPath, ['tpspm_PCF' s_id '.mat']); % Trimmed file
    afname  = fullfile(opts.hpr.dataPath,['atpspm_PCF' s_id '_HPR.mat']); % Artefact corrected file
    
    % Load data
    [~, ~, data] = pspm_load_data(fname);
    
    artcorr_data = data{ppu_chan}.data;
    
    artefacts = pspm_data_editor(artcorr_data);
    
    if ~isempty(artefacts)
        
        for artefact = 1:size(artefacts,1)
            artcorr_data(artefacts(artefact,1):artefacts(artefact,2)) = NaN;
        end
        
        [~, interpdata] = pspm_interpolate(artcorr_data);
        
        newdata = data;
        newdata{1,1}.data = interpdata;
        
        action = 'replace';
        options.chan = ppu_chan;
        
        copyfile(fname,afname)
        sts = pspm_write_channel(fname, newdata, action, options);
        
    else % only copy file
        
        copyfile(fname,afname)
        
    end
    
end

end