function physio_GLM_HPR(opts)

sList = opts.hpr.sList.GLM;

% File output directory
outdir = fullfile(opts.modelPath,'HPR','GLM');
if ~exist(outdir,'dir'); mkdir(outdir); end

%% GLM for each subject
for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    
    %% General settings
    % Find session files with data
    fname = ['tpspm_PCF' s_id];
    sessionfiles = ls(fullfile(opts.hpr.dataPath,[fname '_HPR_sn*.mat']));
    sespath = what(opts.hpr.dataPath);
    for ses = 1:size(sessionfiles,1)
        sessionfiles_fp(ses,:) = [sespath.path '\' sessionfiles(ses,:)]; % full paths
    end
    
    % Exclude second acquisition from GLM for fMRI data set
    if opts.dataset == 2
        sessionfiles_fp = sessionfiles_fp(1:5,:);
    end
    
    % Find CS and US timings
    fname_bhv = [opts.bhvname s_id '.mat'];
    eventfile = fullfile(opts.bhvPath, fname_bhv);
    
    % Define names and onsets
    for blk = 1:size(sessionfiles_fp,1)
        
        [~, onsets, conditions] = physio_get_onsets(sessionfiles_fp(blk,:),eventfile,opts); % get onsets for trials
        
        CSi = conditions{1};
        US = conditions{2};
        
        CS = nan(length(CSi),1);
        CS(CSi==1) = 1;
        CS(CSi==2 & US == 1) = 2;
        CS(CSi==2 & US == 0) = 3;
        CS(CSi==3 & US == 1) = 4;
        CS(CSi==3 & US == 0) = 5;
        CS(CSi==4) = 6;
        
        nCS = 6;
        cCS = 1;
        
        for iCS = 1:nCS
            timing{blk}.names{cCS} = sprintf('Cond%1.0f', iCS);
            timing{blk}.onsets{cCS} = onsets(CS==iCS);
            cCS = cCS + 1;
        end
        
    end
    
    %% GLM settings
    clear model
    
    model.modelspec = 'hp_fc'; % modality
    model.modelfile = fullfile(outdir,['glm_' s_id]);
    model.datafile = cellstr(sessionfiles_fp);
    
    model.timing = timing;
    model.timeunits = 'seconds';
    
    model.latency = 'fixed';
    model.bf.fhandle = 'pspm_bf_hprf_fc'; 
    model.bf.args = [1, 6]; % fc1 and soa = 6 s
    
    options.overwrite = 1;
    
    %% Run GLM
    pspm_glm(model, options);
    
end

fprintf('GLM done\n')

end