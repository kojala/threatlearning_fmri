function physio_preprocess_HPR(opts)

sList = opts.hpr.sList.included; 

for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    
    % File name
    tfname   = fullfile(opts.trimPath, ['tpspm_PCF' s_id '.mat']); % Trimmed file
    newname  = fullfile(opts.hpr.dataPath, ['tpspm_PCF' s_id '_HPR.mat']); % Renamed HPR file
    
    if exist(tfname,'file') && ~exist(newname,'file')

        %% Heart period
        fprintf(['Processing heart period for subject number ' s_id '\n'])
        
        clear options
        copyfile(tfname,newname); % copy file to have own file for HPR
        action = 'delete';
        channels_to_delete = [1 3 4]; % scr, respiration and scanner trigger channels
        options.channel = channels_to_delete;
        pspm_write_channel(newname,[],action,options); % delete non-cardiac channels
        
        % Filter pulse oximetry data to get rid of noise
        [~,~,data] = pspm_load_data(newname);
        ppu = data{1};
        filt.sr        = ppu.header.sr; % old sampling rate (2000 Hz)
        filt.lpfreq    = 10;
        filt.lporder   = 2;
        filt.hpfreq    = 'none';
        filt.hporder   = 2;
        filt.direction = 'bi';
        filt.down      = 'none'; % 50; % downsample to 50 Hz
        [~,ppu.data] = pspm_prepdata(ppu.data,filt);
        
        % Write filtered and downsampled data
        clear options
        action = 'add';
        ppu_chan = opts.hpr.ppu_preproc_chan;
        options.channel = ppu_chan;
        pspm_write_channel(newname,ppu,action,options)

        % Convert pulse oximetry data into heartbeats
        clear options
        options.lsm = opts.hpr.spike_perc_discarded; % percentage largest spikes discarded
        options.diagnostics = true;
        options.replace = true;
        pspm_ppu2hb(newname,ppu_chan,options)
        
        % Convert heartbeats into heart period
        clear options
        options.replace = true;
        pspm_hb2hp(newname,opts.hpr.new_sr,[],options)
        
    end
    
end

end