function physio_preprocess_RAR(opts)

sList = opts.rar.sList.included;

for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    
    % File name
    tfname   = fullfile(opts.trimPath, ['tpspm_PCF' s_id '.mat']); % Trimmed file
    newname  = fullfile(opts.rar.dataPath, ['tpspm_PCF' s_id '_RAR.mat']); % Renamed RAR file
    
    if exist(tfname,'file') && ~exist(newname,'file')

        %% Respiration
        % Breathing belt data to respiration amplitude
        fprintf(['Processing respiration for subject ' s_id '\n'])
        
        copyfile(tfname,newname); % copy file to have own file for RAR
        action = 'delete';
        channels_to_delete = [1 2 4]; % scr, ppu and scanner trigger channels
        options.channel = channels_to_delete;
        pspm_write_channel(newname,[],action,options); % delete non-cardiac channels
        
        newsr = opts.rar.new_sr; % Sampling rate for the interpolated resp channel
        options = struct('overwrite', 0, 'replace', 1, 'systemtype', 'cushion', 'datatype', {{'ra'}});
        
        pspm_resp_pp(newname, newsr, [], options);
        
    end
    
end

end