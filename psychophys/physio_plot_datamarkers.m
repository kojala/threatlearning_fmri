ExpPath = 'D:\fMRI_Karita';
sList = [159:162 164:167 169:177 179:182];

for s = 164%sList
    
    load(['D:\fMRI_Karita\SCR\tpspm_S' num2str(s) '.mat'])
    
    if s == 160
        checkdata = data{6,1}.data;
        sr = data{6,1}.header.sr;
    else
        checkdata = data{7,1}.data;
        sr = data{7,1}.header.sr;
    end
    
    blocks = 1:6;
    condit = [];
    
    for block = blocks
        eventtypes  = fullfile(ExpPath, 'SCR', 'EventTimings', ['event_types_S' num2str(s) '_Block' num2str(block) '.mat']);
        load(eventtypes)
        condit = [condit allconditions];
    end
    
    markers = data{5,1}.data;
    t0 = round(markers*sr);
    markersvec = NaN(1,numel(checkdata));
    markersvec(t0) = t0;
    figure;plot(checkdata,'k')
    hold on
    
    trial = 1;
    for m = 1:numel(markersvec)
        if ~isnan(markersvec(m))
            line([markersvec(m) markersvec(m)], [0 max(checkdata)],'Color','b')
            if condit(trial) == 2 || condit(trial) == 4 || condit(trial) == 6 % If US trial
                line([markersvec(m)+6*sr markersvec(m)+6*sr], [0 max(checkdata)],'Color','r')
            end
            trial = trial+1;
        end
    end
    
end