function physio_plot_avgresponse(opts,modality)

if strcmp(modality,'HPR')
    dataPath = fullfile(opts.hpr.dataPath,'MatlabData');
    unit = 'Heart period (ms)';
end

data = load(fullfile(dataPath,[modality '_trialavg_allcond.mat']));
blocks = {1; 2:5; 6};
plotsesnames = {'Acquisition','Maintenance','Acquisition2'};

for phase = 1:3
    
    for cond = 1:length(data.resp_out.cond)
        
        cond_data = data.resp_out.cond(cond).block(blocks{phase}).trialdata;
        
        allmeans_sub = squeeze(mean(cond_data,2));
        cond_std(:,cond) = squeeze(std(allmeans_sub,[],2));
        cond_mean(:,cond) = squeeze(mean(allmeans_sub,2));

    end
    
    % Plot response to all conditions
    fig = figure('pos',[10 10 800 300]);
    title([modality ': ' plotsesnames{phase}])
    hold on
    plot(cond_mean(:,2),'r','LineWidth',2,'LineStyle',':')
    plot(cond_mean(:,4),'r','LineWidth',2,'LineStyle','--')
    plot(cond_mean(:,6),'r','LineWidth',2)
    plot(cond_mean(:,1),'b','LineWidth',2,'LineStyle',':')
    plot(cond_mean(:,3),'b','LineWidth',2,'LineStyle','--')
    plot(cond_mean(:,5),'b','LineWidth',2)
    %line([60 60], [-0.4 0.4],'Color','k','LineStyle','--')
%     if strcmp(modality,'SCR')
%         ylim([-0.4 0.4])
%     elseif strcmp(modality,'RAR')
%         ylim([-0.004 0.004])
%         if phase == 2; ylim([-0.001 0.001]); end
%     end
    xlim([1 110])
    ylabel(unit)
    xlabel('Time (s)')
    set(gca,'XTick',0:10:110)
    set(gca,'XTickLabel',{'0','1','2','3','4','5','6','7','8','9','10','11'})
    legend('CS(1/3)US+','CS(2/3)US+','CS(1)US+','CS(0)US-','CS(1/3)US-','CS(2/3)US-','Orientation','vertical','Location','eastoutside')
    
    %% Stuff
    % Plot response to CS+US- vs. CS-US-
    %     fig = figure('pos',[10 10 800 300]);
    %     title([modality ': ' plotsesnames{p} ' phase'])
    %     hold on
    %     plot(conditions_CSp,'r','LineWidth',2)
    %     plot(conditions_CSm,'b','LineWidth',2)
    %     line([60 60], [-0.4 0.4],'Color','k','LineStyle','--')
    %     if strcmp(modality,'SCR')
    %         ylim([-0.4 0.4])
    %     elseif strcmp(modality,'RAR')
    %         ylim([-0.004 0.004])
    %         if p == 2; ylim([-0.001 0.001]); end
    %     end
    %     xlim([0 120])
    %     ylabel([modality 'in a.u.'])
    %     xlabel('Time in s')
    %     set(gca,'XTick',0:10:120)
    %     set(gca,'XTickLabel',{'0','1','2','3','4','5','6','7','8','9','10','11','12'})
    %     legend('CS+','CS-','Orientation','vertical','Location','eastoutside')
    
    % Plot response to US+ vs. US-
    %     fig = figure('pos',[10 10 800 300]);
    %     title([modality ': ' plotsesnames{p} ' phase'])
    %     hold on
    %     plot(conditions_USp,'r','LineWidth',2)
    %     plot(conditions_USm,'b','LineWidth',2)
    %     line([60 60], [-0.4 0.4],'Color','k','LineStyle','--')
    %     if strcmp(modality,'SCR')
    %         ylim([-0.4 0.4])
    %     elseif strcmp(modality,'RAR')
    %         ylim([-0.004 0.004])
    %         if p == 2; ylim([-0.001 0.001]); end
    %     end
    %     xlim([0 120])
    %     ylabel([modality 'in a.u.'])
    %     xlabel('Time in s')
    %     set(gca,'XTick',0:10:120)
    %     set(gca,'XTickLabel',{'0','1','2','3','4','5','6','7','8','9','10','11','12'})
    %     legend('US+','US-','Orientation','vertical','Location','eastoutside')
    %
    % Plot response to CS(0), CS(1/3), CS(2/3), CS(1) regardless of US
    %     fig = figure('pos',[10 10 800 300]);
    %     %title([modality ': ' plotsesnames{p} ' phase'])
    %     hold on
    %     plot(conditions(:,1),'b','LineWidth',2)
    %     plot(mean([conditions(:,2),conditions(:,3)],2),'r','LineWidth',2,'LineStyle',':')
    %     plot(mean([conditions(:,4),conditions(:,5)],2),'r','LineWidth',2,'LineStyle','--')
    %     plot(conditions(:,6),'r','LineWidth',2)
    %     %line([60 60], [-0.4 0.4],'Color','k','LineStyle','--')
    %     if strcmp(modality,'SCR')
    %         ylim([-0.09 0.02])
    %     elseif strcmp(modality,'RAR')
    %         ylim([-0.004 0.004])
    %         if p == 2; ylim([-0.001 0.001]); end
    %     end
    %     xlim([0 60])
    %     ylabel([modality ' in a.u.'])
    %     xlabel('Time in s')
    %     set(gca,'XTick',0:10:60)
    %     set(gca,'XTickLabel',{'0','1','2','3','4','5','6'})
    %     legend('CS(0)','CS(1/3)','CS(2/3)','CS(1)','Orientation','vertical','Location','northwest')
    %
    % % Plot response to US+ conditions
    % fig = figure('pos',[10 10 800 300]);
    % %title([modality ': ' plotsesnames{p} ' phase'])
    % hold on
    % plot(conditions(:,2),'r','LineWidth',2,'LineStyle',':')
    % plot(conditions(:,4),'r','LineWidth',2,'LineStyle','--')
    % plot(conditions(:,6),'r','LineWidth',2)
    % plot(mean([conditions(:,1),conditions(:,3),conditions(:,5)],2),'b','LineWidth',2)
    % line([60 60], [-0.4 0.4],'Color','k','LineStyle','--')
    % if strcmp(modality,'SCR')
    %     ylim([-0.15 0.25])
    % elseif strcmp(modality,'RAR')
    %     ylim([-0.004 0.004])
    %     if p == 2; ylim([-0.001 0.001]); end
    % end
    % xlim([0 120])
    % ylabel([modality ' in a.u.'])
    % xlabel('Time in s')
    % set(gca,'XTick',0:10:120)
    % set(gca,'XTickLabel',{'0','1','2','3','4','5','6','7','8','9','10','11','12'})
    % legend('CS(1/3)US+','CS(2/3)US+','CS(1)US+','US-','Orientation','vertical','Location','northwest')
    
    % %
    %     save(fullfile(mainpath,'Plots',[modality '_avgtimecourse_' plotsesnames{p} '.fig']))
    
    %     fig = figure('pos',[10 10 800 300]);
    %     title([modality ': ' plotsesnames{p} ' phase'])
    %     hold on
    %     plot(conditions(:,1),'k','LineWidth',3)
    %     plot(conditions(:,1)+stddevs(:,1)./sqrt(21),'k','LineWidth',1)
    %     plot(conditions(:,1)-stddevs(:,1)./sqrt(21),'k','LineWidth',1)
    %     plot(conditions(:,3),'c','LineWidth',3)
    %     plot(conditions(:,3)+stddevs(:,3)./sqrt(21),'c','LineWidth',1)
    %     plot(conditions(:,3)-stddevs(:,3)./sqrt(21),'c','LineWidth',1)
    %     plot(conditions(:,5),'b','LineWidth',3)
    %     plot(conditions(:,5)+stddevs(:,5)./sqrt(21),'b','LineWidth',1)
    %     plot(conditions(:,5)-stddevs(:,5)./sqrt(21),'b','LineWidth',1)
    %     %line([60 60], [-0.4 0.4],'Color','k','LineStyle')
    % %     if strcmp(modality,'SCR')
    % %         ylim([-0.4 0.4])
    % %     elseif strcmp(modality,'RAR')
    % %         ylim([-0.004 0.004])
    % %         if p == 2; ylim([-0.002 0.001]); end
    % %     end
    %     xlim([0 120])
    %     ylabel([modality 'in a.u.'])
    %     xlabel('Time in s')
    %     set(gca,'XTick',0:10:120)
    %     set(gca,'XTickLabel',{'0','1','2','3','4','5','6','7','8','9','10','11','12'})
    %     legend('CS(0)US-','CS(1/3)US-','CS(2/3)US-','Orientation','vertical','Location','eastoutside')
    %
    %     save(fullfile(mainpath,'Plots',[modality '_avgtimecourse_' plotsesnames{p} '.fig']))
    
    %     resp1 = squeeze(mean(respCS1USm(:,:,:,blocks),2));
    %     resp3 = squeeze(mean(respCS2USm(:,:,:,blocks),2));
    %     resp5 = squeeze(mean(respCS3USm(:,:,:,blocks),2));
    %
    %     figure;
    %     for sub = 1:21
    %         subplot(5,5,sub)
    %         plot(resp1(:,sub),'k','LineWidth',3);
    %         hold on
    %         plot(resp3(:,sub),'c','LineWidth',3);
    %         plot(resp5(:,sub),'b','LineWidth',3);
    %     end
    %condnames = {'CS(0)US-' 'CS(1/3)US+' 'CS(1/3)US-' 'CS(2/3)US+' 'CS(2/3)US-' 'CS(1)US+'};
    
    % figure('pos',[10 10 1000 250])
    % %suptitle([modality ' for all conditions, ' plotsesnames{p}])
    % %for c = 1:3%size(conditions,2)
    % %subplot(ceil(sqrt(size(conditions,2))),ceil(sqrt(size(conditions,2))),c)
    % subplot(1,3,1)
    % %title(condnames(c))
    % title('CS(1)US+ vs. CS(0)US-')
    % hold on
    % plot(conditions(:,6),'r')
    % plot(conditions(:,1),'b')
    % ylim([-6.5 -5.5])
    % xlim([0 150])
    % ylabel(modality ' in a.u.')
    % xlabel('Time in s')
    %
    % subplot(1,3,2)
    % title('CS(1/3)US+ vs. CS(1/3)US-')
    % hold on
    % plot(conditions(:,2),'r')
    % plot(conditions(:,3),'b')
    % ylim([-6.5 -5.5])
    % xlim([0 150])
    % ylabel(modality ' in a.u.')
    % xlabel('Time in s')
    %
    % subplot(1,3,3)
    % title('CS(2/3)US+ vs. CS(2/3)US-')
    % hold on
    % plot(conditions(:,4),'r')
    % plot(conditions(:,5),'b')
    % ylim([-6.5 -5.5])
    % xlim([0 150])
    % ylabel(modality ' in a.u.')
    % xlabel('Time in s')
    %
    % legend('US+','US-')
    
end

end