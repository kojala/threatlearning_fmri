function physio_splitsessions(opts,modality)

options.prefix = opts.scr.trimstart;
options.suffix = opts.scr.trimend;
options.overwrite = true;
options.max_sn = length(opts.runs);

if strcmp(modality,'SCR')
    sList = opts.scr.sList.included;
    modPath = opts.scr.dataPath;
    fname_mod = 'atpspm';
    modname = '';
elseif strcmp(modality,'RAR')
    sList = opts.rar.sList.included;
    modPath = opts.rar.dataPath;
    fname_mod = 'tpspm';
    modname = modality;
elseif strcmp(modality,'HPR')
    sList = opts.hpr.sList.included;
    modPath = opts.hpr.dataPath;
    fname_mod = 'tpspm';
    modname = modality;
end

for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    fname   = fullfile(modPath, [fname_mod '_PCF' s_id '_' modname '.mat']);
    %markerchan = opts.phys.marker_chan;
    pspm_split_sessions(fname,[],options)
    
end

end