function pupil_import_trim(opts)

sList = opts.psr.sList.included;

for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    
    for ses = 1:length(opts.sessions)
        
        filename = ['PCF' s_id opts.sessnames{ses}];
        file = fullfile(opts.psr.eyelinkPath,[filename '.asc']);
        importfile = fullfile(opts.psr.importPath, ['pspm_' filename '.mat']);
        trimfile = fullfile(opts.psr.importPath, ['tpspm_' filename '.mat']);
        
        if exist(file,'file') && ~exist(importfile,'file')
            
            %% Import
            
            sprintf('Importing - Subject %s, Session %d', s_id, ses);
            
            datatype = 'eyelink';
            
            % pilot data has both eyes, fMRI data only either one
            if opts.psr.eyeside.incl == 0
                import{1}.type = 'pupil_l';
                import{1}.eyelink_trackdist = opts.psr.distance.eyetracker;
                import{1}.distance_unit = opts.psr.distance_unit;
                import{2}.type = 'gaze_x_l';
                import{3}.type = 'gaze_y_l';
                import{4}.type = 'pupil_r';
                import{4}.eyelink_trackdist = opts.psr.distance.eyetracker;
                import{4}.distance_unit = opts.psr.distance_unit;
                import{5}.type = 'gaze_x_r';
                import{6}.type = 'gaze_y_r';
                import{opts.psr.marker_chan}.type = 'marker';

            elseif opts.psr.eyeside.incl(s) == 1 % left eye
                import{1}.type = 'pupil_l';
                import{1}.eyelink_trackdist = opts.psr.distance.eyetracker;
                import{1}.distance_unit = opts.psr.distance_unit;
                import{2}.type = 'gaze_x_l';
                import{3}.type = 'gaze_y_l';
                import{opts.psr.marker_chan}.type = 'marker';

            elseif opts.psr.eyeside.incl(s) == 2 % right eye
                import{1}.type = 'pupil_r';
                import{1}.eyelink_trackdist = opts.psr.distance.eyetracker;
                import{1}.distance_unit = opts.psr.distance_unit;
                import{2}.type = 'gaze_x_r';
                import{3}.type = 'gaze_y_r';
                import{opts.psr.marker_chan}.type = 'marker';

            end

            options.overwrite = true;
            
            imported_file = pspm_import(file, datatype, import, options);
            
            pspm_ren(char(imported_file),importfile);
            
        end
        
        if exist(importfile,'file') && ~exist(trimfile,'file')
            
            %% Check markers
            
            [~, ~, markerdata] = pspm_load_data(importfile,'events');
            
            % markers from EyeLink
            markerCS = markerdata{1}.data;
            
            if (opts.dataset == 2 > 0 && (ses == 1 && length(markerCS) ~= opts.psr.trials(1))...
                    || (ses == 2 && length(markerCS) ~= opts.psr.trials(2)))...
                    || (opts.dataset == 1 && length(markerCS) ~= opts.psr.trials)
                sprintf('Wrong number of EyeLink markers, %d! Subject %s, Session %d', length(markerCS), s_id, ses);
            end            
            
            %% Trim
            
            sprintf('Trimming - Subject %s, Session %d', s_id, ses);
            
            options.overwrite = true;
            pspm_trim(importfile,opts.psr.trimstart,opts.psr.trimend,'marker',options);
            
        end
        
    end
    
end

end