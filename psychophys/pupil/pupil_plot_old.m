function pupil_plot(opts)

sList = opts.psr.plot.sList;

for ses = opts.sessions
    
    clear hp plotdata
    
    for s = 1:numel(sList)
        
        s_id = num2str(sList(s));
        
        filename = ['PCF' s_id opts.sessnames{ses}];
        
        if opts.psr.plot.valfix
            pupilfile = fullfile(opts.psr.importPath, ['valfix_tpspm_' filename '.mat']);
        else
            pupilfile = fullfile(opts.psr.importPath, ['tpspm_' filename '.mat']);
        end
        
        if exist(pupilfile,'file')
            
            [~, ~, pupildata] = pspm_load_data(pupilfile);
            
            markers = pupildata{opts.psr.marker_chan}.data;
            sr = pupildata{1}.header.sr;
            
            %% Interpolate
            options.extrapolate = 1;
            if opts.psr.plot.valfix
                pupilchan = opts.psr.pupil_chan(1); 
            else
                pupilchan = opts.psr.pupil_chan_valfix(1);
            end
            % FIX: TAKE MEAN OF BOTH EYES IF TWO EYES!
            [~, pupil_interp] = pspm_interpolate(pupildata{pupilchan}.data,options);
            
            %% Filtering
            LPFREQ = 25;
            LPORDER = 1;
            HPFREQ = 'none';
            HPORDER = nan;
            DOWNSAMPLED_SR = 50;
            FILT_DIRECTION = 'bi';
            
            filter = [];
            filter.sr = sr;
            filter.lpfreq = LPFREQ;
            filter.lporder = LPORDER;
            filter.hpfreq = HPFREQ;
            filter.hporder = HPORDER;
            filter.direction = FILT_DIRECTION;
            filter.down = DOWNSAMPLED_SR;
            [~, pupil, sr] = pspm_prepdata(pupil_interp, filter);
            
            %% Retrieve markers from Cogent data
            sampleno = 10*sr; % 6 seconds (CS-US interval)
            
            % cogent data
            US = [];
            CS = [];
            
            markermiss = 0; 
            
            if opts.dataset == 2 && ses == 1
                for block = 1:5
                    bhvfile = fullfile(opts.bhvPath,[opts.bhvname s_id '.mat']);
                    cogent = load(bhvfile);
                    % parse 5 blocks together
                    US = [US; cogent.cond(block).US];
                    CS = [CS; cogent.cond(block).CS];
                end
            elseif opts.dataset == 2 && ses == 2
                bhvfile = fullfile(opts.bhvPath,[opts.bhvname s_id '.mat']);
                cogent = load(bhvfile);
                US = cogent.cond(6).US;
                CS_raw = cogent.cond(6).CS;
                
                % For Acquisition 2
                % 6 = CS1
                % 7 = CS2
                % 8 = CS3
                % 5 = CS4
                CS(CS_raw==6) = 1;
                CS(CS_raw==7) = 2;
                CS(CS_raw==8) = 3;
                CS(CS_raw==5) = 4;
                CS = CS';
            else
                bhvfile = fullfile(opts.bhvPath,['PCF_02_' s_id '.mat']);
                cogent = load(bhvfile);
                CS = cogent.data(:,4);
                US = cogent.data(:,5);
                markermiss = opts.trials_all - numel(markers); % fix subjects with eye tracker recalibration during block 1
            end
            
            % markers from EyeLink
            %markerCS = pupildata{4}.markerinfo.value;
            % 1 = CS1, p(US+) = 0
            % 2 = CS2, p(US+) = 1/3
            % 3 = CS3, p(US+) = 2/3
            % 4 = CS4, p(US+) = 1
            
            for CStype = 1:4
                CSi = find(CS == CStype);
                if opts.dataset == 1; CSi(CSi>opts.trials_incl) = []; end % for pilot data last trials removed due to startle probes
                %CSi(CSi<25|CSi>164) = [];
                for iTrl = 1:numel(CSi)-1
                    trlonset = markers(CSi(iTrl)-markermiss);
                    trlhp    = pupil(floor(trlonset*sr) + (1:sampleno));
                    %             if sum(any(isnan(trlhp), 1), 2) == 2
                    %                 hp(iTrl, :, CStype, s) = NaN(sampleno, 1);
                    %                 hp(iTrl, :, CStype + 4, s) = NaN(sampleno, 1);
                    %             elseif
                    hp(iTrl,:,CStype,s) = trlhp;
                    if US(CSi(iTrl))
                        hp(iTrl,:,CStype+4,s) = NaN(sampleno,1);
                    else
                        hp(iTrl,:,CStype+4,s) = trlhp;
                    end
                    %   end;
                end
            end
        end
    end
    
    %% Plotting
    fig = figure('Position', [100, 100, 1400, 800]);
    pos = [.03 .53 .40 .40; .5 .53 .40 .40; .03 .04 .40 .40];
    clr = [9 226 183; 9 136 239; 147 0 226; 226 9 78];
    clr = clr./255;

    titles = {'All conditions'...
        'US-'...
        'CS+ - CS-'};
    %pos = axpos(1, 3, .05, .05, .05, .1, .05);
    plotdata = squeeze(nanmean(nanmean(hp,1),4));
    
    % All conditions
    iPanel = 1;
    axes('Position', pos(iPanel,:)); hold on;
    for CStype = 1:4
        plot((1:size(plotdata,1))/sr, plotdata(:,CStype + (iPanel-1) * 4) - plotdata(1,CStype + (iPanel-1) * 4), 'Color', clr(CStype,:), 'LineWidth', 1.5);
    end
    colormap spring
    legend('String', {'0%', '33%', '66%', '100%'},'Location','best');
    legend boxoff
    title(titles{iPanel})
    
    % US-
    iPanel = 2;
    axes('Position', pos(iPanel,:)); hold on;
    for CStype = 1:3
        plot((1:size(plotdata,1))/sr, plotdata(:,CStype + (iPanel-1) * 4) - plotdata(1,CStype + (iPanel-1) * 4), 'Color', clr(CStype,:), 'LineWidth', 1.5);
    end
    colormap spring
    legend('String', {'0%', '33%', '66%'},'Location','best');
    legend boxoff
    title(titles{iPanel})
        
    % CS+ - CS-
    iPanel = 3;
    axes('Position', pos(iPanel,:)); hold on;
    for CStype = 2:4
        plot((1:size(plotdata,1))/sr, plotdata(:,CStype) - plotdata(:,1), 'Color', clr(CStype,:), 'LineWidth', 1.5);
    end
    colormap spring
    legend('String', {'33%', '66%', '100%'},'Location','best');
    legend boxoff
    title(titles{iPanel})
    
    hold on
    suptitle(['Mean pupil area CS-US interval, ' opts.psr.plot.names{ses} ', N = ' num2str(length(sList))])
    
    % Save figure
    if opts.psr.plot.valfix; vfname = 'valfix'; else; vfname = 'novalfix'; end
    sessavenames = {'AcqMaint' 'Acq2'};
    fp = fileparts(opts.expPath);
    plotPath = fullfile(fp,'Plots');
    if ~exist(plotPath,'dir'); mkdir(plotPath); end
    savefig(fig,fullfile(plotPath,['pupil_meanmm_' sessavenames{ses} '_interp_' vfname '_' num2str(length(sList)) 'subs']));
    
end
end