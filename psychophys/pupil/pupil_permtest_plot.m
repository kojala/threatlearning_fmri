function pupil_permtest_plot(opts)

test_type = opts.psr.permtest.test_type;
if strcmp(test_type,'anova')
    datafile = fullfile(opts.psr.linregPath,['pupil_pCS_permuted_anova_' opts.psr.permtest.contrast_type '_pertimebin_clusterp.mat']);
    testname = 'rep. meas. ANOVA';
elseif strcmp(test_type,'ttest')
    datafile = fullfile(opts.psr.linregPath,'pupil_pCS_permuted_ttest_pertimebin_clusterp.mat');
    testname = 'paired t-test';
end

data = load(datafile);

cluster_p = data.cluster_p;
Fvalue_orig = data.Fvalue_orig;
%pvalue_orig = data.pvalue_orig;
Fvalue = data.Fvalue;
%pvalue = data.pvalue;

sr = length(cluster_p)/11; % divided by trial duration in seconds

figure
plot(Fvalue,'LineStyle','none','Marker','o','MarkerFaceColor','b','MarkerEdgeColor','k')
hold on
plot(Fvalue_orig,'LineStyle','none','Marker','o','MarkerFaceColor','r','MarkerEdgeColor','k')
xlabel('Time (s)')
set(gca,'XTick',0:10:110,'XTickLabel',0:11)
xlim([0 11*sr])
%ylabel('F-value')
%text(3,130,{'Red: Test statistics for original condition labels' 'Blue: Test statistics for randomly shuffled labels'})
text(3,7,{'Red: Test statistics for original condition labels' 'Blue: Test statistics for randomly shuffled labels'})
%title('F-values going into the permutation test')

p1 = plot(find(cluster_p>0),cluster_p(cluster_p>0),'o','LineStyle','none',...
    'MarkerFaceColor','b','MarkerEdgeColor','k','MarkerSize',5);
hold on
p2 = plot(find(cluster_p==0),cluster_p(cluster_p==0),'o','LineStyle','none',...
    'MarkerFaceColor','m','MarkerEdgeColor','m','MarkerSize',5);
p3 = plot(find(cluster_p==-1),zeros(1,length(find(cluster_p==-1))),'o','LineStyle','none',...
    'MarkerFaceColor','g','MarkerEdgeColor','g','MarkerSize',5);
set(gca,'XTick',0:sr:11*sr)
set(gca,'XTickLabel',0:11)
xlim([0 11*sr])
xlabel('Time (s)')
%ylabel('Permutation p-value')
ylim([-0.05 ceil(max(Fvalue_orig))+10])
ytickformat('%.1f')
ax = gca;
ax.YAxis.Exponent = 0;
title(['Cluster p-values pupil size ~ p(US+|CS) ' testname ', 10 Hz sampling'])

% titles = {'Above inclusion threshold, exact p-value',...
%     'Above threshold, no exact p-value (0)',...
%     'Below inclusion threshold (-1)'};

titles = {'Above inclusion threshold, exact p-value',...
    'Above inclusion threshold',...
    'Below inclusion threshold'};

titles2draw = [~isempty(p1) ~isempty(p2) ~isempty(p3)];

legend([p2 p3], titles{titles2draw})



% p1 = plot(find(cluster_p>0),cluster_p(cluster_p>0),'o','LineStyle','none',...
%     'MarkerFaceColor','b','MarkerEdgeColor','k','MarkerSize',5);
% hold on
% p2 = plot(find(cluster_p==0),cluster_p(cluster_p==0),'o','LineStyle','none',...
%     'MarkerFaceColor','m','MarkerEdgeColor','m','MarkerSize',5);
% p3 = plot(find(cluster_p==-1),zeros(1,length(find(cluster_p==-1))),'o','LineStyle','none',...
%     'MarkerFaceColor','g','MarkerEdgeColor','g','MarkerSize',5);
% set(gca,'XTick',0:sr:11*sr)
% set(gca,'XTickLabel',0:11)
% xlim([0 11*sr])
% xlabel('Time (s)')
% ylabel('Permutation p-value')
% ylim([-0.05 1])
% ytickformat('%.1f')
% ax = gca;
% ax.YAxis.Exponent = 0;
% title('Cluster p-values pupil size ~ p(US+|CS) rep. meas. ANOVA, 10 Hz sampling')

end