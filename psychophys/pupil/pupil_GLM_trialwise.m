function pupil_GLM_trialwise(ExpPath,sList,session)
%%PUPIL_GLM_trial First-level GLM per subject, trial-by-trial estimates

eyePath = fullfile(ExpPath,'Eyetracker');
options.overwrite = true;

if session == 1; triallist = 1:200; elseif session == 2; triallist = 1:24; end

%% GLM for each subject
for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    
    clear model
    
    model.channel = 6;
    model.timeunits = 'markers';
    model.modelspec = 'ps_fc';

    % Loop over trials
    
    for trial = triallist
        
        model.timing.names{trial} = ['Trial' num2str(triallist(trial))];
        model.timing.onsets{trial} = trial;
        model.timing.durations{trial} = 0;
        
    end
    
    outdir = fullfile(eyePath,'GLM_trialwise');
    if ~exist(outdir,'dir'); mkdir(outdir); end
    
    % Pupil data file and output GLM file name
    if session == 1
        fname_eye = fullfile(eyePath,'Imported_area',['valfix_tpspm_PCF' s_id '.mat']);
        model.modelfile = fullfile(outdir,['GLM_' s_id '.mat']);
    elseif session == 2
        fname_eye = fullfile(eyePath,'Imported_area',['valfix_tpspm_PCF' s_id 'B.mat']);
        model.modelfile = fullfile(outdir,['GLM_' s_id 'B.mat']);
    end
    model.datafile = fname_eye;
    
    % Run GLM
    if exist(fname_eye,'file')
        %fprintf('Subject %3.0f', sList(s))
        pspm_glm(model,options)
    end
    
end

fprintf('GLM done\n')

end