function pupil_preprocess_new(opts)

sList = opts.psr.sList.included;

% Loop over subjects
for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    
    % Loop over sessions
    for ses = 1:length(opts.sessions)
        
        % Data file
        filename = ['PCF' s_id opts.sessnames{ses}];
        pupilfile = fullfile(opts.psr.importPath, ['valfix_tpspm_' filename '.mat']);
        
        options.plot_data = false;
        pspm_pupil_pp(pupilfile, options)
        
    end
end