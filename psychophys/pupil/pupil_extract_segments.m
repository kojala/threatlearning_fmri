function pupil_extract_segments(opts)

sList = opts.psr.sList.included;

%% Segment extraction and finding valid trials
for sub = 1:length(sList) % Loop over subjects
    
    s_id = num2str(sList(sub));
    
    for ses = 1:length(opts.sessions) % Loop over sessions
        
        filename = ['PCF' s_id opts.sessnames{ses}];
        fixed_file = fullfile(opts.psr.importPath, ['valfix_tpspm_' filename '.mat']);
        
        fname_bhv = [opts.bhvname s_id '.mat'];
        eventfile = fullfile(opts.bhvPath, fname_bhv);
        
        fprintf(['Subject ' num2str(sList(sub)) ' Session ' num2str(ses) '\n'])
        
        %% Load file with valid fixations remaining (invalid: NaN)
        if exist(fixed_file,'file')
            
            if ses == 1
                % Split valfix file to sessions
                splitoptions.overwrite = 1;
                splitoptions.splitpoints = opts.psr.split_sessions_markers{ses}; % split points for blocks (5 with 24, 35, 35, 35 and 35 trials)
                splitoptions.prefix = opts.psr.trimstart;
                splitoptions.suffix = opts.psr.trimend;
                splitfiles = pspm_split_sessions(fixed_file,opts.psr.marker_chan,splitoptions);
            else
                % copy and rename file for session 2 file, which is run 6
                [fp,name,ext] = fileparts(fixed_file);
                newname = fullfile(fp,[name(1:end-1) '_sn06' ext]);
                copyfile(fixed_file,newname);
                splitfiles = {newname};
            end
            
            %timing.names = {'CS(0)'; 'CS(1/3'; 'CS(2/3)'; 'CS(1)'};
            timing.names = {'CS1'; 'CS2'; 'CS3'; 'CS4'};
            options.timeunit = 'seconds';
            options.length = opts.psr.trialtime; % Total time for trial
            options.plot = 0;
            options.overwrite = 1;
            options.marker_chan = opts.psr.marker_chan;
            
            sub_validperc = [];
            
            for splitfile = 1:length(splitfiles) % Loop over runs within a session
                
                splitfn = splitfiles{splitfile};
                [~,name,ext] = fileparts(splitfn);
                options.outputfile = fullfile(opts.psr.importPath, ['seg_' name ext]); % output to file
                %options.nan_output = fullfile(dataPath, ['nan_seg_' name ext]);
                onsets = physio_get_onsets(splitfn,eventfile,opts); % get onsets for trials
                timing.onsets = onsets;
                
                nan_perc_trials = [];
                
                for eye = 1:length(opts.psr.pupil_chan_valfix) % loop over eyes
                    
                    all_trials_nan = [];
                    
                    datachan = opts.psr.pupil_chan_valfix(eye);
                    [~, nan_segments] = pspm_extract_segments('manual', splitfn, datachan, timing, options);
                    
                    for cond = 1:length(nan_segments.segments)
                       all_trials_nan = [all_trials_nan nan_segments.segments{cond}.trial_nan_percent];
                    end
                    
                    %all_trials_nan = reshape(all_trials_nan,[1 size(all_trials_nan,1)*size(all_trials_nan,2)]);
                    
                    % Save NaN %
                    nan_perc_trials(eye,:) = all_trials_nan;
                    
                end
               
                % Choose better eye if two and save valid trial %
                if size(nan_perc_trials,1) > 1 % if two eyes
                    
                    left_eye_valid = nan_perc_trials(1,:);
                    right_eye_valid = nan_perc_trials(2,:);
                    besteye(sub,1) = sList(sub);
                    
                    % see which eye has better mean valid data (75% or less NaNs)
                    if mean(left_eye_valid) < mean(right_eye_valid) % left eye better
                        sub_validperc(splitfile) = mean(left_eye_valid <= 0.75);
                        besteye(sub,2) = 1;
                        
                    elseif mean(right_eye_valid) < mean(left_eye_valid) % right eye better
                        sub_validperc(splitfile) = mean(right_eye_valid <= 0.75); 
                        besteye(sub,2) = 2;
                        
                    else % if eyes equal, take right eye
                        sub_validperc(splitfile) = mean(right_eye_valid <= 0.75);
                        besteye(sub,2) = 2;
                    end
                    
                else % if one eye
                    sub_validperc(splitfile) = mean(nan_perc_trials <= 0.75);
                end
                
            end
            
            %% Check valid trials and exclude/include sub
            % See if subject has enough data
            validruns = find(sub_validperc >= 0.25);
            
            validsubs(ses).session(sub,1) = sList(sub);
            
            for run = 1:length(sub_validperc)
                if ismember(run,validruns) % enough data to include (at least 25% data remaining overall)
                    validsubs(ses).session(sub,run+1) = 1;
                else % not enough data
                    validsubs(ses).session(sub,run+1) = 0;
                end
            end
            validsubs(ses).session(sub,run+2) = sum(validruns>0);
            
            fprintf(['Valid runs: ' num2str(validruns) '\n'])
            
        end
        
    end
    
    fprintf('---- \n')
    
end

if opts.dataset == 1; save(fullfile(opts.psr.dataPath,opts.psr.validtrials.besteyefn),'besteye'); end
save(fullfile(opts.psr.dataPath,opts.psr.validtrials.validrunsfn),'validsubs')

end

%% Old stuff
%                     for trial = 1:length(markers)-1
%                         
%                         trialstart = markers(trial);
%                         trialend = markers(trial)+trialtime;
%                         
%                         % Calculate % missing/remaining data within CS-US interval
%                         pupil_nans(eye,trial) = sum(isnan(segments(trial,:)))/length(segments(trial,:));
%                         
%                         if pupil_nans(eye,trial) > 0.75 % If more than 75% data missing, exclude trial and set CS-US interval data and marker to NaN
%                             pupildata(eye,trialstart:trialend-1) = NaN;
%                             pupil_included(eye,trial) = 0;
%                             markers_new(eye,trial) = NaN;
%                         else % Otherwise include and keep data
%                             pupil_included(eye,trial) = 1;
%                             markers_new(eye,trial) = markers(trial);
%                         end
%                         
%                     end