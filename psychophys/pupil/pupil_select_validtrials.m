function [validsubs] = pupil_select_validtrials(ExpPath,sList,time_afterUS,write_newchannels)

if contains(ExpPath,'Eyetracker') % fMRI study data
    
    dataPath = fullfile(ExpPath,'Imported_mm');
    sessions = 1:2; % two separate EyeTracker files in this study (for some subjects)
    eyes = 1; % only 1 eye
    markerchan = 4;
    eyechan = 5; % pupil channel valid fixations
    
else % pilot study data
    
    dataPath = ExpPath;
    sessions = 1;
    eyes = 1:2; % left and right eye
    markerchan = 7;
    eyechan = 8:9; % pupil channel valid fixations
    
end

validsubs = nan(length(sList),2); % list of valid subjects after removing invalid fixations data
sessnames = {'','B'}; % sessions

for s = 1:length(sList) % subjects
    
    s_id = num2str(sList(s));
    
    for ses = 1:length(sessions) % sessions
        
        filename = ['PCF' s_id sessnames{ses}];
        fixed_file = fullfile(dataPath, ['valfix_tpspm_' filename '.mat']);
        
        fprintf(['Subject ' num2str(sList(s)) ', Session ' num2str(sessions(ses)) '\n'])
        
        if exist(fixed_file,'file')
            
            % Load data
            [~,~,pupildata_orig] = pspm_load_data(fixed_file);
            
            %--------------------------------------------------------------
            % Calculate % missing/remaining data within CS-US interval
            %--------------------------------------------------------------
            markers = pupildata_orig{markerchan}.data;
            trialstart = markers;
            trialstart = round(trialstart); % Round to nearest integer
            trialtime = 6+time_afterUS; % Total time for trial
            
            SR = pupildata_orig{1}.header.sr; % Sampling rate
            trialstart = trialstart*SR-499; % Trial start for this SR
            trialend = trialstart+trialtime*SR; % Trial end for this SR
            
            % Loop over existing eye data
            for eye = eyes
                
                pupildata(eye,:) = pupildata_orig{eyechan(eye)}.data; % Pupil in mm
                
                pupil_trials(eye,:,:) = nan(length(markers),SR*trialtime); % trial data matrix
                
                for trial = 1:length(markers)-1 % loop over trials
                    pupil_trials(eye,trial,:) = pupildata(eye,trialstart(trial):trialend(trial)-1);
                end
                
                % Last trial not included due to being too short
                pupil_trials(eye,length(markers),:) = NaN;
                
                % Save data, NaNs for trials with too much missing data
                pupil_nans(eye,:) = nan(length(markers),1); % Percentage of missing data for each trial
                pupil_included(eye,:) = nan(length(markers),1); % Included/excluded mark for each trial
                markers_new(eye,:) = nan(length(markers),1); % New markers for included trials, excluded trial markers set to NaN
                
                for trial = 1:length(markers)-1
                    
                    pupil_nans(eye,trial) = sum(isnan(pupil_trials(eye,trial,:)))/length(pupil_trials(eye,trial,:));
                    
                    if pupil_nans(eye,trial) > 0.75 % If more than 75% data missing, exclude trial and set CS-US interval data and marker to NaN
                        pupildata(eye,trialstart(trial):trialend(trial)-1) = NaN;
                        pupil_included(eye,trial) = 0;
                        markers_new(eye,trial) = NaN;
                    else % Otherwise include and keep data
                        pupil_included(eye,trial) = 1;
                        markers_new(eye,trial) = markers(trial);
                    end
                    
                end
                
            end
            
            %--------------------------------------------------------------
            % Choose best eye and create new markers with included trials only
            %--------------------------------------------------------------
            if size(pupil_included,1) > 1 % if more than one eye
                if sum(pupil_included(1,:)) > sum(pupil_included(2,:))
                    besteye_data = squeeze(pupildata(1,:,:));
                    chantype = 'pupil_l';
                    markers_new = squeeze(markers_new(1,:));
                else
                    besteye_data = squeeze(pupildata(2,:,:));
                    chantype = 'pupil_r';
                    markers_new = squeeze(markers_new(2,:));
                end
            else % if only one eye
                besteye_data = squeeze(pupildata(1,:,:));
                chantype = pupildata_orig{eyechan(1)}.header.chantype;
            end
            
            % Remove excluded trial markers
            markers_new_excl = markers_new;
            markers_new_excl(isnan(markers_new)) = [];
            
            % Missing data over the whole experiment for this subject
            % (during defined CS-US interval)
            pupil_missing = sum(isnan(besteye_data))/length(besteye_data);
            
            validsubs(s,1) = sList(s);
            validsubs(s,2) = pupil_missing;
            
            % If more than 75% of the data missing, exclude subject
            if pupil_missing > 0.75
                validsubs(s,3) = 0;
            else
                validsubs(s,3) = 1;
            end
            
            %--------------------------------------------------------------
            % Save data
            %--------------------------------------------------------------
            % Write only the best eye
            newdata = struct('data', besteye_data(:));
            newdata.header.units = 'mm';
            newdata.header.chantype = chantype;
            newdata.header.sr = SR;
            action = 'add';
            
            %             % Write new markers with excluded trials removed
            %             newdata2 = struct('data', markers_new_excl(:));
            %             newdata2.markerinfo.value = ones(length(markers_new_excl),1);
            %             newdata2.markerinfo.name = cellstr(string(1:length(markers_new_excl)))';
            %             newdata2.header.chantype = 'marker';
            %             newdata2.header.units = 'events';
            %             newdata2.header.sr = 1;
            
            % Write new markers with excluded trials as NaNs
            newdata3 = struct('data', markers_new(:));
            newdata3.markerinfo.value = ones(length(markers_new),1);
            newdata3.markerinfo.name = cellstr(string(1:length(markers_new)))';
            newdata3.header.chantype = 'marker';
            newdata3.header.units = 'events';
            newdata3.header.sr = 1;
            
            if write_newchannels
                pspm_write_channel(fixed_file, newdata, action, []);
                %                 pspm_write_channel(fixed_file, newdata2, action, []);
                pspm_write_channel(fixed_file, newdata3, action, []);
            end
            
        else

            fprintf(['Subject ' num2str(sList(s)) ', Session ' num2str(sessions(ses)) ' VALID FIXATIONS FILE DOES NOT EXIST!\n'])
            
        end
        
        fprintf('---- \n')
        
    end
end

%% Discarded stuff

%             % Load data
%             [~,~,pupildata] = pspm_load_data(fixed_file);
%
%             if length(pupildata) > 6
%
%                 % Delete channels that are not needed
%                 delchans = [ones(1,6) ones(1,2)*2]; % Channels 1-6, 8-9
%                 % Taking into account that previous channel deleted
%
%                 for channel = delchans
%                     physio_deletechannel(fixed_file,channel);
%                 end
%
%             end

%             clear pupildata

%[~,~,pupildata] = pspm_load_data(fixed_file);
%
%             markers = pupildata{7}.data;
%
%             pupil_l = pupildata{12,1}.data;
%             pupil_r = pupildata{13,1}.data;
%             sr = data{12,1}.header.sr;
%             headerunits = data{12,1}.header.units;
%
%             trials_start = markers*sr;
%             %trials_end = (data{4,1}.data+8)*data{1,1}.header.sr;
%
%             trials_start(end) = [];
%
%             clear trials_data
%
%             for trial = 1:length(trials_start)
%
%                 % Include 5.99 s after CS onset (US at 6.00 seconds)
%                 trialdata = pupil_l(trials_start(trial):trials_start(trial)+10*SR-1);
%
%                 trials_data(:,trial) = trialdata;
%
%                 % Trial is included if no more than 75% of data points NaN
%                 if sum(isnan(trialdata)) > 0.75*length(trialdata)
%                     trials_included(trial) = 0;
%                     trials_data(:,trial) = NaN;
%                     newmarkers(trial) = NaN;
%                 else
%                     trials_included(trial) = 1;
%                     newmarkers(trial) = markers(trial);
%                 end
%
%             end