function [x_nan, x_nan_trial, ses_incl, trials_included] = pupil_countNan(ExpPath,sList,valfix)

sessions = 1:2;
sessnames = {'','B'}; % acquisition+maintenance, 2nd acquisition

clear x_nan x_nan_trial

for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    x_nan(s,1) = sList(s);
    x_nan_trial(s,1) = sList(s);
            
    for ses = 1:length(sessions)
        
        filename = ['PCF' s_id sessnames{ses}]; % File name
        if valfix
            filename = fullfile(ExpPath, 'Eyetracker', ['valfix_tpspm_' filename '.mat']);
        else
            filename = fullfile(ExpPath, 'Eyetracker', ['tpspm_' filename '.mat']);
        end
        
        if exist(filename,'file')
            
            load(filename)
            
            % Count NaNs
            markers = data{4,1}.data;

            if valfix
                pupil_data = data{6,1}.data; % pupil area, invalid fixations NaNs
                sr = data{6,1}.header.sr;
                headerunits = data{6,1}.header.units;
            else
                pupil_data = data{1,1}.data;
                sr = data{1,1}.header.sr;
                headerunits = data{1,1}.header.units;
            end
            
            x_nan(s,ses+1) = sum(isnan(pupil_data))/length(pupil_data);
            
            trials_start = markers*sr;
            %trials_end = (data{4,1}.data+8)*data{1,1}.header.sr;
            
            trials_start(end) = [];
            
            clear trials_data
            
            for trial = 1:length(trials_start)
                
                % Include 5.99 s after CS onset (US at 6.00 seconds)
                trialdata = pupil_data(trials_start(trial):trials_start(trial)+2999);
                
                trials_data(:,trial) = trialdata;
                
                % Trial is included if no more than 75% of data points NaN
                if sum(isnan(trialdata)) > 0.75*length(trialdata)
                    trials_included(trial) = 0;
                    trials_data(:,trial) = NaN;
                    newmarkers(trial) = NaN;
                else
                    trials_included(trial) = 1;
                    newmarkers(trial) = markers(trial);
                end
                
            end
            
            % Write new pupil channel without excluded trials
            newdata = struct('data', trials_included(:));
            newdata.header.sr = sr;
            newdata.header.chantype = 'pupil';
            newdata.header.units = headerunits;
            action = 'add';
            options.msg.prefix = 'pupil data with excluded trials NaN';
            
            pspm_write_channel(filename, newdata, action, options);
            
            % Write new marker channel without excluded trials
            newmarkers = struct('data', newmarkers(:));
            newmarkers.header.sr = 1;
            newmarkers.header.chantype = 'marker';
            newmarkers.header.units = 'events';
            options.msg.prefix = 'markers with excluded trials NaN';
            
            pspm_write_channel(filename, newmarkers, 'add');
            
            ses_incl(s,1) = sList(s);
            
            if sum(trials_included) >= 0.25*length(trials_start) % If 25% of trials included, session included
                ses_incl(s,ses+1) = 1;
            else % If 75% of trials excluded, session excluded
                ses_incl(s,ses+1) = 0;
            end
            
            %trials_incl = (s,
            x_nan_trial(s,ses+1) = median(sum(isnan(trials_data))./length(pupil_data)');
            
            % Count zeros of pupil area
            %pupil_zero(s,ses,:) = sum(data{1,1}.data==0)/length(data{3,1}.data);
            
        end
    end
    
end

end