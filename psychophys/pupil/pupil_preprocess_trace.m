function pupil_preprocess_trace(opts)

sList = opts.psr.sList.GLM;

for ses = opts.sessions
    
    clear pupil_subdata pupil_alldata_USp pupil_alldata_USm pupil_alldata_CS
    
    samples = 550;
    
    trialsCS = numel(opts.trials_incl_CS);
    
    if opts.dataset == 1
        pupil_alldata_USp = NaN(trialsCS,samples,3,length(sList));
        pupil_alldata_USm = NaN(trialsCS,samples,3,length(sList));
        pupil_alldata_CS = NaN(samples,4,length(sList));
    elseif opts.dataset == 2 && ses == 1
        pupil_alldata_USp = NaN(trialsCS,samples,3,length(sList));
        pupil_alldata_USm = NaN(trialsCS,samples,3,length(sList));
        pupil_alldata_CS = NaN(samples,4,length(sList));
    end
    
    for s = 1:numel(sList)
        
        s_id = num2str(sList(s));
        
        filename = ['PCF' s_id opts.sessnames{ses}];
        
        if opts.psr.plot.valfix
            pupilfile = fullfile(opts.psr.importPath, ['valfix_tpspm_' filename '.mat']);
        else
            pupilfile = fullfile(opts.psr.importPath, ['tpspm_' filename '.mat']);
        end
        
        if exist(pupilfile,'file')
            
            [~, ~, pupildata] = pspm_load_data(pupilfile);
            
            markers = pupildata{opts.psr.marker_chan}.data;
            sr = pupildata{1}.header.sr;
            
            %% Interpolate
            options.extrapolate = 1;
            
            if opts.dataset == 1
                besteye = load(fullfile(opts.psr.dataPath,opts.psr.validtrials.besteyefn));
                besteye = besteye.besteye;
                besteyechan = besteye(besteye(:,1)==sList(s),2);
            else
                besteyechan = 1;
            end
            
            if opts.psr.plot.valfix
                pupilchan = opts.psr.pupil_chan_valfix(besteyechan);
%                 nanperc = data.segments{cond,1}.trial_nan_percent;
%                 validtrials = nanperc <= 0.75;
%                 trialdata = data.segments{cond,1}.data(:,validtrials);
            else
                pupilchan = opts.psr.pupil_chan(besteyechan);
            end
            
            [~, pupil_interp] = pspm_interpolate(pupildata{pupilchan}.data,options);
            
            %% Filtering
            filter = [];
            filter.sr = sr;
            filter.lpfreq = 25;
            filter.lporder = 1;
            filter.hpfreq = 'none';
            filter.hporder = nan;
            filter.direction = 'uni'; % no bidirectional filter to avoid US responses propagating back to CS response
            filter.down = 50;
            [~, pupil, sr] = pspm_prepdata(pupil_interp, filter);
            
%             pupil = pupil_interp;
            
            %% Retrieve markers from Cogent data
            sampleno = 11*sr; % 11 seconds
            
            % cogent data
            US = [];
            CS = [];
            
            markermiss = 0;
            
            if opts.dataset == 2 && ses == 1
                for block = 1:5
                    bhvfile = fullfile(opts.bhvPath,[opts.bhvname s_id '.mat']);
                    cogent = load(bhvfile);
                    % parse 5 blocks together
                    US = [US; cogent.cond(block).US];
                    CS = [CS; cogent.cond(block).CS];
                end
            elseif opts.dataset == 2 && ses == 2
                bhvfile = fullfile(opts.bhvPath,[opts.bhvname s_id '.mat']);
                cogent = load(bhvfile);
                US = cogent.cond(6).US;
                CS_raw = cogent.cond(6).CS;
                
                % For Acquisition 2
                % 6 = CS1
                % 7 = CS2
                % 8 = CS3
                % 5 = CS4
                CS(CS_raw==6) = 1;
                CS(CS_raw==7) = 2;
                CS(CS_raw==8) = 3;
                CS(CS_raw==5) = 4;
                CS = CS';
            else
                bhvfile = fullfile(opts.bhvPath,['PCF_02_' s_id '.mat']);
                cogent = load(bhvfile);
                CS = cogent.data(:,4);
                US = cogent.data(:,5);
                markermiss = opts.trials_all - numel(markers); % fix subjects with eye tracker recalibration during block 1
            end
            
%             Cond1 = CS == 1;
%             Cond2 = CS == 2 & US == 1;
%             Cond3 = CS == 2 & US == 0;
%             Cond4 = CS == 3 & US == 1;
%             Cond5 = CS == 3 & US == 0;
%             Cond6 = CS == 4;

            % 1 = CS1, p(US+) = 0
            % 2 = CS2, p(US+) = 1/3
            % 3 = CS3, p(US+) = 2/3
            % 4 = CS4, p(US+) = 1
            
            if opts.dataset == 1 
                CS = CS(1:opts.trials_incl); 
                US = US(1:opts.trials_incl); 
            end % for pilot data last trials removed due to startle probes
            %else, CS(end) = []; end % no pupil data for the last trial in fMRI data (EyeLink terminated too soon)

            for iTrl = 1:numel(CS)
                trlonset = markers(iTrl-markermiss);
                if opts.dataset == 2 && iTrl == numel(CS)
                    trldata = NaN(sampleno,1);
                else % last trial NaNs
                    trldata  = pupil(floor(trlonset*sr) + (1:sampleno));
                    trldata = trldata - trldata(1); % baseline correction, 1st timepoint as baseline
                end
                pupil_subdata(:,iTrl) = trldata;
            end
                
            pupil_subdata_z = pupil_subdata(:,opts.trials_analysis)';%zscore(pupil_subdata)';
            CS_taken = CS(opts.trials_analysis);
            US_taken = US(opts.trials_analysis);
            
            % For plotting purposes, separating the 6 CS-US conditions
            for CStype = 1:3
                trials = CS_taken == CStype+1 & US_taken == 1;
                pupil_alldata_USp(1:sum(trials),:,CStype,s) = pupil_subdata_z(trials,:);
            end
            
            for CStype = 1:3
                trials = CS_taken == CStype & US_taken == 0;
                pupil_alldata_USm(1:sum(trials),:,CStype,s) = pupil_subdata_z(trials,:);
            end
            
            if ses == 1
                % For R to create ANOVA data for permutation test
                for CStype = 1:4
                    pupil_alldata_CS(:,CStype,s) = nanmean(pupil_subdata_z(CS_taken == CStype,:)); % take mean over trials of each type within the subject
                end
                
                pupil_alldata_CSUS(:,1,:) = squeeze(nanmean(pupil_alldata_USm(:,:,1,:)));
                pupil_alldata_CSUS(:,2,:) = squeeze(nanmean(pupil_alldata_USp(:,:,1,:)));
                pupil_alldata_CSUS(:,3,:) = squeeze(nanmean(pupil_alldata_USm(:,:,2,:)));
                pupil_alldata_CSUS(:,4,:) = squeeze(nanmean(pupil_alldata_USp(:,:,2,:)));
                pupil_alldata_CSUS(:,5,:) = squeeze(nanmean(pupil_alldata_USm(:,:,3,:)));
                pupil_alldata_CSUS(:,6,:) = squeeze(nanmean(pupil_alldata_USp(:,:,3,:)));
                
            end
            
        end
    end
   
    % Save data for plotting in Matlab
    preproc_file = fullfile(opts.psr.dataPath,['pupil_preproc_ses' opts.sessnames{ses} '.mat']);
    save(preproc_file,'pupil_alldata_USp','pupil_alldata_USm','sr')
    
    % Save data for permuted ANOVA tests in R
    preproc_file_R = fullfile(opts.psr.dataPath,['pupil_preproc_ses' num2str(ses) '_R.mat']);
    if ses == 1; save(preproc_file_R,'pupil_alldata_CS','pupil_alldata_CSUS','sr'); end
    
end

end