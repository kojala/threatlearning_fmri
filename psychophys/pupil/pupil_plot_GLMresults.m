clear all

sList = [160 161 164 166 167 169 171:177 179:181];
ExpPath = 'D:\fMRI_Karita';

phase = 1:3;
phasenames = {'Acq' 'Maint' 'Acq2'};

Condition = {'CS(0)' 'CS(1/3)' 'CS(2/3)' 'CS(1)'};

for p = phase
    filePath = fullfile(ExpPath,'Eyetracker','GLM_fc0',phasenames{p});
    
    sub = 0;
    
    for s = 1:length(sList)
        
        s_id = num2str(sList(s));
        
        fname = fullfile(filePath,['GLM_' s_id '.mat']);
        
        if exist(fname,'file')
            load(fname)
            sub = sub+1;            
            
            CSresp(sub,:) = glm.stats(1:4);
            %CSresp(sub,:) = glm.stats([1 3 5 7]);
            %USresp(sub,:) = glm.stats([2 4 6 8]);
            
        end
        
    end
    
    figure
    boxplot(CSresp,Condition)
    title(['Estimated CS response (FC0), ' phasenames{p} ', N = ' num2str(sub)])
    ylabel('Pupil area (a.u.)')
    xlabel('Conditions')
    
%     figure
%     boxplot(USresp,Condition)
%     title(['Estimated US response, ' phasenames{p} ', N = ' num2str(sub)])
%     ylabel('Pupil area (a.u.)')
%     xlabel('Conditions')
    
    CS1 = mean(CSresp(:,1));
    CS2 = mean(CSresp(:,2));
    CS3 = mean(CSresp(:,3));
    CS4 = mean(CSresp(:,4));
    
%     CS1err = std(CSresp(:,1))/sqrt(size(CSresp,1));
%     CS2err = std(CSresp(:,2))/sqrt(size(CSresp,1));
%     CS3err = std(CSresp(:,3))/sqrt(size(CSresp,1));
%     CS4err = std(CSresp(:,4))/sqrt(size(CSresp,1));

%     US1 = mean(USresp(:,1));
%     US2 = mean(USresp(:,2));
%     US3 = mean(USresp(:,3));
%     US4 = mean(USresp(:,4));
%     US5 = mean(USresp(:,5));
%     US6 = mean(USresp(:,6));
    
    x = [1 2 3 4];

    figure
    bar(x,[CS1 CS2 CS3 CS4])
    hold on
%     errorbar(x,[CS1 CS2 CS3 CS4],[CS1err CS2err CS3err CS4err],'.k')
    title(['Estimated CS response (FC0), ' phasenames{p} ', N = ' num2str(sub)])
    xt = get(gca, 'XTick');
    set(gca, 'XTick', xt, 'XTickLabel', {'CS(0)' 'CS(1/3)' 'CS(2/3)' 'CS(1)'})
    ylabel('Pupil area (a.u.)')
    xlabel('Condition')
    
%     figure
%     bar([US1 US2 US3 US4 US5 US6])
%     title('US response, N = 16')
%     xt = get(gca, 'XTick');
%     set(gca, 'XTick', xt, 'XTickLabel', {'CS(0)US-' 'CS(1/3)US+' 'CS(1/3)US-' 'CS(2/3)US+' 'CS(2/3)US-' 'CS(1)US+'})
    
end