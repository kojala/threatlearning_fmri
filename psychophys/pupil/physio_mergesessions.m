function physio_mergesessions(ExpPath,sList)

eyePath = fullfile(ExpPath,'Eyetracker');

for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    
    fname_eye1 = fullfile(eyePath,'Imported_area',['valfix_tpspm_PCF' s_id '.mat']);
    fname_eye2 = fullfile(eyePath,'Imported_area',['valfix_tpspm_PCF' s_id 'B.mat']);
    
    reference = 'marker';
    options.overwrite = true;
    
    pspm_merge(fname_eye1,fname_eye2,reference,options);
    
end

end