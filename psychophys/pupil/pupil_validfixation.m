function pupil_validfixation(opts)

sList = opts.psr.sList.included;

% Loop over subjects
for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    
    % Loop over sessions
    for ses = 1:length(opts.sessions)
        
        % Data file
        filename = ['PCF' s_id opts.sessnames{ses}];
        file = fullfile(opts.psr.importPath, ['tpspm_' filename '.mat']);
        
        % Set new file with valid fixations
        [pathstr, name, ext] = fileparts(file);
        fixed_file = [pathstr, filesep, 'valfix_', name, ext];
        
        fixation_opt = opts.psr.fixation_opt;
        fixation_opt.newfile = fixed_file;
        
        fprintf(['Finding valid fixations - Subject ' num2str(sList(s)) ', Session ' num2str(ses) '\n'])
        
        if exist(file,'file')
            
            [~,infos,pupildata] = pspm_load_data(file);
            
            %% Convert gaze data to cm if not done yet
            gaze_chan_px = opts.psr.gaze_chan_px;
            
            if length(pupildata) == gaze_chan_px(end)+1 % if pupil data does not contain more gaze channels (cm) yet
                options.channel_action = 'add';
                pspm_convert_pixel2unit(file, gaze_chan_px, opts.psr.convert_unit, opts.psr.eyelink_width, opts.psr.eyelink_height, opts.psr.distance.screen, options);
                %pspm_convert_pixel2unit(file, gaze_chan_px, opts.psr.convert_unit, opts.psr.eyelink_width, opts.psr.eyelink_height, options);
            end
            
            %% Define fixation point
            if strcmp(opts.psr.fixation_type,'default') % default option
                fixation_opt.fixation_point = [0.5 0.5];
                
            elseif strcmp(opts.psr.fixation_type,'biased') % biased fixation
                
                fixation_opt.fixation_point = opts.psr.fixation_point_biased;
                
            elseif strcmp(opts.psr.fixation_type,'gazemedian') % median of gaze coordinates
                % depending on dataset
                if length(gaze_chan_px) == 2 % one eye
                    median_gazex = nanmedian(pupildata{gaze_chan_px(1)}.data); % median x coordinate
                    median_gazey = 200; % y coordinate set to 200 due to calibration error
                elseif length(gaze_chan_px) == 4 % two eyes
                    median_gazex = nanmedian([pupildata{gaze_chan_px(1)}.data; pupildata{gaze_chan_px(3)}.data]); % median x coordinate
                    median_gazey = nanmedian([pupildata{gaze_chan_px(2)}.data; pupildata{gaze_chan_px(4)}.data]); % median y coordinate
                end
                x_coord = median_gazex;
                inverted_y_coord = infos.source.gaze_coords.ymax-median_gazey;
                fixation_opt.fixation_point = [x_coord inverted_y_coord];
                % should be given in eyelink units, the same units as the resolution
                % need to invert y-coordinate because eyelink reports coordinates in an inverted system
                
            end
               
            %% Find valid fixations
            if ~exist(fixed_file,'file')
                pspm_find_valid_fixations(file, opts.psr.box_degrees, opts.psr.distance.screen, opts.psr.distance_unit, fixation_opt);
            end
            
        end
        
    end
    
end
    
    fprintf('---- \n')
    
end

%% Discarded stuff
% Delete extra channels if any
%             if length(pupildata) > 9
%
%                 howmany = length(pupildata)-9;
%                 delchans = ones(1,howmany)*8;
%
%                 for channel = delchans
%                     physio_deletechannel(file,channel);
%                 end
%
%             end


%             clear pupildata
%
%             [~,~,pupildata] = pspm_load_data(file);
%
%             % Find CS-US interval
%             markers = pupildata{7}.data;
%             trialstart = markers;
%             trialstart = round(trialstart);
%             time_afterUS = 4;
%             trialtime = 6+time_afterUS;
%
%             SR = pupildata{1}.header.sr;
%             trialstart = trialstart*SR-499;
%             trialend = trialstart+trialtime*SR;
%
%             pupildata_trials_l = pupildata{1}.data;
%             pupildata_trials_r = pupildata{4}.data;
%
%             [~,~,pupildata] = pspm_load_data(file);
%
%             if length(pupildata) < 9
%
%                 % Put NaN for every sample outside CS-US interval
%                 for trial = 1:length(markers)-1
%                     pupildata_trials_l(trialend(trial)+1:trialstart(trial+1)-1) = NaN;
%                     pupildata_trials_r(trialend(trial)+1:trialstart(trial+1)-1) = NaN;
%                 end
%
%                 % Save data to file
%                 newdata = struct('data', pupildata_trials_l(:));
%                 newdata.header.sr = SR;
%                 newdata.header.chantype = 'pupil_l';
%                 newdata.header.units = pupildata{1}.header.units;
%
%                 action = 'add';
%                 options.msg.prefix = 'CS-US interval only';
%                 pspm_write_channel(file, newdata, action, options);
%
%                 newdata = struct('data', pupildata_trials_r(:));
%                 newdata.header.sr = SR;
%                 newdata.header.chantype = 'pupil_r';
%                 newdata.header.units = pupildata{4}.header.units;
%
%                 action = 'add';
%                 options.msg.prefix = 'CS-US interval only';
%                 pspm_write_channel(file, newdata, action, options);
%
%             end