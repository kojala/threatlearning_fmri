function pupil_GLM_trial(opts)
%%PUPIL_GLM_trial First-level GLM per subject, trial-by-trial estimates

sList = opts.psr.sList.GLM;

options.overwrite = 1;

%% GLM for each subject
for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    
    clear model
    
    %% General settings
    % Find session files with data
    fname_eye = ['valfix_tpspm_PCF' s_id];
    sessionfiles = ls(fullfile(opts.psr.importPath,[fname_eye '_sn*.mat']));
    sespath = what(opts.psr.importPath);
    for ses = 1:size(sessionfiles,1)
        sessionfiles_fp(ses,:) = [sespath.path '\' sessionfiles(ses,:)]; % full paths
    end
    
    % Exclude second acquisition from GLM for fMRI data set
    if opts.dataset == 2
        sessionfiles_fp = sessionfiles_fp(1:5,:);
    end
    
    % Find CS and US timings
    fname_bhv = [opts.bhvname s_id '.mat'];
    eventfile = fullfile(opts.bhvPath, fname_bhv);

    % Define names and onsets
    cCS = 1;
    
    for blk = 1:size(sessionfiles_fp,1)
        
        [~, onsets, ~] = physio_get_onsets(sessionfiles_fp(blk,:),eventfile,opts); % get onsets for trials
        
        % Find trial timings
        for iCS = 1:length(onsets)
            timing{blk}.names{iCS} = sprintf('Trial_%03.0f', cCS);
            timing{blk}.onsets{iCS} = onsets(iCS);
            cCS = cCS + 1;
        end
        
    end
         
    % Output directory
    outdir = fullfile(opts.psr.glmPath.trial,opts.psr.glm_bfname{opts.psr.estimated_response});
    if ~exist(outdir,'dir'); mkdir(outdir); end
    
    %% GLM settings
    model.modelspec = 'ps_fc'; % modality
    model.modelfile = fullfile(outdir,[opts.psr.glm_filename s_id]);
    model.datafile = cellstr(sessionfiles_fp);
    
    model.timing = timing;
    model.timeunits = 'seconds';
    
    if opts.dataset == 1
        besteyedata = load(fullfile(opts.psr.dataPath,opts.psr.validtrials.besteyefn)); % get the best eye index
        model.channel = opts.psr.pupil_chan_valfix(besteyedata.besteye(s,2));
    else
        model.channel = opts.psr.pupil_chan_valfix;
    end
    
    model.bf.fhandle = 'pspm_bf_psrf_fc'; % basis function
    model.bf.args = opts.psr.estimated_response;
    model.latency = 'fixed';
    options.exclude_missing.segment_length = 10;
    options.exclude_missing.cutoff = 50;
    options.overwrite = 1;
    
    %% Run GLM
    pspm_glm(model,options)
    
end

fprintf('GLM done\n')

end