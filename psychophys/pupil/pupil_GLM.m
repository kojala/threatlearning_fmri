function pupil_GLM(opts,glm_type)
%%PUPIL_GLM First-level GLM per subject

sList = opts.psr.sList.GLM;

%% GLM for each subject
for s = 1:length(sList)
    
    s_id = num2str(sList(s));
   
    %% General settings
    % Find session files with data
    fname_eye = ['valfix_tpspm_PCF' s_id];
    sessionfiles = ls(fullfile(opts.psr.importPath,[fname_eye '_sn*.mat']));
    sespath = what(opts.psr.importPath);
    for ses = 1:size(sessionfiles,1)
        sessionfiles_fp(ses,:) = [sespath.path '\' sessionfiles(ses,:)]; % full paths
    end
    
    % Exclude second acquisition from GLM for fMRI data set
    if opts.dataset == 2
        sessionfiles_fp = sessionfiles_fp(1:5,:);
    end
    
    % Number of conditions
    no_conditions = opts.psr.estimated_conditions;
    
    % Find CS and US timings
    fname_events = [opts.eventname s_id '.mat'];
    eventfile = fullfile(opts.bhvPath, fname_events);

    cCS = 1;
    
    for blk = 1:size(sessionfiles_fp,1)
        
        % Get conditionds and onsets
        [~, onsets, conditions] = physio_get_onsets(sessionfiles_fp(blk,:),eventfile,opts);
        
        % Find trial types
        CSi = conditions{1};
        US = conditions{2};
        
        if no_conditions == 4
            CS = CSi;
        elseif no_conditions == 6
            CS = nan(length(CSi),1);
            CS(CSi==1) = 1;
            CS(CSi==2 & US == 1) = 2;
            CS(CSi==2 & US == 0) = 3;
            CS(CSi==3 & US == 1) = 4;
            CS(CSi==3 & US == 0) = 5;
            CS(CSi==4) = 6;
        end
        
        % Find trial timings
        if glm_type == 1 % conditionwise
            
            nCS = no_conditions;
            cCS = 1;
            
            for iCS = 1:nCS
                timing{blk}.names{cCS} = sprintf('Cond%1.0f', iCS);
                timing{blk}.onsets{cCS} = onsets(CS==iCS);
                cCS = cCS + 1;
            end
        
            % Output directory
            cond_no_name = [num2str(no_conditions) 'conds'];
            outdir = fullfile(opts.psr.glmPath.cond,opts.psr.glm_bfname{opts.psr.estimated_response},cond_no_name);
            % Model file
            model.modelfile = fullfile(outdir,[opts.psr.glm_filename.cond s_id]);
            
        else % trialwise

            for iCS = 1:length(onsets)
                timing{blk}.names{iCS} = sprintf('Trial_%03.0f', cCS);
                timing{blk}.onsets{iCS} = onsets(iCS);
                cCS = cCS + 1;
            end
            
            % Output directory
            outdir = fullfile(opts.psr.glmPath.trial,opts.psr.glm_bfname{opts.psr.estimated_response});
            % Model file
            model.modelfile = fullfile(outdir,[opts.psr.glm_filename.trial s_id]);
    
        end
        
    end

    if ~exist(outdir,'dir'); mkdir(outdir); end
    
    %% GLM settings
    model.modelspec = 'ps_fc'; % modality
    model.datafile = cellstr(sessionfiles_fp);
    model.timing = timing;
    model.timeunits = 'seconds';
    
    if opts.dataset == 1
        besteyedata = load(fullfile(opts.psr.dataPath,opts.psr.validtrials.besteyefn)); % get the best eye index
        model.channel = opts.psr.pupil_chan_valfix(besteyedata.besteye(s,2));
    else
        model.channel = opts.psr.pupil_chan_valfix;
    end
    
    model.bf.fhandle = 'pspm_bf_psrf_fc'; % basis function
    model.bf.args = opts.psr.estimated_response;
    model.latency = 'fixed';
    options.exclude_missing.segment_length = 10;
    options.exclude_missing.cutoff = 50;
    options.overwrite = 1;
    
    %% Run GLM
    pspm_glm(model, options);

end

fprintf('GLM done\n')

end