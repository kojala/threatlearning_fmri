function pupil_plot(opts)

for ses = opts.sessions
    
    filename = fullfile(opts.psr.dataPath,['pupil_preproc_ses' opts.sessnames{ses} '.mat']);
    data = load(filename);
    
    pupildata_USp = data.pupil_alldata_USp;
    pupildata_USm = data.pupil_alldata_USm;
    sr = data.sr;
    
    %% Plotting
    fig = figure('Position', [100, 100, 1400, 800]);
    pos = [.03 .53 .40 .40; .5 .53 .40 .40; .03 .04 .40 .40];
    clr = [9 226 183; 9 136 239; 147 0 226; 226 9 78];
    clr = clr./255;

    titles = {'All conditions'...
        'US-'...
        'CS+ - CS-'};
    %pos = axpos(1, 3, .05, .05, .05, .1, .05);
    
    % All conditions
    iPanel = 1;
    pupildata_CS1USm = squeeze(nanmean(pupildata_USm(:,:,1,:)));
    pupildata_CS2USm = squeeze(nanmean(pupildata_USm(:,:,2,:)));
    pupildata_CS3USm = squeeze(nanmean(pupildata_USm(:,:,3,:)));
    pupildata_CS2USp = squeeze(nanmean(pupildata_USp(:,:,1,:)));
    pupildata_CS3USp = squeeze(nanmean(pupildata_USp(:,:,2,:)));
    pupildata_CS4USp = squeeze(nanmean(pupildata_USp(:,:,3,:)));
    plotdata(:,1) = nanmean(pupildata_CS1USm,2);
    plotdata(:,2) = nanmean([nanmean(pupildata_CS2USm,2) nanmean(pupildata_CS2USp,2)],2);
    plotdata(:,3) = nanmean([nanmean(pupildata_CS3USm,2) nanmean(pupildata_CS3USp,2)],2);
    plotdata(:,4) = nanmean(pupildata_CS4USp,2);
    
    axes('Position', pos(iPanel,:)); hold on;
    for CStype = 1:4
        plot((1:size(plotdata,1))/sr, plotdata(:,CStype), 'Color', clr(CStype,:), 'LineWidth', 1.5);
    end
    colormap spring
    legend('String', {'0%', '33%', '66%', '100%'},'Location','best');
    legend boxoff
    title(titles{iPanel})
    
    % US-
    iPanel = 2;
    clear plotdata
    plotdata(:,1) = nanmean(pupildata_CS1USm,2);
    plotdata(:,2) = nanmean(pupildata_CS2USm,2);
    plotdata(:,3) = nanmean(pupildata_CS3USm,2);
    axes('Position', pos(iPanel,:)); hold on;
    for CStype = 1:3
        plot((1:size(plotdata,1))/sr, plotdata(:,CStype), 'Color', clr(CStype,:), 'LineWidth', 1.5);
    end
    colormap spring
    legend('String', {'0%', '33%', '66%'},'Location','best');
    legend boxoff
    title(titles{iPanel})
        
    % CS+US- - CS-US-
    iPanel = 3;
    clear plotdata
    plotdata(:,1) = nanmean(pupildata_CS1USm,2);
    plotdata(:,2) = nanmean(pupildata_CS2USm,2);
    plotdata(:,3) = nanmean(pupildata_CS3USm,2);
    axes('Position', pos(iPanel,:)); hold on;
    for CStype = 2:3
        plot((1:size(plotdata,1))/sr, plotdata(:,CStype) - plotdata(:,1), 'Color', clr(CStype,:), 'LineWidth', 1.5);
    end
    colormap spring
    legend('String', {'33%', '66%', '100%'},'Location','best');
    legend boxoff
    title(titles{iPanel})
    
    hold on
    suptitle(['Mean pupil area CS-US interval, ' opts.psr.plot.names{ses} ', N = ' num2str(size(pupildata_USm,4))]);
    
    % Save figure
    if opts.psr.plot.valfix; vfname = 'valfix'; else; vfname = 'novalfix'; end
    sessavenames = {'AcqMaint' 'Acq2'};
    fp = fileparts(opts.expPath);
    plotPath = fullfile(fp,'Plots');
    if ~exist(plotPath,'dir'); mkdir(plotPath); end
    savefig(fig,fullfile(plotPath,['pupil_meanmm_' sessavenames{ses} '_interp_' vfname '_' num2str(size(pupildata_USm,4)) 'subs']));
    
end

end