function plotPSR_validfix(opts,sessions)

% Loop over sessions
for ses = 1:length(sessions)
    
    sList = opts.psr.sList.sessions{ses};
    
    clear perc_validfix
    
    %% Load valid fixations data, save subject data and plot
    for sIDX = 1:length(sList) % Loop over subjects
        
        s_id = sList(sIDX);
        
        % Data file
        if sList(sIDX) < 100
            fixed_file  = fullfile(opts.psr.path.valfix,['valfix_tpspm_ss7b_0' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']); % Valid fixations
            eventfile = fullfile(opts.bhv.path,['ss7b_0' num2str(s_id) '_behav_' opts.psr.sessionlist{ses} '.mat']);
        else
            fixed_file  = fullfile(opts.psr.path.valfix,['valfix_tpspm_ss7b_' num2str(s_id) '_eyelink_' opts.psr.sessionlist{ses} '.mat']); % Valid fixations
            eventfile = fullfile(opts.bhv.path,['ss7b_' num2str(s_id) '_behav_' opts.psr.sessionlist{ses} '.mat']);
        end
        
        fprintf(['Subject ' num2str(sList(sIDX)) ' Session ' num2str(ses) '\n'])
        
        %% Load file with valid fixations remaining (invalid: NaN)
        if exist(fixed_file,'file')
            
            % Load and save valid fixations data for plotting
            alldata = load(fixed_file);
            
            validfix_data = alldata{opts.psr.channel.valfix}.data;
            
            perc_validfix(sIDX) = ~isnan(validfixdata)/length(valid_fixdata);
            
        end
        
    end

    figure
    histogram(perc_validfix)
    title(['Valid fixations (%), N = ' num2str(length(sList))])
    
end

end