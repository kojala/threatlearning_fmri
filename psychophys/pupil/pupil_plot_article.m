function pupil_plot_article(opts)

conditions = 4;

for ses = opts.sessions
    
    filename = fullfile(opts.psr.dataPath,['pupil_preproc_ses' num2str(ses) '_R.mat']);
    data = load(filename);
    
    if conditions == 4
        pupildata = data.pupil_alldata_CS;
        clr = [189,201,225; 103,169,207; 28,144,153; 10,128,99]; % greenish blues
        %clr = [192,192,192; 128,128,128; 105,105,105; 0,0,0]; % greys
        %baseclr = [0 90 181];
        %clr = [baseclr.*1.2; baseclr.*0.9; baseclr.*0.6; baseclr.*0.3];
        
        % Data for all conditions
        for cond = 1:conditions
            plotdata(:,cond) = nanmean(squeeze(pupildata(:,cond,:)),2);
        end
        
    else
        pupildata = data.pupil_alldata_CSUS;
        baseclr1 = [220 50 32]; % dark red
        baseclr2 = [0 90 181]; % dark blue
        clr = [baseclr1.*1.1; baseclr1.*0.9; baseclr1.*0.7; baseclr2*1.1; baseclr2.*0.9; baseclr2.*0.7;];
        
        % Data for all conditions
        plotdata(:,1) = nanmean(squeeze(pupildata(:,6,:)),2);
        plotdata(:,2) = nanmean(squeeze(pupildata(:,4,:)),2);
        plotdata(:,3) = nanmean(squeeze(pupildata(:,2,:)),2);
        plotdata(:,4) = nanmean(squeeze(pupildata(:,5,:)),2);
        plotdata(:,5) = nanmean(squeeze(pupildata(:,3,:)),2);
        plotdata(:,6) = nanmean(squeeze(pupildata(:,1,:)),2);
        
    end
    
    clr = clr./255;
    
    sr = 50;
    
    %% Plotting
    %fig = figure('Position', [100, 100, 1400, 800]);
    %pos = [.03 .53 .40 .40; .5 .53 .40 .40; .03 .04 .40 .40];
    %clr = [9 226 183; 9 136 239; 147 0 226; 226 9 78];
    
    % Within-subject error bars for all conditions and time points
    subavg = squeeze(nanmean(pupildata,2)); % mean over conditions for each sub for each timepoint
    grandavg = mean(subavg,2); % mean over subjects and conditions for each timepoint
    newvalues = nan(size(pupildata));
    subjects = size(pupildata,3);
    
    % normalization of subject values
    for cond = 1:conditions
        meanremoved = squeeze(pupildata(:,cond,:))-subavg; % remove mean of conditions from each condition value for each sub for each timepoint
        newvalues(:,cond,:) = meanremoved+repmat(grandavg,[1 subjects 1]); % add grand average over subjects to the values where individual sub average was removed
    end
    
    newvar = (cond/(cond-1))*var(newvalues,[],3); % Morey (2005) fix
    errorsem = squeeze(1.96*(sqrt(newvar)./sqrt(subjects))); % calculate error bars
    
%     for cond = 1:conditions
%         errorsem(:,cond) = nanstd(pupildata(:,cond,:),[],3)./sqrt(subjects);
%     end
    
    fig = figure('DefaultAxesFontSize', 12, 'DefaultAxesFontName', 'Arial', 'DefaultTextFontName', 'Arial');
    
    figwidth = 8.5; % 1/2 pdf column = 6, 1 pdf column = 8.5 cm, 1.5 pdf column = 11.6, 2 pdf columns = 17.6
    figheight = 8;
    
    set(fig, 'Units', 'Centimeters',...
        'PaperUnits', 'Centimeters', ...
        'PaperPositionMode','auto')
    
    set(fig,'Position', [0, 0, figwidth, figheight])
    
    set(gca,'LineWidth',1) % axes line width

%     ymin = min(min(plotdata))-0.1;
%     ymax = max(max(plotdata))+0.1;
    ymin = -0.2;
    ymax = 0.6;
    ylim([ymin ymax])
    xlim([0 11])
    line([6 6],[ymin ymax],'LineStyle','--','LineWidth',1.5,'Color',[192 192 192]./255,'HandleVisibility','off')
    
    hold on
    
    for cond = 1:conditions
        x = (1:size(plotdata,1))/sr;
        y = plotdata(:,cond);
        hl(cond) = boundedline(x, y, errorsem(:,cond), 'cmap', clr(cond,:),'alpha');
        %plot((1:size(plotdata,1))/sr, plotdata(:,CStype), 'Color', clr(CStype,:), 'LineWidth', 1.5);
    end
    
    hold on 
    
    if conditions == 4
        
        % Significance lines for t-tests
%         sigpoints(1).ttest = [];
        for test = 1:3%2:4
            cluster_p = load(fullfile(opts.psr.linregPath,['pupil_pCS_permuted_ttest_pertimebin_clusterp' num2str(test) '.mat']),'cluster_p');
            cluster_p = cluster_p.cluster_p;
            sigpoints(test).ttest = find(cluster_p>=0)*5; % correct sampling rate
            sigpoints(test).ttest(sigpoints(test).ttest >= 300) = []; % remove significance points from US onset
        end
        
        for cond = 1:4
            x = (1:size(plotdata,1))/sr;
            y = plotdata(:,cond);
            if cond > 1
                plot(x, y, '-d', 'LineStyle', 'none', 'MarkerIndices', sigpoints(cond-1).ttest, ...
                    'MarkerFaceColor', clr(cond,:), 'MarkerEdgeColor', clr(cond,:), 'MarkerSize', 2.5);
                hold on
            end
        end
        
        % Significance lines for ANOVA
        cluster_p = load(fullfile(opts.psr.linregPath,'pupil_pCS_permuted_anova_linear_pertimebin_clusterp'),'cluster_p');
        cluster_p = cluster_p.cluster_p;
        sigpoints_ANOVA = find(cluster_p>=0)*5; % correct sampling rate
        sigpoints_ANOVA(sigpoints_ANOVA >= 300) = []; % remove significance points from US onset
        
        x = (1:size(plotdata,1))/sr;
        y = -0.17*ones(1,length(x));
        plot(x, y, '-*', 'LineStyle', 'none', 'MarkerIndices', sigpoints_ANOVA, ...
            'MarkerFaceColor', 'm', 'MarkerEdgeColor', [128 128 128]./255, 'MarkerSize', 2.5);
        
        legend([hl(4) hl(3) hl(2) hl(1)],'String',{'CS(100%)', 'CS(66%)', 'CS(33%)', 'CS(0%)'},'Location','northwest');
        legend boxoff

    else
        
        legend([hl(1) hl(2) hl(3) hl(4) hl(5) hl(6)],'String',{'CS(100%)US+', 'CS(66%)US+', 'CS(33%)US+', 'CS(66%)US-', 'CS(33%)US-', 'CS(0%)US-'},'Location','northwest');
        legend boxoff
        
    end
    
    set(gca,'XTick',0:1:11,'FontSize',12)
    set(gca,'YTick',-0.2:0.1:0.6,'FontSize',12)
    
    xlabel('Time since trial onset (sec)','FontSize',12,'FontWeight','normal')
    ylabel('\Delta Pupil size from baseline (mm)','FontSize',12,'FontWeight','normal')
    
    PAPER = get(fig,'Position');
    set(fig,'PaperSize',[PAPER(3), PAPER(4)]);
    plotname = ['PCF02_pupil_preprocessed_4conds_timecourse_AcqMaint'];
    savefig(fig,fullfile(opts.expPath,'Plots',plotname));
    saveas(gcf,fullfile(opts.expPath,'Plots',plotname),'png')
    saveas(gcf,fullfile(opts.expPath,'Plots',plotname),'svg')
    
end