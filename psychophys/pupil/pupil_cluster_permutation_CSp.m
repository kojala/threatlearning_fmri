function pupil_cluster_permutation_CSp(opts)

indir = opts.psr.dataPath;
% Output directory
outdir = opts.psr.linregPath;
if ~exist(outdir,'dir'); mkdir(outdir); end

contrast_type = opts.psr.permtest.contrast_type;
test_type = opts.psr.permtest.test_type;
test_no = opts.psr.permtest.test_no;
% Data file for test statistics and cluster permutation test
if strcmp(test_type,'anova')
    datafile_anova      = fullfile(indir,'pupil_preproc_ses1_permtestdata.mat');
    datafile_clusterp   = fullfile(outdir,['pupil_pCS_permuted_anova_' contrast_type '_pertimebin_clusterp.mat']);
elseif strcmp(test_type,'ttest')
    datafile_anova      = fullfile(indir,'pupil_preproc_ses1_permtestdata_ttest');
    datafile_clusterp   = fullfile(outdir,'pupil_pCS_permuted_ttest_pertimebin_clusterp');
end

%% Old code
% if ~exist(datafile_anova,'file')
%
% %     % Load data
% %     fname = fullfile(opts.psr.dataPath,'pupil_preproc_ses.mat');
% %     data = load(fname);
% %
% %     trialdata_allsubs = squeeze(nanmean(data.pupil_alldata));
% %
% %     %% Time bin data and linear regression
% %     % Explained variable pupil data, explanatory variable CS probability
% %     sr = 50; % sampling rate
% %     timebin = 0.1*sr; % time bins 0.1 seconds (10 Hz)
% %     no_bins = size(trialdata_allsubs,1)/timebin;
% %
% %     bin_ind = 1:timebin;
% %
% %     for bin = 1:no_bins
% %
% %         % Collect relevant data and average for each condition
% %         condmean_allsubs = squeeze(nanmean(trialdata_allsubs(bin_ind,:,:)))';
% %
% %         % Create CS condition vectors
% %         Cond(1:size(condmean_allsubs,1),1) = 1;
% %         Cond(1:size(condmean_allsubs,1),2) = 2;
% %         Cond(1:size(condmean_allsubs,1),3) = 3;
% %         Cond(1:size(condmean_allsubs,1),4) = 4;
% %
% %         % Pupil data
% %         PupilSize = condmean_allsubs;
% %
% %         % Create CS probability vector
% %         CS_prob(1:size(condmean_allsubs,1),1) = 0;
% %         CS_prob(1:size(condmean_allsubs,1),2) = 1/3;
% %         CS_prob(1:size(condmean_allsubs,1),3) = 2/3;
% %         CS_prob(1:size(condmean_allsubs,1),4) = 1;
% %
% %         % CHANGE INTO LINEAR CONTRAST ANOVA
% %         % Create table for repeated measures ANOVA
% %         rmtable = table(PupilSize(:,1),PupilSize(:,2),PupilSize(:,3),PupilSize(:,4),...
% %             'VariableNames',{'CS1','CS2','CS3','CS4'});
% %         Conditions = table([1 2 3 4]','VariableNames',{'CSCondition'});
% %
% %         if strcmp(contrast_type,'linear')
% %             contrast = [-3 -1 1 3]'; % linear contrast
% %         elseif strcmp(contrast_type,'quadratic')
% %             contrast = [-1 1 1 -1]'; % quadratic contrast
% %         end
% %
% %         contrasts = [1 1 1 1; -3 -1 1 3; -1 1 1 -1]';
% %         % Do ANOVA on real labels
% %         rm = fitrm(rmtable,'CS1-CS4~1','WithinDesign',Conditions);
% %         ranovatbl = ranova(rm,'WithinModel',contrasts);
% %         %F_orig(bin) = ranovatbl.F(1);
% %         F_orig = ranovatbl.F(1);
% %         %p_orig(bin) = ranovatbl.pValue(1);
% %         p_orig = ranovatbl.pValue(1);
% %
% %         datafile_r = fullfile(outdir,['pupil_pCS_permuted_anova_' contrast_type '_timebin' num2str(bin) '.mat']);
% %         save(fullfile(datafile_r),'condmean_allsubs','F_orig','p_orig')
% %
% % %         for perm = 1:1000
% % %
% % %             % Shuffle condition labels for all subjects
% % %             for sub = 1:size(Cond,1)
% % %                 permutation = randperm(size(Cond,2));
% % %                 PupilSize(sub,:) = PupilSize(sub,permutation);
% % %             end
% % %
% % %             % Create table for repeated measures ANOVA
% % %             rmtable = table(PupilSize(:,1),PupilSize(:,2),PupilSize(:,3),PupilSize(:,4),...
% % %                 'VariableNames',{'CS1','CS2','CS3','CS4'});
% % %             Conditions = table([1 2 3 4]','VariableNames',{'CSCondition'});
% % %
% % %             % Do ANOVA on shuffled labels
% % %             rm = fitrm(rmtable,'CS1-CS4~1','WithinDesign',Conditions,'WithinModel','orthogonalcontrasts');
% % %             ranovatbl = ranova(rm,'WithinModel',contrast);
% % %             F_perm(bin,perm) = ranovatbl.F(1);
% % %             p_perm(bin,perm) = ranovatbl.pValue(1);
% % %
% % %         end
% %
% %         bin_ind = bin_ind+timebin;
% %
% %     end
% %
% %     save(fullfile(datafile_anova),'F_orig','p_orig','F_perm','p_perm')
% %     permtest_data.F_orig = F_orig;
% %     permtest_data.p_orig = p_orig;
% %     permtest_data.F_perm = F_perm;
% %     permtest_data.p_perm = p_perm;
%
% else
%     permtest_data = load(datafile_anova);
% end

%% Permutation test

for test = test_no
    
    if strcmp(test_type,'anova')
        permtest_data = load(datafile_anova);
    else
        permtest_data = load([datafile_anova num2str(test) '.mat']);
    end
    
    if strcmp(test_type,'anova') && strcmp(contrast_type,'linear')
        
        Fvalue = permtest_data.Fvalue_linear;
        pvalue = permtest_data.pvalue_linear;
        Fvalue_orig = permtest_data.Fvalue_orig_linear;
        pvalue_orig = permtest_data.pvalue_orig_linear;
        
    elseif strcmp(test_type,'anova') && strcmp(contrast_type,'quadratic')
        
        Fvalue = permtest_data.Fvalue_quadratic;
        pvalue = permtest_data.pvalue_quadratic;
        Fvalue_orig = permtest_data.Fvalue_orig_quadratic;
        pvalue_orig = permtest_data.pvalue_orig_quadratic;
        
    elseif strcmp(test_type,'ttest')
        
        Fvalue = permtest_data.tvalue;
        pvalue = permtest_data.pvalue;
        Fvalue_orig = permtest_data.tvalue_orig;
        pvalue_orig = permtest_data.pvalue_orig;
        
    end
    
    figure
    plot(Fvalue,'LineStyle','none','Marker','o','MarkerFaceColor','b','MarkerEdgeColor','k')
    hold on
    plot(Fvalue_orig,'LineStyle','none','Marker','o','MarkerFaceColor','r','MarkerEdgeColor','k')
    xlabel('Time (s)')
    set(gca,'XTick',0:10:110,'XTickLabel',0:11)
    xlim([0 110])
    ylabel('F-value')
    text(3,130,{'Red: Original condition labels' 'Blue: Randomly shuffled condition labels'})
    title('F-values going into the permutation test')
    
    cluster_p = permtest(Fvalue_orig,pvalue_orig,Fvalue,pvalue);
    
    if strcmp(test_type,'anova')
        save(fullfile(datafile_clusterp),'Fvalue_orig','pvalue_orig','cluster_p','Fvalue','pvalue');
    else
        save(fullfile([datafile_clusterp num2str(test) '.mat']),'Fvalue_orig','pvalue_orig','cluster_p','Fvalue','pvalue');
    end
    
end

end