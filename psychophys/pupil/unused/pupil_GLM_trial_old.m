function pupil_GLM_trial(ExpPath,sList)
%%PUPIL_GLM_trial First-level GLM per subject, trial-by-trial estimates

eyePath = fullfile(ExpPath,'Eyetracker');

% Sessions for this study
phases = 1:3;
phasenames = {'Acq','Maint','Acq2'};
blocks = {1; 2:5; 6}; % Blocks 1-5 for first session, block 6 for the second
trials = [24 44 44 44 44 24];

options.overwrite = 1;

%% GLM for each subject
for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    
    % Loop over the three phases (Acq, Maint, Acq2)
    for p = phases
        
        clear model
        
        model.channel = 6;
        model.timeunits = 'markers';
        model.modelspec = 'ps_fc';
        
        % Pupil data file
        if p == 2
            fname_eye1 = fullfile(eyePath,'Imported_area',['valfix_tpspm_PCF' s_id '_sn02.mat']);
            fname_eye2 = fullfile(eyePath,'Imported_area',['valfix_tpspm_PCF' s_id '_sn03.mat']);
            fname_eye3 = fullfile(eyePath,'Imported_area',['valfix_tpspm_PCF' s_id '_sn04.mat']);
            fname_eye4 = fullfile(eyePath,'Imported_area',['valfix_tpspm_PCF' s_id '_sn05.mat']);
            fname_eye = {fname_eye1 fname_eye2 fname_eye3 fname_eye4};
        elseif p == 1
            fname_eye = fullfile(eyePath,'Imported_area',['valfix_tpspm_PCF' s_id '_sn01.mat']);
        elseif p == 3
            fname_eye = fullfile(eyePath,'Imported_area',['valfix_tpspm_PCF' s_id 'B.mat']);
        end
        
        model.datafile = fname_eye;
        
        alltrials = 1; % Counter for all trials over blocks
        %trlnames = cellstr(num2str(triallist(:)));
        
        % Loop over the relevant blocks for the session
        for block = blocks{p}

            if p == 2
                nblock = block-1;
            else
                nblock = 1;
            end
            
            for trial = 1:trials(block)
                
                model.timing{nblock}.names{trial} = ['Trial' num2str(alltrials)];
                model.timing{nblock}.onsets{trial} = trial;
                model.timing{nblock}.durations{trial} = 0;
                
                alltrials = alltrials+1;
                
            end
            
        end
        
        outdir = fullfile(eyePath,'GLM_trialwise',phasenames{p});
        if ~exist(outdir,'dir'); mkdir(outdir); end
        
        model.modelfile = fullfile(outdir,['GLM_' s_id '.mat']);
        
        % Run GLM
        if (p == 2 && exist(fname_eye1,'file')) || (((p == 1 || p == 3) && exist(fname_eye,'file')))
            fprintf('Subject %3.0f, Phase %3.0f \n', sList(s), p)
            pspm_glm(model,options)
        end
        
    end
    
end

fprintf('GLM done\n')

end