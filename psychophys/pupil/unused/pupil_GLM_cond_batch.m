function pupil_GLM_cond(opts)
%%PUPIL_GLM First-level GLM per subject

sList = opts.psr.sList.GLM;

%% GLM for each subject
for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    fname_eye = ['valfix_tpspm_PCF' s_id '.mat'];
    eyefile = fullfile(opts.psr.dataPath, fname_eye);
    fname_bhv = [opts.bhvname s_id '.mat'];
    eventfile = fullfile(opts.bhvPath, fname_bhv);
    
    clear matlabbatch glm_ps_fc
    
    %% General settings
    % File with event types
    
    % Basis function
    estimated_response = opts.psr.estimated_response;
    if estimated_response == 1
        bfname = 'psrf_fc1'; % CS response + derivative, default
        glmname = 'CSderiv';
    elseif estimated_response == 2
        bfname = 'psrf_fc2'; % CS + US response, no derivative
        glmname = 'CSUS';
    elseif estimated_response == 3
        bfname = 'psrf_fc3'; % US response + derivative
        glmname = 'USderiv';
    end
    
    % Output directory
    outdir = fullfile(opts.expPath,'Models',['GLM_6conds' glmname]);
    if ~exist(outdir,'dir'); mkdir(outdir); end
    glm_ps_fc.outdir = cellstr(outdir);
    
    %% GLM settings
    model.modelspec = 'scr'; % modality
    model.modelfile = fullfile(outdir,['GLM_' s_id]);
    model.datafile = cellstr(eyefile);
    
    model.timing = {multiplecondfiles};
    model.timeunits = 'seconds';
    model.channel = opts.psr.pupil_chan_valfix;
    
    model.bf.fhandle = bfname; % basis function
    model.bf.args = [];
    
    model.latency = 'fixed';
    model.window = []; % only if model latency free
    
    options.overwrite = 1;
    
    %% Run GLM
    pspm_glm(model, options);

end

fprintf('GLM done\n')

end

%% Old stuff
% condnames = {'CS(0)','CS(1/3)','CS(2/3)','CS(1)'};
%     type1 = bhvdata.data(:,4) == 1; % CS(0)
%     type2 = bhvdata.data(:,4) == 2; % CS(1/3)
%     type3 = bhvdata.data(:,4) == 3; % CS(2/3)
%     type4 = bhvdata.data(:,4) == 4; % CS(1)