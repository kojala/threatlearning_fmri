function pupil_linregression_pCS(opts)

sList = opts.psr.sList.GLM;

% Output directory
outdir = opts.psr.linregPath;
if ~exist(outdir,'dir'); mkdir(outdir); end

contrast_type = opts.psr.permtest.contrast_type;
% Data file for ANOVA data for cluster permutation test
datafile_anova      = fullfile(outdir,['pupil_pCS_permuted_anova_' contrast_type '_pertimebin.mat']);
datafile_clusterp   = fullfile(outdir,['pupil_pCS_permuted_anova_' contrast_type '_pertimebin_clusterp.mat']);

if ~exist(datafile_anova,'file')
    
    for s = 1:numel(sList)
        
        s_id = num2str(sList(s));
        
        %fname_bhv = [opts.bhvname s_id '.mat'];
        %eventfile = fullfile(opts.bhvPath, fname_bhv);
        
        fname = ['valfix_tpspm_PCF' s_id];
        sessionfiles = ls(fullfile(opts.psr.importPath,[fname '_sn*.mat']));
        
        fname = ['seg_valfix_tpspm_PCF' s_id];
        segfiles = ls(fullfile(opts.psr.importPath,[fname '_sn*.mat']));
        
        sespath = what(opts.psr.importPath);
        
        for ses = 1:size(sessionfiles,1)
            
            sessionfile_fp = [sespath.path '\' sessionfiles(ses,:)]; % full paths
            segfile_fp = [sespath.path '\' segfiles(ses,:)]; % full paths
            
            % Get conditionds and onsets
            %         [~, ~, conditions] = physio_get_onsets(sessionfile_fp,eventfile,opts);
            
            % Find trial types
            %         CS = conditions{1};
            %         pCS(CS == 1) = 0;
            %         pCS(CS == 2) = 1/3;
            %         pCS(CS == 3) = 2/3;
            %         pCS(CS == 4) = 1;
            %US = conditions{2};
            %CS = CS(US == 0); % only US- trials
            
            %         CS_probability(s) = pCS;
            
            % Load data
            data = load(segfile_fp);
            
            for cond = 1:length(data.segments)
                nanperc = data.segments{cond,1}.trial_nan_percent;
                validtrials = nanperc <= 0.75;
                trialdata = data.segments{cond,1}.data(:,validtrials);
                trialdata_allsubs(s,cond,:) = nanmean(trialdata,2);
            end
            
        end
        
    end
    
    %% Time bin data and linear regression
    % Explained variable pupil data, explanatory variable CS probability
    sr = 500; % sampling rate
    timebin = 0.1*sr; % time bins 0.1 seconds (10 Hz)
    no_bins = size(trialdata_allsubs,3)/timebin;
    
    bin_ind = 1:timebin;
    
    for bin = [1 50]%:no_bins
        
        % Collect relevant data and average for each condition
        condmean_allsubs = squeeze(nanmean(trialdata_allsubs(:,:,bin_ind),3));
        
        % Create CS condition vectors
        Cond(1:size(condmean_allsubs,1),1) = 1;
        Cond(1:size(condmean_allsubs,1),2) = 2;
        Cond(1:size(condmean_allsubs,1),3) = 3;
        Cond(1:size(condmean_allsubs,1),4) = 4;
        
        % Pupil data
        PupilSize = condmean_allsubs;
        
        % Create CS probability vector
        CS_prob(1:size(condmean_allsubs,1),1) = 0;
        CS_prob(1:size(condmean_allsubs,1),2) = 1/3;
        CS_prob(1:size(condmean_allsubs,1),3) = 2/3;
        CS_prob(1:size(condmean_allsubs,1),4) = 1;
        
        % CHANGE INTO LINEAR CONTRAST ANOVA
        % Create table for repeated measures ANOVA
        rmtable = table(PupilSize(:,1),PupilSize(:,2),PupilSize(:,3),PupilSize(:,4),...
            'VariableNames',{'CS1','CS2','CS3','CS4'});
        Conditions = table([1 2 3 4]','VariableNames',{'CSCondition'});
        
        if strcmp(contrast_type,'linear')
            contrast = [-3 -1 1 3]'; % linear contrast
        elseif strcmp(contrast_type,'quadratic')
            contrast = [-1 1 1 -1]'; % quadratic contrast
        end

        contrasts = [-3 -1 1 3]';%; -1 1 1 -1]';
        % Do ANOVA on real labels
        rm = fitrm(rmtable,'CS1-CS4~1','WithinDesign',Conditions); 
        ranovatbl = ranova(rm,'WithinModel',contrasts);
        F_orig(bin) = ranovatbl.F(1);
        p_orig(bin) = ranovatbl.pValue(1);
        
        datafile_r = fullfile(outdir,['pupil_pCS_permuted_anova_' contrast_type '_timebin' num2str(bin) '.mat']);
        save(fullfile(datafile_r),'condmean_allsubs','F_orig','p_orig')
        
%         for perm = 1:1000
%             
%             % Shuffle condition labels for all subjects
%             for sub = 1:size(Cond,1)
%                 permutation = randperm(size(Cond,2));
%                 PupilSize(sub,:) = PupilSize(sub,permutation);
%             end
%             
%             % Create table for repeated measures ANOVA
%             rmtable = table(PupilSize(:,1),PupilSize(:,2),PupilSize(:,3),PupilSize(:,4),...
%                 'VariableNames',{'CS1','CS2','CS3','CS4'});
%             Conditions = table([1 2 3 4]','VariableNames',{'CSCondition'});
%             
%             % Do ANOVA on shuffled labels
%             rm = fitrm(rmtable,'CS1-CS4~1','WithinDesign',Conditions,'WithinModel','orthogonalcontrasts');
%             ranovatbl = ranova(rm,'WithinModel',contrast);
%             F_perm(bin,perm) = ranovatbl.F(1);
%             p_perm(bin,perm) = ranovatbl.pValue(1);
%             
%         end
        
        % Linear regression coefficient
        %     pupilsize_bin = trialdata_mean(bin_ind,:);
        %     pupilsize_bin = pupilsize_bin(:); % long format, conditions after each other
        %     CSprob_bin = CS_prob(bin_ind,:);
        %     CSprob_bin = CSprob_bin(:); % long format
        %
        %     b1(bin) = CSprob_bin\pupilsize_bin; % slope/regression coefficient
        
        bin_ind = bin_ind+timebin;
        
    end
    
    save(fullfile(datafile_anova),'F_orig','p_orig','F_perm','p_perm')
    permtest_data.F_orig = F_orig;
    permtest_data.p_orig = p_orig;
    permtest_data.F_perm = F_perm;
    permtest_data.p_perm = p_perm;
    
else
    permtest_data = load(datafile_anova);
end

cluster_p = permtest(permtest_data.F_orig',permtest_data.p_orig',permtest_data.F_perm,permtest_data.p_perm);

% save(fullfile(outdir,'linear_regression_probCS_coefficients_pertimebin.mat'),'b1')
save(fullfile(datafile_clusterp),'cluster_p')

end