function pupil_linregression_pCS_plot(opts)

data = load(fullfile(opts.psr.linregPath,'linear_regression_probCS_coefficients_pertimebin.mat'));

sr = 10;
% Pupil size canonical response function
TD = 1/sr;
cs = 1;
cs_d = 0;
us = 1;
us_shift = 6;
bf = pspm_bf_psrf_fc(TD, cs, cs_d, us, us_shift);
%bf(bf==0) = NaN;

% Normalize regression coefficients to 0-1 range
plotdata = (data.b1-min(data.b1))/(max(data.b1)-min(data.b1));

% Plot regression coefficients and the canonical RF
figure
hold on
line([6*sr 6*sr], [0 1],'Color','r','LineWidth',2)
csbf = plot(bf(:,1),'Color',[0 153 0]/255,'LineWidth',1.5);
usbf = plot(bf(:,2),'Color',[255 128 0]/255,'LineWidth',1.5);
beta = plot(plotdata,'-o','MarkerSize',2,'MarkerFaceColor','b','MarkerEdgeColor','k','LineWidth',1,'Color',[0 76 153]/255);
title('Regression coefficients per time bin, Pupil size ~ p(CS)')
xlabel('Time (s)')
ylabel('Coefficient')
xlim([1 11*sr])
%ylim([4.5 5.1])
set(gca,'XTick',0:sr:11*sr)
set(gca,'XTickLabel',0:11)
legend([csbf usbf beta],{'CS RF','US RF','Beta'},'Location','northwest')

end