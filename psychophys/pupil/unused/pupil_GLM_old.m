function pupil_GLM(ExpPath,sList)
%%PUPIL_GLM First-level GLM per subject

eyePath = fullfile(ExpPath,'Eyetracker');

% Sessions for this study
phases = 1:3;
phasenames = {'Acq','Maint','Acq2'};
blocks = {1; 2:5; 6}; % Blocks 1-5 for first session, block 6 for the second
trials = [24 44 44 44 44 24];
condnames = {'CS(0)','CS(1/3)','CS(2/3)','CS(1)'}; % Add all conditions

%% GLM for each subject
for s = 1:length(sList)
    
    s_id = num2str(sList(s));

    % Loop over the three phases (Acq, Maint, Acq2)
    for p = phases
        
        clear matlabbatch glm_ps_fc
        
        % General settings
        glm_ps_fc.chan.chan_def.best_eye = 6;
        glm_ps_fc.timeunits.seconds = 'seconds';
        glm_ps_fc.latency.fixed = 'fixed';
        glm_ps_fc.bf.psrf_fc0 = 1;
        glm_ps_fc.norm = false;
        glm_ps_fc.filter.def = 0;
        glm_ps_fc.overwrite = true;
        
        % Loop over the relevant blocks for the session
        for block = blocks{p}
            
            blktrials = 1:trials(block);
            
            % Pupil data file
            if p < 3
                fname_eye = ['valfix_tpspm_PCF' s_id '_sn0' num2str(block) '.mat'];
            else
                fname_eye = ['valfix_tpspm_PCF' s_id 'B.mat'];
            end
            
            eyefile = fullfile(eyePath, 'Imported_area', fname_eye);
            
            % File with event types
            fname_bhv = ['event_types_S' s_id '_Block' num2str(block) '.mat'];
            eventfile = fullfile(ExpPath, 'SCR', 'EventTimings', fname_bhv);
            
            load(eventfile)
            % Retrieve condition types
            type1 = allconditions == 1; % CS(0)US-
            type2 = allconditions == 2 | allconditions == 3; % CS(1/3)
            type3 = allconditions == 4 | allconditions == 5; % CS(2/3)
            type4 = allconditions == 6; % CS(1)
            
            if exist(eyefile,'file') % Only if pupil data file exists
                
                load(eyefile)
                markers = data{4}.data;
                
                % Retrieve timing for each trial type and add to onsets
                onset_type1 = markers(blktrials(type1));
                onset_type2 = markers(blktrials(type2));
                onset_type3 = markers(blktrials(type3));
                onset_type4 = markers(blktrials(type4));
                
                if p == 2
                    
                    nblock = block-1;
                    
                    % Set names and durations for conditions
                    for c = 1:length(condnames)
                        glm_ps_fc.session(nblock).data_design.condition(c).name = condnames{c};
                        glm_ps_fc.session(nblock).data_design.condition(c).durations = 0;
                    end
                    
                    %glm_ps_fc.session(nblock).missing.no_epochs = 0;
                    glm_ps_fc.session(nblock).datafile = cellstr(eyefile);
                    glm_ps_fc.session(nblock).data_design.condition(1).onsets = onset_type1;
                    glm_ps_fc.session(nblock).data_design.condition(2).onsets = onset_type2;
                    glm_ps_fc.session(nblock).data_design.condition(3).onsets = onset_type3;
                    glm_ps_fc.session(nblock).data_design.condition(4).onsets = onset_type4;
                    
                else
                    
                    for c = 1:length(condnames)
                        glm_ps_fc.session.data_design.condition(c).name = condnames{c};
                        glm_ps_fc.session.data_design.condition(c).durations = 0;
                    end
                    
                    glm_ps_fc.session.datafile = cellstr(eyefile);
                    glm_ps_fc.session.data_design.condition(1).onsets = onset_type1;
                    glm_ps_fc.session.data_design.condition(2).onsets = onset_type2;
                    glm_ps_fc.session.data_design.condition(3).onsets = onset_type3;
                    glm_ps_fc.session.data_design.condition(4).onsets = onset_type4;
                    
                end
                
            end
            
        end
        
        outdir = fullfile(eyePath,'GLM_fc0',phasenames{p});
        if ~exist(outdir,'dir'); mkdir(outdir); end
        glm_ps_fc.outdir = cellstr(outdir);
        
        glm_ps_fc.modelfile = ['GLM_' s_id];
        
        matlabbatch{1}.pspm{1}.first_level{1}.ps{1}.glm_ps_fc = glm_ps_fc;
        
        % Run batch
        if exist(eyefile,'file')
            fprintf('Subject %3.0f, Phase %3.0f \n', sList(s), p)
            pspm_jobman('run', matlabbatch); 
        end
        
    end
    
end

fprintf('GLM done\n')