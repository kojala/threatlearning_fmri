function pupil_GLM_trial_PCF02(ExpPath,sList)
%%PUPIL_GLM_trial First-level GLM per subject, trial-by-trial estimates

eyePath = fullfile(ExpPath);

options.overwrite = 1;

%% GLM for each subject
for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    
    clear model
    
    model.channel = 10;
    model.timeunits = 'markers';
    model.modelspec = 'ps_fc';
        
    % Pupil data file
    fname_eye = fullfile(eyePath,['valfix_tpspm_PCF' s_id '.mat']);
    model.datafile = fname_eye;
    
    [~,~,pupildata] = pspm_load_data(fname_eye);
    trials = sum(~isnan(pupildata{11}.data));
    alltrials = 1; % Counter for all trials over blocks
        
    for trial = 1:trials
        
        model.timing.names{trial} = ['Trial' num2str(alltrials)];
        model.timing.onsets{trial} = trial;
        model.timing.durations{trial} = 0;
        
        alltrials = alltrials+1;
        
    end
            
        
    outdir = fullfile(eyePath,'GLM_trialwise');
    if ~exist(outdir,'dir'); mkdir(outdir); end
        
    model.modelfile = fullfile(outdir,['GLM_' s_id '.mat']);
        
    % Run GLM
    if exist(fname_eye,'file')
        fprintf('Subject %3.0f \n', sList(s))
        pspm_glm(model,options)
    end
    
end

fprintf('GLM done\n')

end