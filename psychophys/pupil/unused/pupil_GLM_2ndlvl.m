function pupil_GLM_2ndlvl(ExpPath)
%%PUPIL_GLM_trial First-level GLM per subject, trial-by-trial estimates

filePath = fullfile(ExpPath,'GLM');

modelfiles = cellstr(ls(filePath));
modelfiles(1:2) = [];

cd(filePath)

matlabbatch{1}.pspm{1}.second_level{1}.contrast.testtype.one_sample.modelfile = modelfiles;
matlabbatch{1}.pspm{1}.second_level{1}.contrast.outdir = {filePath};
matlabbatch{1}.pspm{1}.second_level{1}.contrast.filename = 'group_glm';
matlabbatch{1}.pspm{1}.second_level{1}.contrast.def_con_name.file.con_all = 'all';
matlabbatch{1}.pspm{1}.second_level{1}.contrast.overwrite = false;

pspm_jobman('run', matlabbatch); 

end