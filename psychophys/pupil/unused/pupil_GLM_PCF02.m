function outdir = pupil_GLM_PCF02(ExpPath,sList,estimated_response)
%%PUPIL_GLM First-level GLM per subject

% Sessions for this study
condnames = {'CS(0)','CS(1/3)US+','CS(1/3)US-','CS(2/3)US+','CS(2/3)US-','CS(1)'}; % Add all conditions
% condnames = {'CS(0)','CS(1/3)','CS(2/3)','CS(1)'};

%% GLM for each subject
for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    
    clear matlabbatch glm_ps_fc
    
    % General settings
    glm_ps_fc.chan.chan_def.best_eye = 10;
    glm_ps_fc.timeunits.seconds = 'seconds';
    glm_ps_fc.latency.fixed = 'fixed';
    if estimated_response == 1
        glm_ps_fc.bf.psrf_fc1 = 1; % CS response + derivative, default
        glmname = 'CSderiv';
    elseif estimated_response == 2
        glm_ps_fc.bf.psrf_fc2 = 2; % CS + US response, no derivative
        glmname = 'CSUS';
    elseif estimated_response == 3
        glm_ps_fc.bf.psrf_fc3 = 3; % US response + derivative
        glmname = 'USderiv';
    end
    glm_ps_fc.norm = false;
    glm_ps_fc.filter.def = 0;
    glm_ps_fc.overwrite = true;
    
    fname_eye = ['valfix_tpspm_PCF' s_id '.mat'];
    
    eyefile = fullfile(ExpPath, fname_eye);
    
    % File with event types
    fname_bhv = ['PCF_02_' s_id '.mat'];
    eventfile = fullfile(ExpPath, fname_bhv);
    
    bhvdata = load(eventfile);
    % Retrieve condition types
    type1 = bhvdata.data(:,4) == 1; % CS(0)
    type2 = bhvdata.data(:,4) == 2 & bhvdata.data(:,5) == 1; % CS(1/3)US+
    type3 = bhvdata.data(:,4) == 2 & bhvdata.data(:,5) == 0; % CS(1/3)US-
    type4 = bhvdata.data(:,4) == 3 & bhvdata.data(:,5) == 1; % CS(2/3)US+
    type5 = bhvdata.data(:,4) == 3 & bhvdata.data(:,5) == 0; % CS(2/3)US-
    type6 = bhvdata.data(:,4) == 4; % CS(1)
    
%     type1 = bhvdata.data(:,4) == 1; % CS(0)
%     type2 = bhvdata.data(:,4) == 2; % CS(1/3)
%     type3 = bhvdata.data(:,4) == 3; % CS(2/3)
%     type4 = bhvdata.data(:,4) == 4; % CS(1)

    if exist(eyefile,'file') % Only if pupil data file exists
        
        [~,~,data] = pspm_load_data(eyefile);
        markers = data{7}.data;
        
        % Retrieve timing for each trial type and add to onsets
        onset_type1 = markers(type1)+6;
        onset_type2 = markers(type2)+6;
        onset_type3 = markers(type3)+6;
        onset_type4 = markers(type4)+6;
        onset_type5 = markers(type5)+6;
        onset_type6 = markers(type6)+6;
        
        for c = 1:length(condnames)
            glm_ps_fc.session.data_design.condition(c).name = condnames{c};
            glm_ps_fc.session.data_design.condition(c).durations = 0;
        end
        
        glm_ps_fc.session.datafile = cellstr(eyefile);
        glm_ps_fc.session.data_design.condition(1).onsets = onset_type1;
        glm_ps_fc.session.data_design.condition(2).onsets = onset_type2;
        glm_ps_fc.session.data_design.condition(3).onsets = onset_type3;
        glm_ps_fc.session.data_design.condition(4).onsets = onset_type4;
        glm_ps_fc.session.data_design.condition(5).onsets = onset_type5;
        glm_ps_fc.session.data_design.condition(6).onsets = onset_type6;
        
    end
    
    outdir = fullfile(ExpPath,['GLM_besteye_6conds' glmname]);
    
    if ~exist(outdir,'dir'); mkdir(outdir); end
    glm_ps_fc.outdir = cellstr(outdir);
    
    glm_ps_fc.modelfile = ['GLM_' s_id];
    
    matlabbatch{1}.pspm{1}.first_level{1}.ps{1}.glm_ps_fc = glm_ps_fc;
    
    % Run batch
    if exist(eyefile,'file')
        fprintf('Subject %3.0f \n', sList(s))
        pspm_jobman('run', matlabbatch);
    end
    
end

fprintf('GLM done\n')

end