function pupil_GLM_trial_phasestogether(ExpPath,sList)
%%PUPIL_GLM_trial First-level GLM per subject, trial-by-trial estimates

eyePath = fullfile(ExpPath,'Eyetracker');

% Sessions for this study
trials = [24 44 44 44 44 24];

options.overwrite = true;

%% GLM for each subject
for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    
    clear model
    
    model.channel = 6;
    model.timeunits = 'markers';
    model.modelspec = 'ps_fc';
    
    % Pupil data file
    fname_eye1 = fullfile(eyePath,'Imported_area',['valfix_tpspm_PCF' s_id '_sn01.mat']);
    fname_eye2 = fullfile(eyePath,'Imported_area',['valfix_tpspm_PCF' s_id '_sn02.mat']);
    fname_eye3 = fullfile(eyePath,'Imported_area',['valfix_tpspm_PCF' s_id '_sn03.mat']);
    fname_eye4 = fullfile(eyePath,'Imported_area',['valfix_tpspm_PCF' s_id '_sn04.mat']);
    fname_eye5 = fullfile(eyePath,'Imported_area',['valfix_tpspm_PCF' s_id '_sn05.mat']);
    fname_eye6 = fullfile(eyePath,'Imported_area',['valfix_tpspm_PCF' s_id 'B.mat']);
    
    if sList(s) < 163 % 160 and 161 don't have eyetracking data for Acq2
        fname_eye = {fname_eye1 fname_eye2 fname_eye3 fname_eye4 fname_eye5};
    else
        fname_eye = {fname_eye1 fname_eye2 fname_eye3 fname_eye4 fname_eye5 fname_eye6};
    end
    
    model.datafile = fname_eye;
    
    alltrials = 1; % Counter for all trials over blocks
    
    if sList(s) < 163
        blocks = 1:5;
        triallist = 1:200;
    else
        blocks = 1:6;
        triallist = 1:224;
    end
    
    trlnames = cellstr(num2str(triallist(:)));
    
    % Loop over the relevant blocks for the session
    for block = blocks
        
        for trial = 1:trials(block)
            
            model.timing{block}.onsets{trial} = trial;
            model.timing{block}.names{trial} = trlnames{alltrials};
            model.timing{block}.durations{trial} = 0;
            
            alltrials = alltrials+1;
            
        end
        
    end
    
    outdir = fullfile(eyePath,'GLM_trialwise','AllPhases');
    if ~exist(outdir,'dir'); mkdir(outdir); end
    
    model.modelfile = fullfile(outdir,['GLM_' s_id '.mat']);
    
    % Run GLM
    if exist(fname_eye1,'file')
        fprintf('Subject %3.0f \n', sList(s))
        pspm_glm(model,options)
    end
    
end

fprintf('GLM done\n')

end