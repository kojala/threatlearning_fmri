function physio_simpleplotGLM(opts,modality)

% Modality options
if strcmp(modality,'PSR')
    sList = opts.psr.sList.GLM;
    no_conditions = opts.psr.estimated_conditions;
    cond_no_name = [num2str(no_conditions) 'conds'];
    glmpath = fullfile(opts.psr.glmPath.cond,opts.psr.glm_bfname{opts.psr.estimated_response},cond_no_name);
    glm_filename = opts.psr.glm_filename.cond;
    %bfname = opts.psr.glm_bfname{opts.psr.estimated_response};
    condition_indices_cs = 1:no_conditions; % CR only model (no US resp)
    unit = 'Pupil size (cm)';
elseif strcmp(modality,'RAR')
    sList = opts.rar.sList.GLM;
    no_conditions = opts.rar.estimated_conditions;
    glmpath = fullfile(opts.modelPath,'RAR','GLM');
    glm_filename = 'glm_';
    %bfname = '';
    condition_indices_cs = 1:2:no_conditions*2; % CR
    condition_indices_us = 2:2:no_conditions*2; % UR
    unit = 'Respiratory amplitude (a.u.)';
elseif strcmp(modality,'HPR')
    sList = opts.hpr.sList.GLM;
    no_conditions = opts.hpr.estimated_conditions;
    glmpath = fullfile(opts.modelPath,'HPR','GLM');
    glm_filename = 'glm_';
    %bfname = '';
    condition_indices_cs = 1:2:no_conditions*2; % CR
    condition_indices_us = 2:2:no_conditions*2; % UR
    unit = 'Heart period (a.u.)';
else
   fprintf('Modality not supported! Enter either PSR, RAR or HPR.'); 
end

% Condition names
if no_conditions == 4
    condnames = {'CS(0)','CS(1/3)','CS(2/3)','CS(1)'};
elseif no_conditions == 6
    condnames = {'CS(0)','CS(1/3)US+','CS(1/3)US-','CS(2/3)US+','CS(2/3)US-','CS(1)'};
end

%% Retrieve GLM stats
glm_bf1 = nan(length(sList),no_conditions);
glm_bf2 = nan(length(sList),no_conditions);

for s = 1:numel(sList)
    
    s_id = num2str(sList(s));
    
    
    filename = [glm_filename s_id '.mat'];
    datafile = fullfile(glmpath,filename);
    
    if exist(datafile,'file')
        
        glmdata = load(datafile);
        
        for cond = 1:no_conditions
            glm_bf1(s,cond) = glmdata.glm.stats(condition_indices_cs(cond));
        end
        
        if exist('condition_indices_us','var') % if US estimates included
            for cond = 1:no_conditions
                glm_bf2(s,cond) = glmdata.glm.stats(condition_indices_us(cond));
            end
        end
        
    end
end

%% Reconstruct response
% if strcmp(modality,'HPR') || strcmp(modality,'RAR')
%    pspm_reconresp 
% end

%% Calculate within-subject error bars

% CR error bars
subavg = mean(glm_bf1,2); % mean over conditions for each sub
grandavg = mean(subavg); % mean over subjects and conditions

newvalues_us = nan(size(glm_bf1));

% normalization of subject values
for cond = 1:no_conditions
    meanremoved = glm_bf1(:,cond)-subavg; % remove mean of conditions from each condition value for each sub
    newvalues_us(:,cond) = meanremoved+repmat(grandavg,[length(sList) 1 1]); % add grand average over subjects to the values where individual sub average was removed
    plotdata_mean(:,cond) = mean(newvalues_us(:,cond));
    plotdata_median(:,cond) = median(glm_bf1(:,cond));
end

newvar = (cond/(cond-1))*var(newvalues_us);
errorbars = squeeze(1.96*(sqrt(newvar)./sqrt(length(sList)))); % calculate error bars according to Cousineau (2005) with Morey (2005) fix

% UR error bars
if exist('condition_indices_us','var')
    subavg = mean(glm_bf2,2); % mean over conditions for each sub
    grandavg = mean(subavg); % mean over subjects and conditions
    
    newvalues_us = nan(size(glm_bf2));
    
    % normalization of subject values
    for cond = 1:no_conditions
        meanremoved = glm_bf2(:,cond)-subavg; % remove mean of conditions from each condition value for each sub
        newvalues_us(:,cond) = meanremoved+repmat(grandavg,[length(sList) 1 1]); % add grand average over subjects to the values where individual sub average was removed
        plotdata_mean_us(:,cond) = mean(newvalues_us(:,cond));
        plotdata_median_us(:,cond) = median(glm_bf2(:,cond));
    end
    
    newvar_us = (cond/(cond-1))*var(newvalues_us);
    errorbars_us = squeeze(1.96*(sqrt(newvar_us)./sqrt(length(sList)))); % calculate error bars according to Cousineau (2005) with Morey (2005) fix
end

%% Plot

figure('Position',[300,300,800,600]);

if exist('condition_indices_us','var'); subplot_col = 2; else; subplot_col = 1; end

subplot(2,subplot_col,1)
hold on
bar(plotdata_mean)
errorbar(plotdata_mean,errorbars,'k','LineStyle','none')
title('Mean BF1')
set(gca,'xTick',1:no_conditions)
set(gca,'xTickLabel', condnames)
ylabel(unit)

subplot(2,subplot_col,2)
hold on
bar(plotdata_median)
title('Median BF1')
set(gca,'xTick',1:length(condnames))
set(gca,'xTickLabel', condnames)
ylabel(unit)

if exist('condition_indices_us','var')
    subplot(2,2,3)
    hold on
    bar(plotdata_mean_us)
    errorbar(plotdata_mean_us,errorbars_us,'k','LineStyle','none')
    title('Mean BF2')
    set(gca,'xTick',1:no_conditions)
    set(gca,'xTickLabel', condnames)
    ylabel(unit)
    
    subplot(2,2,4)
    hold on
    bar(plotdata_median_us)
    title('Median BF2')
    set(gca,'xTick',1:length(condnames))
    set(gca,'xTickLabel', condnames)
    ylabel(unit)
end

suptitle([modality ' GLM estimates, ' num2str(no_conditions) ' conditions, N = ' num2str(length(sList))]);


%% Other stuff

% figure
% imagesc(glm_bf1)
% set(gca,'YTick',1:19)
% colorbar
% title(modality ' - Individual GLM estimates, FC1, BF1')
% ylabel('Subjects')
% set(gca,'xTickLabel', {'CS(0)','CS(1/3)US+','CS(1/3)US-','CS(2/3)US+','CS(2/3)US-','CS(1)'}) 

% figure
% imagesc(glm_bf1([1:6 8:13 15:19],:))
% set(gca,'YTick',1:19)
% colorbar
% title(modality ' - Individual GLM estimates, FC1, BF1')
% ylabel('Subjects')
% set(gca,'xTickLabel', {'CS(0)','CS(1/3)US+','CS(1/3)US-','CS(2/3)US+','CS(2/3)US-','CS(1)'}) 

end