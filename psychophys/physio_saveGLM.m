function physio_saveGLM(opts)

sList = opts.psr.sList.GLM;

no_conditions = opts.psr.estimated_conditions;
cond_no_name = [num2str(no_conditions) 'conds'];
glmpath = fullfile(opts.psr.glmPath.cond,opts.psr.glm_bfname{opts.psr.estimated_response},cond_no_name);

summaryfile = fullfile(glmpath,'GLM_cond_summary'); 
summarytablefile = fullfile(glmpath,'GLM_cond_summarytable.csv');
summarytablefile2 = fullfile(glmpath,'GLM_cond_summarytable_JASP.csv');

% condnames = {'' 'CS(0)','CS(1/3)','CS(2/3)','CS(1)' ''};

%% Retrieve GLM stats
for s = 1:numel(sList)
    
    s_id = num2str(sList(s));

    filename = [opts.psr.glm_filename.cond s_id '.mat'];
    pupilfile = fullfile(glmpath,filename);
    
    if exist(pupilfile,'file')
        
        glmdata = load(pupilfile);
        
        for cond = 1:no_conditions
            subs(s) = sList(s);
            conditions(s,cond) = cond;
            names(s,cond) = glmdata.glm.names(cond);
            data_out(s,cond) = glmdata.glm.stats(cond);
        end
        
    end
end

% save data in format suitable for manipulating and plotting in MATLAB
data.subs = subs;
data.names = names;
data.glm = data_out;

save(summaryfile,'data')

% take the mean over trials of each type
% newsize = size(amplitude);
% newsize(2) = newsize(2)/4;
% StartleAmplitude_CSps = mean(reshape(amplitude(stimuli_CS == 1 & stimuli_complex == 0),newsize),2);
% StartleAmplitude_CSms = mean(reshape(amplitude(stimuli_CS == 0 & stimuli_complex == 0),newsize),2);
% StartleAmplitude_CSpc = mean(reshape(amplitude(stimuli_CS == 1 & stimuli_complex == 1),newsize),2);
% StartleAmplitude_CSmc = mean(reshape(amplitude(stimuli_CS == 0 & stimuli_complex == 1),newsize),2);

% save data in format suitable for importing into R
% long format of data
Subjects = repmat(subs,[1,no_conditions]);
Subjects = Subjects(:);
GLMestimate = data_out(:);
Condition = repmat(1:no_conditions,[length(subs) 1]);
Condition = Condition(:);

if no_conditions == 4
    CSprobability(Condition==1) = 0;
    CSprobability(Condition==2) = 1/3;
    CSprobability(Condition==3) = 2/3;
    CSprobability(Condition==4) = 1;
    CSprobability = CSprobability';
elseif no_conditions == 6
    CSprobability(Condition==1) = 0;
    CSprobability(Condition==2 | Condition==3) = 1/3;
    CSprobability(Condition==4 | Condition==5) = 2/3;
    CSprobability(Condition==6) = 1;
    CSprobability = CSprobability';
end

datatable = table(Subjects,Condition,CSprobability,GLMestimate);

writetable(datatable,summarytablefile);

% save data for JASP
% wide format
Subjects = subs';

GLMestimate_Cond1 = data_out(:,1);
GLMestimate_Cond2 = data_out(:,2);
GLMestimate_Cond3 = data_out(:,3);
GLMestimate_Cond4 = data_out(:,4);
datatable2 = table(Subjects,GLMestimate_Cond1,GLMestimate_Cond2,GLMestimate_Cond3,GLMestimate_Cond4);

if no_conditions == 6
    GLMestimate_Cond5 = data_out(:,5);
    GLMestimate_Cond6 = data_out(:,6);
    datatable2 = table(Subjects,GLMestimate_Cond1,GLMestimate_Cond2,GLMestimate_Cond3,GLMestimate_Cond4,GLMestimate_Cond5,GLMestimate_Cond6);
end

writetable(datatable2,summarytablefile2);

end