function [onsets, onsets_all, conditions] = physio_get_onsets(splitfn,eventfile,opts)
%Get trial onsets

% Load data
[~,~,data] = pspm_load_data(splitfn,'events');
markers = data{1}.data;

% Session numbers
session = str2num(splitfn(end-4));
% Find markers for the different conditions

bhvdata = load(eventfile);

if opts.dataset == 2 && session < 6 % if fMRI data and Acquisition 1 or Maintenance phase
    CS = bhvdata.cond(session).CS;
    US = bhvdata.cond(session).US;
    markers_cs1 = markers(bhvdata.cond(session).CS == 1);
    markers_cs2 = markers(bhvdata.cond(session).CS == 2);
    markers_cs3 = markers(bhvdata.cond(session).CS == 3);
    markers_cs4 = markers(bhvdata.cond(session).CS == 4);
elseif opts.dataset == 2 && session == 6 % fMRI Acquisition 2
    CS = bhvdata.cond(session).CS;
    US = bhvdata.cond(session).US;
    markers_cs1 = markers(bhvdata.cond(session).CS == 6);
    markers_cs2 = markers(bhvdata.cond(session).CS == 7);
    markers_cs3 = markers(bhvdata.cond(session).CS == 8);
    markers_cs4 = markers(bhvdata.cond(session).CS == 5);
    CS(CS==6) = 1;
    CS(CS==7) = 2;
    CS(CS==8) = 3;
    CS(CS==5) = 4;
elseif opts.dataset == 1 % pilot study
    CS = bhvdata.data(opts.sessind{session},4);
    US = bhvdata.data(opts.sessind{session},5);
    markers_cs1 = markers(bhvdata.data(opts.sessind{session},4) == 1); % CS(0)
    markers_cs2 = markers(bhvdata.data(opts.sessind{session},4) == 2); % CS(1/3)
    markers_cs3 = markers(bhvdata.data(opts.sessind{session},4) == 3); % CS(2/3)
    markers_cs4 = markers(bhvdata.data(opts.sessind{session},4) == 4); % CS(1)
end

onsets = {markers_cs1; markers_cs2; markers_cs3; markers_cs4};
onsets_all = markers;
conditions{1} = CS;
conditions{2} = US;

end
%% Old stuff

%markers_cs1 = []; markers_cs2 = []; markers_cs3 = []; markers_cs4 = [];
%                 for block = 1:length(bhvdata.cond)
%
%                     if block == length(bhvdata.cond)
%                         markers_cs1 = [markers_cs1; markers(bhvdata.cond(block).CS == 6)];
%                         markers_cs2 = [markers_cs2; markers(bhvdata.cond(block).CS == 7)];
%                         markers_cs3 = [markers_cs3; markers(bhvdata.cond(block).CS == 8)];
%                         markers_cs4 = [markers_cs4; markers(bhvdata.cond(block).CS == 5)];
%                     else
%                         markers_cs1 = [markers_cs1; markers(bhvdata.cond(block).CS == 1)];
%                         markers_cs2 = [markers_cs2; markers(bhvdata.cond(block).CS == 2)];
%                         markers_cs3 = [markers_cs3; markers(bhvdata.cond(block).CS == 3)];
%                         markers_cs4 = [markers_cs4; markers(bhvdata.cond(block).CS == 4)];
%                     end
%
%                 end