function physio_trialavg(opts,modality)

% Modality options
% Filter settings from default pspm_init settings
% Baseline windows based on the articles Castegnetti 2016, Korn 2017 etc

if strcmp(modality,'PSR')
    
    sList = opts.psr.sList.included;
    dataPath = opts.psr.dataPath;
    datachan = opts.psr.pupil_chan_valfix;
    markerchan = opts.psr.marker_chan;
    
    filt.lpfreq = 50;
    filt.lporder = 1;
    filt.hpfreq = NaN;
    filt.hporder = NaN;
    filt.direction = 'bi';
    filt.down = 100;
    
    timewindow = 11; % Time window for collecting responses in seconds
    %bswindow = 0.002; % Baseline window in seconds (500 Hz SR)
    % Pupil baseline window is 1st datapoint after CS onset
    
elseif strcmp(modality,'SCR')
    
    sList = opts.scr.sList.included;
    dataPath = opts.scr.dataPath;
    datachan = opts.scr.scr_chan;
    markerchan = opts.scr.marker_chan;
    
    filt.lpfreq = 5;
    filt.lporder = 1;
    filt.hpfreq = 0.0159; % 0.05?
    filt.hporder = 1;
    filt.direction = 'bi';
    filt.down = 10;
    
    timewindow = 11; % Time window for collecting responses in seconds
    bswindow = 0.5; % Baseline window in seconds
    
elseif strcmp(modality,'RAR')
    
    sList = opts.rar.sList.included;
    dataPath = opts.rar.dataPath;
    datachan = opts.rar.ra_chan;
    markerchan = opts.rar.marker_chan;
    
    filt.lpfreq = 2;
    filt.lporder = 6;
    filt.hpfreq = 0.01;
    filt.hporder = 6;
    filt.direction = 'bi';
    filt.down = 10;
    
    timewindow = 11; % Time window for collecting responses in seconds
    bswindow = 4; % Baseline window in seconds
    
elseif strcmp(modality,'HPR')
    
    sList = opts.hpr.sList.included;
    dataPath = opts.hpr.dataPath;
    datachan = opts.hpr.hp_chan;
    markerchan = opts.hpr.marker_chan;
    
    filt.lpfreq = 0.5;
    filt.lporder = 4;
    filt.hpfreq = 0.015;
    filt.hporder = 4;
    filt.direction = 'bi';
    filt.down = 10;
    
    timewindow = 11; % Time window for collecting responses in seconds
    bswindow = 5; % Baseline window in seconds
    
else
    fprintf('Modality not supported! Enter either PSR (CS deriv. only), RAR or HPR.');
end

for s = 1:length(sList)
    
    s_id = num2str(sList(s));
    
    filename = ['PCF' s_id];
    fname   = fullfile(dataPath, ['tpspm_' filename '_' modality '.mat']);
    bhvfile = fullfile(opts.bhvPath,[opts.bhvname s_id '.mat']);
    
    if exist(fname,'file') && exist(bhvfile,'file')
        
        [~, ~, data] = pspm_load_data(fname);
        
        origsr = data{datachan}.header.sr; % Second SCR channel
        y  = data{datachan}.data;
        
        % Interpolate NaNs away
        [~,y] = pspm_interpolate(y,[]);
        
        % Bandpass & downsampling (for plotting purposes only!)
        filt.sr = origsr;
        
        [~,y_filtered,sr] = pspm_prepdata(y,filt);
        respdata = y_filtered;
        
        % Collect responses
        %------------------------------------------------------------------
        markers = data{markerchan}.data;
            
            clear resp respmaxampl
            
            markersfound(s,1) = sList(s);
            markersfound(s,2) = numel(markers);
            
            resp = NaN(timewindow*sr+1,numel(markers),length(sList));
            
            for trial = 1:numel(markers)
                t0 = round(markers(trial)*sr);
                if t0 == 0; t0 = t0+1; end
                t1 = t0+(timewindow*sr);
                if strcmp(modality,'PSR'); bs = respdata(t0);
                else; bs = mean(respdata(t0-(bswindow*sr):t0)); end
                resp(:,trial,s) = respdata(t0:t1)-bs;
                respmaxampl(trial,s) = max(respdata(t0:t1)-bs);
            end
            
            bhvdata = load(bhvfile);
            
            for bl = opts.runs
                
                if bl < 6
                    resp_out.cond(1).block(bl).trialdata(:,:,s) = resp(:,bhvdata.cond(bl).CS == 1 & bhvdata.cond(bl).US == 0,s);
                    resp_out.cond(2).block(bl).trialdata(:,:,s) = resp(:,bhvdata.cond(bl).CS == 2 & bhvdata.cond(bl).US == 1,s);
                    resp_out.cond(3).block(bl).trialdata(:,:,s) = resp(:,bhvdata.cond(bl).CS == 2 & bhvdata.cond(bl).US == 0,s);
                    resp_out.cond(4).block(bl).trialdata(:,:,s) = resp(:,bhvdata.cond(bl).CS == 3 & bhvdata.cond(bl).US == 1,s);
                    resp_out.cond(5).block(bl).trialdata(:,:,s) = resp(:,bhvdata.cond(bl).CS == 3 & bhvdata.cond(bl).US == 0,s);
                    resp_out.cond(6).block(bl).trialdata(:,:,s) = resp(:,bhvdata.cond(bl).CS == 4 & bhvdata.cond(bl).US == 1,s);
                else
                    resp_out.cond(1).block(bl).trialdata(:,:,s) = resp(:,bhvdata.cond(bl).CS == 6 & bhvdata.cond(bl).US == 0,s);
                    resp_out.cond(2).block(bl).trialdata(:,:,s) = resp(:,bhvdata.cond(bl).CS == 7 & bhvdata.cond(bl).US == 1,s);
                    resp_out.cond(3).block(bl).trialdata(:,:,s) = resp(:,bhvdata.cond(bl).CS == 7 & bhvdata.cond(bl).US == 0,s);
                    resp_out.cond(4).block(bl).trialdata(:,:,s) = resp(:,bhvdata.cond(bl).CS == 8 & bhvdata.cond(bl).US == 1,s);
                    resp_out.cond(5).block(bl).trialdata(:,:,s) = resp(:,bhvdata.cond(bl).CS == 8 & bhvdata.cond(bl).US == 0,s);
                    resp_out.cond(6).block(bl).trialdata(:,:,s) = resp(:,bhvdata.cond(bl).CS == 5 & bhvdata.cond(bl).US == 1,s);
                end
                
            end
        
    end
    
end

% Save data in Matlab variable format
outdirname = fullfile(dataPath, 'MatlabData');
if ~exist(outdirname,'dir'); mkdir(outdirname); end
save(fullfile(outdirname, [modality '_trialavg_allcond.mat']),'resp_out');

end