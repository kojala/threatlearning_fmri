function physio_saveGLM_trial(opts)

sList = opts.psr.sList.GLM;

no_trials = opts.trials_incl;

glmpath = fullfile(opts.psr.glmPath.trial,opts.psr.glm_bfname{opts.psr.estimated_response});

summaryfile = fullfile(glmpath,'GLM_trial_summary'); 
summarytablefile = fullfile(glmpath,'GLM_trial_summarytable.csv');

%% Retrieve GLM stats
for s = 1:numel(sList)
    
    s_id = num2str(sList(s));

    filename = [opts.psr.glm_filename.trial s_id '.mat'];
    pupilfile = fullfile(glmpath,filename);
    bhvfile = fullfile(opts.bhvPath,[opts.bhvname s_id]);
    bhvdata = load(bhvfile);
    CS = bhvdata.data(1:no_trials,4);
    
    if exist(pupilfile,'file')
        
        glmdata = load(pupilfile);
        
        for trial = 1:no_trials
            subs(s) = sList(s);
            trials(s,trial) = trial;
            cs_type(s,trial) = CS(trial);
            names(s,trial) = glmdata.glm.names(trial);
            data_out(s,trial) = glmdata.glm.stats(trial);
        end
        
    end
end

% save data in format suitable for manipulating and plotting in MATLAB
data.subs = subs;
data.trials = trials;
data.cs_type = cs_type;
data.names = names;
data.glm = data_out;

save(summaryfile,'data')

% save data in format suitable for importing into R
% long format of data
Subjects = repmat(subs,[no_trials 1]);
Subjects = Subjects(:);
data_out = data_out';
GLMestimate = data_out(:);
Trial = repmat(1:no_trials,[length(subs) 1])';
Trial = Trial(:);

cs_type = cs_type';
cs_type = cs_type(:);

% find CS info
CSprobability(cs_type==1) = 0;
CSprobability(cs_type==2) = 1/3;
CSprobability(cs_type==3) = 2/3;
CSprobability(cs_type==4) = 1;
CSprobability = CSprobability';
    
datatable = table(Subjects,Trial,CSprobability,GLMestimate);

writetable(datatable,summarytablefile);

end