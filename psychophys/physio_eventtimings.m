function eventfiles = physio_eventtimings(opts,s_id,sessionfiles)

fname_bhv = [opts.bhvname s_id '.mat'];

% File with event types
bhvfile = fullfile(opts.bhvPath, fname_bhv);
trials = opts.sessind;

% Load behavioural file with event IDs
bhvdata = load(bhvfile);

% Create an event file for each session
for session = 1:size(sessionfiles,1)
    
    % File output directory
    outdirname = fullfile(opts.eventPath);
    if ~exist(outdirname,'dir'); mkdir(outdirname); end
    
    clear allconditions conditions CS2pUSp CS3pUSp CS4pUSp CS2pUSm CS3pUSm CSm CS_onset
    
    if opts.dataset == 2
        if session < 6
            CS2pUSp = bhvdata.cond(session).CS == 2 & bhvdata.cond(session).US == 1;
            CS3pUSp = bhvdata.cond(session).CS == 3 & bhvdata.cond(session).US == 1;
            CS4pUSp = bhvdata.cond(session).CS == 4 & bhvdata.cond(session).US == 1;
            CS2pUSm = bhvdata.cond(session).CS == 2 & bhvdata.cond(session).US == 0;
            CS3pUSm = bhvdata.cond(session).CS == 3 & bhvdata.cond(session).US == 0;
            CSm = bhvdata.cond(session).CS == 1;
        else
            CS2pUSp = bhvdata.cond(session).CS == 7 & bhvdata.cond(session).US == 1;
            CS3pUSp = bhvdata.cond(session).CS == 8 & bhvdata.cond(session).US == 1;
            CS4pUSp = bhvdata.cond(session).CS == 5 & bhvdata.cond(session).US == 1;
            CS2pUSm = bhvdata.cond(session).CS == 7 & bhvdata.cond(session).US == 0;
            CS3pUSm = bhvdata.cond(session).CS == 8 & bhvdata.cond(session).US == 0;
            CSm = bhvdata.cond(session).CS == 6;
        end
    else
        CS2pUSp = bhvdata.data(trials{session},4) == 2 & bhvdata.data(trials{session},5) == 1;
        CS3pUSp = bhvdata.data(trials{session},4) == 3 & bhvdata.data(trials{session},5) == 1;
        CS4pUSp = bhvdata.data(trials{session},4) == 4 & bhvdata.data(trials{session},5) == 1;
        CS2pUSm = bhvdata.data(trials{session},4) == 2 & bhvdata.data(trials{session},5) == 0;
        CS3pUSm = bhvdata.data(trials{session},4) == 3 & bhvdata.data(trials{session},5) == 0;
        CSm = bhvdata.data(trials{session},4) == 1;
    end
    
    % Create an event type file
    conditions{1} = CSm;
    conditions{2} = CS2pUSp;
    conditions{3} = CS2pUSm;
    conditions{4} = CS3pUSp;
    conditions{5} = CS3pUSm;
    conditions{6} = CS4pUSp;
    
    allconditions(CSm)     = 1;
    allconditions(CS2pUSp) = 2;
    allconditions(CS2pUSm) = 3;
    allconditions(CS3pUSp) = 4;
    allconditions(CS3pUSm) = 5;
    allconditions(CS4pUSp) = 6;
    
    eventstype = fullfile(opts.eventPath,['event_types_' s_id '_sn0' num2str(session) '.mat']);
    save(eventstype,'conditions','allconditions')
    
    % Create an event timing file from session data markers
    [~,~,data] = pspm_load_data(sessionfiles(session,:),'events');
    CS_markers = data{1}.data;
    CS_onset = CS_markers;
    US_onset = CS_onset + opts.soa;
    events{1} = [CS_onset US_onset];
    events{2} = US_onset;
    eventstime  = fullfile(opts.eventPath,['event_timings_' s_id '_sn0' num2str(session) '.mat']);
    save(eventstime,'events')
    
    eventfiles{session} = eventstime;
    
end

end

% Sessions for this study
%condnames = {'CS(0)','CS(1/3)US+','CS(1/3)US-','CS(2/3)US+','CS(2/3)US-','CS(1)'}; % Add all conditions
% % Load ready event timings file
% bhvdata = load(eventfile);
%
% % Retrieve condition types
% if opts.dataset = 2;
%     type1 = []; type2 = []; type3 = []; type4 = []; type5 = []; type6 = [];
%     if ses == 1 % Acq & Maint
%         for cond = 1:length(bhvdata.cond)
%             type1 = [type1 bhvdata.cond(cond).CS == 1]; % CS(0)
%             type2 = [type2 bhvdata.cond(cond).CS == 2 & bhvdata.cond(cond).US == 1]; % CS(1/3)US+
%             type3 = [type3 bhvdata.cond(cond).CS == 2 & bhvdata.cond(cond).US == 0]; % CS(1/3)US-
%             type4 = [type4 bhvdata.cond(cond).CS == 3 & bhvdata.cond(cond).US == 1]; % CS(2/3)US+
%             type5 = [type5 bhvdata.cond(cond).CS == 3 & bhvdata.cond(cond).US == 0]; % CS(2/3)US-
%             type6 = [type6 bhvdata.cond(cond).CS == 4]; % CS(1)
%         end
%     else
%         for cond = 6 % Acq2
%             type1 = [type1 bhvdata.cond(cond).CS == 1]; % CS(0)
%             type2 = [type2 bhvdata.cond(cond).CS == 2 & bhvdata.cond(cond).US == 1]; % CS(1/3)US+
%             type3 = [type3 bhvdata.cond(cond).CS == 2 & bhvdata.cond(cond).US == 0]; % CS(1/3)US-
%             type4 = [type4 bhvdata.cond(cond).CS == 3 & bhvdata.cond(cond).US == 1]; % CS(2/3)US+
%             type5 = [type5 bhvdata.cond(cond).CS == 3 & bhvdata.cond(cond).US == 0]; % CS(2/3)US-
%             type6 = [type6 bhvdata.cond(cond).CS == 4]; % CS(1)
%         end
%     end
% else
%     type1 = bhvdata.data(:,4) == 1; % CS(0)
%     type2 = bhvdata.data(:,4) == 2 & bhvdata.data(:,5) == 1; % CS(1/3)US+
%     type3 = bhvdata.data(:,4) == 2 & bhvdata.data(:,5) == 0; % CS(1/3)US-
%     type4 = bhvdata.data(:,4) == 3 & bhvdata.data(:,5) == 1; % CS(2/3)US+
%     type5 = bhvdata.data(:,4) == 3 & bhvdata.data(:,5) == 0; % CS(2/3)US-
%     type6 = bhvdata.data(:,4) == 4; % CS(1)
% end
%
% if exist(eyefile,'file') % Only if pupil data file exists
%
%     [~,~,data] = pspm_load_data(eyefile);
%     markers = data{opts.psr.marker_chan}.data;
%
%     %% Retrieve timing for each trial type and add to onsets
%     if opts.psr.estimated_response == 3
%         onset_type1 = markers(type1)+6;
%         onset_type2 = markers(type2)+6;
%         onset_type3 = markers(type3)+6;
%         onset_type4 = markers(type4)+6;
%         onset_type5 = markers(type5)+6;
%         onset_type6 = markers(type6)+6;
%     else
%         onset_type1 = markers(type1);
%         onset_type2 = markers(type2);
%         onset_type3 = markers(type3);
%         onset_type4 = markers(type4);
%         onset_type5 = markers(type5);
%         onset_type6 = markers(type6);
%     end
%
%     for c = 1:length(condnames)
%         glm_ps_fc.session.data_design.condition(c).name = condnames{c};
%         glm_ps_fc.session.data_design.condition(c).durations = 0;
%     end
%
%     glm_ps_fc.session.data_design.condition(1).onsets = onset_type1;
%     glm_ps_fc.session.data_design.condition(2).onsets = onset_type2;
%     glm_ps_fc.session.data_design.condition(3).onsets = onset_type3;
%     glm_ps_fc.session.data_design.condition(4).onsets = onset_type4;
%     glm_ps_fc.session.data_design.condition(5).onsets = onset_type5;
%     glm_ps_fc.session.data_design.condition(6).onsets = onset_type6;
%
% end