clear all

ExpPath = '..';
sList = [21:27 29:40];
% sList = 172;
% sList = [160 161 164 166 167 169 171:177 179:181];

% sessions = 1:2;
% sessnames = {'','B'}; % acquisition+maintenance, 2nd acquisition

for ses = sessions
    
    for s = 1:numel(sList)
        
        s_id = num2str(sList(s));
        
%         filename = ['PCF' s_id sessnames{ses}];
        filename = ['PCF' s_id];
%         filename = ['valfix_tpspm_PCF' s_id '.mat'];
        
        pupilfile = fullfile(ExpPath, 'PSR', 'Imported_mm', ['valfix_tpspm_' filename '.mat']);
         
%         pupilfile = fullfile(ExpPath,filename);
        
        if exist(pupilfile,'file')
            [~, ~, pupildata] = pspm_load_data(pupilfile);
            
            figure
            plot(pupildata{1}.data,'k');
            hold on
            plot(pupildata{4}.data,'b');
            plot(pupildata{12}.data,'r');
%             plot(pupildata{10}.data,'r');
%             title(['Subject ' num2str(sList(s)) ', Session ' num2str(ses)])
            title(['Subject ' num2str(sList(s))])
            
        end
    end
end