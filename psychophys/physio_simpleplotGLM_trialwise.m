function physio_simpleplotGLM_trialwise(opts)

sList = opts.psr.sList.GLM; 

%condnames = {'CS(0)','CS(1/3)','CS(2/3)','CS(1)'};
condnames = {'CS(0)US-','CS(1/3)US-','CS(2/3)US-'};

%% Retrieve GLM stats

glm_est_cs1 = nan(length(sList),1);
glm_est_cs2 = nan(length(sList),1);
glm_est_cs3 = nan(length(sList),1);
glm_est_cs4 = nan(length(sList),1);

for s = 1:numel(sList)
    
    s_id = num2str(sList(s));
    
    glmpath = fullfile(opts.psr.glmPath.trial,opts.psr.glm_bfname{opts.psr.estimated_response});
    filename = [opts.psr.glm_filename.trial s_id '.mat'];
    glmfile = fullfile(glmpath,filename);
    
    if exist(glmfile,'file')
        
        glmdata = load(glmfile);
        
        trials = 1:(length(glmdata.glm.stats)-glmdata.glm.interceptno);
        
        glm_est = glmdata.glm.stats(trials);
        
        fname_bhv = [opts.bhvname s_id '.mat'];
        eventfile = fullfile(opts.bhvPath, fname_bhv);
        bhvdata = load(eventfile);
        
        cs1 = bhvdata.data(:,4) == 1; % CS(0)
        cs2 = bhvdata.data(:,4) == 2 & bhvdata.data(:,5) == 0; % CS(1/3)
        cs3 = bhvdata.data(:,4) == 3 & bhvdata.data(:,5) == 0; % CS(2/3)
        %cs4 = bhvdata.data(:,4) == 4; % CS(1)
        
        if opts.psr.summarystat == 1
            glm_est_cs1(s) = mean(glm_est(cs1));
            glm_est_cs2(s) = mean(glm_est(cs2));
            glm_est_cs3(s) = mean(glm_est(cs3));
            %glm_est_cs4(s) = mean(glm_est(cs4));
        else
            glm_est_cs1(s) = median(glm_est(cs1));
            glm_est_cs2(s) = median(glm_est(cs2));
            glm_est_cs3(s) = median(glm_est(cs3));
            %glm_est_cs4(s) = median(glm_est(cs4_ind));
        end
        
%         figure
%         plot(glm_est(cs1_ind),'ob','MarkerFaceColor','b')
%         hold on
%         plot(glm_est(cs2_ind),'og','MarkerFaceColor','g')
%         plot(glm_est(cs3_ind),'om','MarkerFaceColor','m')
%         plot(glm_est(cs4_ind),'or','MarkerFaceColor','r')
%         title(['Subject ' s_id ])
%         legend({'CS(0)' 'CS(1/3)' 'CS(2/3)' 'CS(1)'})
        
    end
    
end


%% Plot

figure('Position',[300,300,800,600]);

hold on
if opts.psr.summarystat == 1
    bar([mean(glm_est_cs1); mean(glm_est_cs2); mean(glm_est_cs3); mean(glm_est_cs4)])
    title(['Pupil size mean of GLM trial-by-trial estimates, N = ' num2str(length(sList))]);
else
    bar([median(glm_est_cs1); median(glm_est_cs2); median(glm_est_cs3); median(glm_est_cs4)])
    title(['Pupil size median of GLM trial-by-trial estimates, N = ' num2str(length(sList))]);
end
set(gca,'xTick', 1:length(condnames))
set(gca,'xTickLabel', condnames)

colors = [102 255 255; 153 51 255; 255 0 127]/255;
figure
hold on
line([0 19],[mean(glm_est_cs1) mean(glm_est_cs1)],'Color',colors(1,:),'LineWidth',2)
line([0 19],[mean(glm_est_cs2) mean(glm_est_cs2)],'Color',colors(2,:),'LineWidth',2)
line([0 19],[mean(glm_est_cs3) mean(glm_est_cs3)],'Color',colors(3,:),'LineWidth',2)
cond1 = plot(glm_est_cs1,'o','MarkerFaceColor',colors(1,:),'MarkerEdgeColor','k','MarkerSize',8);
cond2 = plot(glm_est_cs2,'o','MarkerFaceColor',colors(2,:),'MarkerEdgeColor','k','MarkerSize',8);
cond3 = plot(glm_est_cs3,'o','MarkerFaceColor',colors(3,:),'MarkerEdgeColor','k','MarkerSize',8);
xlim([0 19])
set(gca,'XTick',1:18)
set(gca,'XTickLabel',1:18)
xlabel('Subjects')
ylabel('Coefficient (a.u.)')
legend([cond1 cond2 cond3],condnames,'Location','best')
title('Pupil size individual GLM estimates for all conditions');

end