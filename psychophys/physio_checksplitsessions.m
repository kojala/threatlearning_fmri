
sList = [159:162 164:167 169:177 179:182];

for s = 165%sList(2:end)
    
    load(['D:\fMRI_Karita\SCR\tpspm_S' num2str(s) '.mat'])
    if s == 160
        rar = data{6,1}.data;
    else
        rar = data{7,1}.data;
    end
    rarmarkers = data{5,1}.data;
    
    blockind = [1 25 25+44 25+44+44 25+44+44+44 25+44+44+44+44];
    
    figure;plot(rar)
    title(['Subject ' num2str(s) ', Full data'])
    hold on
    
    lastmarker = 0;
    
    for i = 1:6
        clear rarses
        load(['D:\fMRI_Karita\SCR\tpspm_S' num2str(s) '_sn0' num2str(i) '.mat'])
        
        if s == 160
            sesdata = data{6,1}.data;
            sr = data{6,1}.header.sr;
        else
            sesdata = data{7,1}.data;
            sr = data{7,1}.header.sr;
        end
            
        if i == 1
            rarses = [NaN(5.9*sr,1); sesdata];
        else
            rarses(1:round(rarmarkers(blockind(i))*sr-1),1) = NaN;
            rarses = [rarses; sesdata];
        end
        plot(rarses,'r')
        
    end
    
end