function physio_reconresp(opts,modality)

% Modality options
if strcmp(modality,'PSR') && opts.psr.estimated_response == 1
    sList = opts.psr.sList.GLM;
    no_conditions = opts.psr.estimated_conditions;
    cond_no_name = [num2str(no_conditions) 'conds'];
    glmpath = fullfile(opts.psr.glmPath.cond,opts.psr.glm_bfname{opts.psr.estimated_response},cond_no_name);
    glm_filename = opts.psr.glm_filename.cond;
    %bfname = opts.psr.glm_bfname{opts.psr.estimated_response};
    %condition_indices_cs = 1:no_conditions; % CR only model (no US resp)
    unit = 'Pupil size (cm)';
elseif strcmp(modality,'RAR')
    sList = opts.rar.sList.GLM;
    no_conditions = opts.rar.estimated_conditions;
    glmpath = fullfile(opts.modelPath,'RAR','GLM');
    glm_filename = 'glm_';
    %bfname = '';
    %condition_indices_cs = 1:2:no_conditions*2; % CR
    %condition_indices_us = 2:2:no_conditions*2; % UR
    unit = 'Respiratory amplitude (a.u.)';
elseif strcmp(modality,'HPR')
    sList = opts.hpr.sList.GLM;
    no_conditions = opts.hpr.estimated_conditions;
    glmpath = fullfile(opts.modelPath,'HPR','GLM');
    glm_filename = 'glm_';
    %bfname = '';
    %condition_indices_cs = 1:2:no_conditions*2; % CR
    %condition_indices_us = 2:2:no_conditions*2; % UR
    unit = 'Heart period (ms)';
else
    fprintf('Modality not supported! Enter either PSR (CS deriv. only), RAR or HPR.');
end

if no_conditions == 4
    condnames = {'CS(0)','CS(1/3)','CS(2/3)','CS(1)'};
elseif no_conditions == 6
    condnames = {'CS(0)','CS(1/3)US+','CS(1/3)US-','CS(2/3)US+','CS(2/3)US-','CS(1)'};
end

%% Retrieve GLM stats
recresp = nan(length(sList),no_conditions);

for s = 1:numel(sList)
    
    s_id = num2str(sList(s));
    
    filename = [glm_filename s_id '.mat'];
    datafile = fullfile(glmpath,filename);
    
    if exist(datafile,'file')
        
        [~, glm] = pspm_glm_recon(datafile);
        
        recresp(s,:) = glm.recon;
        
    end
    
end

%% Calculate within-subject error bars

subavg = mean(recresp,2); % mean over conditions for each sub
grandavg = mean(subavg); % mean over subjects and conditions

newvalues = nan(size(recresp));

% normalization of subject values
for cond = 1:size(recresp,2)
    meanremoved = recresp(:,cond)-subavg; % remove mean of conditions from each condition value for each sub
    newvalues(:,cond) = meanremoved+repmat(grandavg,[length(sList) 1 1]); % add grand average over subjects to the values where individual sub average was removed
    plotdata_mean(:,cond) = mean(newvalues(:,cond));
    plotdata_median(:,cond) = median(recresp(:,cond));
end

newvar = (cond/(cond-1))*var(newvalues);
errorbars = squeeze(1.96*(sqrt(newvar)./sqrt(length(sList)))); % calculate error bars according to Cousineau (2005) with Morey (2005) fix

%% Plot

figure('Position',[300,300,800,600]);

subplot(2,1,1)
hold on
bar(plotdata_mean)
errorbar(plotdata_mean,errorbars,'k','LineStyle','none')
title('Mean amplitude +- SEM')
set(gca,'xTick',1:no_conditions)
set(gca,'xTickLabel', condnames)
%ylim([-0.25 0.25])
ylabel(unit)

subplot(2,1,2)
hold on
bar(plotdata_median)
title('Median amplitude')
set(gca,'xTick',1:no_conditions)
set(gca,'xTickLabel', condnames)
%ylim([-0.25 0.25])
ylabel(unit)

suptitle([modality ' reconstructed CS response, N = ' num2str(length(sList))]);


end