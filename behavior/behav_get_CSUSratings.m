function behav_get_CSUSratings(opts)
%Retrieve subjective ratings of US probability

sList = opts.behav.sList;

CSsubj = NaN(length(sList),5);
CSsubj2 = NaN(length(sList),5);

%% Retrieve subject rating data, sort and save in usable format
for s = 1:length(sList)
    
    if opts.dataset == 1
        behavPath = opts.bhvPath;
    else
        behavPath = fullfile(opts.behav.origPath,['S' num2str(sList(s))]);
    end
    
    behavFile = [opts.bhvname num2str(sList(s)) '.mat']; % Acquisition and Maintenance
    behavFile2 = [opts.bhvname num2str(sList(s)) '_Part2.mat']; % Acquisition 2
    behavFile = fullfile(behavPath,behavFile);
    behavFile2 = fullfile(behavPath,behavFile2);
    
    % Acquisition and Maintenance phases
    bhvdata = load(behavFile);
    
    % Objective US probability and subjective ratings of US probability
    ratings = [bhvdata.postdata(:,1) bhvdata.postdata(:,3)];
    
    % Sort subjective ratings according to objective p(shock)
    ratings = sortrows(ratings);
    ratings(ratings==1,1) = 0;
    ratings(ratings==2,1) = 1/3*100;
    ratings(ratings==3,1) = 2/3*100;
    ratings(ratings==4,1) = 1*100;
    
    if sList(s) == 176 % This subject reported pressing a wrong button for 100% true shock stimulus, was supposed to be 70% subjective rating
        ratings(4,2) = 70.00;
    end
    
    CStrue(s,:) = ratings(:,1); %#ok<*AGROW> % True US probability
    CSsubj(s,1) = sList(s);
    CSsubj(s,2:end) = ratings(:,2); % Subjective US probability
    
    subs(s) = sList(s);
    
    % Acquisition 2 phase
    if exist(behavFile2,'file')
        bhvdata2 = load(behavFile2);
        
        ratings = [bhvdata2.postdata(:,1) bhvdata2.postdata(:,3)];
        
        ratings = sortrows(ratings);
        ratings(ratings==2,1) = 0;
        ratings(ratings==3,1) = 1/3*100;
        ratings(ratings==4,1) = 2/3*100;
        ratings(ratings==1,1) = 1*100;
        ratings = sortrows(ratings);
        
        CStrue2(s,:) = ratings(:,1); %#ok<AGROW> % True US probability
        CSsubj2(s,1) = sList(s);
        CSsubj2(s,2:end) = ratings(:,2); % Subjective US probability
    else
        CSsubj2 = [];
    end
    
end

save(fullfile(behavPath,'subjratings_pshock.mat'),'CSsubj','CSsubj2')

% Save as csv for R
no_conditions = 4; % Number of CS types
Subjects = repmat(subs,[1,no_conditions]);
Subjects = Subjects(:);
ExpectancyRating = CSsubj(:,2:end); % Subjective rating
ExpectancyRating = ExpectancyRating(:);
Condition = repmat(1:no_conditions,[length(subs) 1]); % CS type
Condition = Condition(:);

CSprobability = CStrue(:); % True US probability for each CS type

datatable = table(Subjects,Condition,CSprobability,ExpectancyRating);
summarytablefile = fullfile(behavPath,'ExpectancyRatings_summarytable_AcqMaint.csv');
writetable(datatable,summarytablefile);

if exist(behavFile2,'file')

    ExpectancyRating = CSsubj2(:,2:end);
    ExpectancyRating = ExpectancyRating(:);
    CSprobability = CStrue2(:);
    
    datatable = table(Subjects,Condition,CSprobability,ExpectancyRating);
    summarytablefile = fullfile(behavPath,'ExpectancyRatings_summarytable_Acq2.csv');
    writetable(datatable,summarytablefile);

end

end