function behav_plot_CSUSratings(opts)
%Plot subjective ratings of CS-US contingencies / US probability for each
%CS type against the objective US probability

%[~,idx] = intersect(opts.behav.sList,opts.psr.sList.GLM,'stable');
[~,idx] = intersect(opts.behav.sList,opts.behav.sList,'stable');

if opts.dataset == 2 % fMRI scanner dataset
    behavPath = opts.behav.origPath;
    filename = 'subjective_pshock_AcqMaint.mat'; % Acquisitition + Maintence ratings
    filename2 = 'subjective_pshock_Acq2.mat'; % Acquisition 2 ratings
    data = load(fullfile(behavPath,filename),'CSsubj');
    CSsubj = data.CSsubj(idx,:);
    if exist(filename2,'file'); data2 = load(fullfile(behavPath,filename2),'CSsubj2'); CSsubj2 = data.CSsubj2(idx,2:end); end %#ok<*NASGU>
else % Pilot dataset
    behavPath = opts.bhvPath;
    filename = 'subjratings_pshock.mat';
    data = load(fullfile(behavPath,filename),'CSsubj');
    CSsubj = data.CSsubj(idx,2:end);
end

markersize = 14;
%barcolor = [169 169 169]; grey
barcolor = [189,201,225; 103,169,207; 28,144,153; 10,128,99]; % greenish blues
scattercolor = [105 105 105];
linecolor = [105 105 105];%[169 169 169]; %[211 211 211]; % light grey

%% Plotting

% Bar graphs of the mean subjective US probability ratings for each CS type

% Acquisition and Maintenance
fig = figure('DefaultAxesFontSize', 12, 'DefaultAxesFontName', 'Arial', 'DefaultTextFontName', 'Arial');

figwidth = 8.5; % 1/2 pdf column = 6, 1 pdf column = 8.5 cm, 1.5 pdf column = 11.6, 2 pdf columns = 17.6
figheight = 8;

set(fig, 'Units', 'Centimeters',...
    'PaperUnits', 'Centimeters', ...
    'PaperPositionMode','auto')

set(fig,'Position', [0, 0, figwidth, figheight])
    
set(gca,'LineWidth',1) % Axes line width

x = [0 33 66 100]; % True US probability
y = [mean(CSsubj(:,1)) mean(CSsubj(:,2)) mean(CSsubj(:,3)) mean(CSsubj(:,4))]; % Subjective US probability rating

for cond = 1:length(x)
    bar(x(cond),y(cond),20,'FaceColor',barcolor(cond,:)./255,'LineWidth',1.5); %
    hold on
end

% Labels
xlabel('True US+ probability','FontSize',12,'FontWeight','normal')
ylabel('Subjective US+ probability','FontSize',12,'FontWeight','normal')
xlim([-20 120])
ylim([0 105])
set(gca,'XTick',[0 33 66 100],'FontSize',12)
xticklabel = {'0%' '33%' '66%' '100%'};
set(gca,'xticklabel',xticklabel)
set(gca,'YTick',0:10:100,'FontSize',12)
set(gca,'box','off')

% Individual values jittered
subjects = size(CSsubj,1);
CStrue = repmat(x,[subjects 1]);
%ax1 = gca;
% ax2 = axes('Position', get(ax1, 'Position'), 'XAxisLocation', 'top', ...
%     'YAxisLocation', 'right', 'Color', 'none', 'XColor', 'k', 'YColor', 'k');
%hold(ax2, 'all');
jitter_amount = 8; % Amount of jitter for markers
jittered_CStrue = CStrue + (rand(size(CStrue))-0.5)*(2*jitter_amount);
plot(jittered_CStrue',CSsubj','-','Color',linecolor./255);
hold on
scatter_plot = scatter(jittered_CStrue(:),CSsubj(:), markersize, 'filled', 'MarkerFaceColor', scattercolor./255, 'MarkerEdgeColor', 'k');%, 'jitter', 'on', 'jitterAmount', 7);
%hl2 = scatter(CStrue(:),CSsubj(:), 'filled', 'Parent', ax2);
%title('Conscious awareness of CS-US association')

% Within-subject standard error of the mean (Cousineau, 2005)
subavg = mean(CSsubj,2); % Mean over conditions for each sub for each roi
grandavg = mean(subavg); % Mean over subjects and conditions for each roi
newvalues = nan(size(CSsubj));

% Normalization of subject values
for cond = 1:4
    meanremoved = CSsubj(:,cond)-subavg; % Remove mean of conditions from each condition value for each sub for each roi
    newvalues(:,cond) = meanremoved+repmat(grandavg,[subjects 1]); % Add grand average over subjects to the values where individual sub average was removed
end

newvar = (cond/(cond-1))*var(newvalues); % Morey (2005) fix
tvalue = tinv(1-0.05, subjects-1);
errorsem = squeeze(tvalue*(sqrt(newvar)./sqrt(subjects)));

% Draw error bars
errorbar(x,y,errorsem,'k','LineStyle','none','LineWidth',1.75,'CapSize',0);

% Set paper size and save figure
PAPER = get(fig,'Position');
set(fig,'PaperSize',[PAPER(3), PAPER(4)]);
savefig(fig,fullfile(behavPath,'subj_pUS_ratings_AcqMaint'));
saveas(gcf,fullfile(behavPath,'subj_pUS_ratings_AcqMaint'),'png')
saveas(gcf,fullfile(behavPath,'subj_pUS_ratings_AcqMaint'),'svg')

% Acquisition 2
if exist(data2,'var')
    figure
    x = [0 33 66 100];
    y = [mean(CSsubj2(:,1)) mean(CSsubj2(:,2)) mean(CSsubj2(:,3)) mean(CSsubj2(:,4))];
    sqrtn = sqrt(numel(CSsubj(:,1)));
    %error = [std(CSsubj2(:,1)') std(CSsubj2(:,2)') std(CSsubj2(:,3)') std(CSsubj2(:,4)')];
    errorm = [std(CSsubj2(:,1)')/sqrtn std(CSsubj2(:,2)')/sqrtn std(CSsubj2(:,3)')/sqrtn std(CSsubj2(:,4)')/sqrtn];
    bar(x,y,0.6)
    hold on
    %errorbar(x,y,error,'xb')
    errorbar(x,y,errorm,'xr')
    xlabel('True p(shock)')
    xlim([-10 110])
    ylim([-10 110])
    ylabel('Average subjective p(shock)')
    title('Conscious awareness of CS-US association Part 2')
end

% % Scatter plots of individual ratings for subjective p(shock) vs. true
% % Acquisition and Maintenance
% figure
% scatter(CStrue(:,1),CSsubj(:,1),'k','filled')
% hold on
% scatter(CStrue(:,2),CSsubj(:,2),'g','filled')
% scatter(CStrue(:,3),CSsubj(:,3),'b','filled')
% scatter(CStrue(:,4),CSsubj(:,4),'r','filled')
% xlabel('True p(shock)')
% xlim([-10 110])
% ylim([-10 110])
% ylabel('Subjective p(shock)')
% title('Learning of CS-US association')
% legend('p(shock) = 0', 'p(shock) = 1/3', 'p(shock) = 2/3', 'p(shock) = 1','Location','northeastoutside')
% 
% % Acquisition 2
% figure
% scatter(CStrue2(:,1),CSsubj2(:,1),'k','filled')
% hold on
% scatter(CStrue2(:,2),CSsubj2(:,2),'g','filled')
% scatter(CStrue2(:,3),CSsubj2(:,3),'b','filled')
% scatter(CStrue2(:,4),CSsubj2(:,4),'r','filled')
% xlabel('True p(shock)')
% xlim([-10 110])
% ylim([-10 110])
% ylabel('Subjective p(shock)')
% title('Learning of CS-US association Part 2')
% legend('p(shock) = 0', 'p(shock) = 1/3', 'p(shock) = 2/3', 'p(shock) = 1','Location','northeastoutside')

% % Boxplot
% objp1 = cell(1,size(CStrue,1));
% objp1(:) = {'0'};
% objp2 = cell(1,size(CStrue,1));
% objp2(:) = {'1/3'};
% objp3 = cell(1,size(CStrue,1));
% objp3(:) = {'2/3'};
% objp4 = cell(1,size(CStrue,1));
% objp4(:) = {'1'};
% 
% objp = [objp1 objp2 objp3 objp4];

% addpath('D:\gramm')
% g = gramm('x',CStrue(:),'y',CSsubj(:));
% g.stat_boxplot('width',0.25);
% g.set_names('x','True p(US+)','y','Subjective p(US+)');
% g.set_color_options('lightness',70,'chroma',0);
% g.set_text_options('font','Arial');
% figure(); %'Position',[50 50 450 400]
% g.draw();
% g.geom_point()
% g.update()

end