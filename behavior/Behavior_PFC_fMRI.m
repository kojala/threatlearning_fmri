function Behavior_PFC_fMRI(fpath,subj)
%Create behavioural data files for analysis use from original data files

fpath = fullfile(fpath, 'Behavior');
nruns = 6; % Number of runs
SOA = 6000; % Stimulus onset asynchrony, 6 seconds

for subInd = subj % Loop over subjects
    
    s_id = num2str(subInd);
    fpath_s = fullfile(fpath, ['\S' s_id]);
    
    for bb = 1:nruns % Loop over runs
        clear indata condTime bl t0
        if bb < 6 % Acquisition 1 + Maintenance phases
            load([fpath_s, 'PCF_05_', s_id, '_Block', num2str(bb),'.mat']) %#ok<*LOAD>
            t0_b(bb) = t0; %#ok<*AGROW>
        elseif bb == 6 % Acquisition 2 phase (separate file)
            load([fpath_s, 'PCF_05_', s_id, '_Block', num2str(bb),'_Part2.mat'])
            t0_b(bb) = NaN;
        end
        
        tim(bb).CS_on = indata(:,5); %#ok<*USENS> % CS onset
        tim(bb).US_on = indata(:,5)+SOA; % US onset
        tim(bb).CS_off = indata(:,7); % CS offset
        
        cond(bb).CS = indata(:,2); % CS type
        cond(bb).US = indata(:,3); % US type
        
        resp(bb).corr = indata(:,11); % Correct responses
        resp(bb).rts = indata(:,10); % Reaction time
        
    end
    
    % Save data
    save([fpath_s, 'Behavior_S', s_id, '.mat'], 'tim', 'cond', 'resp', 't0_b')
    
end
    
end