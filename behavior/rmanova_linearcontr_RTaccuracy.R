rm(list=ls())

library(car)
library(ggplot2)
library(psych)
library(tidyverse)
library(ez)
library(heplots)
library(lme4)
library(lmerTest)

dataraw <- read.csv("D:/fMRI_Karita/Behavior/RTAccuracy_summarytable.csv")

attach(dataraw)
par(mfrow=c(2,2))
hist(dataraw$ReactionTime[dataraw$Condition==1], main="Cond1")
hist(dataraw$ReactionTime[dataraw$Condition==2], main="Cond2")
hist(dataraw$ReactionTime[dataraw$Condition==3], main="Cond3")
hist(dataraw$ReactionTime[dataraw$Condition==4], main="Cond4")

par(mfrow=c(2,2))
hist(dataraw$ResponseCorrect[dataraw$Condition==1], main="Cond1")
hist(dataraw$ResponseCorrect[dataraw$Condition==2], main="Cond2")
hist(dataraw$ResponseCorrect[dataraw$Condition==3], main="Cond3")
hist(dataraw$ResponseCorrect[dataraw$Condition==4], main="Cond4")

data1 <- dataraw %>% 
  group_by(Subjects) %>%
  group_by(Condition, add = TRUE) %>%
  summarize_at(vars(-group_cols(),-Trial), function(x) mean(x, na.rm = TRUE))

## Reaction time

data_RT <- data1[,c(1:3)] # drop out accuracy

widedata_RT <- data.frame("Subjects" = unique(data_RT$Subjects), 
                          "Cond1" = data_RT$ReactionTime[data_RT$Condition==1], 
                          "Cond2" = data_RT$ReactionTime[data_RT$Condition==2],
                          "Cond3" = data_RT$ReactionTime[data_RT$Condition==3],
                          "Cond4" = data_RT$ReactionTime[data_RT$Condition==4])

summary(widedata_RT)
sd(widedata_RT$Cond1)
sd(widedata_RT$Cond2)
sd(widedata_RT$Cond3)
sd(widedata_RT$Cond4)

#aggregate(data_RT$ReactionTime, by = list(data_RT$Condition), mean)
#aggregate(data_RT$ReactionTime, by = list(data_RT$Condition), sd)

group_by(data_RT, Condition) %>%
  summarise(
    count = n(),
    mean = mean(ReactionTime, na.rm = TRUE),
    sd = sd(ReactionTime, na.rm = TRUE)
  )

# LME
lmedata_RT <- dataraw[,1:4]
lmedata_RT$Subjects <- factor(lmedata_RT$Subjects)
lmedata_RT$Trial <- factor(lmedata_RT$Trial) # trial as factor (not necessarily linear relationship with pupil size)

lmedata_RT$Condition <- factor(lmedata_RT$Condition, 
                         labels = c("Cond1", "Cond2", "Cond3", "Cond4"))
# random intercepts
lme_RT <- lmer(ReactionTime ~ Condition + (1|Subjects) + (1|Trial), REML = TRUE, data = lmedata_RT)
anova(lme_RT)
summary(lme_RT)

# Traditional ANOVA
#data_RT$Condition <- ordered(data_RT$Condition,
#                         levels = c("Cond1", "Cond2", "Cond3", "Cond4"))

data_RT$Condition <- factor(data_RT$Condition, 
                               labels = c("Cond1", "Cond2", "Cond3", "Cond4"))

anova_RT <- aov(ReactionTime ~ Condition + Error(Subjects/Condition), data = data_RT)
summary(anova_RT, type = 3)

## Accuracy

data_acc <- data1[,c(1:2,4)] # drop out reaction time

widedata_acc <- data.frame("Subjects" = unique(data_acc$Subjects), 
                          "Cond1" = data_acc$ResponseCorrect[data_acc$Condition==1], 
                          "Cond2" = data_acc$ResponseCorrect[data_acc$Condition==2],
                          "Cond3" = data_acc$ResponseCorrect[data_acc$Condition==3],
                          "Cond4" = data_acc$ResponseCorrect[data_acc$Condition==4])

summary(widedata_acc)
sd(widedata_acc$Cond1)
sd(widedata_acc$Cond2)
sd(widedata_acc$Cond3)
sd(widedata_acc$Cond4)

lmedata_acc <- dataraw[,c(1:3,5)]
lmedata_acc$Subjects <- factor(lmedata_acc$Subjects)
lmedata_acc$Condition <- factor(lmedata_acc$Condition, 
                             labels = c("Cond1", "Cond2", "Cond3", "Cond4"))

lme_acc <- glmer(ResponseCorrect ~ Condition + (1 | Subjects) + (1 | Trial), data = lmedata_acc, family = binomial)
summary(lme_acc)

anova(lme_acc)
summary(lme_RT)

# Traditional ANOVA
data_acc$Condition <- factor(data_acc$Condition, 
                            labels = c("Cond1", "Cond2", "Cond3", "Cond4"))

anova_acc <- aov(ResponseCorrect ~ Condition + Error(Subjects/Condition), data = data_acc)
summary(anova_acc, type = 3)

#glmer(Accuracy ~ Condition + (1|Subjects) + (1|Trial), data = lmedata_acc, family="binomial")
