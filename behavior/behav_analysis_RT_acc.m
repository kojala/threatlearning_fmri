function behav_analysis_RT_acc(opts)
%Extract reaction time and accuracy data and save them for R analysis
sList = opts.behav.sList;

% Initialize data matrices
RTacc_data = NaN(length(sList)*224,5);
RT_mat = NaN(length(sList),4,44); %#ok<*NASGU>

trial_all = 1; % Trial counter

for s = 1:length(sList) % Loop over subjects
   
    s_id = num2str(sList(s)); % Subject ID
    fname = fullfile(opts.bhvPath,[opts.bhvname,s_id]);
    bhvdata = load(fname); % Load data
    
    trial_sub = 1; % Trial counter for current subject
    
    for block = 1:6 % Loop over blocks/runs
    
        CS = bhvdata.cond(block).CS; % Conditioned Stimuli type
        US = bhvdata.cond(block).US; % Unconditioned Stimuli type
        
        clear condtype
        
        % Set the same CS type coding for all phases
        condtype(CS == 1 | CS == 6) = 1; %#ok<*AGROW>
        condtype(CS == 2 | CS == 7) = 2;
        condtype(CS == 3 | CS == 8) = 3;
        condtype(CS == 4 | CS == 5) = 4;
        
        for trial = 1:length(condtype) % Loop over trials within the block
            RTacc_data(trial_all,1) = sList(s); % Subject
            RTacc_data(trial_all,2) = condtype(trial); % Condition
            RTacc_data(trial_all,3) = trial_sub; % Trial number
            RTacc_data(trial_all,4) = bhvdata.resp(block).rts(trial); % Reaction time
            RTacc_data(trial_all,5) = bhvdata.resp(block).corr(trial); % Response correct or not (1/0)
            
            trial_sub = trial_sub + 1;
            trial_all = trial_all + 1;
        end
        
%         conddata = bhvdata.resp(block).rts(condtype==block);
%         RT_mat(s,block,1:length(conddata)) = conddata;
%         conddata = bhvdata.resp(block).corr(condtype==block);
%         acc_mat(s,block,1:length(conddata)) = conddata;
        
    end
    
end

% Save data
save(fullfile(opts.bhvPath,'RT_corrresp_data_alltrials.mat'),'RTacc_data');

% Remove reaction time values for incorrect/missed responses
newdata = RTacc_data;
incorr = sum(newdata(:,5)~=1)/length(newdata(:,5)~=1)*100; % percent incorrect responses
newdata(newdata(:,5) ~= 1,4) = NaN;
newdata(newdata(:,5) == -1,5) = 0; % change incorrect responses to 0

% Remove any too short reaction time values (< 200 ms)
tooshort = sum(newdata(:,4) < 250)/length(newdata(:,4) < 250)*100; % percent too short responses
newdata(newdata(:,4) < 200,4) = NaN;
newdata(newdata(:,4) < 200,5) = NaN; % change too slow responses to NaN

disp(['% incorrect: ' num2str(incorr) ', % too short RT: ' num2str(tooshort)])

% Save as csv for R
Subjects = newdata(:,1);
Condition = newdata(:,2);
Trial = newdata(:,3);
ReactionTime = newdata(:,4);
ResponseCorrect = newdata(:,5);

datatable = table(Subjects,Condition,Trial,ReactionTime,ResponseCorrect);
summarytablefile = fullfile(opts.behav.origPath,'RTAccuracy_summarytable.csv');
writetable(datatable,summarytablefile);

% Plot mean RT and accuracy for each condition
no_conditions = 4;
condnames = {'CS(0)' 'CS(1/3)' 'CS(2/3)' 'CS(1)'};

grandavg = nanmean(newdata(:,4)); % mean over subjects and conditions
newvalues = nan(1,no_conditions);

for sub = 1:21
    subind = newdata(:,1)==sList(sub);
    subavg(sub) = nanmean(newdata(subind,4));
    for cond = 1:no_conditions
        condind = newdata(:,2)==cond;
        conddata = newdata(condind & subind,4);
        subcond(sub,cond) = nanmean(conddata);
    end
end

% Error bars for RT    
meanremoved = subcond-subavg'; % Remove mean of conditions from each condition value for each sub
newvalues = meanremoved+repmat(grandavg,[length(sList) no_conditions]); % Add grand average over subjects to the values where individual sub average was removed
newvar = (no_conditions/(no_conditions-1))*var(newvalues);
tvalue = tinv(1-0.05, length(sList)-1); % Find T-value with alpha level 0.05, for degrees of freedom N-1
errorbars = squeeze(tvalue*(sqrt(newvar)./sqrt(length(sList)))); % Calculate error bars according to Cousineau (2005) with Morey (2005) fix
    
for cond = 1:no_conditions
    %plotdata_mean_RT(:,cond) = nanmean(newdata(newdata(:,2)==cond,4));
    plotdata_mean_RT(:,cond) = nanmean(subcond(:,cond));
    plotdata_mean_acc(:,cond) = nanmean(newdata(newdata(:,2)==cond,5)); % Mean accuracy
end

figure
subplot(2,1,1) % Reaction time subplot
hold on
bar(plotdata_mean_RT)
errorbar(plotdata_mean_RT,errorbars,'k','LineStyle','none')
title('Mean reaction time')
set(gca,'xTick',1:no_conditions)
set(gca,'xTickLabel', condnames)
ylabel('Reaction time (ms)')

subplot(2,1,2) % Accuracy subplot
hold on
bar(plotdata_mean_acc)
%errorbar(plotdata_mean,errorbars,'k','LineStyle','none')
title('Mean accuracy')
set(gca,'xTick',1:no_conditions)
set(gca,'xTickLabel', condnames)
ylabel('Accuracy (% correct responses)')

end