function behav_find_monotonic_CSUSratings(opts)
%Find those subjects that have monotonic ratings for CS-US contingency

% Behavioural data subject list
sList = opts.behav.sList;

% Initialize lists of monotonic subjects
monot_sList = NaN(length(sList),2);
monot_sList2 = NaN(length(sList),2);

%% Retrieve subject rating data, sort and save in usable format
for s = 1:length(sList)
    
    if opts.dataset == 1 % Pilot data
        behavPath = opts.bhvPath;
    else % fMRI scanner data
        behavPath = fullfile(opts.behav.origPath,['S' num2str(sList(s))]);
    end
    
    behavFile = [opts.bhvname num2str(sList(s)) '.mat']; % Acquisition and Maintenance
    behavFile2 = [opts.bhvname num2str(sList(s)) '_Part2.mat']; % Acquisition 2
    behavFile = fullfile(behavPath,behavFile);
    behavFile2 = fullfile(behavPath,behavFile2);
    
    % Acquisition and Maintenance phases
    bhvdata = load(behavFile);
    
    % Objective US probability and subjective ratings of US probability
    ratings = [bhvdata.postdata(:,1) bhvdata.postdata(:,3)];
    
    % Sort subjective ratings according to objective US probability
    ratings = sortrows(ratings);
    ratings(ratings==1,1) = 0;
    ratings(ratings==2,1) = 1/3*100;
    ratings(ratings==3,1) = 2/3*100;
    ratings(ratings==4,1) = 1*100;
    
    if sList(s) == 176 % This subject reported pressing a wrong button for 100% true shock stimulus, was supposed to be 70% subjective rating
        ratings(4,2) = 70.00;
    end
    
    % Define whether a subject has monotonic ratings or not
    if (ratings(1,2) <= ratings(2,2)) &&  (ratings(2,2) <= ratings(3,2)) && (ratings(3,2) <= ratings(4,2))
        subj_monot = 1; % monotonic
    else
        subj_monot = 0; % non-monotonic
    end
    
    monot_sList(s,1) = sList(s);
    monot_sList(s,2) = subj_monot;
    
    % Acquisition 2 phase
    if exist(behavFile2,'file')
        bhvdata2 = load(behavFile2);
        
        ratings = [bhvdata2.postdata(:,1) bhvdata2.postdata(:,3)];
        
        ratings = sortrows(ratings);
        ratings(ratings==2,1) = 0;
        ratings(ratings==3,1) = 1/3*100;
        ratings(ratings==4,1) = 2/3*100;
        ratings(ratings==1,1) = 1*100;
        ratings = sortrows(ratings);
        
        % Define whether subject has monotonic ratings or not
        if (ratings(1,2) <= ratings(2,2)) &&  (ratings(2,2) <= ratings(3,2)) && (ratings(3,2) <= ratings(4,2))
            subj_monot = 1; % monotonic
        else
            subj_monot = 0; % non-monotonic
        end
        
        monot_sList2(s,1) = sList(s);
        monot_sList2(s,2) = subj_monot;
        
    else
        CSsubj2 = []; %#ok<NASGU>
    end
    
end

% Display subject lists
disp(sum(monot_sList(:,2)))  % Acquisition 1 + Maintennce
disp(sum(monot_sList2(:,2))) % Acquisition 2

% Save data
save(fullfile(behavPath,'subjratings_pshock_monotonic_sList.mat'),'monot_sList','monot_sList2')

end