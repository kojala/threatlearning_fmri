function behav_get_subjectinfo(opts)
%Retrieve descriptive information about the subjects

sList = opts.behav.sList;

for s = 1:length(sList)
    
    subjdata = load(fullfile(opts.behav.origPath,['S' num2str(sList(s))],['PCF_05_' num2str(sList(s)) '_input.mat']),'subject');
    
    subinfo(s,1) = sList(s); %#ok<*AGROW>
    sex = subjdata.subject.gender; % Sex
    if strcmp(sex,'f'); subinfo(s,2) = 1; elseif strcmp(sex,'m'); subinfo(s,2) = 2; end % Female = 1, Male = 2
    subinfo(s,3) = str2double(subjdata.subject.age); % Age
    subinfo(s,4) = str2double(subjdata.subject.current); % Electric shock current used in the experiment (in mA)
    
end

descriptives.sex.female = sum(subinfo(:,2)==1); % Number of women
descriptives.sex.male = size(subinfo(:,1),1)-descriptives.sex.female; % Number of men

descriptives.age.mean = mean(subinfo(:,3)); % Mean age
descriptives.age.sd = std(subinfo(:,3)); % Standard deviation age
descriptives.age.min = min(subinfo(:,3)); % Range minimum age
descriptives.age.max = max(subinfo(:,3)); % Range maximum age

descriptives.current.mean = mean(subinfo(:,4)); % Mean current
descriptives.current.sd = std(subinfo(:,4)); % Standard deviation current
descriptives.current.min = min(subinfo(:,4)); % Range minimum current
descriptives.current.max = max(subinfo(:,4)); % Range maximum current

% Save data
save(fullfile(opts.bhvPath,'participant_descriptives.mat'),'descriptives');

end