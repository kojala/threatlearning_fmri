%Holm-Bonferroni correction for contingency knowledge paired t-tests

% fMRI sample
pvalues_fmri = [0.0003, 0.028, 0.00003]; % CS(100%)>CS(66%), CS(66%)>CS(33%), CS(33%)>CS(0%)

[c_pvalue, c_alpha, h] = fwer_holmbonf(pvalues_fmri,0.05); %#ok<*ASGLU>

% Pilot sample
pvalues_pilot = [0.0167, 0.0003, 0.00009]; 

[c_pvalue2, c_alpha2, h2] = fwer_holmbonf(pvalues_pilot,0.05);