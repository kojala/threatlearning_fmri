Bach lab GitLab repository for the PCF05 fMRI project on aversive prediction errors during Pavlovian threat conditioning.  
  
Article preprint: xx  
  
Contributors:  
1) Karita Ojala, majority of fMRI and psychophysiological analysis scripts,  
2) Athina Tzovara, initial fMRI preprocessing and 1st level analysis scripts for the axiomatic approach, Bayesian learning model script,  
3) Filip Melinscak, help with learning model scripts  
  
-----------------------------------------------------------  
<b>fMRI analysis in MATLAB/SPM:</b>  
1) fMRI whole-brain analysis pipeline:  
- fmri-analysis\start_pipeline_wholebrain.m  
2) fMRI region-of-interest analysis pipeline:  
- fmri-analysis\start_pipeline_roi.m (requires the whole-brain analysis pipeline to be done)  
3) fMRI model check analysis pipeline:  
- fmri-analysis\start_pipeline_modelcheck.m (requires the whole-brain analysis pipeline to be done)  
4) fMRI MVPA pipeline:  
- fmri-analysis\start_pipeline_mvpa.m (not finished or used in the article)  
-----------------------------------------------------------    
<b>Behavioural and psychophysiological analysis in MATLAB/Psychophysiological Modelling toolbox:</b>  
- physio_start.m  
(pupil size, reaction time, accuracy, descriptives and CS-US contingency learning included in the article)  
-----------------------------------------------------------    
<b>R scripts for statistical analyses:</b>  
1) fMRI ROI analysis:  
- fmri-analysis\roi-analysis\axiomatictests_betas_CohensD.R  
- fmri-analysis\roi-analysis\modelcomparison_PEmodels_vs_betas.R  
2) Behavioural analysis:  
- behaviour\rmanova_linearcontr_expectancyrating.R  
- behaviour\rmanova_linearcontr_RTaccuracy.R  
3) Pupil size analysis:  
- psychophys\pupil_permtest_anova.R  
- psychophys\pupil_permtest_ttest.R  
-----------------------------------------------------------   
Contact: Karita Ojala, k.ojala@uke.de / karita.ojala@alumni.helsinki.fi